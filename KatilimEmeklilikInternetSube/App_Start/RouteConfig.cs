﻿using KatilimEmeklilikInternetSube.Classes.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace KatilimEmeklilikInternetSube
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            
            #region Bireysel Pages
            string strIndividual = Utilities.Friendly(ConfigurationManager.AppSettings["BireyselTitle"]);

            routes.MapRoute(name: "IndividualIndex_Bireysel", url: strIndividual, defaults: new { controller = "Individual", action = "Index" }).DataTokens["RouteName"] = "bireysel";

            routes.MapRoute(name: "IndividualContractInfo_Bireysel", url: strIndividual + "/sozlesme-bilgileri", defaults: new { controller = "Individual", action = "ContractInfo" }).DataTokens["RouteName"] = "bireysel";

            routes.MapRoute(name: "IndividualAccumulationInfo_Bireysel", url: strIndividual + "/birikim-bilgileri", defaults: new { controller = "Individual", action = "AccumulationInfo" }).DataTokens["RouteName"] = "bireysel";

            routes.MapRoute(name: "IndividualPaymentInfo_Bireysel", url: strIndividual + "/odeme-bilgileri", defaults: new { controller = "Individual", action = "PaymentInfo" }).DataTokens["RouteName"] = "bireysel";

            routes.MapRoute(name: "IndividualPaymentInfoPdf_Bireysel", url: strIndividual + "/odeme-bilgileri-pdf", defaults: new { controller = "Individual", action = "PaymentInfoPdf" }).DataTokens["RouteName"] = "bireysel";

            routes.MapRoute(name: "IndividualAlteration_Bireysel", url: strIndividual + "/degisiklik", defaults: new { controller = "Individual", action = "Alteration" }).DataTokens["RouteName"] = "bireysel";

            routes.MapRoute(name: "IndividualForms_Bireysel", url: strIndividual + "/formlar", defaults: new { controller = "Individual", action = "Forms" }).DataTokens["RouteName"] = "bireysel";

            routes.MapRoute(name: "IndividualFundDistributionChange_Bireysel", url: strIndividual + "/fon-dagilim-degisikligi", defaults: new { controller = "Individual", action = "FundDistributionChange" }).DataTokens["RouteName"] = "bireysel";

            routes.MapRoute(name: "IndividualContributionChanges_Bireysel", url: strIndividual + "/katki-payi-degisikligi", defaults: new { controller = "Individual", action = "ContributionChanges" }).DataTokens["RouteName"] = "bireysel";

            routes.MapRoute(name: "IndividualAdditionalContribution_Bireysel", url: strIndividual + "/ek-katki-payi-talebi", defaults: new { controller = "Individual", action = "AdditionalContribution" }).DataTokens["RouteName"] = "bireysel";

            routes.MapRoute(name: "IndividualContractPdf_Bireysel", url: strIndividual + "/sozlesme-indir", defaults: new { controller = "Individual", action = "ContractPdf" }).DataTokens["RouteName"] = "bireysel"; 
            #endregion

            #region Ferdi Kaza Pages
            string strPerson = Utilities.Friendly(ConfigurationManager.AppSettings["FerdiTitle"]);

            routes.MapRoute(name: "PersonalIndex", url: strPerson, defaults: new { controller = "Personal", action = "Index" }).DataTokens["RouteName"] = "ferdi";

            routes.MapRoute(name: "IndividualPaymentInfo_Ferdi", url: strPerson + "/odeme-bilgileri", defaults: new { controller = "Individual", action = "PaymentInfo" }).DataTokens["RouteName"] = "ferdi";
            
            routes.MapRoute(name: "PersonalPolicyDetail_Ferdi", url: strPerson + "/police-detay", defaults: new { controller = "Personal", action = "PolicyDetail" }).DataTokens["RouteName"] = "ferdi";

            routes.MapRoute(name: "PersonalContractPdf_Ferdi", url: strPerson + "/sozlesme-indir", defaults: new { controller = "Personal", action = "ContractPdf" }).DataTokens["RouteName"] = "ferdi";

            routes.MapRoute(name: "IndividualPaymentInfoPdf_Ferdi", url: strPerson + "/odeme-bilgileri-pdf", defaults: new { controller = "Individual", action = "PaymentInfoPdf" }).DataTokens["RouteName"] = "ferdi"; 
            #endregion

            #region Hayat Pages
            string strLife = Utilities.Friendly(ConfigurationManager.AppSettings["HayatTitle"]);

            routes.MapRoute(name: "LifeIndex", url: strLife, defaults: new { controller = "Life", action = "Index" }).DataTokens["RouteName"] = "hayat";

            routes.MapRoute(name: "IndividualPaymentInfo_Hayat", url: strLife + "/odeme-bilgileri", defaults: new { controller = "Individual", action = "PaymentInfo" }).DataTokens["RouteName"] = "hayat";

            routes.MapRoute(name: "PersonalPolicyDetail_Hayat", url: strLife + "/police-detay", defaults: new { controller = "Personal", action = "PolicyDetail" }).DataTokens["RouteName"] = "hayat";

            routes.MapRoute(name: "LifeContractPdf_Hayat", url: strLife + "/sozlesme-indir", defaults: new { controller = "Life", action = "ContractPdf" }).DataTokens["RouteName"] = "hayat";

            routes.MapRoute(name: "IndividualPaymentInfoPdf_Hayat", url: strLife + "/odeme-bilgileri-pdf", defaults: new { controller = "Individual", action = "PaymentInfoPdf" }).DataTokens["RouteName"] = "hayat"; 
            #endregion

            #region Otomatik Katilim Pages
            string strAutomatic = Utilities.Friendly(ConfigurationManager.AppSettings["OtomatikKatilimTitle"]);

            routes.MapRoute(name: "IndividualIndex_Otomatik", url: strAutomatic, defaults: new { controller = "Individual", action = "Index" }).DataTokens["RouteName"] = "otomatik";

            routes.MapRoute(name: "IndividualContractInfo_Otomatik", url: strAutomatic + "/sozlesme-bilgileri", defaults: new { controller = "Individual", action = "ContractInfo" }).DataTokens["RouteName"] = "otomatik";

            routes.MapRoute(name: "IndividualAccumulationInfo_Otomatik", url: strAutomatic + "/birikim-bilgileri", defaults: new { controller = "Individual", action = "AccumulationInfo" }).DataTokens["RouteName"] = "otomatik";

            routes.MapRoute(name: "IndividualPaymentInfo_Otomatik", url: strAutomatic + "/odeme-bilgileri", defaults: new { controller = "Individual", action = "PaymentInfo" }).DataTokens["RouteName"] = "otomatik";

            routes.MapRoute(name: "IndividualPaymentInfoPdf_Otomatik", url: strAutomatic + "/odeme-bilgileri-pdf", defaults: new { controller = "Individual", action = "PaymentInfoPdf" }).DataTokens["RouteName"] = "otomatik";

            routes.MapRoute(name: "IndividualAlteration_Otomatik", url: strAutomatic + "/degisiklik", defaults: new { controller = "Individual", action = "Alteration" }).DataTokens["RouteName"] = "otomatik";

            routes.MapRoute(name: "IndividualForms_Otomatik", url: strAutomatic + "/formlar", defaults: new { controller = "Individual", action = "Forms" }).DataTokens["RouteName"] = "otomatik";

            routes.MapRoute(name: "IndividualFundDistributionChange_Otomatik", url: strAutomatic + "/fon-dagilim-degisikligi", defaults: new { controller = "Individual", action = "FundDistributionChange" }).DataTokens["RouteName"] = "otomatik";

            routes.MapRoute(name: "IndividualContributionChanges_Otomatik", url: strAutomatic + "/katki-payi-degisikligi", defaults: new { controller = "Individual", action = "ContributionChanges" }).DataTokens["RouteName"] = "otomatik";

            routes.MapRoute(name: "IndividualContributionPercentChanges_Otomatik", url: strAutomatic + "/katki-payi-oran-degisikligi", defaults: new { controller = "Individual", action = "ContributionPercentChanges" }).DataTokens["RouteName"] = "otomatik";

            routes.MapRoute(name: "IndividualWithDrawChanges_Otomatik", url: strAutomatic + "/cayma-talebi-degisikligi", defaults: new { controller = "Individual", action = "WithDrawChanges" }).DataTokens["RouteName"] = "otomatik";

            routes.MapRoute(name: "IndividualContractPdf_Otomatik", url: strAutomatic + "/sozlesme-indir", defaults: new { controller = "Individual", action = "ContractPdf" }).DataTokens["RouteName"] = "otomatik"; 
            #endregion

            #region Saglik Pages
            string strHealth = Utilities.Friendly(ConfigurationManager.AppSettings["SaglikTitle"]);

            routes.MapRoute(name: "HealthIndex", url: strHealth, defaults: new { controller = "Health", action = "Index" }).DataTokens["RouteName"] = "saglik";

            routes.MapRoute(name: "HealthPolicyInfo", url: strHealth + "/police-bilgileri", defaults: new { controller = "Health", action = "PolicyInfo" }).DataTokens["RouteName"] = "saglik";

            routes.MapRoute(name: "HealthPaymentInfo", url: strHealth + "/odeme-bilgileri", defaults: new { controller = "Health", action = "PaymentInfo" }).DataTokens["RouteName"] = "saglik";

            routes.MapRoute(name: "HealthPolicyDetail", url: strHealth + "/police-detay", defaults: new { controller = "Health", action = "PolicyDetail" }).DataTokens["RouteName"] = "saglik";

            routes.MapRoute(name: "HealthContractPdf", url: strHealth + "/sozlesme-indir", defaults: new { controller = "Health", action = "ContractPdf" }).DataTokens["RouteName"] = "saglik";

            routes.MapRoute(name: "HealthPaymentInfoPdf", url: strHealth + "/odeme-bilgileri-pdf", defaults: new { controller = "Health", action = "PaymentInfoPdf" }).DataTokens["RouteName"] = "saglik";

            routes.MapRoute(name: "HealthContractedInstitution", url: strHealth + "/anlasmali-kurumlar", defaults: new { controller = "Health", action = "ContractedInstitution" }).DataTokens["RouteName"] = "saglik";
            #endregion

            #region Other Pages
            routes.MapRoute(name: "IndividualMainPage_Anasayfa", url: "anasayfa", defaults: new { controller = "Individual", action = "MainPage" });

            routes.MapRoute(name: "DocumentsIndex", url: "dokumanlar", defaults: new { controller = "Documents", action = "Index" });

            routes.MapRoute(name: "ContactRequestSuggest", url: "iletisim/sikayet-oneri-talep", defaults: new { controller = "Contact", action = "RequestSuggest" });

            routes.MapRoute(name: "ContactInfo", url: "iletisim/bilgi-alma-formu", defaults: new { controller = "Contact", action = "Info" });

            routes.MapRoute(name: "ContactFormDatas", url: "iletisim/gonderilenler", defaults: new { controller = "Contact", action = "FormDatas" });

            routes.MapRoute(name: "ContactIndex", url: "iletisim", defaults: new { controller = "Contact", action = "Index" });

            routes.MapRoute(name: "AdditionalInformationIndex", url: "kisisel-bilgiler", defaults: new { controller = "AdditionalInformation", action = "Index" });

            routes.MapRoute(name: "ErrorStatus500", url: "error/500", defaults: new { controller = "Error", action = "status500" });

            routes.MapRoute(name: "ErrorStatus404", url: "error/404", defaults: new { controller = "Error", action = "status404" });

            routes.MapRoute(name: "ErrorStatus505", url: "error/505", defaults: new { controller = "Error", action = "status505" });

            routes.MapRoute(name: "UserControl", url: "user/usercontrol", defaults: new { controller = "User", action = "UserControl" }); 
            #endregion

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "User", action = "SignUp", id = UrlParameter.Optional }
            );
        }
    }
}