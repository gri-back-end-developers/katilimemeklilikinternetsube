﻿#region Directives
using KatilimEmeklilikInternetSube.Classes;
using KatilimEmeklilikInternetSube.Classes.ClassData;
using KatilimEmeklilikInternetSube.Classes.ClassData.Individual;
using KatilimEmeklilikInternetSube.Classes.ClassData.Individual.AccumulationInfo;
using KatilimEmeklilikInternetSube.Classes.ClassData.Individual.ContractInfo;
using KatilimEmeklilikInternetSube.Classes.ClassData.Individual.FundDistributionChange;
using KatilimEmeklilikInternetSube.Classes.ClassData.Individual.PaymentInfo;
using KatilimEmeklilikInternetSube.Classes.DBData;
using KatilimEmeklilikInternetSube.Classes.LiveData;
using KatilimEmeklilikInternetSube.Classes.Service;
using KatilimEmeklilikInternetSube.Classes.Tools;
using KatilimEmeklilikInternetSube.Models;
using KatilimEmeklilikInternetSube.WsOperations;
using KatilimEmeklilikInternetSube.WsReport;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
#endregion

namespace KatilimEmeklilikInternetSube.Controllers
{
    public class IndividualController : Controller
    {
        cService_Methods serviceMethods;

        #region GetPensionGeneralInfo
        private cIndividual_PensionGeneralInfo GetPensionGeneralInfo(long policyNo, string strPage)
        {
            cIndividual_PensionGeneralInfo cPensionGeneralInfo = new cIndividual_PensionGeneralInfo();

            try
            {
                serviceMethods = new cService_Methods(strPage);

                PensionGeneralInfo pensionGeneralInfo = null;
                Beneficiary[] listBeneficiaryInfo = null;
                FundCurrentInfo[] listFundInfo = null;

                var getPensionGeneralInfo = serviceMethods.GetPensionGeneralInfo(policyNo, ref pensionGeneralInfo, ref listBeneficiaryInfo, ref listFundInfo);
                if (getPensionGeneralInfo)
                {
                    if (pensionGeneralInfo != null)
                    {
                        cPensionGeneralInfo = new cIndividual_PensionGeneralInfo
                        {
                            BesGirisTarih = pensionGeneralInfo.BesGirisTarih.HasValue ? pensionGeneralInfo.BesGirisTarih.Value.ToString("dd MMMM yyyy") : "",
                            KatkiPayiTutar = Utilities.Price(pensionGeneralInfo.KatkiPayiTutar.HasValue ? pensionGeneralInfo.KatkiPayiTutar.Value : 0),
                            UrunAd = pensionGeneralInfo.UrunAd,
                            OdemeAraciAck = pensionGeneralInfo.OdemeAraciAck
                        };

                        Sessions.TitleName = cPensionGeneralInfo.UrunAd;
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage + "-GetPensionGeneralInfo");

            }

            return cPensionGeneralInfo;
        }
        #endregion

        #region GetAccumulationInfo
        private cIndividual_AccumulationInfoData GetAccumulationInfo(long policyNo, string strPage)
        {
            cIndividual_AccumulationInfoData cAccumulationInfo = null;
            try
            {
                serviceMethods = new cService_Methods(strPage);

                AccumulationInfo accumulationInfo = null;
                FundCurrentInfo[] listFundInfo = null;
                TransferInfo transferInfo = null;

                var getAccumulationInfo = serviceMethods.GetAccumulationInfo(policyNo, ref listFundInfo, ref transferInfo, ref accumulationInfo);
                if (getAccumulationInfo)
                {
                    cAccumulationInfo = new cIndividual_AccumulationInfoData
                    {
                        DevletKatkisi = accumulationInfo.DkBirikimTutar.HasValue ? accumulationInfo.DkBirikimTutar.Value : 0,
                        DevletKatkisiGetirisi = accumulationInfo.DkFonGetiriTutar.HasValue ? accumulationInfo.DkFonGetiriTutar.Value : 0,
                        KatilimciKatkisi = (accumulationInfo.KkBirikimTutar.HasValue ? accumulationInfo.KkBirikimTutar.Value : 0),
                        YatirilanDevletKatkisi = (transferInfo.DkToplamTutar.HasValue ? transferInfo.DkToplamTutar.Value : 0) + (accumulationInfo.DkKatkiPayiTutar.HasValue ? accumulationInfo.DkKatkiPayiTutar.Value : 0),
                        YatirilanToplamTutar = (transferInfo.KkToplamTutar.HasValue ? transferInfo.KkToplamTutar.Value : 0) + (accumulationInfo.EkpToplam.HasValue ? accumulationInfo.EkpToplam.Value : 0) + (accumulationInfo.KatkiPayiTutar.HasValue ? accumulationInfo.KatkiPayiTutar.Value : 0),
                        YatirimGetirisi = accumulationInfo.KkFonGetiriTutar.HasValue ? accumulationInfo.KkFonGetiriTutar.Value : 0,
                        KatkiPayiTutar = accumulationInfo.KatkiPayiTutar.HasValue ? accumulationInfo.KatkiPayiTutar.Value : 0
                    };

                    cAccumulationInfo.ToplamBirikim = cAccumulationInfo.KatilimciKatkisi + cAccumulationInfo.DevletKatkisi;
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage + "-GetAccumulationInfo");
            }

            return cAccumulationInfo;
        }
        #endregion

        #region Index
        [UserFilter]
        public ActionResult Index(string polNo = "")
        {
            cIndividual individual = new cIndividual();
            string strPage = "Individual-Index";

            try
            {
                long policyNo = string.IsNullOrEmpty(polNo) ? 0 : Utilities.NullFixLong(cCrypto.DecryptDES(polNo));

                ViewBag.Msg = Sessions.Message;
                Sessions.Message = "";

                CustomerProduct[] listCustomerProducts = Sessions.CustomerProduct;

                if (listCustomerProducts != null)
                {
                    individual.Policies = new List<cIndividual_Policies>();

                    var pageRouteName = individual.PageSettings.RouteName;

                    string strOk = "H";
                    if (pageRouteName == cEnums.PageType.OtomatikKatilim)
                    {
                        strOk = "E";
                    }

                    foreach (var item in listCustomerProducts.Where(w => w.YI == "Y" && w.Ok == strOk && w.BransKod == ((int)cEnums.BransKod.BireyselEmeklilik).ToString()).ToList())
                    {
                        bool bolForms = true;
                        if (pageRouteName == cEnums.PageType.Bireysel)
                        {
                            if (!string.IsNullOrEmpty(item.TarifeKod) ? (item.TarifeKod.Substring(0, 1) == "I") : true)
                            {
                                bolForms = false;
                            }
                        }

                        individual.Policies.Add(new cIndividual_Policies
                        {
                            PoliceNo = item.PoliceNo.Value,
                            bolForms = bolForms,
                            TarifeKod = item.TarifeKod
                        });
                    }

                    if (policyNo == 0)
                    {
                        policyNo = individual.Policies.First().PoliceNo;
                        Sessions.TarifeKod = individual.Policies.First().TarifeKod;
                    }
                    else
                    {
                        Sessions.TarifeKod = individual.Policies.First(f => f.PoliceNo == policyNo).TarifeKod;
                    }
                }

                ViewBag.policyNo = policyNo;

                cIndividual_PensionGeneralInfo pensionGeneralInfo = GetPensionGeneralInfo(policyNo, strPage);
                cIndividual_AccumulationInfoData getAccumulationInfo = GetAccumulationInfo(policyNo, strPage);

                if (pensionGeneralInfo != null)
                {
                    individual.IsHaveData = true;
                    individual.PensionGeneralInfo = pensionGeneralInfo;
                }

                if (getAccumulationInfo != null)
                {
                    individual.IsHaveData = true;

                    if (individual.PensionGeneralInfo == null)
                        individual.PensionGeneralInfo = new cIndividual_PensionGeneralInfo();

                    individual.PensionGeneralInfo.KatkiPayiTutar = Utilities.Price(getAccumulationInfo.KatkiPayiTutar);

                    individual.AccumulationInfo = getAccumulationInfo;
                }

                if (individual.AccumulationInfo == null)
                    individual.AccumulationInfo = new cIndividual_AccumulationInfoData();

                individual.pieChartData = new List<cPieChartData>();
                individual.pieChartData.Add(new cPieChartData
                {
                    strText = "Yatırılan Toplam Tutar",
                    strValue = getAccumulationInfo.YatirilanToplamTutar.ToString()
                });

                individual.pieChartData.Add(new cPieChartData
                {
                    strText = "Yatırılan Devlet Katkısı",
                    strValue = getAccumulationInfo.YatirilanDevletKatkisi.ToString()
                });
                individual.pieChartData.Add(new cPieChartData
                {
                    strText = "Yatırım Getisi",
                    strValue = getAccumulationInfo.YatirimGetirisi.ToString()
                });

                individual.pieChartData.Add(new cPieChartData
                {
                    strText = "Devlet Katkısı Getirisi",
                    strValue = getAccumulationInfo.DevletKatkisiGetirisi.ToString()
                });
                
                OBJ_IRR_LIST[] irrAndAccumulationResult = serviceMethods.GetIrrAndAccumulationInfo(policyNo);
                if (irrAndAccumulationResult != null)
                {
                    individual.AlternativeInvestmentTools = new List<cIndividual_AlternativeInvestmentTools>();

                    foreach (var item in irrAndAccumulationResult)
                    {
                        individual.AlternativeInvestmentTools.Add(new cIndividual_AlternativeInvestmentTools
                        {
                            FonKod = item.FON_KOD,
                            Tutar = Math.Round(item.BIRIKIM.HasValue ? item.BIRIKIM.Value : 0),
                            Yuzde = item.XIRR.HasValue ? item.XIRR.Value : 0
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(individual);
        }
        #endregion

        #region GetAccumulationChange
        public string GetAccumulationChange(string pol, string startdate, string enddate)
        {
            long policyNo = Utilities.NullFixLong(cCrypto.DecryptDES(pol));
            decimal? changeAmount = 0;
            string strMsg = "";

            serviceMethods = new cService_Methods("Individual");

            var getAccumulationChange = serviceMethods.GetAccumulationChange(policyNo, startdate.Replace("-", "/"), enddate.Replace("-", "/"), ref changeAmount);
            if (!getAccumulationChange)
            {
                strMsg = "Seçtiğiniz tarih aralığında birikim bulunamadı!";
            }

            JavaScriptSerializer jss = new JavaScriptSerializer();
            return jss.Serialize(new
            {
                price = strMsg != "" ? "-" : Utilities.Price(changeAmount.HasValue ? changeAmount.Value : 0),
                msg = strMsg
            });
        }
        #endregion

        #region ContractInfo
        [UserFilter]
        public ActionResult ContractInfo()
        {
            cIndividual_ContractInfo contractInfo = new cIndividual_ContractInfo();
            string strPage = "Individual-ContractInfo";

            try
            {
                serviceMethods = new cService_Methods(strPage);

                PensionGeneralInfo pensionGeneralInfo = null;
                Beneficiary[] listBeneficiaryInfo = null;
                FundCurrentInfo[] listFundInfo = null;

                var getPensionGeneralInfo = serviceMethods.GetPensionGeneralInfo(QueryStrings.policy, ref pensionGeneralInfo, ref listBeneficiaryInfo, ref listFundInfo);
                if (getPensionGeneralInfo)
                {
                    if (listBeneficiaryInfo != null)
                    {
                        if (listBeneficiaryInfo.Length > 0)
                        {
                            contractInfo.LehtarBilgileri = new List<cIndividual_ContractInfo_LehtarBilgileri>();

                            foreach (var item in listBeneficiaryInfo)
                            {
                                if (!string.IsNullOrEmpty(item.AdSoyad.Trim()) || item.MenfaattarOran.HasValue)
                                {
                                    contractInfo.LehtarBilgileri.Add(new cIndividual_ContractInfo_LehtarBilgileri
                                    {
                                        AdSoyad = item.AdSoyad,
                                        dcOran = item.MenfaattarOran.HasValue ? item.MenfaattarOran.Value : 0
                                    });
                                }
                            }

                            if (contractInfo.LehtarBilgileri.Count == 0)
                            {
                                contractInfo.LehtarBilgileri = null;
                            }
                            else
                            {
                                decimal toplamOran = 100 / contractInfo.LehtarBilgileri.Sum(s => s.dcOran);
                                foreach (var item in contractInfo.LehtarBilgileri)
                                {
                                    item.Oran = (toplamOran * item.dcOran).ToString();
                                }
                            }
                        }
                    }

                    if (pensionGeneralInfo != null)
                    {
                        contractInfo.SozlesmeBilgileri = new cIndividual_ContractInfo_SozlesmeBilgileri
                        {
                            DuzenliKatkiPayiTutari = Utilities.Price(pensionGeneralInfo.KatkiPayiTutar.HasValue ? pensionGeneralInfo.KatkiPayiTutar.Value : 0),
                            EmeklilikTarihi = pensionGeneralInfo.EmeklilikTarih.HasValue ? pensionGeneralInfo.EmeklilikTarih.Value.ToShortDateString() : "",
                            ErtelenmisGirisAidatiTutari = Utilities.Price(pensionGeneralInfo.ErtGirisAidatTutar.HasValue ? pensionGeneralInfo.ErtGirisAidatTutar.Value : 0),
                            GirisAidatiTutari = Utilities.Price(pensionGeneralInfo.GirisAidatTutar.HasValue ? pensionGeneralInfo.GirisAidatTutar.Value : 0),
                            IlkGirisTarihi = pensionGeneralInfo.BesGirisTarih.HasValue ? pensionGeneralInfo.BesGirisTarih.Value.ToShortDateString() : "",
                            Katilimci = pensionGeneralInfo.KatilimciAd + " " + pensionGeneralInfo.KatilimciSoyad,
                            KatilimciTCKN = pensionGeneralInfo.KatilimciKimlikNo,
                            KatkiPayiOdemeDonemi = pensionGeneralInfo.OdemeSekli,
                            KTLMMusteriNo = pensionGeneralInfo.KatilimciNo.ToString(),
                            OdemeAraci = pensionGeneralInfo.OdemeAraciAck,
                            SozlesmeNo = pensionGeneralInfo.PoliceNo.HasValue ? pensionGeneralInfo.PoliceNo.Value.ToString() : "",
                            UrunAdi = pensionGeneralInfo.UrunAd,
                            YonetimGiderKesintisi = Utilities.Price(pensionGeneralInfo.YgkTutar.HasValue ? pensionGeneralInfo.YgkTutar.Value : 0),
                            YururlukTarihi = pensionGeneralInfo.YururlukTarih.HasValue ? pensionGeneralInfo.YururlukTarih.Value.ToShortDateString() : "",
                            BKTutar = Utilities.Price(pensionGeneralInfo.BkTutar.HasValue ? pensionGeneralInfo.BkTutar.Value : 0),
                        };
                    }

                    if (listFundInfo != null)
                    {
                        if (listFundInfo.Length > 0)
                        {
                            if (contractInfo.PageSettings.RouteName == cEnums.PageType.OtomatikKatilim)
                            {
                                var dkFonlari = listFundInfo.Where(w => w.FON_KOD.Trim() == "KEA").FirstOrDefault();
                                if (dkFonlari != null)
                                {
                                    contractInfo.DKFonlari = new List<cIndividual_ContractInfo_Fonlar>();
                                    contractInfo.DKFonlari.Add(new cIndividual_ContractInfo_Fonlar
                                    {
                                        FonAdi = dkFonlari.FON_AD,
                                        Oran = dkFonlari.FON_ORAN.ToString()
                                    });
                                }
                            }

                            var tercihEdilen = listFundInfo.Where(w => w.FON_KOD.Trim() != "KEA" && w.FON_ORAN > 0).ToList();
                            if (tercihEdilen != null)
                            {
                                if (tercihEdilen.Count > 0)
                                {
                                    contractInfo.TercihEdilenFonDagilimi = new List<cIndividual_ContractInfo_Fonlar>();

                                    foreach (var item in tercihEdilen)
                                    {
                                        contractInfo.TercihEdilenFonDagilimi.Add(new cIndividual_ContractInfo_Fonlar
                                        {
                                            FonAdi = item.FON_AD,
                                            Oran = item.FON_ORAN.ToString()
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(contractInfo);
        }
        #endregion

        #region AccumulationInfo
        [UserFilter]
        public ActionResult AccumulationInfo()
        {
            cIndividual_AccumulationInfo accumulationInfo_Data = new cIndividual_AccumulationInfo();
            string strPage = "Individual-AccumulationInfo";

            try
            {
                serviceMethods = new cService_Methods(strPage);

                PensionGeneralInfo pensionGeneralInfo = null;
                Beneficiary[] listBeneficiaryInfo = null;
                FundCurrentInfo[] listFundInfo = null;
                string DevletKatkisiHakEdisOraniTutari = "";

                var getPensionGeneralInfo = serviceMethods.GetPensionGeneralInfo(QueryStrings.policy, ref pensionGeneralInfo, ref listBeneficiaryInfo, ref listFundInfo);
                if (getPensionGeneralInfo)
                {
                    if (pensionGeneralInfo != null)
                    {
                        accumulationInfo_Data.SozlesmeBilgileri = new cIndividual_AccumulationInfo_SozlesmeBilgileri
                        {
                            Katilimci = pensionGeneralInfo.KatilimciAd + " " + pensionGeneralInfo.KatilimciSoyad,
                            KTLMMusteriNo = pensionGeneralInfo.KatilimciNo.ToString(),
                            SozlesmeNo = pensionGeneralInfo.PoliceNo.HasValue ? pensionGeneralInfo.PoliceNo.Value.ToString() : "",
                            UrunAdi = pensionGeneralInfo.UrunAd
                        };

                        if (pensionGeneralInfo.DKHakedisTutar.HasValue && pensionGeneralInfo.DKHakedisTutar.Value > 0)
                        {
                            DevletKatkisiHakEdisOraniTutari = "%" + pensionGeneralInfo.DKHakedisOran + "/" + Utilities.Price(pensionGeneralInfo.DKHakedisTutar.Value);
                        }
                    }
                }

                listFundInfo = null;
                TransferInfo transferInfo = null;
                AccumulationInfo accumulationInfo = null;

                var getAccumulationInfo = serviceMethods.GetAccumulationInfo(QueryStrings.policy, ref listFundInfo, ref transferInfo, ref accumulationInfo);
                if (getAccumulationInfo)
                {
                    if (transferInfo != null)
                    {
                        decimal AktarimAnaParaDK = transferInfo.DkAnaParaTutar.HasValue ? transferInfo.DkAnaParaTutar.Value : 0;
                        decimal AktarimAnaParaKK = transferInfo.KkAnaParaTutar.HasValue ? transferInfo.KkAnaParaTutar.Value : 0;
                        decimal AktarimGetiriDK = transferInfo.DkGetiriTutar.HasValue ? transferInfo.DkGetiriTutar.Value : 0;
                        decimal AktarimGetiriKK = transferInfo.KkGetiriTutar.HasValue ? transferInfo.KkGetiriTutar.Value : 0;
                        decimal AktarimToplamiDK = transferInfo.DkToplamTutar.HasValue ? transferInfo.DkToplamTutar.Value : 0;
                        decimal AktarimToplamiKK = transferInfo.KkToplamTutar.HasValue ? transferInfo.KkToplamTutar.Value : 0;

                        if (AktarimAnaParaDK > 0 || AktarimAnaParaKK > 0 || AktarimGetiriDK > 0 || AktarimGetiriKK > 0 || AktarimToplamiDK > 0 || AktarimToplamiKK > 0)
                        {
                            accumulationInfo_Data.AktarimBilgileri = new cIndividual_AccumulationInfo_AktarimBilgileri
                            {
                                AktarimAnaParaDK = Utilities.Price(AktarimAnaParaDK),
                                AktarimAnaParaKK = Utilities.Price(AktarimAnaParaKK),
                                AktarimGetiriDK = Utilities.Price(AktarimGetiriDK),
                                AktarimGetiriKK = Utilities.Price(AktarimGetiriKK),
                                AktarimToplamiDK = Utilities.Price(AktarimToplamiDK),
                                AktarimToplamiKK = Utilities.Price(AktarimToplamiKK)
                            };
                        }
                    }

                    if (listFundInfo != null)
                    {
                        if (listFundInfo.Length > 0)
                        {
                            if (accumulationInfo_Data.PageSettings.RouteName == cEnums.PageType.Bireysel)
                            {
                                var dkFon = listFundInfo.Where(w => w.FON_KOD.Trim() == "KEA" && (w.FON_ORAN > 0 || w.GUNCEL_FON_ORAN > 0)).FirstOrDefault();
                                if (dkFon != null)
                                {
                                    accumulationInfo_Data.DetayBirikimBilgileri = new cIndividual_AccumulationInfo_DetayBirikimBilgileri();

                                    accumulationInfo_Data.DetayBirikimBilgileri.Data.Add(new cAccumulationInfo_DetayBirikimBilgileri_Data
                                    {
                                        FonAd = dkFon.FON_AD,
                                        FonAdedi = dkFon.FON_ADET.ToString(),
                                        Oran = dkFon.FON_ORAN.ToString(),
                                        FonTipi = "dk",
                                        FonFiyat = dkFon.XFON_FIYAT.ToString(),
                                        Tutar = Utilities.Price(dkFon.TUTAR),
                                        GuncelOran = dkFon.GUNCEL_FON_ORAN.ToString()
                                    });
                                }
                            }

                            var kkFon = listFundInfo.Where(w => w.FON_KOD.Trim() != "KEA" && (w.FON_ORAN > 0 || w.GUNCEL_FON_ORAN > 0)).ToList();
                            if (kkFon != null)
                            {
                                if (accumulationInfo_Data.DetayBirikimBilgileri == null)
                                    accumulationInfo_Data.DetayBirikimBilgileri = new cIndividual_AccumulationInfo_DetayBirikimBilgileri();

                                foreach (var item in kkFon)
                                {
                                    accumulationInfo_Data.DetayBirikimBilgileri.Data.Add(new cAccumulationInfo_DetayBirikimBilgileri_Data
                                    {
                                        FonAd = item.FON_AD,
                                        FonAdedi = item.FON_ADET.ToString(),
                                        Oran = item.FON_ORAN.ToString(),
                                        FonTipi = "kk",
                                        FonFiyat = item.XFON_FIYAT.ToString(),
                                        Tutar = Utilities.Price(item.TUTAR),
                                        GuncelOran = item.GUNCEL_FON_ORAN.ToString()
                                    });
                                }
                            }
                        }
                    }

                    if (accumulationInfo != null)
                    {
                        if (accumulationInfo_Data.DetayBirikimBilgileri == null)
                            accumulationInfo_Data.DetayBirikimBilgileri = new cIndividual_AccumulationInfo_DetayBirikimBilgileri();

                        accumulationInfo_Data.DetayBirikimBilgileri.DevletciKatkisiIcVerimOrani = accumulationInfo.DkIrr;
                        accumulationInfo_Data.DetayBirikimBilgileri.KatilimciKatkisiIcVerimOrani = accumulationInfo.KkIrr;

                        if (accumulationInfo_Data.PageSettings.RouteName == cEnums.PageType.Bireysel)
                        {
                            accumulationInfo_Data.DetayBirikimBilgileri.ToplamBirikim_DevletKatkisiDahil = Utilities.Price((accumulationInfo.KkBirikimTutar.HasValue ? accumulationInfo.KkBirikimTutar.Value : 0) + (accumulationInfo.DkBirikimTutar.HasValue ? accumulationInfo.DkBirikimTutar.Value : 0));
                            accumulationInfo_Data.DetayBirikimBilgileri.ToplamBirikim_DevletKatkisiHaric = Utilities.Price(accumulationInfo.KkBirikimTutar.HasValue ? accumulationInfo.KkBirikimTutar.Value : 0);

                            accumulationInfo_Data.TahsilatDetayBilgileri_DevletKatkisi = new cIndividual_AccumulationInfo_TahsilatDetayBilgileriDevletKatkisi
                            {
                                DevletKatkisiBirikim = Utilities.Price(accumulationInfo.DkBirikimTutar.HasValue ? accumulationInfo.DkBirikimTutar.Value : 0),
                                DevletKatkisiGetiriTutari = Utilities.Price(accumulationInfo.DkFonGetiriTutar.HasValue ? accumulationInfo.DkFonGetiriTutar.Value : 0),
                                DevletKatkisiHakedisOrani_Tutar = "%" + (accumulationInfo.DkKatilimciHakedisOran.HasValue ? accumulationInfo.DkKatilimciHakedisOran.Value.ToString() : "0") + " / " + Utilities.Price(accumulationInfo.DkKatilimciHakedisTutar.HasValue ? accumulationInfo.DkKatilimciHakedisTutar.Value : 0),
                                DevletKatkisiTahsilati = Utilities.Price(accumulationInfo.DkKatkiPayiTutar.HasValue ? accumulationInfo.DkKatkiPayiTutar.Value : 0),
                                StopajOrani_StopajTutari = "%" + (accumulationInfo.DkStopajYuzde.HasValue ? accumulationInfo.DkStopajYuzde.Value.ToString() : "0") + " / " + Utilities.Price(accumulationInfo.DkStopajTutar.HasValue ? accumulationInfo.DkStopajTutar.Value : 0)
                            };
                        }
                        else
                        {
                            var stateContribution = serviceMethods.GetStateContribution(QueryStrings.policy);
                            if (stateContribution.ServiceResult != null)
                            {
                                var TaahhutBaslangicDKAnaTutar = stateContribution.InitialStateContributions.Where(w => w.Kod == "001").FirstOrDefault();
                                var TaahhutDKAnaTutarToplami = stateContribution.DeathProvisionStateContributions.Where(w => w.Kod == "003").FirstOrDefault();

                                accumulationInfo_Data.TahsilatDetayBilgileri_DevletKatkisi = new cIndividual_AccumulationInfo_TahsilatDetayBilgileriDevletKatkisi
                                {
                                    TaahhutBaslangicDKAnaTutar = Utilities.Price(TaahhutBaslangicDKAnaTutar != null ? (TaahhutBaslangicDKAnaTutar.Value.HasValue ? TaahhutBaslangicDKAnaTutar.Value.Value : 0) : 0),
                                    TaahhutDKAnaTutarToplami = Utilities.Price(TaahhutDKAnaTutarToplami != null ? (TaahhutDKAnaTutarToplami.Value.HasValue ? TaahhutDKAnaTutarToplami.Value.Value : 0) : 0),
                                    DevletKatkisiHakEdisOraniTutari = DevletKatkisiHakEdisOraniTutari
                                };
                            }
                        }

                        accumulationInfo_Data.TahsilatDetayBilgileri_KatilimciKatkisi = new cIndividual_AccumulationInfo_TahsilatDetayBilgileriKatilimciKatkisi
                        {
                            GirisAidati = Utilities.Price(accumulationInfo.GaTutar.HasValue ? accumulationInfo.GaTutar.Value : 0),
                            KatilimciKatkisiGetiriTutari = Utilities.Price(accumulationInfo.KkFonGetiriTutar.HasValue ? accumulationInfo.KkFonGetiriTutar.Value : 0),
                            ProvizyondaBekleyenTutar = Utilities.Price(accumulationInfo.ProvizyonTutar.HasValue ? accumulationInfo.ProvizyonTutar.Value : 0),
                            StopajOrani_StopajTutari = "%" + (accumulationInfo.KkStopajYuzde.HasValue ? accumulationInfo.KkStopajYuzde.Value.ToString() : "0") + " / " + Utilities.Price(accumulationInfo.KkStopajTutar.HasValue ? accumulationInfo.KkStopajTutar.Value : 0),
                            ToplamBirikim_DevletKatkisiHaric = Utilities.Price(accumulationInfo.KkBirikimTutar.HasValue ? accumulationInfo.KkBirikimTutar.Value : 0),
                            YatirilanToplamTutar_GirisAidatiHaric = Utilities.Price(accumulationInfo.KatkiPayiTutar.HasValue ? accumulationInfo.KatkiPayiTutar.Value : 0),
                            YatirimaYonlendirilenTutar = Utilities.Price(accumulationInfo.YatYonTutar.HasValue ? accumulationInfo.YatYonTutar.Value : 0),
                            YonetimGideriKesintiTutari = Utilities.Price(accumulationInfo.YgkTutar.HasValue ? accumulationInfo.YgkTutar.Value : 0),
                            EkKatkiPayiTutari = Utilities.Price(accumulationInfo.EkpToplam.HasValue ? accumulationInfo.EkpToplam.Value : 0),
                            BKTutar = pensionGeneralInfo != null ? (Utilities.Price(pensionGeneralInfo.BkTutar.HasValue ? pensionGeneralInfo.BkTutar.Value : 0)) : ""
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(accumulationInfo_Data);
        }
        #endregion

        #region PaymentInfo
        [UserFilter]
        public ActionResult PaymentInfo()
        {
            cPaymentInfo paymentInfo = new cPaymentInfo();
            string strPage = "Individual-PaymentInfo";

            try
            {
                ViewBag.Msg = Sessions.Message;
                Sessions.Message = "";

                serviceMethods = new cService_Methods(strPage);

                if (paymentInfo.PageSettings.RouteName == cEnums.PageType.Bireysel)
                {
                    PensionGeneralInfo pensionGeneralInfo = null;
                    Beneficiary[] listBeneficiaryInfo = null;
                    FundCurrentInfo[] listFundInfo = null;

                    var getPensionGeneralInfo = serviceMethods.GetPensionGeneralInfo(QueryStrings.policy, ref pensionGeneralInfo, ref listBeneficiaryInfo, ref listFundInfo);
                    if (getPensionGeneralInfo)
                    {
                        if (pensionGeneralInfo != null)
                        {
                            paymentInfo.SozlesmeBilgileri = new cIndividual_PaymentInfo_SozlesmeBilgileri
                            {
                                Katilimci = pensionGeneralInfo.KatilimciAd + " " + pensionGeneralInfo.KatilimciSoyad,
                                KTLMMusteriNo = pensionGeneralInfo.KatilimciNo.ToString(),
                                SozlesmeNo = pensionGeneralInfo.PoliceNo.HasValue ? pensionGeneralInfo.PoliceNo.Value.ToString() : "",
                                UrunAdi = pensionGeneralInfo.UrunAd,
                                KrediKarti = pensionGeneralInfo.OdemeAraciAck
                            };
                        }
                    }
                }
                else
                {
                    LifeGeneralInfo lifeGeneralInfo = null;
                    Beneficiary[] listBeneficiaryInfo = null;
                    CoverageLastStatus[] listCoverageInfo = null;
                    LifeCollectingInfo collectingInfo = null;
                    MediatorInfo mediatorInfo = null;

                    var getLifeGeneralInfo = serviceMethods.GetLifeGeneralInfo(QueryStrings.policy, ref lifeGeneralInfo, ref listBeneficiaryInfo, ref listCoverageInfo, ref collectingInfo, ref mediatorInfo);
                    if (getLifeGeneralInfo)
                    {
                        paymentInfo.SozlesmeBilgileri = new cIndividual_PaymentInfo_SozlesmeBilgileri
                        {
                            Katilimci = lifeGeneralInfo.KatilimciAd + " " + lifeGeneralInfo.KatilimciSoyad,
                            KTLMMusteriNo = lifeGeneralInfo.KatilimciNo.ToString(),
                            SozlesmeNo = lifeGeneralInfo.PoliceNo.HasValue ? lifeGeneralInfo.PoliceNo.Value.ToString() : "",
                            UrunAdi = lifeGeneralInfo.UrunAd,
                            KrediKarti = lifeGeneralInfo.OdemeAraciAck
                        };
                    }
                }

                CollectingDetail[] collectingDetail = null;

                var getCollectingDetailInfo = serviceMethods.GetCollectingDetailInfo(QueryStrings.policy, ref collectingDetail);
                if (getCollectingDetailInfo)
                {
                    if (collectingDetail != null)
                    {
                        if (collectingDetail.Length > 0)
                        {
                            paymentInfo.Tahsilat_Makbuzu = new List<cIndividual_PaymentInfo_TahsilatMakbuzu>();

                            foreach (var item in collectingDetail)
                            {
                                paymentInfo.Tahsilat_Makbuzu.Add(new cIndividual_PaymentInfo_TahsilatMakbuzu
                                {
                                    KesintiTutari = Utilities.Price(item.MasrafTutar.HasValue ? item.MasrafTutar.Value : 0),
                                    ReferansNo = item.ReferansNo.ToString(),
                                    OdemeTarihi = item.TahsilTarih.HasValue ? item.TahsilTarih.Value.ToShortDateString() : "",
                                    DtOdemeTarihi = item.TahsilTarih.HasValue ? item.TahsilTarih.Value : new DateTime(),
                                    OdemeTutari = Utilities.Price(item.TutarToplam.HasValue ? item.TutarToplam.Value : 0),
                                    VadeTarihi = item.Vade.HasValue ? item.Vade.Value.ToShortDateString() : "",
                                    VadeTipi = item.TaksitTipAck
                                });
                            }

                            paymentInfo.Tahsilat_Makbuzu = paymentInfo.Tahsilat_Makbuzu.OrderByDescending(o => o.DtOdemeTarihi).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(paymentInfo);
        }
        #endregion

        #region PaymentInfoPdf
        [UserFilter]
        public EmptyResult PaymentInfoPdf()
        {
            string strPage = "Individual-PaymentInfoPdf";

            try
            {
                KeyValuePair<string, string>[] parameters = new KeyValuePair<string, string>[]
                {
                    new KeyValuePair<string, string>("P_REFERANS_NO", QueryStrings.ReferansNo)
                };

                serviceMethods = new cService_Methods(strPage);

                string strMsg = "";

                var report = serviceMethods.GetReport(parameters, KatilimEmeklilikInternetSube.WsReport.ReportKey.TahsilatMakbuzu, ref strMsg);
                if (report != null)
                {
                    Utilities.DownloadFile(report.ReportPdf, "TahsilatMakbuzu.pdf", "application/pdf");
                }
                else
                {
                    if (!string.IsNullOrEmpty(strMsg))
                    {
                        Sessions.Message = Utilities.Msg(Utilities.MsgType.warning, strMsg);
                    }

                    PageSettings pageSettings = new PageSettings();
                    Response.Redirect("/" + pageSettings.UrlRoot + "/odeme-bilgileri?policy=" + QueryStrings.policyRequest, false);
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);

                PageSettings pageSettings = new PageSettings();
                Response.Redirect("/" + pageSettings.UrlRoot + "/odeme-bilgileri?policy=" + QueryStrings.policyRequest, false);
            }

            return new EmptyResult();
        }
        #endregion

        #region Alteration
        [UserFilter]
        public ActionResult Alteration()
        {
            cIndividual_Alteration alteration = new cIndividual_Alteration();
            string strPage = "Individual-Alteration";

            try
            {
                serviceMethods = new cService_Methods(strPage);

                var pensionGeneralInfo = GetPensionGeneralInfo(QueryStrings.policy, strPage);
                alteration.UrunAdi = pensionGeneralInfo.UrunAd;
                alteration.OdemeAraciAck = pensionGeneralInfo.OdemeAraciAck;

                CustomerProduct[] listCustomerProducts = Sessions.CustomerProduct;
                if (listCustomerProducts != null)
                {
                    var customerProducts = listCustomerProducts.Where(w => w.YI == "Y" && w.BransKod == ((int)cEnums.BransKod.BireyselEmeklilik).ToString() && w.PoliceNo == QueryStrings.policy).FirstOrDefault();
                    if (customerProducts != null)
                    {
                        alteration.OkBireyselDevam = customerProducts.OkBireyselDevam == "E" ? cEnums.OkBireyselDevam.KP : cEnums.OkBireyselDevam.Cayma;
                        alteration.EkKatkiPayi = customerProducts.TarifeKod.Substring(0, 1) != "I";
                        alteration.IGES = !string.IsNullOrEmpty(customerProducts.TarifeKod) ? (customerProducts.TarifeKod.Substring(0, 1) == "I") : true;
                    }
                }

                var getContributionRateDemand = serviceMethods.GetContributionRateDemand(QueryStrings.policy);
                if (getContributionRateDemand != null)
                {
                    if (getContributionRateDemand.ProtocolStatus == "4C")
                    {
                        alteration.KpOran = true;
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(alteration);
        }
        #endregion

        #region Forms
        [UserFilter]
        public ActionResult Forms(string strMsg = "")
        {
            cIndividual_Forms forms = new cIndividual_Forms();
            string strPage = "Individual-Forms";

            ViewBag.Msg = strMsg;

            try
            {
                forms.UrunAdi = Sessions.TitleName;

                //using (DBEntities db = new DBEntities())
                //{
                //    string strRouteName = forms.PageSettings.RouteName == cEnums.PageType.OtomatikKatilim ? "otomatikkatilim" : "bireysel";

                //    forms.CmsForms = db.tblCmsForms.Where(w => !w.bolIsDelete && w.bolIsActive && (string.IsNullOrEmpty(w.strType) ? true : w.strType == strRouteName)).OrderBy(o => o.intOrder).ToList();
                //}

                if (QueryStrings.ReportID > 0 && string.IsNullOrEmpty(strMsg))
                {
                    strMsg = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deyiniz.");

                    KeyValuePair<string, string>[] parameters = null;
                    string strFileName = "";

                    ReportKey reportKey = cReportKey.Get(QueryStrings.ReportID);

                    switch (reportKey)
                    {
                        case ReportKey.EmeklilikBilgiFormu:
                        case ReportKey.AyrilmaFormu:
                        case ReportKey.AktarimFormu:
                            parameters = new KeyValuePair<string, string>[]
                            {
                                new KeyValuePair<string, string>("P_POLICE_NO", QueryStrings.policy.ToString())
                            };
                            break;
                        case ReportKey.HesapBildirimCetveli:
                            parameters = new KeyValuePair<string, string>[]
                            {
                                new KeyValuePair<string, string>("P_POLICE_NO", QueryStrings.policy.ToString()),
                                new KeyValuePair<string, string>("P_DATE_START", DateTime.Now.AddYears(-1).ToShortDateString()),
                                new KeyValuePair<string, string>("P_DATE_END", DateTime.Now.ToShortDateString())
                            };
                            break;
                        case ReportKey.HesapOzetiProfili:
                            parameters = new KeyValuePair<string, string>[]
                            {
                                new KeyValuePair<string, string>("P_POLICE_NO", QueryStrings.policy.ToString()),
                                new KeyValuePair<string, string>("P_SECIM_KOD", Request.QueryString["type"] == "ayrilma" ? "SA" : "BSA")
                            };
                            break;
                        default:
                            strMsg = Utilities.Msg(Utilities.MsgType.warning, "Rapor bulunamadı!");
                            return Forms(strMsg);
                    }

                    switch (reportKey)
                    {
                        case ReportKey.EmeklilikBilgiFormu:
                            strFileName = "EmeklilikBilgiFormu";
                            break;
                        case ReportKey.AyrilmaFormu:
                            strFileName = "AyrilmaFormu";
                            break;
                        case ReportKey.AktarimFormu:
                            strFileName = "AktarimFormu";
                            break;
                        case ReportKey.HesapBildirimCetveli:
                            strFileName = "HesapBildirimCetveli";
                            break;
                        case ReportKey.HesapOzetiProfili:
                            strFileName = "HesapOzetiProfili";
                            break;
                    }

                    serviceMethods = new cService_Methods(strPage);

                    var report = serviceMethods.GetReport(parameters, reportKey, ref strMsg);
                    if (report != null)
                    {
                        Utilities.DownloadFile(report.ReportPdf, strFileName + "-" + QueryStrings.policy + ".pdf", "application/pdf");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(strMsg))
                        {
                            strMsg = Utilities.Msg(Utilities.MsgType.warning, report.Result.ErrorList[0].Split('|')[1]);
                        }
                    }

                    return Forms(strMsg);
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(forms);
        }
        #endregion

        #region ContractPdf
        [UserFilter]
        public EmptyResult ContractPdf()
        {
            string strPage = "Individual-ContractPdf";
            try
            {
                KeyValuePair<string, string>[] parameters = new KeyValuePair<string, string>[]
                {
                    new KeyValuePair<string, string>("P_POLICE_NO", QueryStrings.policy.ToString()),
                    new KeyValuePair<string, string>("P_ZEYIL_NO", "0")
                };

                serviceMethods = new cService_Methods(strPage);

                string strMsg = "";

                var report = serviceMethods.GetReport(parameters, KatilimEmeklilikInternetSube.WsReport.ReportKey.BesSozlesmeBasim, ref strMsg);
                if (report != null)
                {
                    Utilities.DownloadFile(report.ReportPdf, "Sozlesme-" + QueryStrings.policy + ".pdf", "application/pdf");
                }
                else
                {
                    if (!string.IsNullOrEmpty(strMsg))
                    {
                        Sessions.Message = Utilities.Msg(Utilities.MsgType.warning, strMsg);
                    }

                    PageSettings pageSettings = new PageSettings();
                    Response.Redirect("/" + pageSettings.UrlRoot, false);
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);

                PageSettings pageSettings = new PageSettings();
                Response.Redirect("/" + pageSettings.UrlRoot, false);
            }

            return new EmptyResult();
        }
        #endregion

        #region FundDistributionChange

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult FundDistributionChange(string polNo = "", string strMsg = "")
        {
            cIndividual_FundDistributionChange fundDistributionChange = new cIndividual_FundDistributionChange();
            string strPage = "Individual-FundDistributionChange-Get";

            try
            {
                long policy = 0;
                if (string.IsNullOrEmpty(polNo))
                {
                    policy = QueryStrings.policy;
                }
                else
                {
                    policy = Utilities.NullFixLong(cCrypto.DecryptDES(polNo));
                }

                if (string.IsNullOrEmpty(polNo))
                {
                    Sessions.FundDistributionChange = new cIndividual_FundDistributionChange_Session();
                }

                var fundDistributionChangeSesion = Sessions.FundDistributionChange;
                fundDistributionChange.UrunAdi = Sessions.TitleName;

                serviceMethods = new cService_Methods(strPage);

                var getFundDemandInfo = serviceMethods.GetFundDemandInfo(QueryStrings.policy, "KK");
                if (getFundDemandInfo != null)
                {
                    if (getFundDemandInfo.IsDemandExist == 1)
                    {
                        fundDistributionChange.bolVisible = true;
                        fundDistributionChange.strMsg = "Bekleyen talebiniz var! Fon dağılım değişikliği yapılamaz.";

                        return View(fundDistributionChange);
                    }
                    else if (getFundDemandInfo.ServiceResult != null && getFundDemandInfo.ServiceResult.ErrorList != null && getFundDemandInfo.ServiceResult.ErrorList.Length > 0)
                    {
                        string[] strErr = getFundDemandInfo.ServiceResult.ErrorList[0].Split('|');
                        if (strErr.Length > 1)
                        {
                            fundDistributionChange.bolVisible = true;
                            fundDistributionChange.strMsg = strErr[1];

                            return View(fundDistributionChange);
                        }
                    }
                }

                ViewBag.Msg = strMsg;

                if (!string.IsNullOrEmpty(fundDistributionChangeSesion.strType))
                {
                    #region rgptype yes
                    if (fundDistributionChangeSesion.strType == "rgptype" && fundDistributionChangeSesion.strRrpType == "yes")
                    {
                        var getRrpDefinition = serviceMethods.GetRrpDefinition();
                        if (getRrpDefinition != null)
                        {
                            fundDistributionChange.RrpQuestions = new List<cIndividual_FundDistributionChange_RrpQuestions>();

                            foreach (var soru in getRrpDefinition.RrpDefinitionList.P_TAB_RGP_SORU.Value)
                            {
                                cIndividual_FundDistributionChange_RrpQuestions question = new cIndividual_FundDistributionChange_RrpQuestions
                                {
                                    ID = soru.SQ_RGP_SORU_ID,
                                    Soru = soru.SORU,
                                    Cevaplar = new List<cIndividual_FundDistributionChange_RrpAnswer>()
                                };

                                foreach (var cevap in soru.P_TAB_RGP_CEVAP.Value)
                                {
                                    question.Cevaplar.Add(new cIndividual_FundDistributionChange_RrpAnswer
                                    {
                                        Cevap = cevap.CEVAP,
                                        ID = cevap.SQ_RGP_CEVAP_ID,
                                        Puan = cevap.PUAN.HasValue ? cevap.PUAN.Value : 0
                                    });
                                }

                                fundDistributionChange.RrpQuestions.Add(question);
                            }
                        }
                    }
                    #endregion
                    #region question
                    else if (fundDistributionChangeSesion.strType == "question")
                    {
                        var pointTotal = fundDistributionChangeSesion.RrpUserAnswer.Sum(s => s.point);

                        var rrpPoint = serviceMethods.GetRrpPoint(pointTotal, Sessions.TarifeKod);
                        if (rrpPoint != null)
                        {
                            var rrpStrategyResultGroup = rrpPoint.RrpStrategyResult.Value.GroupBy(g => new
                            {
                                g.SECIM_TIP,
                                g.RF_RGP_DEGER_ID,
                                g.RF_RGP_DEGER_DETAY_ID
                            }).Select(s => new { s.Key.SECIM_TIP, s.Key.RF_RGP_DEGER_ID, s.Key.RF_RGP_DEGER_DETAY_ID }).ToList();

                            fundDistributionChange.RrpPointResults = new List<cIndividual_FundDistributionChange_RrpPointResult>();

                            foreach (var group in rrpStrategyResultGroup)
                            {
                                cIndividual_FundDistributionChange_RrpPointResult rrpPointResult = new cIndividual_FundDistributionChange_RrpPointResult
                                {
                                    fonlar = new List<cIndividual_FundDistributionChange_Fund>(),
                                    SecimTip = group.SECIM_TIP,
                                    DEGER_ID = group.RF_RGP_DEGER_ID.Value.ToString(),
                                    DETAY_ID = group.RF_RGP_DEGER_DETAY_ID.Value.ToString()
                                };

                                foreach (var result in rrpPoint.RrpStrategyResult.Value)
                                {
                                    rrpPointResult.fonlar.Add(new cIndividual_FundDistributionChange_Fund
                                    {
                                        FonAd = result.FON_AD,
                                        FonKod = result.FON_KOD,
                                        FonOran = result.FON_ORAN.HasValue ? result.FON_ORAN.Value.ToString() : "0"
                                    });
                                }

                                fundDistributionChange.RrpPointResults.Add(rrpPointResult);
                            }
                        }
                    }
                    #endregion
                    #region secim || rgptype no
                    else if (fundDistributionChangeSesion.strType == "secim" || (fundDistributionChangeSesion.strType == "rgptype" && fundDistributionChangeSesion.strRrpType == "no"))
                    {
                        if (getFundDemandInfo != null)
                        {
                            fundDistributionChange.RemainingFundChangeRight = getFundDemandInfo.RemainingFundChangeRight;

                            if (getFundDemandInfo.ListCurrentFund != null)
                            {
                                if (getFundDemandInfo.ListCurrentFund.Length > 0)
                                {
                                    fundDistributionChange.MevcutFonlar = new List<cIndividual_FundDistributionChange_Fund>();
                                    foreach (var item in getFundDemandInfo.ListCurrentFund.Where(w => w.FonOran > 0).ToList())
                                    {
                                        fundDistributionChange.MevcutFonlar.Add(new cIndividual_FundDistributionChange_Fund
                                        {
                                            FonKod = item.FonKod,
                                            FonAd = item.KisaAd,
                                            FonOran = item.FonOran.HasValue ? item.FonOran.Value.ToString() : "0"
                                        });
                                    }
                                }
                            }

                            if (getFundDemandInfo.ListNewFund != null)
                            {
                                if (getFundDemandInfo.ListNewFund.Length > 0)
                                {
                                    fundDistributionChange.YeniDagilimFonlar = new List<cIndividual_FundDistributionChange_Fund>();
                                    foreach (var item in getFundDemandInfo.ListNewFund)
                                    {
                                        cIndividual_FundDistributionChange_Fund fund = new cIndividual_FundDistributionChange_Fund
                                        {
                                            FonKod = item.FonKod,
                                            FonAd = item.FonKisaAd,
                                            FonOran = "0"
                                        };

                                        if (fundDistributionChange.MevcutFonlar != null)
                                        {
                                            if (fundDistributionChange.MevcutFonlar.Count(c => c.FonKod == item.FonKod) > 0)
                                            {
                                                fund.FonOran = fundDistributionChange.MevcutFonlar.First(f => f.FonKod == item.FonKod).FonOran;
                                            }
                                        }

                                        fundDistributionChange.YeniDagilimFonlar.Add(fund);
                                    }

                                    if (fundDistributionChangeSesion.strType == "secim" && fundDistributionChangeSesion.strSecim == "devam")
                                    {
                                        foreach (var item in fundDistributionChange.YeniDagilimFonlar)
                                        {
                                            if (fundDistributionChangeSesion.RrpPointResult.fonlar.Count(c => c.FonKod == item.FonKod) > 0)
                                            {
                                                item.YeniFonOran = fundDistributionChangeSesion.RrpPointResult.fonlar.First(f => f.FonKod == item.FonKod).FonOran;
                                            }
                                            else
                                            {
                                                item.YeniFonOran = "0";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(fundDistributionChange);
        }
        #endregion

        #region Set
        [UserFilter]
        [HttpPost]
        public ActionResult FundDistributionChange()
        {
            string strMsg = "";
            string strPage = "Individual-FundDistributionChange-Set";

            try
            {
                if (Request.Form["hdType"] == "rgptype")
                {
                    Sessions.FundDistributionChange = new cIndividual_FundDistributionChange_Session
                    {
                        strRrpType = Request.Form["rgpType"],
                        strType = "rgptype"
                    };

                    return FundDistributionChange(QueryStrings.policyRequest, "");
                }
                else if (Request.Form["hdType"] == "question")
                {
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    Sessions.FundDistributionChange.RrpUserAnswer = jss.Deserialize<List<cIndividual_FundDistributionChange_RrpUserAnswer>>(Request.Form["questionanswers"]);
                    Sessions.FundDistributionChange.strType = "question";

                    return FundDistributionChange(QueryStrings.policyRequest, "");
                }
                else if (Request.Form["hdType"] == "secimdevam")
                {
                    Sessions.FundDistributionChange.strType = "secim";
                    Sessions.FundDistributionChange.strSecim = "devam";

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    Sessions.FundDistributionChange.RrpPointResult = jss.Deserialize<cIndividual_FundDistributionChange_RrpPointResult>(Request.Form["fonsecim"]);

                    return FundDistributionChange(QueryStrings.policyRequest, "");
                }
                else if (Request.Form["hdType"] == "secimiptal")
                {
                    Sessions.FundDistributionChange.strType = "secim";
                    Sessions.FundDistributionChange.strSecim = "iptal";

                    return FundDistributionChange(QueryStrings.policyRequest, "");
                }
                else if (Request.Form["hdType"] == "form")
                {
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    var newFunds = jss.Deserialize<List<cIndividual_FundDistributionChange_Funds>>(Request.Form["yenidagilim"]);

                    string newDistribution = "";
                    foreach (var item in newFunds)
                    {
                        if (Utilities.NullFixInt(item.FonOran) > 0)
                        {
                            if (newDistribution == "")
                            {
                                newDistribution = item.FonKod + ":" + item.FonOran;
                            }
                            else
                            {
                                newDistribution += ":/" + item.FonKod + ":" + item.FonOran;
                            }
                        }
                    }

                    long demandNo = 0;
                    int rgpSelectType = 0; //İSTEMİYORUM 0 İSTİYORUM 1 
                    TAB_RGP_SONUC_FON rgpFund = null;
                    TAB_RGP_SONUC_DETAY rgpAnswer = null;

                    var fundDistributionChangeSession = Sessions.FundDistributionChange;
                    if (fundDistributionChangeSession.strRrpType == "yes")
                    {
                        if (fundDistributionChangeSession.strSecim == "devam")
                        {
                            rgpSelectType = 1;
                            rgpFund = new TAB_RGP_SONUC_FON();

                            OBJ_RGP_SONUC_FON[] sonuc_fon = new OBJ_RGP_SONUC_FON[fundDistributionChangeSession.RrpPointResult.fonlar.Count];
                            for (int i = 0; i < fundDistributionChangeSession.RrpPointResult.fonlar.Count; i++)
                            {
                                sonuc_fon[i] = new OBJ_RGP_SONUC_FON
                                {
                                    FON_KOD = fundDistributionChangeSession.RrpPointResult.fonlar[i].FonKod,
                                    FON_ORAN = Utilities.NullFixLong(fundDistributionChangeSession.RrpPointResult.fonlar[i].FonOran),
                                    RF_RGP_DEGER_DETAY_ID = Utilities.NullFixLong(fundDistributionChangeSession.RrpPointResult.DETAY_ID),
                                    RF_RGP_DEGER_ID = Utilities.NullFixLong(fundDistributionChangeSession.RrpPointResult.DEGER_ID)
                                };
                            }

                            rgpFund.Value = sonuc_fon;
                        }
                        else if (fundDistributionChangeSession.strSecim == "iptal")
                        {
                            rgpSelectType = 2;
                        }

                        rgpAnswer = new TAB_RGP_SONUC_DETAY();
                        OBJ_RGP_SONUC_DETAY[] sonuc_detay = new OBJ_RGP_SONUC_DETAY[fundDistributionChangeSession.RrpUserAnswer.Count];

                        for (int i = 0; i < fundDistributionChangeSession.RrpUserAnswer.Count; i++)
                        {
                            sonuc_detay[i] = new OBJ_RGP_SONUC_DETAY
                            {
                                RF_RGP_SORU_ID = Utilities.NullFixDecimal(fundDistributionChangeSession.RrpUserAnswer[i].questionId),
                                RF_RGP_CEVAP_ID = Utilities.NullFixDecimal(fundDistributionChangeSession.RrpUserAnswer[i].answerId)
                            };
                        }

                        rgpAnswer.Value = sonuc_detay;
                    }

                    serviceMethods = new cService_Methods(strPage);

                    var saveFundDemand = serviceMethods.SaveFundDemand(QueryStrings.policy, Request.Form["demandType"], "KK", newDistribution, rgpAnswer, rgpFund, rgpSelectType, ref demandNo, ref strMsg);
                    if (saveFundDemand)
                    {
                        Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Fon dağılım değişikliği talebiniz başarılı bir şekilde gerçekleşmiştir.");

                        PageSettings pageSetting = new PageSettings();

                        return Redirect("/" + pageSetting.UrlRoot);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(strMsg))
                        {
                            strMsg = Utilities.Msg(Utilities.MsgType.warning, strMsg);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                strMsg = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deyiniz.");

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return FundDistributionChange(QueryStrings.policyRequest, strMsg);
        }
        #endregion

        #endregion

        #region ContributionChanges

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult ContributionChanges(string polNo = "", string strMsg = "")
        {
            cIndividual_ContributionChanges contributionChanges = new cIndividual_ContributionChanges();
            string strPage = "Individual-ContributionChanges-Get";

            try
            {
                long policy = 0;
                if (string.IsNullOrEmpty(polNo))
                {
                    policy = QueryStrings.policy;
                }
                else
                {
                    policy = Utilities.NullFixLong(cCrypto.DecryptDES(polNo));
                }

                ViewBag.Msg = strMsg;

                serviceMethods = new cService_Methods(strPage);

                CurrentPremiumInfo currentPremiumInfo = null;
                KeyValue[] listOpenDues = null;

                var getPremiumChangeInfo = serviceMethods.GetPremiumChangeInfo(policy, ref currentPremiumInfo, ref listOpenDues);
                if (getPremiumChangeInfo)
                {
                    if (currentPremiumInfo != null)
                    {
                        contributionChanges.KatkiPayi = Utilities.Price(currentPremiumInfo.KatkiPayiTutar.HasValue ? currentPremiumInfo.KatkiPayiTutar.Value : 0);
                        contributionChanges.KatkiPayiOdemeDonemi = currentPremiumInfo.OdemeSekli;

                        if (listOpenDues != null)
                        {
                            contributionChanges.Tarihler = new List<DateTime>();

                            foreach (var item in listOpenDues)
                            {
                                if (!string.IsNullOrEmpty(item.Value))
                                {
                                    contributionChanges.Tarihler.Add(Utilities.NullFixDate(item.Value));
                                }
                            }

                            contributionChanges.Tarihler = contributionChanges.Tarihler.OrderBy(o => o).ToList();
                        }
                    }
                }

                contributionChanges.UrunAdi = Sessions.TitleName;
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(contributionChanges);
        }
        #endregion

        #region Set
        [UserFilter]
        [HttpPost]
        public ActionResult ContributionChanges(string katkipayi)
        {
            string strMsg = "";
            string strPage = "Individual-ContributionChanges-Set";

            try
            {
                serviceMethods = new cService_Methods(strPage);

                long demandNo = 0;

                var savePremiumDemand = serviceMethods.SavePremiumDemand(QueryStrings.policy, Request.Form["tarih"], Utilities.NullFixFloat(katkipayi), ref demandNo, ref strMsg);
                if (savePremiumDemand)
                {
                    Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Güncelleme başarılı.");
                    PageSettings pageSetting = new PageSettings();

                    return Redirect("/" + pageSetting.UrlRoot);
                }
                else
                {
                    if (!string.IsNullOrEmpty(strMsg))
                    {
                        strMsg = Utilities.Msg(Utilities.MsgType.warning, strMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                strMsg = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deyiniz.");

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return ContributionChanges(QueryStrings.policyRequest, strMsg);
        }
        #endregion

        #endregion

        #region WithDrawChanges

        #region GetBankBranchList
        public string GetBankBranchList(string BankCode)
        {
            List<cIndividual_WithDrawChangesInputs> withDrawChangesInputs = new List<cIndividual_WithDrawChangesInputs>();

            serviceMethods = new cService_Methods("Individual-GetBankBranchList");

            var getBankBranchList = serviceMethods.GetBankBranchList(BankCode);
            if (getBankBranchList != null)
            {
                var bankBranchList = getBankBranchList.BankBranchList;
                if (bankBranchList.Data != null)
                {
                    foreach (var item in bankBranchList.Data)
                    {
                        withDrawChangesInputs.Add(new cIndividual_WithDrawChangesInputs
                        {
                            Text = item.Value,
                            Value = item.Key
                        });
                    }
                }
            }

            JavaScriptSerializer jss = new JavaScriptSerializer();
            return jss.Serialize(withDrawChangesInputs);
        }
        #endregion

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult WithDrawChanges(string strMsg = "")
        {
            cIndividual_WithDrawChanges withDrawChanges = new cIndividual_WithDrawChanges();

            ViewBag.Msg = strMsg;

            try
            {
                withDrawChanges.UrunAdi = Sessions.TitleName;
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, "Individual-WithDrawChanges-Get");
            }

            return View(withDrawChanges);
        }
        #endregion

        #region Set
        [UserFilter]
        [HttpPost]
        public ActionResult WithDrawChanges()
        {
            string strMsg = "";
            string strPage = "Individual-WithDrawChanges-Set";

            try
            {
                serviceMethods = new cService_Methods(strPage);

                string strDbMsg = "";

                var savePremiumDemand = serviceMethods.SaveWithdrawalDemand(QueryStrings.policy, "CAY", "CAY", "AY", ref strMsg);
                if (savePremiumDemand)
                {
                    strMsg = "Başarılı";
                    Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Cayma talebiniz alınmıştır. İşleminiz 10 iş günü içerisinde  gerçekleştirilecektir.");

                    PageSettings pageSetting = new PageSettings();
                    return Redirect("/" + pageSetting.UrlRoot);
                }
                else
                {
                    if (!string.IsNullOrEmpty(strMsg))
                    {
                        strMsg = Utilities.Msg(Utilities.MsgType.warning, strMsg);
                    }

                    strDbMsg = "Başarısız";
                }

                using (DBEntities db = new DBEntities())
                {
                    db.tblWithDrawChangeLogs.Add(new tblWithDrawChangeLogs
                    {
                        dtRegisterDate = DateTime.Now,
                        intCustomerNo = Sessions.CurrentUser.intCustomerNo,
                        strMsg = strDbMsg,
                        strPolicy = QueryStrings.policy.ToString()
                    });
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                strMsg = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deyiniz.");

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return WithDrawChanges(strMsg);
        }
        #endregion

        #endregion

        #region MainPage
        [UserFilter]
        public ActionResult MainPage()
        {
            cMainPage mainpage = new cMainPage();
            string strPage = "Individual-MainPage";

            try
            {
                serviceMethods = new cService_Methods(strPage);

                var getPensionTotalAccumulation = serviceMethods.GetPensionTotalAccumulation(Sessions.CurrentUser.intCustomerNo);
                if (getPensionTotalAccumulation != null)
                {
                    mainpage.KatilimciKatkisi = getPensionTotalAccumulation.TotalAccumulation.KkBirikimTutar.HasValue ? getPensionTotalAccumulation.TotalAccumulation.KkBirikimTutar.Value : 0;
                    mainpage.YatirilanToplamTutar = getPensionTotalAccumulation.TotalAccumulation.YatYonTutar.HasValue ? getPensionTotalAccumulation.TotalAccumulation.YatYonTutar.Value : 0;
                    mainpage.YatirimGetirisi = getPensionTotalAccumulation.TotalAccumulation.KkFonGetiriTutar.HasValue ? getPensionTotalAccumulation.TotalAccumulation.KkFonGetiriTutar.Value : 0;
                    mainpage.DevletKatkisi = getPensionTotalAccumulation.TotalAccumulation.DkBirikimTutar.HasValue ? getPensionTotalAccumulation.TotalAccumulation.DkBirikimTutar.Value : 0;
                    mainpage.YatirilanDevletKatkisi = getPensionTotalAccumulation.TotalAccumulation.DkYatYonTutar.HasValue ? getPensionTotalAccumulation.TotalAccumulation.DkYatYonTutar.Value : 0;
                    mainpage.DevletKatkisiGetirisi = getPensionTotalAccumulation.TotalAccumulation.DkFonGetiriTutar.HasValue ? getPensionTotalAccumulation.TotalAccumulation.DkFonGetiriTutar.Value : 0;
                    mainpage.ToplamBirikim = Utilities.Price(mainpage.KatilimciKatkisi + mainpage.DevletKatkisi);
                    mainpage.KatılımcıIcVerimOraniDK = getPensionTotalAccumulation.TotalAccumulation.SigortaliDkIrr;
                    mainpage.KatılımcıIcVerimOraniKK = getPensionTotalAccumulation.TotalAccumulation.SigortaliKkIrr;
                }

                mainpage.pieChartData = new List<cPieChartData>();

                mainpage.pieChartData.Add(new cPieChartData
                {
                    strText = "Yatırılan Toplam Tutar",
                    strValue = mainpage.YatirilanToplamTutar.ToString()
                });

                mainpage.pieChartData.Add(new cPieChartData
                {
                    strText = "Yatırılan Devlet Katkısı",
                    strValue = mainpage.YatirilanDevletKatkisi.ToString()
                });

                mainpage.pieChartData.Add(new cPieChartData
                {
                    strText = "Yatırım Getirisi",
                    strValue = mainpage.YatirimGetirisi.ToString()
                });

                mainpage.pieChartData.Add(new cPieChartData
                {
                    strText = "Devlet Katkısı Getirisi",
                    strValue = mainpage.DevletKatkisiGetirisi.ToString()
                });
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(mainpage);
        }
        #endregion

        #region ContributionPercentChanges

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult ContributionPercentChanges(string polNo = "", string strMsg = "")
        {
            cIndividual_ContributionPercentChanges contributionPercentChanges = new cIndividual_ContributionPercentChanges();
            string strPage = "Individual-ContributionPercentChanges-Get";

            try
            {
                long policy = 0;
                if (string.IsNullOrEmpty(polNo))
                {
                    policy = QueryStrings.policy;
                }
                else
                {
                    policy = Utilities.NullFixLong(cCrypto.DecryptDES(polNo));
                }

                ViewBag.Msg = strMsg;

                serviceMethods = new cService_Methods(strPage);

                var getContributionRateDemand = serviceMethods.GetContributionRateDemand(policy);
                if (getContributionRateDemand != null)
                {
                    contributionPercentChanges.CurrentRate = getContributionRateDemand.Rate.HasValue ? (getContributionRateDemand.Rate.Value < 3 ? "3" : getContributionRateDemand.Rate.Value.ToString()) : "3";
                }

                contributionPercentChanges.UrunAdi = Sessions.TitleName;
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(contributionPercentChanges);
        }
        #endregion

        #region Set
        [UserFilter]
        [HttpPost]
        public ActionResult ContributionPercentChanges(string katkipayi)
        {
            string strMsg = "";
            string strPage = "Individual-ContributionPercentChanges-Set";

            try
            {
                serviceMethods = new cService_Methods(strPage);

                var saveContributionRateDemand = serviceMethods.SaveContributionRateDemand(QueryStrings.policy, Utilities.NullFixDecimal(Request.Form["percent"]), ref strMsg);
                if (saveContributionRateDemand)
                {
                    Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Güncelleme başarılı.");
                    PageSettings pageSetting = new PageSettings();

                    return Redirect("/" + pageSetting.UrlRoot);
                }
                else
                {
                    if (!string.IsNullOrEmpty(strMsg))
                    {
                        strMsg = Utilities.Msg(Utilities.MsgType.warning, strMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                strMsg = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deyiniz.");

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return ContributionPercentChanges(QueryStrings.policyRequest, strMsg);
        }
        #endregion

        #endregion

        #region AdditionalContribution

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult AdditionalContribution(string polNo = "", string strMsg = "")
        {
            cIndividual_AdditionalContribution additionalContribution = new cIndividual_AdditionalContribution();
            string strPage = "Individual-AdditionalContribution-Get";

            try
            {
                if (string.IsNullOrEmpty(polNo))
                {
                    additionalContribution.Policy = QueryStrings.policy;
                }
                else
                {
                    additionalContribution.Policy = Utilities.NullFixLong(cCrypto.DecryptDES(polNo));
                }

                ViewBag.Msg = strMsg;

                serviceMethods = new cService_Methods(strPage);

                var getFinancialRecords = serviceMethods.GetFinancialRecords(additionalContribution.Policy);
                if (getFinancialRecords != null)
                {
                    additionalContribution.FinansBilgileri = getFinancialRecords.FinancialInfo.Where(w => w.HesapTip != "NKT").ToList();
                }

                additionalContribution.UrunAdi = Sessions.TitleName;
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(additionalContribution);
        }
        #endregion

        #region Set
        [UserFilter]
        [HttpPost]
        public ActionResult AdditionalContribution()
        {
            string strMsg = "";
            string strPage = "Individual-AdditionalContribution-Set";

            try
            {
                decimal amount = Utilities.NullFixDecimal(Request.Form["price"].Replace(".", ","));
                if (amount == 0)
                {
                    Sessions.Message = Utilities.Msg(Utilities.MsgType.warning, "Tutarı giriniz!");
                    return AdditionalContribution(QueryStrings.policyRequest, strMsg);
                }

                cIndividual_PensionGeneralInfo pensionGeneralInfo = GetPensionGeneralInfo(QueryStrings.policy, strPage);
                if (pensionGeneralInfo.OdemeAraciAck.Contains("NKT"))
                {
                    Sessions.Message = Utilities.Msg(Utilities.MsgType.warning, "Sözleşmeniz için tercih ettiğiniz ödeme aracı nedeniyle ek katkı payı ödeme talebiniz işleme alınamamıştır. Farklı bir ödeme aracı ile ek katkı payı ödemek için lütfen 0850 226 0 123 no’lu Çağrı Merkezimizi arayınız.");
                    return AdditionalContribution(QueryStrings.policyRequest, strMsg);
                }

                DateTime? startDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                if (!string.IsNullOrEmpty(Request.Form["txtStartDate"]))
                {
                    startDate = Utilities.NullFixDate(Request.Form["txtStartDate"]);
                }

                DateTime enddate = startDate.Value.AddDays(30);
                long? mustMaliID = Utilities.NullFixLong(Request.Form["finans"]);

                serviceMethods = new cService_Methods(strPage);

                var saveAdditionalContribution = serviceMethods.SaveAdditionalContribution(QueryStrings.policy, amount, startDate, enddate, mustMaliID, ref strMsg);
                if (saveAdditionalContribution)
                {
                    Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Ek katkı payı talebiniz alınmıştır.");
                    PageSettings pageSetting = new PageSettings();

                    return Redirect("/" + pageSetting.UrlRoot);
                }
                else
                {
                    if (!string.IsNullOrEmpty(strMsg))
                    {
                        strMsg = Utilities.Msg(Utilities.MsgType.warning, strMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                strMsg = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deyiniz.");

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return AdditionalContribution(QueryStrings.policyRequest, strMsg);
        }
        #endregion

        #region AdditionalContributionControl
        public string AdditionalContributionControl(string pol)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();

            serviceMethods = new cService_Methods("Individual-AdditionalContribution-Control");

            AdditionalContributionRecord[] additonalContributionRecords = serviceMethods.GetAdditonalContributionRecords(cCrypto.DecryptDES(pol));
            if (additonalContributionRecords == null || additonalContributionRecords.Length == 0)
            {
                return jss.Serialize(new { count = "0" });
            }
            return jss.Serialize(new { count = "1" });
        }
        #endregion

        #endregion
    }
}
