﻿#region Directives
using KatilimEmeklilikInternetSube.Classes.ClassData;
using KatilimEmeklilikInternetSube.Classes.DBData;
using KatilimEmeklilikInternetSube.Classes.LiveData;
using KatilimEmeklilikInternetSube.Classes.Service;
using KatilimEmeklilikInternetSube.Classes.Tools;
using KatilimEmeklilikInternetSube.Models;
using KatilimEmeklilikInternetSube.WsOperations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.SessionState;
#endregion

namespace KatilimEmeklilikInternetSube.Controllers
{
    public class UserController : Controller
    {
        #region SignUp

        #region Get
        [HttpGet]
        [jCryptionHandler]
        public ActionResult SignUp(cSignUp signUp = null)
        {
            if (signUp == null)
            {
                signUp = new cSignUp();
            }

            if (Request.QueryString["getPublicKey"] == null)
            {
                if (Sessions.Message != "")
                {
                    signUp.strMessage = Sessions.Message;
                }

                Session.Clear();
                Session.RemoveAll();
                Session.Abandon();

                SessionIDManager manager = new SessionIDManager();
                string NewID = manager.CreateSessionID(this.HttpContext.ApplicationInstance.Context);
                bool redirected = false;
                bool IsAdded = false;
                manager.SaveSessionID(this.HttpContext.ApplicationInstance.Context, NewID, out redirected, out IsAdded);

                using (DBEntities db = new DBEntities())
                {
                    var setting = db.tblSettings.Where(w => w.strName == "bakim").FirstOrDefault();
                    if (setting != null)
                    {
                        if (setting.strValue == "1")
                        {
                            return Redirect("/Error/status500");
                        }
                    }
                }
            }

            return View(signUp);
        }
        #endregion

        #region Set
        [HttpPost]
        [jCryptionHandler]
        public ActionResult SignUp(String logintype, String tc_no, String musteri_no_input, String cep_no, String cep_no_2, String smscode, String lgkey, String countdown)
        {
            if (string.IsNullOrEmpty(logintype) && string.IsNullOrEmpty(smscode))
            {
                return View();
            }

            string strPage = "User-SignUp-Set";

            try
            {
                cService_Methods serviceMethods = new cService_Methods(strPage);

                if (!string.IsNullOrEmpty(logintype))
                {
                    int intLoginType = Utilities.NullFixInt(logintype);

                    if (ConfigurationManager.AppSettings["UserLoginControl"] == "1")
                    {
                        using (DBEntities db = new DBEntities())
                        {
                            tblUserLogs user = null;
                            if (intLoginType == 2)
                            {
                                string strTCKN = cCrypto.EncryptDES(tc_no);
                                user = db.tblUserLogs.Where(w => w.strTCKN == strTCKN).OrderByDescending(o => o.dtRegisterDate).FirstOrDefault();
                            }
                            else
                            {
                                long intCustomerNo = Utilities.NullFixInt(musteri_no_input);
                                user = db.tblUserLogs.Where(w => w.intCustomerNo == intCustomerNo).OrderByDescending(o => o.dtRegisterDate).FirstOrDefault();
                            }

                            if (user != null)
                            {
                                if (user.dtLiveDate.HasValue)
                                {
                                    TimeSpan ts = DateTime.Now.Subtract(user.dtLiveDate.Value);
                                    if (ts.TotalSeconds < 6)
                                    {
                                        user.bolConnectedMsg = true;
                                        db.SaveChanges();

                                        return View(new cSignUp
                                        {
                                            strLoginType = logintype,
                                            strCustomerNo = musteri_no_input,
                                            strPhoneCustomer = cep_no_2,
                                            strPhoneTckn = cep_no,
                                            strTckn = tc_no,
                                            strMessage = Utilities.Msg(Utilities.MsgType.warning, "Sisteme bağlı görünüyorsunuz!")
                                        });
                                    }
                                }
                            }
                        }
                    }

                    string strAuthenticatedString = cAuthenticatedString.Get;

                    bool bolLogin = !string.IsNullOrEmpty(strAuthenticatedString);
                    if (bolLogin)
                    {
                        string strPhoneNumber = intLoginType == 2 ? cep_no : cep_no_2;
                        strPhoneNumber = strPhoneNumber.Replace("(", "").Replace(")", "").Replace(" ", "").Trim();

                        string strControlNumber = intLoginType == 2 ? tc_no : musteri_no_input;
                        strControlNumber = strControlNumber.Trim();

                        int? intLoginID = null;

                        var getLoginID = serviceMethods.GetLoginID(strAuthenticatedString, intLoginType, strControlNumber, strPhoneNumber, ref intLoginID);
                        if (getLoginID)
                        {
                            string strLoginID = cCrypto.EncryptDES(intLoginID.Value.ToString());

                            return View(new cSignUp
                            {
                                bolConfirmForm = true,
                                strLoginID = strLoginID,
                                strCountdown = ConfigurationManager.AppSettings["countdown"]
                            });
                        }
                        else
                        {
                            bolLogin = false;
                        }
                    }

                    if (!bolLogin)
                    {
                        return View(new cSignUp
                        {
                            strLoginType = logintype,
                            strCustomerNo = musteri_no_input,
                            strPhoneCustomer = cep_no_2,
                            strPhoneTckn = cep_no,
                            strTckn = tc_no,
                            strMessage = Utilities.Msg(Utilities.MsgType.warning, "Hatalı giriş! Bilgilerinizi kontrol ediniz!")
                        });
                    }
                }
                else if (!string.IsNullOrEmpty(smscode))
                {
                    int intSmsCount = Sessions.SmsCount;
                    if (intSmsCount < 3)
                    {
                        Sessions.SmsCount = intSmsCount + 1;

                        long intLoginID = Utilities.NullFixLong(cCrypto.DecryptDES(lgkey));
                        int intSmsCode = Utilities.NullFixInt(smscode);
                        long? intCustomerNo = 0;
                        string strSessionKey = "";

                        var validatePassword = serviceMethods.ValidatePassword(intLoginID, intSmsCode, ref intCustomerNo, ref strSessionKey);
                        if (validatePassword)
                        {
                            CustomerInfo customerInfo = null;
                            CustomerAddress customerAddress = null;

                            var getCustomerPersonalInfo = serviceMethods.GetCustomerPersonalInfo(strSessionKey, intCustomerNo.Value, ref customerInfo, ref customerAddress);
                            if (getCustomerPersonalInfo && customerInfo != null)
                            {
                                using (DBEntities db = new DBEntities())
                                {
                                    db.tblUserLogs.Add(new tblUserLogs
                                    {
                                        dtRegisterDate = DateTime.Now,
                                        intCustomerNo = intCustomerNo.Value,
                                        strIp = Utilities.ClientIP,
                                        strSessionKey = strSessionKey,
                                        dtLiveDate = DateTime.Now,
                                        strTCKN = cCrypto.EncryptDES(customerInfo.KimlikNo)
                                    });
                                    db.SaveChanges();
                                }

                                cUser user = new cUser();
                                user.strIP = Utilities.ClientIP;
                                user.intCustomerNo = intCustomerNo.Value;
                                user.strNameSurname = customerInfo.Ad + " " + customerInfo.Soyad;
                                user.strSessionKey = strSessionKey;
                                user.strTCKN = customerInfo.KimlikNo;

                                if (customerAddress != null)
                                {
                                    user.strEmail = customerAddress.Email;
                                    user.strPhone = string.IsNullOrEmpty(customerAddress.CepTel) ? customerAddress.EvTel : customerAddress.CepTel;
                                }

                                if (ConfigurationManager.AppSettings["test"] == "1")
                                {
                                    Cookies.SessionKey = strSessionKey;
                                }

                                List<cUserPages> pages = new List<cUserPages>();

                                #region pages
                                CustomerProduct[] listCustomerProducts = null;
                                var getCustomerProducts = serviceMethods.GetCustomerProducts(strSessionKey, user.intCustomerNo, ref listCustomerProducts);
                                if (getCustomerProducts)
                                {
                                    if (listCustomerProducts != null)
                                    {
                                        Sessions.CustomerProduct = listCustomerProducts;

                                        bool bolBireysel = listCustomerProducts.Count(w => w.YI == "Y" && w.Ok == "H" && w.BransKod == ((int)cEnums.BransKod.BireyselEmeklilik).ToString()) > 0;
                                        bool bolOtKat = listCustomerProducts.Count(w => w.YI == "Y" && w.Ok == "E" && w.BransKod == ((int)cEnums.BransKod.BireyselEmeklilik).ToString()) > 0;

                                        pages.Add(new cUserPages
                                        {
                                            strClass = "icon-home",
                                            strName = "Ana Sayfa",
                                            strPath = "/anasayfa",
                                            bolIsActive = bolBireysel || bolOtKat
                                        });

                                        pages.Add(new cUserPages
                                        {
                                            strClass = "icon-coins",
                                            strName = ConfigurationManager.AppSettings["BireyselTitle"],
                                            strPath = "/" + Utilities.Friendly(ConfigurationManager.AppSettings["BireyselTitle"]),
                                            bolIsActive = bolBireysel
                                        });

                                        pages.Add(new cUserPages
                                        {
                                            strClass = "icon-heart",
                                            strName = ConfigurationManager.AppSettings["HayatTitle"],
                                            strPath = "/" + Utilities.Friendly(ConfigurationManager.AppSettings["HayatTitle"]),
                                            bolIsActive = listCustomerProducts.Count(w => w.YI == "Y" && w.BransKod == ((int)cEnums.BransKod.Hayat).ToString()) > 0
                                        });

                                        pages.Add(new cUserPages
                                        {
                                            strClass = "icon-healtcare",
                                            strName = ConfigurationManager.AppSettings["FerdiTitle"],
                                            strPath = "/" + Utilities.Friendly(ConfigurationManager.AppSettings["FerdiTitle"]),
                                            bolIsActive = listCustomerProducts.Count(w => w.YI == "Y" && w.BransKod == ((int)cEnums.BransKod.FerdiKaza).ToString()) > 0
                                        });

                                        pages.Add(new cUserPages
                                        {
                                            strClass = "icon-time-passing",
                                            strName = ConfigurationManager.AppSettings["OtomatikKatilimTitle"],
                                            strPath = "/" + Utilities.Friendly(ConfigurationManager.AppSettings["OtomatikKatilimTitle"]),
                                            bolIsActive = bolOtKat
                                        });

                                        pages.Add(new cUserPages
                                        {
                                            strClass = "icon-charity-new",
                                            strName = ConfigurationManager.AppSettings["SaglikTitle"],
                                            strPath = "/" + Utilities.Friendly(ConfigurationManager.AppSettings["SaglikTitle"]),
                                            bolIsActive = listCustomerProducts.Count(w => w.YI == "Y" && w.BransKod == ((int)cEnums.BransKod.Saglik).ToString()) > 0
                                        });

                                        user.pages = pages;
                                    }
                                }
                                #endregion

                                Sessions.CurrentUser = user;
                                FormsAuthentication.SetAuthCookie(intLoginID.ToString(), false);
                                Cookies.CookieSss = "1";

                                return Redirect(pages.Count(c => c.bolIsActive) > 0 ? pages.First(f => f.bolIsActive).strPath : "/kisisel-bilgiler");
                            }
                            else
                            {
                                return SignUp(new cSignUp
                                {
                                    strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.")
                                });
                            }
                        }
                        else
                        {
                            return SignUp(new cSignUp
                            {
                                bolConfirmForm = true,
                                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Doğrulama kodunu hatalı girdiniz!"),
                                strScript = "_clickBtn = 0;",
                                strCountdown = (Utilities.NullFixInt(countdown) - 1000).ToString()
                            });
                        }
                    }
                    else
                    {
                        Sessions.SmsCount = 0;

                        return SignUp(new cSignUp
                        {
                            strMessage = Utilities.Msg(Utilities.MsgType.warning, "Hatalı giriş! Tekrar deneyiniz.")
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);

                return SignUp(new cSignUp
                {
                    strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.")
                });
            }

            return View(new cSignUp());
        }
        #endregion

        #region UserControl
        public ActionResult UserControl()
        {
            bool bolSession = Session["CurrentUser"] != null && Session["CustomerProduct"] != null;

            if (bolSession)
            {
                try
                {
                    using (DBEntities db = new DBEntities())
                    {
                        var user = db.tblUserLogs.Where(w => w.intCustomerNo == Sessions.CurrentUser.intCustomerNo && w.strSessionKey == Sessions.CurrentUser.strSessionKey).FirstOrDefault();
                        if (user != null)
                        {
                            if (user.bolConnectedMsg)
                            {
                                Sessions.Message = Utilities.Msg(Utilities.MsgType.warning, "Farklı bir oturumda giriş yapmaya çalıştığınız için oturumunuz sonlandırıldı.");
                                BrCntrl();
                            }

                            user.dtLiveDate = DateTime.Now;
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.Exception(ex.ToString(), 0, "user/usercontrol");
                }
            }

            if (!bolSession || Cookies.CookieSss == "")
                return Json("", JsonRequestBehavior.AllowGet);

            return Json("1", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region BrCntrl
        public string BrCntrl()
        {
            Cookies.CookieSss = "";
            Session["CurrentUser"] = null;
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();

            return "";
        }
        #endregion

        #region Info
        public ActionResult Info()
        { 
            if (Session["CurrentUser"] != null)
            {
                try
                {
                    cService_Methods serviceMethods = new cService_Methods("User-Info");

                    using (DBEntities db = new DBEntities())
                    {
                        bool bolControl = false;
                        var bireyselChat = db.tblSettings.Where(w => w.strName == "chat").FirstOrDefault();
                        if (bireyselChat != null)
                        {
                            bolControl = bireyselChat.strValue == "1";
                        }

                        var user = Sessions.CurrentUser;

                        return Json(new
                        {
                            namesurname = user.strNameSurname,
                            email = user.strEmail,
                            phone = user.strPhone,
                            control = bolControl ? "1" : "0"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch { }
            }

            return Json(new
            {
                namesurname = "",
                email = "",
                phone = "",
                control = "0"
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
