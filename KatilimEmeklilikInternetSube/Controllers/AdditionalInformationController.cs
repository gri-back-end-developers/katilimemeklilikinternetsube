﻿using KatilimEmeklilikInternetSube.Classes;
using KatilimEmeklilikInternetSube.Classes.ClassData.AdditionalInformation;
using KatilimEmeklilikInternetSube.Classes.DBData;
using KatilimEmeklilikInternetSube.Classes.LiveData;
using KatilimEmeklilikInternetSube.Classes.Service;
using KatilimEmeklilikInternetSube.WsOperations;
using System;
using System.Diagnostics;
using System.Web.Mvc;

namespace KatilimEmeklilikInternetSube.Controllers
{
    public class AdditionalInformationController : Controller
    {
        #region Index
        [UserFilter]
        [HttpGet]
        public ActionResult Index()
        {
            cAdditionalInformation additionalInformation = new cAdditionalInformation();
            string strPage = "AdditionalInformation-Index";

            try
            {
                cService_Methods serviceMethods = new cService_Methods(strPage);

                CustomerInfo customerInfo = null;
                CustomerAddress customerAddress = null;

                var getCustomerPersonalInfo = serviceMethods.GetCustomerPersonalInfo(Sessions.CurrentUser.strSessionKey, Sessions.CurrentUser.intCustomerNo, ref customerInfo, ref customerAddress);
                if (getCustomerPersonalInfo)
                {
                    if (customerInfo != null)
                    {
                        additionalInformation.KatilimciBilgileri.AdSoyad = customerInfo.Ad + " " + customerInfo.Soyad;
                        additionalInformation.KatilimciBilgileri.AnneAdi = customerInfo.AnaAdi;
                        additionalInformation.KatilimciBilgileri.BabaAdi = customerInfo.BabaAdi;
                        additionalInformation.KatilimciBilgileri.Cinsiyet = customerInfo.CinsiyetAck;
                        additionalInformation.KatilimciBilgileri.CocukSayisi = customerInfo.CocukAdet.HasValue ? customerInfo.CocukAdet.Value.ToString() : "-";
                        additionalInformation.KatilimciBilgileri.DogumTarihi = customerInfo.DogumTarih.HasValue ? customerInfo.DogumTarih.Value.ToShortDateString() : "0";
                        additionalInformation.KatilimciBilgileri.DogumYeri = customerInfo.DogumYeriAck;
                        additionalInformation.KatilimciBilgileri.EgitimDurumu = string.IsNullOrEmpty(customerInfo.Egitim) ? "-" : customerInfo.Egitim;
                        additionalInformation.KatilimciBilgileri.MedeniDurum = customerInfo.MedeniDurumAck;
                        additionalInformation.KatilimciBilgileri.TCKN = customerInfo.KimlikNo;
                        additionalInformation.KatilimciBilgileri.Uyruk = customerInfo.UyrukAck;
                        additionalInformation.KatilimciBilgileri.VergiNumarasi = customerInfo.VergiNo;
                    }

                    if (customerAddress != null)
                    {
                        additionalInformation.İletisimBilgileri.Adres = customerAddress.Adres;
                        additionalInformation.İletisimBilgileri.CepTelefonu = customerAddress.CepTel;
                        additionalInformation.İletisimBilgileri.Eposta = customerAddress.Email;
                        additionalInformation.İletisimBilgileri.EvTelefonu = customerAddress.EvTel;
                        additionalInformation.İletisimBilgileri.IsTelefonu = customerAddress.IsTel;
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(additionalInformation);
        }
        #endregion
    }
}
