﻿#region Directives
using KatilimEmeklilikInternetSube.Classes;
using KatilimEmeklilikInternetSube.Classes.ClassData.Personal;
using KatilimEmeklilikInternetSube.Classes.ClassData.PolicyDetail;
using KatilimEmeklilikInternetSube.Classes.DBData;
using KatilimEmeklilikInternetSube.Classes.LiveData;
using KatilimEmeklilikInternetSube.Classes.Service;
using KatilimEmeklilikInternetSube.Classes.Tools;
using KatilimEmeklilikInternetSube.WsOperations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
#endregion

namespace KatilimEmeklilikInternetSube.Controllers
{
    public class PersonalController : Controller
    {
        cService_Methods serviceMethods;

        #region Index
        [UserFilter]
        public ActionResult Index(string polNo = "")
        {
            cPersonal personal = new cPersonal();
            string strPage = "Personal-Index";

            try
            {
                long policyNo = string.IsNullOrEmpty(polNo) ? 0 : Utilities.NullFixLong(cCrypto.DecryptDES(polNo));

                serviceMethods = new cService_Methods(strPage);

                CustomerProduct[] listCustomerProducts = Sessions.CustomerProduct;
                if (listCustomerProducts != null)
                {
                    personal.Policies = new List<KatilimEmeklilikInternetSube.Classes.ClassData.cPolicies>();

                    foreach (var item in listCustomerProducts.Where(w => w.YI == "Y" && w.BransKod == ((int)cEnums.BransKod.FerdiKaza).ToString()).ToList())
                    {
                        personal.Policies.Add(new KatilimEmeklilikInternetSube.Classes.ClassData.cPolicies
                        {
                            PoliceNo = item.PoliceNo.Value
                        });
                    }

                    if (policyNo == 0)
                    {
                        policyNo = personal.Policies.First().PoliceNo;
                    }
                }

                ViewBag.policyNo = policyNo;

                LifeGeneralInfo lifeGeneralInfo = null;
                Beneficiary[] listBeneficiaryInfo = null;
                CoverageLastStatus[] listCoverageInfo = null;
                LifeCollectingInfo collectingInfo = null;
                MediatorInfo mediatorInfo = null;

                var getLifeGeneralInfo = serviceMethods.GetLifeGeneralInfo(policyNo, ref lifeGeneralInfo, ref listBeneficiaryInfo, ref listCoverageInfo, ref collectingInfo, ref mediatorInfo);
                if (getLifeGeneralInfo)
                {
                    if (lifeGeneralInfo != null)
                    {
                        personal.IsHaveData = true;

                        personal.GeneralInfo = new cPersonal_GeneralInfo
                        {
                            GirisTarih = lifeGeneralInfo.BaslamaTarih.HasValue ? lifeGeneralInfo.BaslamaTarih.Value.ToString("dd MMMM yyyy") : "",
                            BitisTarih = lifeGeneralInfo.BitisTarih.HasValue ? lifeGeneralInfo.BitisTarih.Value.ToString("dd MMMM yyyy") : "",
                            UrunAd = lifeGeneralInfo.UrunAd
                        };
                    }

                    if (listCoverageInfo != null)
                    {
                        if (listCoverageInfo.Length > 0)
                        {
                            personal.IsHaveData = true;
                            personal.PoliceBilgileri = new List<cPersonal_PolicyInfo>();

                            foreach (var item in listCoverageInfo)
                            {
                                personal.PoliceBilgileri.Add(new cPersonal_PolicyInfo
                                {
                                    Bedel = Utilities.Price(item.Bedel.HasValue ? item.Bedel.Value : 0),
                                    Prim = Utilities.Price(item.BrutPrim.HasValue ? item.BrutPrim.Value : 0),
                                    TeminatAdi = item.TeminatAd,
                                    TeminatKodu = item.TeminatKod
                                });
                            }

                            personal.ToplamPrim = Utilities.Price(listCoverageInfo.Sum(s => s.BrutPrim).Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(personal);
        }
        #endregion

        #region PolicyDetail
        [UserFilter]
        public ActionResult PolicyDetail()
        {
            cPolicyDetail policyDetail = new cPolicyDetail();
            string strPage = "Personal-PolicyDetail";

            try
            {
                serviceMethods = new cService_Methods(strPage);

                CustomerProduct[] listCustomerProducts = Sessions.CustomerProduct;
                if (listCustomerProducts != null)
                {
                    var listCustomerProduct = listCustomerProducts.Where(w => w.YI == "Y" && w.PoliceNo == QueryStrings.policy).FirstOrDefault();
                    if (listCustomerProduct != null)
                    {
                        policyDetail.PoliceBilgileri = new cPoliceBilgileri
                        {
                            Odeyen = listCustomerProduct.BorcluAd,
                            OdeyenTCKimlikNo = listCustomerProduct.BorcluKimlikNo
                        };
                    }
                }

                LifeGeneralInfo lifeGeneralInfo = null;
                Beneficiary[] listBeneficiaryInfo = null;
                CoverageLastStatus[] listCoverageInfo = null;
                LifeCollectingInfo collectingInfo = null;
                MediatorInfo mediatorInfo = null;

                var getLifeGeneralInfo = serviceMethods.GetLifeGeneralInfo(QueryStrings.policy, ref lifeGeneralInfo, ref listBeneficiaryInfo, ref listCoverageInfo, ref collectingInfo, ref mediatorInfo);
                if (getLifeGeneralInfo)
                {
                    if (lifeGeneralInfo != null)
                    {
                        if (policyDetail.PoliceBilgileri == null)
                            policyDetail.PoliceBilgileri = new cPoliceBilgileri();

                        policyDetail.PoliceBilgileri.BaslangicTarihi = lifeGeneralInfo.BaslamaTarih.HasValue ? lifeGeneralInfo.BaslamaTarih.Value.ToShortDateString() : "";
                        policyDetail.PoliceBilgileri.ToplamPrimTutari = Utilities.Price(lifeGeneralInfo.PrimTutar.HasValue ? lifeGeneralInfo.PrimTutar.Value : 0);
                        policyDetail.PoliceBilgileri.BitisTarihi = lifeGeneralInfo.BitisTarih.HasValue ? lifeGeneralInfo.BitisTarih.Value.ToShortDateString() : "";
                        policyDetail.PoliceBilgileri.OdemeAraci = lifeGeneralInfo.OdemeAraciAck;
                        policyDetail.PoliceBilgileri.OdemePeriyodu = lifeGeneralInfo.OdemeSekli;
                        policyDetail.PoliceBilgileri.PoliceNo_SertifikaNo = lifeGeneralInfo.PoliceNo.Value.ToString();
                        policyDetail.PoliceBilgileri.Sigortali = lifeGeneralInfo.KatilimciAd + " " + lifeGeneralInfo.KatilimciSoyad;
                        policyDetail.PoliceBilgileri.SigortaliKatilimEmeklilikMusteriNo = lifeGeneralInfo.KatilimciNo.HasValue ? lifeGeneralInfo.KatilimciNo.Value.ToString() : "";
                        policyDetail.PoliceBilgileri.SigortaliTCKimlikNo = lifeGeneralInfo.KatilimciKimlikNo;
                        policyDetail.PoliceBilgileri.ToplamKalanPrimTutari = Utilities.Price(collectingInfo.KalanPrimTutar.HasValue ? collectingInfo.KalanPrimTutar.Value : 0);
                        policyDetail.PoliceBilgileri.UrunAdi = lifeGeneralInfo.UrunAd;
                    }

                    if (collectingInfo != null)
                    {
                        decimal IdariMasrafKesintisi = collectingInfo.MasrafTutar.HasValue ? collectingInfo.MasrafTutar.Value : 0;
                        decimal KesintilerToplami = collectingInfo.ToplamKesintiTutar.HasValue ? collectingInfo.ToplamKesintiTutar.Value : 0;
                        decimal KomisyonKesintisi = collectingInfo.KomisyonTutar.HasValue ? collectingInfo.KomisyonTutar.Value : 0;
                        decimal ToplamOdemeTutari = collectingInfo.OdemeTutar.HasValue ? collectingInfo.OdemeTutar.Value : 0;
                        decimal VergiTutar = collectingInfo.VergiTutar.HasValue ? collectingInfo.VergiTutar.Value : 0;

                        if (IdariMasrafKesintisi > 0 || KesintilerToplami > 0 || KomisyonKesintisi > 0 || ToplamOdemeTutari > 0 || VergiTutar > 0)
                        {
                            policyDetail.OdemeDetayBilgileri = new cOdemeDetayBilgileri
                            {
                                IdariMasrafKesintisi = Utilities.Price(IdariMasrafKesintisi),
                                KesintilerToplami = Utilities.Price(KesintilerToplami),
                                KomisyonKesintisi = Utilities.Price(KomisyonKesintisi),
                                ToplamOdemeTutari = Utilities.Price(ToplamOdemeTutari),
                                VergiTutar = Utilities.Price(VergiTutar)
                            };
                        }
                    }

                    if (listBeneficiaryInfo != null)
                    {
                        if (listBeneficiaryInfo.Length > 0)
                        {
                            policyDetail.VefatDurumundaLehtarlar = new List<cVefatDurumundaLehtarlar>();

                            foreach (var item in listBeneficiaryInfo)
                            {
                                if (!string.IsNullOrEmpty(item.AdSoyad.Trim()) || item.MenfaattarOran.HasValue)
                                {
                                    policyDetail.VefatDurumundaLehtarlar.Add(new cVefatDurumundaLehtarlar
                                    {
                                        AdSoyad = item.AdSoyad,
                                        Oran = item.MenfaattarOran.HasValue ? item.MenfaattarOran.Value.ToString() : ""
                                    });
                                }
                            }

                            if (policyDetail.VefatDurumundaLehtarlar.Count == 0)
                                policyDetail.VefatDurumundaLehtarlar = null;
                        }

                        if (listCoverageInfo != null)
                        {
                            if (listCoverageInfo.Length > 0)
                            {
                                policyDetail.TeminatBilgileri = new List<cTeminatBilgileri>();

                                foreach (var item in listCoverageInfo)
                                {
                                    policyDetail.TeminatBilgileri.Add(new cTeminatBilgileri
                                    {
                                        TeminatAd = item.TeminatAd,
                                        Bedel = Utilities.Price(item.Bedel.HasValue ? item.Bedel.Value : 0)
                                    });
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(policyDetail);
        }
        #endregion

        #region ContractPdf
        [UserFilter]
        public EmptyResult ContractPdf()
        {
            string strPage = "Personal-ContractPdf";
            try
            {
                KeyValuePair<string, string>[] parameters = new KeyValuePair<string, string>[]
                {
                    new KeyValuePair<string, string>("P_POLICE_NO", QueryStrings.policy.ToString()),
                    new KeyValuePair<string, string>("P_ZEYIL_NO", "0")
                };

                serviceMethods = new cService_Methods(strPage);

                string strMsg = "";

                var report = serviceMethods.GetReport(parameters, KatilimEmeklilikInternetSube.WsReport.ReportKey.PoliceBasim, ref strMsg);
                if (report != null)
                {
                    Utilities.DownloadFile(report.ReportPdf, "Police-" + QueryStrings.policy + ".pdf", "application/pdf");
                }
                else
                {
                    if (!string.IsNullOrEmpty(strMsg))
                    {
                        Sessions.Message = Utilities.Msg(Utilities.MsgType.warning, strMsg);
                    }

                    PageSettings pageSettings = new PageSettings();
                    Response.Redirect("/" + pageSettings.UrlRoot, false);
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);

                PageSettings pageSettings = new PageSettings();
                Response.Redirect("/" + pageSettings.UrlRoot, false);
            }

            return new EmptyResult(); 
        }
        #endregion
    }
}
