﻿using KatilimEmeklilikInternetSube.Classes;
using KatilimEmeklilikInternetSube.Classes.ClassData;
using KatilimEmeklilikInternetSube.Classes.DBData;
using KatilimEmeklilikInternetSube.Classes.LiveData;
using KatilimEmeklilikInternetSube.Classes.Service;
using KatilimEmeklilikInternetSube.Classes.Tools;
using KatilimEmeklilikInternetSube.Models;
using KatilimEmeklilikInternetSube.WsOperations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;

namespace KatilimEmeklilikInternetSube.Controllers
{
    public class ContactController : Controller
    {
        #region RequestSuggest

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult RequestSuggest(string strMsg = "")
        {
            cRequestSuggest requestSuggest = new cRequestSuggest();
            string strPage = "Contact-RequestSuggest-Get";

            try
            {
                ViewBag.Msg = strMsg;

                cService_Methods serviceMethods = new cService_Methods(strPage);

                CustomerProduct[] listCustomerProducts = Sessions.CustomerProduct;

                if (listCustomerProducts != null)
                {
                    requestSuggest.Policeler = new List<cRequestSuggestPolicy>();

                    foreach (var item in listCustomerProducts.Where(w => w.YI == "Y").ToList())
                    {
                        if (item.PoliceNo.HasValue)
                        {
                            requestSuggest.Policeler.Add(new cRequestSuggestPolicy
                            {
                                Police = item.PoliceNo.Value.ToString()
                            });
                        }
                    }
                }

                CustomerInfo customerInfo = null;
                CustomerAddress customerAddress = null;

                var getCustomerPersonalInfo = serviceMethods.GetCustomerPersonalInfo(Sessions.CurrentUser.strSessionKey, Sessions.CurrentUser.intCustomerNo, ref customerInfo, ref customerAddress);
                if (getCustomerPersonalInfo)
                {
                    if (customerAddress != null)
                    {
                        requestSuggest.Eposta = customerAddress.Email.Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);

                strMsg = Utilities.Msg(Utilities.MsgType.error, "Mesajınız gönderilemedi! Tekrar deneyiniz.");
            }

            return View(requestSuggest);
        }
        #endregion

        #region Post
        [UserFilter]
        [HttpPost]
        public ActionResult RequestSuggest()
        {
            string strMsg = "";
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    tblFormDatas formData = new tblFormDatas()
                    {
                        intCustomerNo = Sessions.CurrentUser.intCustomerNo,
                        strFormTypeName = "İletişim / Öneri Formları",
                        dtRegisterDate = DateTime.Now,
                        strEmail = "",
                        strMessage = Request.Form["message"],
                        strTopic = Request.Form["topic"],
                        strPolicyNo = Request.Form["policyno"]
                    };

                    db.tblFormDatas.Add(formData);
                    db.SaveChanges();
                }

                string strContent = "<p><strong>Form : </strong> İletişim / Öneri Formları</p>";
                strContent += "<p><strong>Konu : </strong> " + Request.Form["topic"] + "</p>";
                strContent += "<p><strong>Poliçe No : </strong> " + Request.Form["policyno"] + "</p>";
                strContent += "<p><strong>Poliçe No : </strong> " + Request.Form["policyno"] + "</p>";
                strContent += "<p><strong>Mesaj : </strong> " + Request.Form["message"] + "</p>";

                string strHtml = Utilities.GetFileText(Server.MapPath("~/template/contact.html"));
                strHtml = strHtml.Replace("#content#", strContent);

                string[] strEmails = ConfigurationManager.AppSettings["ContactMails"].ToString().Split(';');

                foreach (var item in strEmails)
                {
                    Email.SendEmail("İletişim / Öneri Formları", strHtml, item);
                }

                strMsg = Utilities.Msg(Utilities.MsgType.success, "Mesajınız gönderildi.");
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, "Contact-RequestSuggest-Post");

                strMsg = Utilities.Msg(Utilities.MsgType.error, "Mesajınız gönderilemedi! Tekrar deneyiniz.");
            }

            return RequestSuggest(strMsg);
        }
        #endregion

        #endregion

        #region Info

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult Info(string strMsg = "")
        {
            ViewBag.Msg = strMsg;

            return View();
        }
        #endregion

        #region Post
        [UserFilter]
        [HttpPost]
        public ActionResult Info()
        {
            string strMsg = "";
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    tblFormDatas formData = new tblFormDatas()
                    {
                        intCustomerNo = Sessions.CurrentUser.intCustomerNo,
                        strFormTypeName = "Bilgi Alma Formu",
                        dtRegisterDate = DateTime.Now,
                        strEmail = Request.Form["email"],
                        strPhone = Request.Form["phone"],
                        strNameSurname = Request.Form["namesurname"],
                        strInfoTopic = Request.Form["infotopic"]
                    };

                    db.tblFormDatas.Add(formData);
                    db.SaveChanges();
                }

                string strContent = "<p><strong>Form : </strong> Bilgi Alma Formu</p>";
                strContent += "<p><strong>Bilgi Alınacak Konu : </strong> " + Request.Form["infotopic"] + "</p>";
                strContent += "<p><strong>Ad, Soyad : </strong> " + Request.Form["namesurname"] + "</p>";
                strContent += "<p><strong>E-posta : </strong> " + Request.Form["email"] + "</p>";
                strContent += "<p><strong>Telefon : </strong> " + Request.Form["phone"] + "</p>";

                string strHtml = Utilities.GetFileText(Server.MapPath("~/template/contact.html"));
                strHtml = strHtml.Replace("#content#", strContent);

                string[] strEmails = ConfigurationManager.AppSettings["ContactMails"].ToString().Split(';');

                foreach (var item in strEmails)
                {
                    Email.SendEmail("Bilgi Alma Formu", strHtml, item);
                }

                strMsg = Utilities.Msg(Utilities.MsgType.success, "Mesajınız gönderildi.");
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, "Contact-Info");

                strMsg = Utilities.Msg(Utilities.MsgType.error, "Mesajınız gönderilemedi! Tekrar deneyiniz.");
            }

            return Info(strMsg);
        }
        #endregion

        #endregion

        #region FormDatas
        [UserFilter]
        public ActionResult FormDatas()
        {
            List<tblFormDatas> datas = new List<tblFormDatas>();

            try
            {
                using (DBEntities db = new DBEntities())
                {
                    datas = db.tblFormDatas.Where(w => w.intCustomerNo == Sessions.CurrentUser.intCustomerNo).OrderBy(o => o.dtRegisterDate).ToList();
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), 0, "Contact-FormDatas");
            }

            return View(datas);
        }
        #endregion

        #region Index
        [UserFilter]
        public ActionResult Index()
        {
            return View();
        }
        #endregion
    }
}
