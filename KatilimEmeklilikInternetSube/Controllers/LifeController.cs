﻿#region Directives
using KatilimEmeklilikInternetSube.Classes;
using KatilimEmeklilikInternetSube.Classes.ClassData;
using KatilimEmeklilikInternetSube.Classes.ClassData.Life;
using KatilimEmeklilikInternetSube.Classes.DBData;
using KatilimEmeklilikInternetSube.Classes.LiveData;
using KatilimEmeklilikInternetSube.Classes.Service;
using KatilimEmeklilikInternetSube.Classes.Tools;
using KatilimEmeklilikInternetSube.WsOperations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
#endregion

namespace KatilimEmeklilikInternetSube.Controllers
{
    public class LifeController : Controller
    {
        cService_Methods serviceMethods;

        #region Index
        [UserFilter]
        public ActionResult Index(string polNo = "")
        {
            cLife life = new cLife();
            string strPage = "Life-Index";

            try
            {
                long policyNo = string.IsNullOrEmpty(polNo) ? 0 : Utilities.NullFixLong(cCrypto.DecryptDES(polNo));

                serviceMethods = new cService_Methods(strPage);
                
                CustomerProduct[] listCustomerProducts = Sessions.CustomerProduct;

                if (listCustomerProducts != null)
                {
                    life.Policies = new List<cPolicies>();

                    foreach (var item in listCustomerProducts.Where(w => w.YI == "Y" && w.BransKod == ((int)cEnums.BransKod.Hayat).ToString()).ToList())
                    {
                        life.Policies.Add(new cPolicies
                        {
                            PoliceNo = item.PoliceNo.Value
                        });
                    }

                    if (policyNo == 0)
                    {
                        policyNo = life.Policies.First().PoliceNo;
                    }
                }

                ViewBag.policyNo = policyNo;

                LifeGeneralInfo lifeGeneralInfo = null;
                Beneficiary[] listBeneficiaryInfo = null;
                CoverageLastStatus[] listCoverageInfo = null;
                LifeCollectingInfo collectingInfo = null;
                MediatorInfo mediatorInfo = null;

                var getLifeGeneralInfo = serviceMethods.GetLifeGeneralInfo(policyNo, ref lifeGeneralInfo, ref listBeneficiaryInfo, ref listCoverageInfo, ref collectingInfo, ref mediatorInfo);
                if (getLifeGeneralInfo)
                {
                    if (lifeGeneralInfo != null)
                    {
                        life.IsHaveData = true;

                        life.LifeGeneralInfo = new cLife_LifeGeneralInfo
                        {
                            GirisTarih = lifeGeneralInfo.BaslamaTarih.HasValue ? lifeGeneralInfo.BaslamaTarih.Value.ToString("dd MMMM yyyy") : "",
                            BitisTarih = lifeGeneralInfo.BitisTarih.HasValue ? lifeGeneralInfo.BitisTarih.Value.ToString("dd MMMM yyyy") : "",
                            UrunAd = lifeGeneralInfo.UrunAd
                        };
                    }

                    if (listCoverageInfo != null)
                    {
                        if (listCoverageInfo.Length > 0)
                        {
                            life.IsHaveData = true;

                            life.PoliceBilgileri = new List<cLife_PoliceBilgileri>();

                            foreach (var item in listCoverageInfo)
                            {
                                life.PoliceBilgileri.Add(new cLife_PoliceBilgileri
                                {
                                    Bedel = Utilities.Price(item.Bedel.HasValue ? item.Bedel.Value : 0),
                                    Prim = Utilities.Price(item.BrutPrim.HasValue ? item.BrutPrim.Value : 0),
                                    TeminatAdi = item.TeminatAd,
                                    TeminatKodu = item.TeminatKod
                                });
                            }

                            life.ToplamPrim = Utilities.Price(listCoverageInfo.Sum(s => s.BrutPrim).Value);
                        }
                    }
                } 
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(life);
        }
        #endregion

        #region ContractPdf
        [UserFilter]
        public EmptyResult ContractPdf()
        {
            string strPage = "Life-ContractPdf";
            try
            {
                KeyValuePair<string, string>[] parameters = new KeyValuePair<string, string>[]
                {
                    new KeyValuePair<string, string>("P_POLICE_NO", QueryStrings.policy.ToString()),
                    new KeyValuePair<string, string>("P_ZEYIL_NO", "0")
                };

                serviceMethods = new cService_Methods(strPage);

                string strMsg = "";

                var report = serviceMethods.GetReport(parameters, KatilimEmeklilikInternetSube.WsReport.ReportKey.PoliceBasim, ref strMsg);
                if (report != null)
                {
                    Utilities.DownloadFile(report.ReportPdf, "Police-" + QueryStrings.policy + ".pdf", "application/pdf");
                }
                else
                {
                    if (!string.IsNullOrEmpty(strMsg))
                    {
                        Sessions.Message = Utilities.Msg(Utilities.MsgType.warning, strMsg);
                    }

                    PageSettings pageSettings = new PageSettings();
                    Response.Redirect("/" + pageSettings.UrlRoot, false);
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);

                PageSettings pageSettings = new PageSettings();
                Response.Redirect("/" + pageSettings.UrlRoot, false);
            }

            return new EmptyResult(); 
        }
        #endregion
    }
}
