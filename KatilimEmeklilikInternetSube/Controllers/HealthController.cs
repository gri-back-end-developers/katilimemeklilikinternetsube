﻿#region Directives
using KatilimEmeklilikInternetSube.Classes;
using KatilimEmeklilikInternetSube.Classes.ClassData.Health;
using KatilimEmeklilikInternetSube.Classes.ClassData.Health.ContractedInstitution;
using KatilimEmeklilikInternetSube.Classes.ClassData.Health.PaymentInfo;
using KatilimEmeklilikInternetSube.Classes.ClassData.Health.PolicyDetail;
using KatilimEmeklilikInternetSube.Classes.DBData;
using KatilimEmeklilikInternetSube.Classes.LiveData;
using KatilimEmeklilikInternetSube.Classes.Service;
using KatilimEmeklilikInternetSube.Classes.Tools;
using KatilimEmeklilikInternetSube.WsOperations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
#endregion

namespace KatilimEmeklilikInternetSube.Controllers
{
    public class HealthController : Controller
    {
        cService_Methods serviceMethods;

        #region Index
        [UserFilter]
        public ActionResult Index(string polNo = "")
        {
            cHealth health = new cHealth();
            string strPage = "Health-Index";

            try
            {
                long policyNo = string.IsNullOrEmpty(polNo) ? 0 : Utilities.NullFixLong(cCrypto.DecryptDES(polNo));

                var listCustomerProducts = Sessions.CustomerProduct;
                if (listCustomerProducts != null)
                {
                    health.Policies = new List<KatilimEmeklilikInternetSube.Classes.ClassData.cPolicies>();

                    foreach (var item in listCustomerProducts.Where(w => w.YI == "Y" && w.BransKod == ((int)cEnums.BransKod.Saglik).ToString()).ToList())
                    {
                        health.Policies.Add(new KatilimEmeklilikInternetSube.Classes.ClassData.cPolicies
                        {
                            PoliceNo = item.PoliceNo.Value
                        });
                    }

                    if (policyNo == 0)
                    {
                        policyNo = health.Policies.First().PoliceNo;
                    }
                }

                ViewBag.policyNo = policyNo;

                serviceMethods = new cService_Methods(strPage);

                var healthPolicyInfo = serviceMethods.GetHealthPolicyInfo(policyNo, Sessions.CurrentUser.strTCKN);
                if (healthPolicyInfo != null)
                {
                    health.IsHaveData = true;

                    health.GeneralInfo = new cHealth_GeneralInfo
                    {
                        BaslangicTarih = healthPolicyInfo.HealthPolicyInfo.Value[0].POLICE_BASLAMA_TARIH.HasValue ? healthPolicyInfo.HealthPolicyInfo.Value[0].POLICE_BASLAMA_TARIH.Value.ToString("dd MMMM yyyy") : "",
                        BitisTarih = healthPolicyInfo.HealthPolicyInfo.Value[0].POLICE_BITIS_TARIH.HasValue ? healthPolicyInfo.HealthPolicyInfo.Value[0].POLICE_BITIS_TARIH.Value.ToString("dd MMMM yyyy") : "",
                        TarifeAd = healthPolicyInfo.HealthPolicyInfo.Value[0].TARIFE_ADI
                    };

                    Sessions.TitleName = health.GeneralInfo.TarifeAd;

                    if (healthPolicyInfo.HealthPolicyInfo.Value != null && healthPolicyInfo.HealthPolicyInfo.Value.Length > 0)
                    {
                        if (healthPolicyInfo.HealthPolicyInfo.Value[0].P_TAB_SAGLIK_MUSTERI_BILGI != null && healthPolicyInfo.HealthPolicyInfo.Value[0].P_TAB_SAGLIK_MUSTERI_BILGI.Value.Length > 0)
                        {
                            health.Members = new List<cHealth_Member>();

                            foreach (var item in healthPolicyInfo.HealthPolicyInfo.Value[0].P_TAB_SAGLIK_MUSTERI_BILGI.Value)
                            {
                                health.Members.Add(new cHealth_Member
                                {
                                    AdSoyad = item.SIGORTALI_AD_SOYAD,
                                    MusteriNo = item.SIGORTALI_MUSTERI_NO.HasValue ? item.SIGORTALI_MUSTERI_NO.Value.ToString() : "",
                                    Yakinlik = item.SIGORTALI_YAKINLIK_DERECESI
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(health);
        }
        #endregion

        #region PolicyInfo
        [UserFilter]
        public ActionResult PolicyInfo()
        {
            cHealth_PolicyInfo policyInfo = new cHealth_PolicyInfo();
            string strPage = "Health-PolicyInfo";

            try
            {
                serviceMethods = new cService_Methods(strPage);

                var healthPolicyInfoFromTPA = serviceMethods.GetHealthPolicyInfoFromTPA(QueryStrings.policy, QueryStrings.CustomerNo);
                if (healthPolicyInfoFromTPA != null)
                {
                    foreach (var item in healthPolicyInfoFromTPA.HealthPolicyInfo.Value)
                    {
                        if (item.EX_MUSTERI_NO == QueryStrings.CustomerNo)
                        {
                            if (item.TEMINAT_LISTESI != null && item.TEMINAT_LISTESI.Value != null && item.TEMINAT_LISTESI.Value.Length > 0)
                            {
                                policyInfo.PoliceBilgileri = new List<cHealth_PolicyInfoData>();

                                foreach (var teminat in item.TEMINAT_LISTESI.Value)
                                {
                                    policyInfo.PoliceBilgileri.Add(new cHealth_PolicyInfoData
                                    {
                                        Kalan = teminat.KALAN,
                                        Katilim = "",
                                        Kullanilan = teminat.KULLANIM,
                                        Limitiniz = teminat.TOPLAM,
                                        LimitTipi = teminat.UYGULAMA,
                                        TeminatAdi = teminat.TEMINAT_AD
                                    });
                                }
                            }
                        }
                    }
                }

                policyInfo.UrunAdi = Sessions.TitleName;
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(policyInfo);
        }
        #endregion

        #region PolicyDetail
        [UserFilter]
        public ActionResult PolicyDetail(string musteritckn = "")
        {
            cHealth_PolicyDetail policyDetail = new cHealth_PolicyDetail();
            string strPage = "Health-PolicyDetail";

            try
            {
                ViewBag.musteritckn = musteritckn;

                string strTCKN = string.IsNullOrEmpty(musteritckn) ? Sessions.CurrentUser.strTCKN : cCrypto.DecryptDES(musteritckn);

                serviceMethods = new cService_Methods(strPage);

                var healthPolicyInfo = serviceMethods.GetHealthPolicyInfo(QueryStrings.policy, strTCKN);
                if (healthPolicyInfo != null)
                {
                    var _data = healthPolicyInfo.HealthPolicyInfo.Value[0];

                    policyDetail.PoliceBilgileri = new cHealth_PolicyDetailData
                    {
                        AcenteAd = _data.ACENTE_AD,
                        AcenteIletisimBilgileri = _data.ACENTE_ILETISIM,
                        AcenteNo = _data.ACENTE_NO.HasValue ? _data.ACENTE_NO.Value.ToString() : "",
                        OdeyenAdSoyad = _data.ODEYEN_AD,
                        OdeyenTCKN = _data.ODEYEN_KIMLIK,
                        PoliceNo = _data.POLICE_NO.HasValue ? _data.POLICE_NO.Value.ToString() : "",
                        PoliceSure = _data.POLICE_SURESI,
                        SigortaliAdSoyad = "",
                        SigortaliMusteriNo = "",
                        SigortaliTCKN = "",
                        TanzimTarih = _data.POLICE_BASLAMA_TARIH.HasValue ? _data.POLICE_BASLAMA_TARIH.Value.ToShortDateString() : "",
                        TarifeAd = _data.TARIFE_ADI,
                        TarifeKod = _data.TARIFE_KOD
                    };

                    if (_data.P_TAB_SAGLIK_MUSTERI_BILGI.Value != null && _data.P_TAB_SAGLIK_MUSTERI_BILGI.Value.Length > 0)
                    {
                        policyDetail.Sigortalilar = new List<cHealth_Member>();
                        foreach (var item in _data.P_TAB_SAGLIK_MUSTERI_BILGI.Value)
                        {
                            policyDetail.Sigortalilar.Add(new cHealth_Member
                            {
                                AdSoyad = item.SIGORTALI_AD_SOYAD,
                                TCKN = item.SIGORTALI_KIMLIK_NO
                            });

                            if (item.SIGORTALI_KIMLIK_NO == strTCKN)
                            {
                                policyDetail.PoliceBilgileri.SigortaliAdSoyad = item.SIGORTALI_AD_SOYAD;
                                policyDetail.PoliceBilgileri.SigortaliMusteriNo = item.SIGORTALI_MUSTERI_NO.HasValue ? item.SIGORTALI_MUSTERI_NO.Value.ToString() : "";
                                policyDetail.PoliceBilgileri.SigortaliTCKN = item.SIGORTALI_KIMLIK_NO;
                            }
                        }
                    }
                }

                policyDetail.UrunAdi = Sessions.TitleName;
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(policyDetail);
        }
        #endregion

        #region PaymentInfo
        [UserFilter]
        public ActionResult PaymentInfo()
        {
            cHealth_PaymentInfo paymentInfo = new cHealth_PaymentInfo();
            string strPage = "Health-PaymentInfo";

            try
            {
                serviceMethods = new cService_Methods(strPage);

                var healthPolicyInfo = serviceMethods.GetHealthPolicyInfo(QueryStrings.policy, Sessions.CurrentUser.strTCKN);
                if (healthPolicyInfo != null)
                {
                    var _data = healthPolicyInfo.HealthPolicyInfo.Value[0];

                    paymentInfo.OdemeBilgileri = new cHealth_PaymentInfo_OdemeBilgileri
                    {
                        NetPrimTutari = Utilities.Price(_data.NET_PRIM_TUTAR.HasValue ? _data.NET_PRIM_TUTAR.Value : 0),
                        OdemeAraci = _data.ODEME_ARACI,
                        OdemeTuru = _data.ODEME_TUR
                    };
                }

                KatilimEmeklilikInternetSube.WsOperations.CollectingDetail[] collectingDetail = null;

                var collectingDetailInfo = serviceMethods.GetCollectingDetailInfo(QueryStrings.policy, ref collectingDetail);
                if (collectingDetailInfo)
                {
                    paymentInfo.Tahsilat_Makbuzu = new List<cHealth_PaymentInfo_TahsilatMakbuzu>();

                    foreach (var item in collectingDetail)
                    {
                        paymentInfo.Tahsilat_Makbuzu.Add(new cHealth_PaymentInfo_TahsilatMakbuzu
                        {
                            KesintiTutari = Utilities.Price(item.MasrafTutar.HasValue ? item.MasrafTutar.Value : 0),
                            ReferansNo = item.ReferansNo.ToString(),
                            OdemeTarihi = item.TahsilTarih.HasValue ? item.TahsilTarih.Value.ToShortDateString() : "",
                            DtOdemeTarihi = item.TahsilTarih.HasValue ? item.TahsilTarih.Value : new DateTime(),
                            OdemeTutari = Utilities.Price(item.TutarToplam.HasValue ? item.TutarToplam.Value : 0),
                            VadeTarihi = item.Vade.HasValue ? item.Vade.Value.ToShortDateString() : "",
                            VadeTipi = item.TaksitTipAck
                        });
                    }

                    paymentInfo.Tahsilat_Makbuzu = paymentInfo.Tahsilat_Makbuzu.OrderByDescending(o => o.DtOdemeTarihi).ToList();
                }

                paymentInfo.UrunAdi = Sessions.TitleName;
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(paymentInfo);
        }
        #endregion

        #region PaymentInfoPdf
        [UserFilter]
        public EmptyResult PaymentInfoPdf()
        {
            string strPage = "Health-PaymentInfoPdf";

            try
            {
                KeyValuePair<string, string>[] parameters = new KeyValuePair<string, string>[]
                {
                    new KeyValuePair<string, string>("P_REFERANS_NO", QueryStrings.ReferansNo)
                };

                serviceMethods = new cService_Methods(strPage);

                string strMsg = "";

                var report = serviceMethods.GetReport(parameters, KatilimEmeklilikInternetSube.WsReport.ReportKey.TahsilatMakbuzu, ref strMsg);
                if (report != null)
                {
                    Utilities.DownloadFile(report.ReportPdf, "TahsilatMakbuzu.pdf", "application/pdf");
                }
                else
                {
                    if (!string.IsNullOrEmpty(strMsg))
                    {
                        Sessions.Message = Utilities.Msg(Utilities.MsgType.warning, strMsg);
                    }

                    PageSettings pageSettings = new PageSettings();
                    Response.Redirect("/" + pageSettings.UrlRoot + "/odeme-bilgileri?policy=" + QueryStrings.policyRequest, false);
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);

                PageSettings pageSettings = new PageSettings();
                Response.Redirect("/" + pageSettings.UrlRoot + "/odeme-bilgileri?policy=" + QueryStrings.policyRequest, false);
            }

            return new EmptyResult();
        }
        #endregion

        #region ContractPdf
        [UserFilter]
        public EmptyResult ContractPdf()
        {
            string strPage = "Health-ContractPdf";
            try
            {
                KeyValuePair<string, string>[] parameters = new KeyValuePair<string, string>[]
                {
                    new KeyValuePair<string, string>("P_POLICE_NO", QueryStrings.policy.ToString()),
                    new KeyValuePair<string, string>("P_ZEYIL_NO", "0")
                };

                serviceMethods = new cService_Methods(strPage);

                string strMsg = "";

                var report = serviceMethods.GetReport(parameters, KatilimEmeklilikInternetSube.WsReport.ReportKey.PoliceBasim, ref strMsg);
                if (report != null)
                {
                    Utilities.DownloadFile(report.ReportPdf, "Police-" + QueryStrings.policy + ".pdf", "application/pdf");
                }
                else
                {
                    if (!string.IsNullOrEmpty(strMsg))
                    {
                        Sessions.Message = Utilities.Msg(Utilities.MsgType.warning, strMsg);
                    }

                    PageSettings pageSettings = new PageSettings();
                    Response.Redirect("/" + pageSettings.UrlRoot, false);
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);

                PageSettings pageSettings = new PageSettings();
                Response.Redirect("/" + pageSettings.UrlRoot, false);
            }

            return new EmptyResult();
        }
        #endregion

        #region ContractedInstitution

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult ContractedInstitution(string city = "", string district = "", string facilityName = "", string strPost = "")
        {
            cHealth_ContractedInstitution contractedInstitution = new cHealth_ContractedInstitution();
            string strPage = "Health-ContractedInstitution";

            try
            {
                ViewBag.city = city;
                ViewBag.district = district;
                ViewBag.facilityName = facilityName;

                List<cHealth_ContractedInstitution_CityData> _data = GetKetValuePair(TypeKey.IlCombo, "");
                if (_data.Count > 0)
                {
                    contractedInstitution.cityData = _data;

                    if (!string.IsNullOrEmpty(city))
                    {
                        _data = GetKetValuePair(TypeKey.IlceCombo, city);
                        if (_data.Count > 0)
                        {
                            contractedInstitution.districtData = _data;
                        }
                    }
                }

                if (strPost == "1")
                {
                    var healthFacilitiesInfo = serviceMethods.GetHealthFacilitiesInfo(facilityName, city, district);
                    if (healthFacilitiesInfo != null)
                    {
                        contractedInstitution.tableData = new List<cHealth_ContractedInstitution_TableData>();

                        foreach (var item in healthFacilitiesInfo)
                        {
                            contractedInstitution.tableData.Add(new cHealth_ContractedInstitution_TableData
                            {
                                Ad = item.KURUM_AD,
                                Adres = item.KURUM_ADRES,
                                Telefon = PhoneSet(item.KURUM_TELEFON_NO)
                            });
                        }
                    }
                }

                contractedInstitution.UrunAdi = Sessions.TitleName;
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, strPage);
            }

            return View(contractedInstitution);
        }

        #region PhoneSet
        private string PhoneSet(string strPhone)
        {
            if (!string.IsNullOrEmpty(strPhone))
            {
                if (strPhone.Length == 10 || strPhone.Length == 11)
                {
                    if (strPhone.Length == 10)
                    {
                        strPhone = "0" + strPhone;
                    }

                    strPhone = strPhone.Substring(0, 1) + " (" + strPhone.Substring(1, 3) + ") " + strPhone.Substring(4, 3) + " " + strPhone.Substring(7, 2) + " " + strPhone.Substring(9, 2);
                }
            }

            return strPhone;
        }
        #endregion

        #endregion

        #region Set
        [UserFilter]
        [HttpPost]
        public ActionResult ContractedInstitution()
        {
            return ContractedInstitution(Request.Form["city"], Request.Form["district"], Request.Form["facilityName"], "1");
        }
        #endregion

        #endregion

        #region GetDistrict
        public string GetDistrict(string city)
        {
            List<cHealth_ContractedInstitution_CityData> _data = GetKetValuePair(TypeKey.IlceCombo, city);

            JavaScriptSerializer jss = new JavaScriptSerializer();
            return jss.Serialize(_data);
        }
        #endregion

        #region GetKetValuePair
        private List<cHealth_ContractedInstitution_CityData> GetKetValuePair(WsOperations.TypeKey typeKey, string city)
        {
            List<cHealth_ContractedInstitution_CityData> _data = new List<cHealth_ContractedInstitution_CityData>();

            serviceMethods = new cService_Methods("Health-ContractedInstitution");

            ListSource1[] listSource = null;
            var cities = serviceMethods.GetKeyValuePair(typeKey, city, ref listSource);
            if (cities)
            {
                foreach (var item in listSource)
                {
                    _data.Add(new cHealth_ContractedInstitution_CityData
                    {
                        strID = item.Kod,
                        strName = item.Aciklama
                    });
                }
            }

            return _data;
        }
        #endregion
    }
}
