$(function () {
	$('.modal-opener').on('click', function () {
		if (!$('#perfect-form-modal-overlay').length) {
			$('body').append('<div id="perfect-form-modal-overlay" class="perfect-form-modal-overlay"></div>');
		}

		$('#perfect-form-modal-overlay').on('click', function () {
			$('#perfect-form-modal-overlay').fadeOut();
			$('.perfect-form-modal').fadeOut();
		});

		form = $($(this).attr('href'));
		$('#perfect-form-modal-overlay').fadeIn();
		form.css('top', '50%').css('left', '50%').css('margin-top', -form.outerHeight() / 2).css('margin-left', -form.outerWidth() / 2).fadeIn();

		return false;
	});

	$('.modal-closer').on('click', function () {
		$('#perfect-form-modal-overlay').fadeOut();
		$('.perfect-form-modal').fadeOut();

		return false;
	});
});