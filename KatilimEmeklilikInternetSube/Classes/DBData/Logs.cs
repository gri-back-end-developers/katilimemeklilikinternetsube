﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KatilimEmeklilikInternetSube.Models;
using KatilimEmeklilikInternetSube.Classes.LiveData;

namespace KatilimEmeklilikInternetSube.Classes.DBData
{
    public class Logs
    {
        public static void Exception(string strEx, int intLine, string strPage)
        {
            long intCustomerNo = 0;
            try
            {
                intCustomerNo = Sessions.CurrentUser.intCustomerNo;
            }
            catch {}

            try
            {
                using (DBEntities db = new DBEntities())
                {
                    db.tblExceptions.Add(new tblExceptions()
                    {
                        dtRegisterDate = DateTime.Now,
                        intLine = intLine,
                        strException = strEx,
                        strPage = strPage,
                        intCustomerNo = intCustomerNo
                    });
                    db.SaveChanges();
                }
            }
            catch { }
        }
    }
}