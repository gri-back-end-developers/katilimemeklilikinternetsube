#region Directives 
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Script.Serialization;
#endregion

namespace KatilimEmeklilikInternetSube.Classes.Tools
{
    public class Cookies
    {
        #region SessionKey
        public static string SessionKey
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["SessionKey"].Value;
                    if (objValue == null)
                    {
                        return "";
                    }
                    return objValue.ToString();
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("SessionKey");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion

        #region CookieSss
        public static string CookieSss
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["CookieSss"].Value;
                    if (objValue == null)
                    {
                        return "";
                    }
                    return objValue.ToString();
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("CookieSss");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddMinutes(int.Parse(ConfigurationManager.AppSettings["TimeOut"]));
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion
    }
}