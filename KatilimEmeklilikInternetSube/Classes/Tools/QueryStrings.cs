#region Directives
using System;
using System.Collections.Generic;
using System.Web;
#endregion

namespace KatilimEmeklilikInternetSube.Classes.Tools
{
    public class QueryStrings
    {
        #region policy
        public static long policy
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["policy"];
                    if (value == null)
                    {
                        value = 0;
                    }
                    return Utilities.NullFixLong(cCrypto.DecryptDES(value.ToString()));
                }
                catch
                {
                    return 0;
                }
            }
        }

        public static string policyRequest
        {
            get
            {
                try
                {
                    return HttpContext.Current.Request.QueryString["policy"];
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region ReferansNo
        public static string ReferansNo
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["refno"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return cCrypto.DecryptDES(value.ToString());
                }
                catch
                {
                    return "";
                }
            }
        }

        public static string ReferansNoRequest
        {
            get
            {
                try
                {
                    return HttpContext.Current.Request.QueryString["refno"];
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region ReportID
        public static int ReportID
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["reportid"];
                    if (value == null)
                    {
                        value = "0";
                    }
                    return Utilities.NullFixInt(value.ToString());
                }
                catch
                {
                    return 0;
                }
            }
        }

        public static string ReportIDRequest
        {
            get
            {
                try
                {
                    return HttpContext.Current.Request.QueryString["reportid"];
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion

        #region CustomerNo
        public static string CustomerNo
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["cno"];
                    if (value == null)
                    {
                        value = "";
                    }
                    return cCrypto.DecryptDES(value.ToString());
                }
                catch
                {
                    return "";
                }
            }
        }

        public static string CustomerNoRequest
        {
            get
            {
                try
                {
                    return HttpContext.Current.Request.QueryString["cno"];
                }
                catch
                {
                    return "";
                }
            }
        }
        #endregion 
    }
}