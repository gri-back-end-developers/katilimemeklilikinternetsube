﻿using KatilimEmeklilikInternetSube.Classes.LiveData;
using KatilimEmeklilikInternetSube.Classes.Tools;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace KatilimEmeklilikInternetSube.Classes
{
    public class PageSettings
    {
        public cEnums.PageType RouteName
        {
            get
            {
                var route = RouteTable.Routes.OfType<Route>().Where(w => w.DataTokens != null).ToList();

                var resultRoute = route.FirstOrDefault(x => x.Url == HttpContext.Current.Request.Url.AbsolutePath.Remove(0, 1));
                if (resultRoute != null)
                {
                    if (resultRoute.DataTokens["RouteName"] != null)
                    {
                        switch (resultRoute.DataTokens["RouteName"].ToString())
                        {
                            case "bireysel":
                                return cEnums.PageType.Bireysel;
                            case "hayat":
                                return cEnums.PageType.Hayat;
                            case "ferdi":
                                return cEnums.PageType.Ferdi;
                            case "otomatik":
                                return cEnums.PageType.OtomatikKatilim;
                            case "saglik":
                                return cEnums.PageType.Saglik;
                        }
                    }
                }

                return cEnums.PageType.DigerSayfalar;
            }
        }

        public string Title
        {
            get
            {
                var routeName = RouteName;
                switch (routeName)
                {
                    case cEnums.PageType.Ferdi:
                        return ConfigurationManager.AppSettings["FerdiTitle"];
                    case cEnums.PageType.Hayat:
                        return ConfigurationManager.AppSettings["HayatTitle"];
                    case cEnums.PageType.Bireysel:
                        return ConfigurationManager.AppSettings["BireyselTitle"];
                    case cEnums.PageType.OtomatikKatilim:
                        return ConfigurationManager.AppSettings["OtomatikKatilimTitle"];
                    case cEnums.PageType.Saglik:
                        return ConfigurationManager.AppSettings["SaglikTitle"];
                }

                return "";
            }
        }

        public string Type
        {
            get
            {
                var routeName = RouteName;
                switch (routeName)
                {
                    case cEnums.PageType.Ferdi:
                    case cEnums.PageType.Hayat:
                    case cEnums.PageType.Saglik:
                        return "Poliçe";
                    case cEnums.PageType.Bireysel:
                    case cEnums.PageType.OtomatikKatilim:
                        return "Sözleşme";
                }

                return "";
            }
        }

        public string UrlRoot
        {
            get
            {
                var routeName = RouteName;
                switch (routeName)
                {
                    case cEnums.PageType.Ferdi:
                        return Utilities.Friendly(ConfigurationManager.AppSettings["FerdiTitle"]);
                    case cEnums.PageType.Hayat:
                        return Utilities.Friendly(ConfigurationManager.AppSettings["HayatTitle"]);
                    case cEnums.PageType.Bireysel:
                        return Utilities.Friendly(ConfigurationManager.AppSettings["BireyselTitle"]);
                    case cEnums.PageType.OtomatikKatilim:
                        return Utilities.Friendly(ConfigurationManager.AppSettings["OtomatikKatilimTitle"]);
                    case cEnums.PageType.Saglik:
                        return Utilities.Friendly(ConfigurationManager.AppSettings["SaglikTitle"]);
                }

                return "";
            }
        }
    }
}