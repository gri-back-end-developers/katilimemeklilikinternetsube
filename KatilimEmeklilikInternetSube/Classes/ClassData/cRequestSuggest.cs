﻿using System.Collections.Generic;

namespace KatilimEmeklilikInternetSube.Classes.ClassData
{
    public class cRequestSuggest
    {
        public List<cRequestSuggestPolicy> Policeler { get; set; }
        public string Eposta { get; set; }
    }

    public class cRequestSuggestPolicy
    {
        public string Police { get; set; }
    }
}