﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.PolicyDetail
{
    public class cPolicyDetail
    {
        public PageSettings PageSettings { get; set; }
        public cPoliceBilgileri PoliceBilgileri { get; set; }
        public List<cTeminatBilgileri> TeminatBilgileri { get; set; }
        public List<cVefatDurumundaLehtarlar> VefatDurumundaLehtarlar { get; set; }
        public cOdemeDetayBilgileri OdemeDetayBilgileri { get; set; }
        public cAraciBilgileri AraciBilgileri { get; set; }

        public cPolicyDetail()
        {
            PageSettings = new PageSettings();
        }
    }    
}