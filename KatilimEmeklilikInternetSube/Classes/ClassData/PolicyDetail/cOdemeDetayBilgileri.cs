﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.PolicyDetail
{
    public class cOdemeDetayBilgileri
    {
        public string ToplamOdemeTutari { get; set; }
        public string IdariMasrafKesintisi { get; set; }
        public string KomisyonKesintisi { get; set; }
        public string KesintilerToplami { get; set; }
        public string VergiTutar { get; set; }
    }
}