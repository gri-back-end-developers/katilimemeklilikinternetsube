﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.PolicyDetail
{
    public class cPoliceBilgileri
    {
        public string PoliceNo_SertifikaNo { get; set; }
        public string SigortaliKatilimEmeklilikMusteriNo { get; set; }
        public string Sigortali { get; set; }
        public string SigortaliTCKimlikNo { get; set; }
        public string Odeyen { get; set; }
        public string OdeyenTCKimlikNo { get; set; }
        public string UrunAdi { get; set; }
        public string BaslangicTarihi { get; set; }
        public string BitisTarihi { get; set; }
        public string OdemePeriyodu { get; set; }
        public string ToplamPrimTutari { get; set; }
        public string ToplamKalanPrimTutari { get; set; }
        public string OdemeAraci { get; set; } 
    }
}