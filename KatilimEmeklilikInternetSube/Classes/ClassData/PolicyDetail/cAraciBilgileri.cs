﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.PolicyDetail
{
    public class cAraciBilgileri
    {
        public string DagitimKanali { get; set; }
        public string SicilNo { get; set; }
        public string AdSoyad { get; set; }
        public string Telefon { get; set; }
        public string EPosta { get; set; }
    }
}