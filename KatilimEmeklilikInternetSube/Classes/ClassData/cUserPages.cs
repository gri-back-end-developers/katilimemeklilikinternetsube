﻿using System;
using System.Collections.Generic;
using System.Linq; 

namespace KatilimEmeklilikInternetSube.Classes.ClassData
{
    public class cUserPages
    {
        public string strPath { get; set; }
        public string strName { get; set; }
        public string strClass { get; set; }
        public bool bolIsActive { get; set; }
    }
}