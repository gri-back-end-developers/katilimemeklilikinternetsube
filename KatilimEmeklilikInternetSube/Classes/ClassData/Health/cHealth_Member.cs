﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Health
{
    public class cHealth_Member
    {
        public string AdSoyad { get; set; }
        public string Yakinlik { get; set; }
        public string MusteriNo { get; set; }
        public string TCKN { get; set; } 
    }
}