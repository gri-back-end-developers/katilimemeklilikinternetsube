﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Health.PaymentInfo
{
    public class cHealth_PaymentInfo
    {
        public PageSettings PageSettings { get; set; }
        public cHealth_PaymentInfo_OdemeBilgileri OdemeBilgileri { get; set; }
        public List<cHealth_PaymentInfo_TahsilatMakbuzu> Tahsilat_Makbuzu { get; set; }
        public string UrunAdi { get; set; }

        public cHealth_PaymentInfo()
        {
            PageSettings = new PageSettings();
        }
    } 
}