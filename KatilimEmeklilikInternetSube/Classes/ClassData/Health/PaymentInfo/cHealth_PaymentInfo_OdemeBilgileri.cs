﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Health.PaymentInfo
{
    public class cHealth_PaymentInfo_OdemeBilgileri
    {
        public string OdemeAraci { get; set; }
        public string NetPrimTutari { get; set; }
        public string OdemeTuru { get; set; } 
    }
}