﻿using System;
using System.Collections.Generic;
using System.Linq; 

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Health
{
    public class cHealth_PolicyInfo
    {
        public PageSettings PageSettings { get; set; }
        public List<cHealth_PolicyInfoData> PoliceBilgileri { get; set; }
        public string UrunAdi { get; set; } 

        public cHealth_PolicyInfo()
        {
            PageSettings = new PageSettings();
        }
    }


    public class cHealth_PolicyInfoData
    {
        public string TeminatAdi { get; set; }
        public string LimitTipi { get; set; }
        public string Limitiniz { get; set; }
        public string Katilim { get; set; }
        public string Kullanilan { get; set; }
        public string Kalan { get; set; }
    }
}