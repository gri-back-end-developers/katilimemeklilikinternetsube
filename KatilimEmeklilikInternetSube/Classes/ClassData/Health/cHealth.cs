﻿using System;
using System.Collections.Generic;
using System.Linq; 

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Health
{
    public class cHealth
    {
        public PageSettings PageSettings { get; set; }
        public List<cPolicies> Policies { get; set; }
        public cHealth_GeneralInfo GeneralInfo { get; set; }
        public List<cHealth_Member> Members { get; set; } 
        public bool IsHaveData { get; set; }

        public cHealth()
        {
            PageSettings = new PageSettings();
            Policies = new List<cPolicies>();
            GeneralInfo = new cHealth_GeneralInfo
            {
                BitisTarih = "-",
                BaslangicTarih = "-",
                TarifeAd = PageSettings.Title
            };
            IsHaveData = false;
        }
    }  
}