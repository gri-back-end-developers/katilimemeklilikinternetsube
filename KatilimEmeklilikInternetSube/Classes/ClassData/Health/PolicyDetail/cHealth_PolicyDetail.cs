﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Health.PolicyDetail
{
    public class cHealth_PolicyDetail
    {
        public PageSettings PageSettings { get; set; }
        public cHealth_PolicyDetailData PoliceBilgileri { get; set; }
        public List<cHealth_Member> Sigortalilar { get; set; }
        public string UrunAdi { get; set; } 

        public cHealth_PolicyDetail()
        {
            PageSettings = new PageSettings();
        }
    } 
}