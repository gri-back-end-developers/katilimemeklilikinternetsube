﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Health.PolicyDetail
{
    public class cHealth_PolicyDetailData
    {
        public string PoliceNo { get; set; }
        public string TarifeKod { get; set; }
        public string TarifeAd { get; set; }
        public string TanzimTarih { get; set; }
        public string PoliceSure { get; set; }
        public string AcenteNo { get; set; }
        public string AcenteAd { get; set; }
        public string AcenteIletisimBilgileri { get; set; }
        public string SigortaliMusteriNo { get; set; }
        public string SigortaliAdSoyad { get; set; }
        public string SigortaliTCKN { get; set; }
        public string OdeyenAdSoyad { get; set; }
        public string OdeyenTCKN { get; set; }
    }
}