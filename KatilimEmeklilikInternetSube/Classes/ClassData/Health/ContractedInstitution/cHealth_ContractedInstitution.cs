﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Health.ContractedInstitution
{
    public class cHealth_ContractedInstitution
    {
        public PageSettings PageSettings { get; set; } 
        public string UrunAdi { get; set; }
        public List<cHealth_ContractedInstitution_CityData> cityData;
        public List<cHealth_ContractedInstitution_CityData> districtData;
        public List<cHealth_ContractedInstitution_TableData> tableData;

        public cHealth_ContractedInstitution()
        {
            PageSettings = new PageSettings();
        }
    } 
}