﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Health.ContractedInstitution
{
    public class cHealth_ContractedInstitution_TableData
    {
        public string Ad { get; set; }
        public string Adres { get; set; } 
        public string Telefon { get; set; }
    }
}