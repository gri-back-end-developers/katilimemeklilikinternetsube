﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Health.ContractedInstitution
{
    public class cHealth_ContractedInstitution_CityData
    {
        public string strName { get; set; }
        public string strID { get; set; }
    }
}