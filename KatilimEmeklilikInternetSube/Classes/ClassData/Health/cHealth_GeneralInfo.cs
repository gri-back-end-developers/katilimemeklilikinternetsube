﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Health
{
    public class cHealth_GeneralInfo
    {
        public string BaslangicTarih { get; set; }
        public string BitisTarih { get; set; }
        public string TarifeAd { get; set; } 
    }
}