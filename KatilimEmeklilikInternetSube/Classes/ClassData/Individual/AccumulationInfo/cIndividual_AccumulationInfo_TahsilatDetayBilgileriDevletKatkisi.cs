﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.AccumulationInfo
{
    public class cIndividual_AccumulationInfo_TahsilatDetayBilgileriDevletKatkisi
    {
        public string DevletKatkisiBirikim { get; set; }
        public string DevletKatkisiTahsilati { get; set; }
        public string DevletKatkisiGetiriTutari { get; set; }
        public string DevletKatkisiHakedisOrani_Tutar { get; set; }
        public string StopajOrani_StopajTutari { get; set; }
        public string TaahhutBaslangicDKAnaTutar { get; set; }
        public string TaahhutDKAnaTutarToplami { get; set; }
        public string DevletKatkisiHakEdisOraniTutari { get; set; }
    }
}