﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.AccumulationInfo
{
    public class cIndividual_AccumulationInfo
    {
        public PageSettings PageSettings { get; set; }
        public cIndividual_AccumulationInfo_SozlesmeBilgileri SozlesmeBilgileri { get; set; }
        public cIndividual_AccumulationInfo_DetayBirikimBilgileri DetayBirikimBilgileri { get; set; }
        public cIndividual_AccumulationInfo_AktarimBilgileri AktarimBilgileri { get; set; }
        public cIndividual_AccumulationInfo_TahsilatDetayBilgileriKatilimciKatkisi TahsilatDetayBilgileri_KatilimciKatkisi { get; set; }
        public cIndividual_AccumulationInfo_TahsilatDetayBilgileriDevletKatkisi TahsilatDetayBilgileri_DevletKatkisi { get; set; }

        public cIndividual_AccumulationInfo()
        {
            PageSettings = new PageSettings();
        }
    }    
}