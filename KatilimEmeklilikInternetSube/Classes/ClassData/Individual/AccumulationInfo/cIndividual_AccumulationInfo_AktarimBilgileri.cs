﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.AccumulationInfo
{
    public class cIndividual_AccumulationInfo_AktarimBilgileri
    {
        public string AktarimAnaParaKK { get; set; }
        public string AktarimGetiriKK { get; set; }
        public string AktarimToplamiKK { get; set; }
        public string AktarimAnaParaDK { get; set; }
        public string AktarimGetiriDK { get; set; }
        public string AktarimToplamiDK { get; set; }
    }
}