﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.AccumulationInfo
{
    public class cIndividual_AccumulationInfo_DetayBirikimBilgileri
    {
        public List<cAccumulationInfo_DetayBirikimBilgileri_Data> Data { get; set; }
        public string ToplamBirikim_DevletKatkisiHaric { get; set; }
        public string ToplamBirikim_DevletKatkisiDahil { get; set; }
        public string KatilimciKatkisiIcVerimOrani { get; set; }
        public string DevletciKatkisiIcVerimOrani { get; set; }

        public cIndividual_AccumulationInfo_DetayBirikimBilgileri()
        {
            Data = new List<cAccumulationInfo_DetayBirikimBilgileri_Data>();
        }
    }

    public class cAccumulationInfo_DetayBirikimBilgileri_Data
    {
        public string FonAd { get; set; }
        public string FonAdedi { get; set; }
        public string Oran { get; set; }
        public string FonTipi { get; set; }
        public string FonFiyat { get; set; }
        public string Tutar { get; set; }
        public string GuncelOran { get; set; }
    }
}