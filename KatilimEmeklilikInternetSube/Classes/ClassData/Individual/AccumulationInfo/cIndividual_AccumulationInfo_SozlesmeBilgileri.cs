﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.AccumulationInfo
{
    public class cIndividual_AccumulationInfo_SozlesmeBilgileri
    {
        public string SozlesmeNo { get; set; }
        public string KTLMMusteriNo { get; set; }
        public string Katilimci { get; set; }
        public string UrunAdi { get; set; }
    }
}