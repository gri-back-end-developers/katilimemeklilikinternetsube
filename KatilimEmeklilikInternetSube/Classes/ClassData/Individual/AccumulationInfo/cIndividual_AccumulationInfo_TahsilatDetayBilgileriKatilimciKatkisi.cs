﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.AccumulationInfo
{
    public class cIndividual_AccumulationInfo_TahsilatDetayBilgileriKatilimciKatkisi
    {
        public string ProvizyondaBekleyenTutar { get; set; }
        public string YatirilanToplamTutar_GirisAidatiHaric { get; set; }
        public string YatirimaYonlendirilenTutar { get; set; }
        public string KatilimciKatkisiGetiriTutari { get; set; }
        public string StopajOrani_StopajTutari { get; set; }
        public string YonetimGideriKesintiTutari { get; set; }
        public string GirisAidati { get; set; }
        public string ToplamBirikim_DevletKatkisiHaric { get; set; }
        public string EkKatkiPayiTutari { get; set; }
        public string BKTutar { get; set; }
    }
}