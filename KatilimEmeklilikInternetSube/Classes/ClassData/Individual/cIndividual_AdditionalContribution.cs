﻿using System;
using System.Collections.Generic;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual
{
    public class cIndividual_AdditionalContribution
    {
        public PageSettings PageSettings { get; set; }
        public long Policy { get; set; }
        public string UrunAdi { get; set; }
        public List<KatilimEmeklilikInternetSube.WsOperations.TenderFinancialInfo> FinansBilgileri { get; set; }

        public cIndividual_AdditionalContribution()
        {
            PageSettings = new PageSettings();
        }
    } 
}