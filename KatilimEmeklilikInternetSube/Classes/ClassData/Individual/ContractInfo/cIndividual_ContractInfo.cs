﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.ContractInfo
{
    public class cIndividual_ContractInfo
    {
        public PageSettings PageSettings { get; set; }
        public cIndividual_ContractInfo_SozlesmeBilgileri SozlesmeBilgileri { get; set; }
        public List<cIndividual_ContractInfo_LehtarBilgileri> LehtarBilgileri { get; set; }
        public List<cIndividual_ContractInfo_Fonlar> TercihEdilenFonDagilimi { get; set; }
        public List<cIndividual_ContractInfo_Fonlar> DKFonlari { get; set; }

        public cIndividual_ContractInfo()
        {
            PageSettings = new PageSettings();
        }
    }   
}