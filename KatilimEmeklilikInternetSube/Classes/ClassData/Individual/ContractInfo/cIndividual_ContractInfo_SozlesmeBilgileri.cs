﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.ContractInfo
{
    public class cIndividual_ContractInfo_SozlesmeBilgileri
    {
        public string SozlesmeNo { get; set; }
        public string KTLMMusteriNo { get; set; }
        public string Katilimci { get; set; }
        public string KatilimciTCKN { get; set; }
        public string UrunAdi { get; set; }
        public string IlkGirisTarihi { get; set; }
        public string YururlukTarihi { get; set; }
        public string EmeklilikTarihi { get; set; }
        public string KatkiPayiOdemeDonemi { get; set; }
        public string DuzenliKatkiPayiTutari { get; set; }
        public string OdemeAraci { get; set; }
        public string GirisAidatiTutari { get; set; }
        public string ErtelenmisGirisAidatiTutari { get; set; }
        public string YonetimGiderKesintisi { get; set; }
        public string BKTutar { get; set; }
    }
}