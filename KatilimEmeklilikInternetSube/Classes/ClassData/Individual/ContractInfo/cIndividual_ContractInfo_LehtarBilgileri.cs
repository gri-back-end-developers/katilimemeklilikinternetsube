﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.ContractInfo
{
    public class cIndividual_ContractInfo_LehtarBilgileri
    {
        public string AdSoyad { get; set; }
        public string Oran { get; set; }
        public decimal dcOran { get; set; }
    }
}