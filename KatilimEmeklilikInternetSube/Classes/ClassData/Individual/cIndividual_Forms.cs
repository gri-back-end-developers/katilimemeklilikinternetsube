﻿using System.Collections.Generic;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual
{
    public class cIndividual_Forms
    {
        public PageSettings PageSettings { get; set; }
        public string UrunAdi { get; set; }
        public List<KatilimEmeklilikInternetSube.Models.tblCmsForms> CmsForms { get; set; }

        public cIndividual_Forms()
        {
            PageSettings = new PageSettings();
        }
    }
}