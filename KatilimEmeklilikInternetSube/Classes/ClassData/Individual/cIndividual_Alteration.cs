﻿using KatilimEmeklilikInternetSube.Classes.LiveData;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual
{
    public class cIndividual_Alteration
    {
        public PageSettings PageSettings { get; set; }
        public string UrunAdi { get; set; }
        public cEnums.OkBireyselDevam OkBireyselDevam { get; set; }
        public bool KpOran { get; set; }
        public bool EkKatkiPayi { get; set; }
        public bool IGES { get; set; }
        public string OdemeAraciAck { get; set; }

        public cIndividual_Alteration()
        {
            PageSettings = new PageSettings();
            OkBireyselDevam = cEnums.OkBireyselDevam.KP;
            OdemeAraciAck = "";
            IGES = true;
        }
    }
}