﻿using System;
using System.Collections.Generic;
using System.Linq; 

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.FundDistributionChange
{
    public class cIndividual_FundDistributionChange_RrpQuestions
    {
        public long? ID { get; set; }
        public string Soru { get; set; }
        public List<cIndividual_FundDistributionChange_RrpAnswer> Cevaplar { get; set; }
    }
}