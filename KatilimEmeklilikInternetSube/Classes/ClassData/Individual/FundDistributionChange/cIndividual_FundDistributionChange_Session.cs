﻿using System;
using System.Collections.Generic;
using System.Linq; 

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.FundDistributionChange
{
    public class cIndividual_FundDistributionChange_Session
    {
        public string strType { get; set; }
        public string strRrpType { get; set; }
        public string strSecim { get; set; }
        public List<cIndividual_FundDistributionChange_RrpUserAnswer> RrpUserAnswer { get; set; }
        public cIndividual_FundDistributionChange_RrpPointResult RrpPointResult { get; set; }
    }
}