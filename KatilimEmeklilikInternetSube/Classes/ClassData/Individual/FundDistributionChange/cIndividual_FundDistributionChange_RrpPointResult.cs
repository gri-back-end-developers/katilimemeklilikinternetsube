﻿using System;
using System.Collections.Generic;
using System.Linq; 

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.FundDistributionChange
{
    public class cIndividual_FundDistributionChange_RrpPointResult
    {
        public string SecimTip { get; set; }
        public string DEGER_ID { get; set; }
        public string DETAY_ID { get; set; }
        public List<cIndividual_FundDistributionChange_Fund> fonlar { get; set; }
    }
}