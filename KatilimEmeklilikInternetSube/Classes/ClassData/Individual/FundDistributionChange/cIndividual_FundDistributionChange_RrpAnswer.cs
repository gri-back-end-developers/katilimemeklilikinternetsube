﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.FundDistributionChange
{
    public class cIndividual_FundDistributionChange_RrpAnswer
    {
        public long? ID { get; set; }
        public string Cevap { get; set; }
        public decimal Puan { get; set; }
    }
}