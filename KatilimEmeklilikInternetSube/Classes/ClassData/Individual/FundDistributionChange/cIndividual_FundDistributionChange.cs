﻿using System;
using System.Collections.Generic;
using System.Linq; 

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.FundDistributionChange
{
    public class cIndividual_FundDistributionChange
    {
        public PageSettings PageSettings { get; set; }
        public List<cIndividual_FundDistributionChange_Fund> MevcutFonlar { get; set; }
        public List<cIndividual_FundDistributionChange_Fund> YeniDagilimFonlar { get; set; }
        public string UrunAdi { get; set; }
        public List<cIndividual_FundDistributionChange_RrpQuestions> RrpQuestions { get; set; }
        public List<cIndividual_FundDistributionChange_RrpPointResult> RrpPointResults { get; set; }
        public bool bolVisible { get; set; }
        public string strMsg { get; set; }
        public int RemainingFundChangeRight { get; set; }

        public cIndividual_FundDistributionChange()
        {
            PageSettings = new PageSettings();
        }
    } 
}