﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.FundDistributionChange
{
    public class cIndividual_FundDistributionChange_RrpUserAnswer
    {
        public string questionId { get; set; }
        public string answerId { get; set; }
        public int point { get; set; }
    }
}