﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.FundDistributionChange
{
    public class cIndividual_FundDistributionChange_Fund
    {
        public string FonAd { get; set; }
        public string FonKod { get; set; }
        public string FonOran { get; set; }
        public string YeniFonOran { get; set; }
    }
}