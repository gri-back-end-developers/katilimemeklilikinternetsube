﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual
{
    public class cIndividual_AccumulationInfoData
    {
        public decimal KatilimciKatkisi { get; set; }
        public decimal YatirilanToplamTutar { get; set; }
        public decimal YatirimGetirisi { get; set; }
        public decimal DevletKatkisi { get; set; }
        public decimal YatirilanDevletKatkisi { get; set; }
        public decimal DevletKatkisiGetirisi { get; set; }
        public decimal ToplamBirikim { get; set; }
        public decimal KatkiPayiTutar { get; set; }
    }
}