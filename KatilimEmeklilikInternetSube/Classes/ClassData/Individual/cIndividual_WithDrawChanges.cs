﻿using System.Collections.Generic;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual
{
    public class cIndividual_WithDrawChanges
    {
        public PageSettings PageSettings { get; set; }
        public List<cIndividual_WithDrawChangesInputs> HesapTip { get; set; }
        public List<cIndividual_WithDrawChangesInputs> Bankalar { get; set; }
        public string UrunAdi { get; set; }

        public cIndividual_WithDrawChanges()
        {
            PageSettings = new PageSettings();
            HesapTip = new List<cIndividual_WithDrawChangesInputs>();
            Bankalar = new List<cIndividual_WithDrawChangesInputs>();
        }
    }

    public class cIndividual_WithDrawChangesInputs
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
}