﻿using System;
using System.Collections.Generic;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual
{
    public class cIndividual_ContributionChanges
    {
        public PageSettings PageSettings { get; set; }
        public string KatkiPayiOdemeDonemi { get; set; }
        public string KatkiPayi { get; set; }
        public string UrunAdi { get; set; }
        public List<DateTime> Tarihler { get; set; }

        public cIndividual_ContributionChanges()
        {
            PageSettings = new PageSettings();
        }
    } 
}