﻿using System.Collections.Generic;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual
{
    public class cMainPage
    {
        public PageSettings PageSettings { get; set; }
        public List<cPieChartData> pieChartData { get; set; }
        public string ToplamBirikim { get; set; } 
        public decimal KatilimciKatkisi { get; set; }
        public decimal YatirilanToplamTutar { get; set; }
        public decimal YatirimGetirisi { get; set; }
        public decimal DevletKatkisi { get; set; }
        public decimal YatirilanDevletKatkisi { get; set; }
        public decimal DevletKatkisiGetirisi { get; set; }
        public string KatılımcıIcVerimOraniDK { get; set; }
        public string KatılımcıIcVerimOraniKK { get; set; }

        public cMainPage()
        {
            PageSettings = new PageSettings();  
            pieChartData = new List<cPieChartData>();   
        } 
    }   
}