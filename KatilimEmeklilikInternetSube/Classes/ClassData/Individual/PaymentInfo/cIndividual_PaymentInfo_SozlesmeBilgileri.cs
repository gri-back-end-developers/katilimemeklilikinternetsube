﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.PaymentInfo
{
    public class cIndividual_PaymentInfo_SozlesmeBilgileri
    {
        public string SozlesmeNo { get; set; }
        public string KTLMMusteriNo { get; set; }
        public string Katilimci { get; set; }
        public string UrunAdi { get; set; }
        public string KrediKarti { get; set; }
    }
}