﻿using System;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.PaymentInfo
{
    public class cIndividual_PaymentInfo_TahsilatMakbuzu
    {
        public string ReferansNo { get; set; }
        public DateTime DtOdemeTarihi { get; set; }
        public string OdemeTarihi { get; set; }
        public string VadeTarihi { get; set; }
        public string VadeTipi { get; set; }
        public string OdemeTutari { get; set; }
        public string KesintiTutari { get; set; }
    }
}