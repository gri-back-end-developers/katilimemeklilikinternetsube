﻿using System;
using System.Collections.Generic;
using System.Linq; 

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual.PaymentInfo
{
    public class cPaymentInfo
    {
        public PageSettings PageSettings { get; set; }
        public cIndividual_PaymentInfo_SozlesmeBilgileri SozlesmeBilgileri { get; set; }
        public List<cIndividual_PaymentInfo_TahsilatMakbuzu> Tahsilat_Makbuzu { get; set; }

        public cPaymentInfo()
        {
            PageSettings = new PageSettings();
        }
    } 
}