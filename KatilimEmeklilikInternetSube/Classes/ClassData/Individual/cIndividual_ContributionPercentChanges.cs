﻿using System;
using System.Collections.Generic;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual
{
    public class cIndividual_ContributionPercentChanges
    {
        public PageSettings PageSettings { get; set; }
        public string CurrentRate { get; set; } 
        public string UrunAdi { get; set; }

        public cIndividual_ContributionPercentChanges()
        {
            PageSettings = new PageSettings();
        }
    } 
}