﻿using System.Collections.Generic;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual
{
    public class cIndividual
    {
        public PageSettings PageSettings { get; set; }
        public List<cIndividual_Policies> Policies { get; set; }
        public cIndividual_PensionGeneralInfo PensionGeneralInfo { get; set; }
        public cIndividual_AccumulationInfoData AccumulationInfo { get; set; } 
        public List<cPieChartData> pieChartData { get; set; }
        public bool IsHaveData { get; set; }
        public List<cIndividual_AlternativeInvestmentTools> AlternativeInvestmentTools { get; set; }

        public cIndividual()
        {
            PageSettings = new PageSettings();
            Policies = new List<cIndividual_Policies>();
            AccumulationInfo = new cIndividual_AccumulationInfoData(); 
            pieChartData = new List<cPieChartData>(); 
            PensionGeneralInfo = new cIndividual_PensionGeneralInfo
            {
                KatkiPayiTutar = "0.00 &#x20BA;",
                BesGirisTarih = "-",
                UrunAd = PageSettings.Title
            };
            IsHaveData = false;
        } 
    }   
}