﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual
{
    public class cIndividual_PensionGeneralInfo
    {
        public string BesGirisTarih { get; set; }
        public string KatkiPayiTutar { get; set; }
        public string UrunAd { get; set; }
        public string OdemeAraciAck { get; set; }
    }
}