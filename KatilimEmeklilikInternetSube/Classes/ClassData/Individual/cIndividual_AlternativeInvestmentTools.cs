﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Individual
{
    public class cIndividual_AlternativeInvestmentTools
    {
        private string _FonKod;

        public string FonKod { set { _FonKod = value; } }
        public decimal Tutar { get; set; }
        public decimal Yuzde { get; set; }
        public cIndividual_AlternativeInvestmentTools_Fund Fon
        {
            get
            { 
                switch (_FonKod)
                {
                    case "-":
                        return new cIndividual_AlternativeInvestmentTools_Fund { Ad = "Mevcut Dağılım", Renk = "#ffbaba" };
                    case "KEA":
                        return new cIndividual_AlternativeInvestmentTools_Fund { Ad = "Katılım Katkı EYF", Renk = "#d6d6d6" };
                    case "KEB":
                        return new cIndividual_AlternativeInvestmentTools_Fund { Ad = "Katılım Standart EYF", Renk = "#fff3ba" };
                    case "KEF":
                        return new cIndividual_AlternativeInvestmentTools_Fund { Ad = "Altın Katılım EYF", Renk = "#d9ffba" };
                    case "KEG":
                        return new cIndividual_AlternativeInvestmentTools_Fund { Ad = "Dengeli Katılım Değişken EYF", Renk = "#badcff" };
                    case "KEH":
                        return new cIndividual_AlternativeInvestmentTools_Fund { Ad = "Katılım Hisse Senedi EYF", Renk = "#bdbaff" };
                    case "KEK":
                        return new cIndividual_AlternativeInvestmentTools_Fund { Ad = "Katılım Değişken Grup EYF", Renk = "#ffe5ba" };
                    case "KES":
                        return new cIndividual_AlternativeInvestmentTools_Fund { Ad = "Agresif Katılım Değişken (Döviz) EYF", Renk = "#c8f8ff" };
                    case "KEY":
                        return new cIndividual_AlternativeInvestmentTools_Fund { Ad = "Katılım Başlangıç EYF", Renk = "#baffdc" };
                    case "KET":
                        return new cIndividual_AlternativeInvestmentTools_Fund { Ad = "OKS Atak Katılım Değişken EYF", Renk = "#bacbff" };
                    case "KEZ":
                        return new cIndividual_AlternativeInvestmentTools_Fund { Ad = "OKS Agresif Katılım Değişken EYF", Renk = "#c7baff" };
                    case "KTZ":
                        return new cIndividual_AlternativeInvestmentTools_Fund { Ad = "OKS Katılım Standart EYF", Renk = "#e5baff" };
                    default:
                        return new cIndividual_AlternativeInvestmentTools_Fund();
                }
            }
        } 
    }

    public class cIndividual_AlternativeInvestmentTools_Fund
    {
        public string Ad { get; set; }
        public string Renk { get; set; } 
    }
}