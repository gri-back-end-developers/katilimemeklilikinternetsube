﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Personal
{
    public class cPersonal_GeneralInfo
    {
        public string GirisTarih { get; set; }
        public string BitisTarih { get; set; }
        public string UrunAd { get; set; } 
    }
}