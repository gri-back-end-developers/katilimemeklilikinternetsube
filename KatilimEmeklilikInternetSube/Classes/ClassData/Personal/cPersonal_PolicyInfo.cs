﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Personal
{
    public class cPersonal_PolicyInfo
    {
        public string TeminatKodu { get; set; }
        public string TeminatAdi { get; set; }
        public string Bedel { get; set; }
        public string Prim { get; set; }
    }
}