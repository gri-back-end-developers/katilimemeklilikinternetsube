﻿using System;
using System.Collections.Generic;
using System.Linq; 

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Personal
{
    public class cPersonal
    {
        public PageSettings PageSettings { get; set; }
        public List<cPolicies> Policies { get; set; }
        public cPersonal_GeneralInfo GeneralInfo { get; set; }
        public List<cPersonal_PolicyInfo> PoliceBilgileri { get; set; }
        public string ToplamPrim { get; set; }
        public bool IsHaveData { get; set; }

        public cPersonal()
        {
            PageSettings = new PageSettings();
            Policies = new List<cPolicies>();
            GeneralInfo = new cPersonal_GeneralInfo
            {
                BitisTarih = "-",
                GirisTarih = "-",
                UrunAd = PageSettings.Title
            };
            ToplamPrim = "0.00";
            IsHaveData = false;
        }
    }   
}