﻿using System;
using System.Collections.Generic;
using System.Linq; 

namespace KatilimEmeklilikInternetSube.Classes.ClassData
{
    public class cSignUp
    { 
        public string strLoginType { get; set; }
        public string strTckn { get; set; }
        public string strPhoneTckn { get; set; }
        public string strCustomerNo { get; set; }
        public string strPhoneCustomer { get; set; }
        public bool bolConfirmForm { get; set; }
        public string strMessage { get; set; }
        public string strLoginID { get; set; }
        public string strScript { get; set; }
        public string strCountdown { get; set; }
    }
}