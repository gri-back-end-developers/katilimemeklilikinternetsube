﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Life
{
    public class cLife_PoliceBilgileri
    {
        public string TeminatKodu { get; set; }
        public string TeminatAdi { get; set; }
        public string Bedel { get; set; }
        public string Prim { get; set; }
    }
}