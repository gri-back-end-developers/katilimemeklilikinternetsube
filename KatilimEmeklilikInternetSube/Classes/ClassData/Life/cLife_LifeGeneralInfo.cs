﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.Life
{
    public class cLife_LifeGeneralInfo
    {
        public string GirisTarih { get; set; }
        public string BitisTarih { get; set; }
        public string UrunAd { get; set; }
    }
}