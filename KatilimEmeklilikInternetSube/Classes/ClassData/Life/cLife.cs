﻿using System;
using System.Collections.Generic;
using System.Linq; 

namespace KatilimEmeklilikInternetSube.Classes.ClassData.Life
{
    public class cLife
    {
        public PageSettings PageSettings { get; set; }
        public List<cPolicies> Policies { get; set; }
        public cLife_LifeGeneralInfo LifeGeneralInfo { get; set; }
        public List<cLife_PoliceBilgileri> PoliceBilgileri { get; set; } 
        public string ToplamPrim { get; set; }
        public bool IsHaveData { get; set; }

        public cLife()
        {
            PageSettings = new PageSettings();
            Policies = new List<cPolicies>();
            LifeGeneralInfo = new cLife_LifeGeneralInfo
            {
                BitisTarih = "-",
                GirisTarih = "-",
                UrunAd = PageSettings.Title
            };
            ToplamPrim = "0.00";
            IsHaveData = false;
        }
    }   
}