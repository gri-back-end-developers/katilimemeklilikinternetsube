﻿using System;
using System.Collections.Generic;
using System.Linq; 

namespace KatilimEmeklilikInternetSube.Classes.ClassData
{
    public class cUser
    {
        public long intCustomerNo { get; set; }
        public string strIP { get; set; }
        public string strNameSurname { get; set; }
        public string strPhone { get; set; }
        public string strEmail { get; set; }
        public string strTCKN { get; set; }
        public string strSessionKey { get; set; }
        public List<cUserPages> pages { get; set; } 
    }
}