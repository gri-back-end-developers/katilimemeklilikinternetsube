﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.AdditionalInformation
{
    public class cAdditionalInformation_İletisimBilgileri
    {
        public string Adres { get; set; }
        public string CepTelefonu { get; set; }
        public string EvTelefonu { get; set; }
        public string IsTelefonu { get; set; }
        public string Eposta { get; set; }
    }
}