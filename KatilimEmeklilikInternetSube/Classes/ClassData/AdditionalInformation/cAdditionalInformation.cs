﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KatilimEmeklilikInternetSube.Classes.ClassData.AdditionalInformation
{
    public class cAdditionalInformation
    {
        public cAdditionalInformation_KatilimciBilgileri KatilimciBilgileri { get; set; }
        public cAdditionalInformation_İletisimBilgileri İletisimBilgileri { get; set; }

        public cAdditionalInformation()
        {
            KatilimciBilgileri = new cAdditionalInformation_KatilimciBilgileri();
            İletisimBilgileri = new cAdditionalInformation_İletisimBilgileri();
        }
    } 
}