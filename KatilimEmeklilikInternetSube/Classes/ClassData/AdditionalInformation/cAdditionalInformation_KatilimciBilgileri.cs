﻿namespace KatilimEmeklilikInternetSube.Classes.ClassData.AdditionalInformation
{
    public class cAdditionalInformation_KatilimciBilgileri
    {
        public string AdSoyad { get; set; }
        public string BabaAdi { get; set; }
        public string AnneAdi { get; set; }
        public string TCKN { get; set; }
        public string DogumTarihi { get; set; }
        public string DogumYeri { get; set; }
        public string Uyruk { get; set; }
        public string VergiNumarasi { get; set; }
        public string MedeniDurum { get; set; }
        public string Cinsiyet { get; set; }
        public string CocukSayisi { get; set; }
        public string EgitimDurumu { get; set; }
    }
}