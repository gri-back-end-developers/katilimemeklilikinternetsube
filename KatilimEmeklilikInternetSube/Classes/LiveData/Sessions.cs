﻿using KatilimEmeklilikInternetSube.Classes.ClassData;
using KatilimEmeklilikInternetSube.Classes.ClassData.Individual.FundDistributionChange;
using KatilimEmeklilikInternetSube.Classes.Tools;
using KatilimEmeklilikInternetSube.WsOperations;
using System.Web;

namespace KatilimEmeklilikInternetSube.Classes.LiveData
{
    public class Sessions
    {
        #region CurrentUser
        public static cUser CurrentUser
        {
            get
            {
                try
                {
                    var user = (cUser)HttpContext.Current.Session["CurrentUser"];

                    return user == null ? new cUser() : user;
                }
                catch
                {
                    return new cUser();
                }
            }
            set
            {
                HttpContext.Current.Session["CurrentUser"] = value;
            }
        }
        #endregion
         
        #region SmsCount
        public static int SmsCount
        {
            get
            {
                if (HttpContext.Current.Session["SmsCount"] == null)
                {
                    return 0;
                }
                else
                {
                    return Utilities.NullFixInt(HttpContext.Current.Session["SmsCount"].ToString());
                }
            }
            set
            {
                HttpContext.Current.Session["SmsCount"] = value;
            }
        }
        #endregion 

        #region TarifeKod
        public static string TarifeKod
        {
            get
            {
                if (HttpContext.Current.Session["TarifeKod"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["TarifeKod"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["TarifeKod"] = value;
            }
        }
        #endregion 

        #region FundDistributionChange
        public static cIndividual_FundDistributionChange_Session FundDistributionChange
        {
            get
            {
                try
                {
                    var res = HttpContext.Current.Session["FundDistributionChange"];
                    if (res == null)
                    {
                        return new cIndividual_FundDistributionChange_Session();
                    }

                    return (cIndividual_FundDistributionChange_Session)res; 
                }
                catch
                {
                    return new cIndividual_FundDistributionChange_Session();
                }
            }
            set
            {
                HttpContext.Current.Session["FundDistributionChange"] = value;
            }
        }
        #endregion

        #region Message
        public static string Message
        {
            get
            {
                if (HttpContext.Current.Session["Message"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["Message"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["Message"] = value;
            }
        }
        #endregion 

        #region CustomerProduct
        public static CustomerProduct[] CustomerProduct
        {
            get
            {
                try
                {
                    var res = HttpContext.Current.Session["CustomerProduct"];
                    if (res == null)
                    {
                        return null;
                    }

                    return (CustomerProduct[])res;
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["CustomerProduct"] = value;
            }
        }
        #endregion

        #region TitleName
        public static string TitleName
        {
            get
            {
                try
                {
                    var res = HttpContext.Current.Session["TitleName"];
                    if (res == null)
                    {
                        return "";
                    }

                    return res.ToString();
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                HttpContext.Current.Session["TitleName"] = value;
            }
        }
        #endregion
    }
}