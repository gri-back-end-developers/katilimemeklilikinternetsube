﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KatilimEmeklilikInternetSube.Classes.LiveData
{
    public class cEnums
    {
        public enum BransKod
        {
            FerdiKaza = 550,
            Saglik = 585,
            Hayat = 590,
            BireyselEmeklilik = 595
        }

        public enum PageType
        {
            Bireysel,
            Hayat,
            Ferdi,
            OtomatikKatilim,
            Saglik,
            DigerSayfalar
        }

        public enum OkBireyselDevam
        {
            KP,
            Cayma
        }
    }
}