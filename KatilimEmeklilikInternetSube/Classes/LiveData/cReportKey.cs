﻿using KatilimEmeklilikInternetSube.WsReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KatilimEmeklilikInternetSube.Classes.LiveData
{
    public class cReportKey
    {
        public static ReportKey Get(int intReportID)
        {
            switch (intReportID)
            {
                case 1:
                    return ReportKey.PoliceBasim;
                case 2:
                    return ReportKey.TahsilatMakbuzu;
                case 5:
                    return ReportKey.AktarimFormu;
                case 6:
                    return ReportKey.EmeklilikBilgiFormu;
                case 7:
                    return ReportKey.HesapOzetiProfili;
                case 8:
                    return ReportKey.BirikimBildirimFormu;
                case 9:
                    return ReportKey.AyrilmaFormu; 
                case 11:
                    return ReportKey.MusteriBilgiFormu;
                case 22:
                    return ReportKey.HesapBildirimCetveli;
                case 31:
                    return ReportKey.BesSozlesmeBasim;
                case 32:
                    return ReportKey.BesTeklifBasim;
                case 38:
                    return ReportKey.HayatTeklifBasim;
            }

            return ReportKey.PoliceBasim;
        }
    }
}