﻿using KatilimEmeklilikInternetSube.Classes.ClassData;
using KatilimEmeklilikInternetSube.Classes.LiveData;
using KatilimEmeklilikInternetSube.Classes.Tools;
using KatilimEmeklilikInternetSube.Models;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KatilimEmeklilikInternetSube.Classes
{
    public class UserFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                bool bolResponse = false;

                if (HttpContext.Current.Request.UrlReferrer != null)
                {
                    if (HttpContext.Current.Session["CurrentUser"] != null && HttpContext.Current.Session["CustomerProduct"] != null)
                    {
                        var currentUser = Sessions.CurrentUser;
                        if (currentUser.intCustomerNo > 0)
                        {
                            PageSettings pageSetting = new PageSettings();

                            if (currentUser.strIP != Utilities.ClientIP)
                            {
                                bolResponse = true;
                            }
                            else if (pageSetting.Title != "")
                            {
                                if (currentUser.pages.Count(c => c.bolIsActive) > 0)
                                {
                                    if (currentUser.pages.Count(c => c.bolIsActive && c.strName == pageSetting.Title) == 0)
                                    {
                                        HttpContext.Current.Response.Redirect(currentUser.pages.First(f => f.bolIsActive).strPath);
                                    }
                                }
                                else
                                {
                                    HttpContext.Current.Response.Redirect("/kisisel-bilgiler");
                                }
                            }
                        }
                    }
                    else
                    {
                        bolResponse = true;

                        #region local
                        if (ConfigurationManager.AppSettings["test"] == "1")
                        {
                            List<cUserPages> pages = new List<cUserPages>();
                            pages.Add(new cUserPages
                            {
                                strClass = "icon-coins",
                                strName = ConfigurationManager.AppSettings["BireyselTitle"],
                                strPath = "/" + Utilities.Friendly(ConfigurationManager.AppSettings["BireyselTitle"]),
                                bolIsActive = true
                            });

                            pages.Add(new cUserPages
                            {
                                strClass = "icon-heart",
                                strName = ConfigurationManager.AppSettings["HayatTitle"],
                                strPath = "/" + Utilities.Friendly(ConfigurationManager.AppSettings["HayatTitle"]),
                                bolIsActive = true
                            });

                            pages.Add(new cUserPages
                            {
                                strClass = "icon-healtcare",
                                strName = ConfigurationManager.AppSettings["FerdiTitle"],
                                strPath = "/" + Utilities.Friendly(ConfigurationManager.AppSettings["FerdiTitle"]),
                                bolIsActive = true
                            });

                            pages.Add(new cUserPages
                            {
                                strClass = "icon-time-passing",
                                strName = ConfigurationManager.AppSettings["OtomatikKatilimTitle"],
                                strPath = "/" + Utilities.Friendly(ConfigurationManager.AppSettings["OtomatikKatilimTitle"]),
                                bolIsActive = true
                            });

                            Sessions.CurrentUser = new ClassData.cUser
                            {
                                //intCustomerNo = 10850013,
                                //intCustomerNo = 10148362,
                                intCustomerNo = 10811887,
                                pages = pages,
                                strIP = Utilities.ClientIP,
                                strNameSurname = "Fatih Uzun",
                                strSessionKey = Cookies.SessionKey
                            };
                            bolResponse = false;
                        }
                        #endregion
                    }
                }
                else
                {
                    bolResponse = true;
                }

                if (bolResponse)
                {
                    HttpContext.Current.Session["CurrentUser"] = null;
                    HttpContext.Current.Session.Clear();
                    HttpContext.Current.Session.RemoveAll();
                    HttpContext.Current.Session.Abandon();

                    HttpContext.Current.Response.Redirect("/");
                }
                else
                {
                    using (DBEntities db = new DBEntities())
                    {
                        var setting = db.tblSettings.Where(w => w.strName == "bakim").FirstOrDefault();
                        if (setting != null)
                        {
                            if (setting.strValue == "1")
                            {
                                HttpContext.Current.Response.Redirect("/Error/status500");
                            }
                        }
                    }
                }
            }
            catch { }
        }
    } 
}