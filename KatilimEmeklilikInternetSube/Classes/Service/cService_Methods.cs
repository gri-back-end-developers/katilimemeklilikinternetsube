﻿#region Directives
using KatilimEmeklilikInternetSube.Classes.DBData;
using KatilimEmeklilikInternetSube.Classes.LiveData;
using KatilimEmeklilikInternetSube.WsOperations;
using KatilimEmeklilikInternetSube.WsReport;
using System;
using System.Collections.Generic;
using System.Diagnostics;
#endregion

namespace KatilimEmeklilikInternetSube.Classes.Service
{
    public class cService_Methods
    {
        #region Instance
        OnlineOperationsServiceClient wsOperation;

        private string _strPage;
        public cService_Methods(string strPage)
        {
            _strPage = strPage;
        }
        #endregion

        #region GetLoginID
        public bool GetLoginID(string strAuthenticatedString, int controlType, string controlNumber, string phoneNumber, ref int? loginID)
        {
            _strPage += "-GetLoginID";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var getLoginID = wsOperation.GetLoginID(strAuthenticatedString, controlType, controlNumber, phoneNumber, ref loginID);
                    if (getLoginID.IsOk && loginID.HasValue)
                    {
                        return true;
                    }
                    else
                    {
                        if (getLoginID.ErrorList.Length > 0)
                        {
                            Logs.Exception(getLoginID.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region ValidatePassword
        public bool ValidatePassword(long intLoginID, int intSmsCode, ref long? intCustomerNo, ref string strSessionKey)
        {
            _strPage += "ValidatePassword";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var validatePassword = wsOperation.ValidatePassword(cAuthenticatedString.Get, intLoginID, intSmsCode, ref intCustomerNo, ref strSessionKey);
                    if (validatePassword.IsOk)
                    {
                        return true;
                    }
                    else
                    {
                        if (validatePassword.ErrorList.Length > 0)
                        {
                            Logs.Exception(validatePassword.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region GetCustomerProducts
        public bool GetCustomerProducts(string strSessionKey, long customerNo, ref CustomerProduct[] listCustomerProducts)
        {
            _strPage += "-GetCustomerProducts";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var getCustomerProducts = wsOperation.GetCustomerProducts(cAuthenticatedString.Get, strSessionKey, "INTSUBE", customerNo, ref listCustomerProducts);
                    if (getCustomerProducts.IsOk)
                    {
                        return true;
                    }
                    else
                    {
                        if (getCustomerProducts.ErrorList.Length > 0)
                        {
                            Logs.Exception(getCustomerProducts.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region GetPensionGeneralInfo
        public bool GetPensionGeneralInfo(long policyNo, ref PensionGeneralInfo pensionGeneralInfo, ref Beneficiary[] listBeneficiaryInfo, ref FundCurrentInfo[] listFundInfo)
        {
            _strPage += "-GetPensionGeneralInfo";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var getPensionGeneralInfo = wsOperation.GetPensionGeneralInfo(cAuthenticatedString.Get, Sessions.CurrentUser.strSessionKey, "INTSUBE", policyNo, ref pensionGeneralInfo, ref listBeneficiaryInfo, ref listFundInfo);
                    if (getPensionGeneralInfo.IsOk)
                    {
                        return true;
                    }
                    else
                    {
                        if (getPensionGeneralInfo.ErrorList.Length > 0)
                        {
                            Logs.Exception(getPensionGeneralInfo.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region GetAccumulationInfo
        public bool GetAccumulationInfo(long policyNo, ref FundCurrentInfo[] listFundInfo, ref TransferInfo transferInfo, ref AccumulationInfo accumulationInfo)
        {
            _strPage += "-GetAccumulationInfo";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var getPensionGeneralInfo = wsOperation.GetAccumulationInfo(cAuthenticatedString.Get, Sessions.CurrentUser.strSessionKey, "INTSUBE", policyNo, ref listFundInfo, ref transferInfo, ref accumulationInfo);
                    if (getPensionGeneralInfo.IsOk)
                    {
                        return true;
                    }
                    else
                    {
                        if (getPensionGeneralInfo.ErrorList.Length > 0)
                        {
                            Logs.Exception(getPensionGeneralInfo.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region GetAccumulationChange
        public bool GetAccumulationChange(long policyNo, string startdate, string enddate, ref decimal? changeAmount)
        {
            _strPage += "-GetAccumulationChange";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var getAccumulationChange = wsOperation.GetAccumulationChange(cAuthenticatedString.Get, Sessions.CurrentUser.strSessionKey, "INTSUBE", policyNo, startdate.Replace("-", "/"), enddate.Replace("-", "/"), ref changeAmount);
                    if (getAccumulationChange.IsOk && changeAmount.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        if (getAccumulationChange.ErrorList.Length > 0)
                        {
                            Logs.Exception(getAccumulationChange.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region GetStateContribution
        public StateContributionResult GetStateContribution(long policyNo)
        {
            _strPage += "-GetStateContribution";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    StateContributionResult stateContribution = wsOperation.GetStateContribution(new StateContributionInput
                    {
                        AuthenticationKey = cAuthenticatedString.Get,
                        PolicyNo = policyNo
                    });

                    if (stateContribution.ServiceResult.IsOk)
                    {
                        return stateContribution;
                    }
                    else
                    {
                        if (stateContribution.ServiceResult.ErrorList.Length > 0)
                        {
                            Logs.Exception(stateContribution.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return null;
        }
        #endregion

        #region GetContributionRateDemand
        public GetContributionRateDemandResult GetContributionRateDemand(long policyNo)
        {
            _strPage += "-GetContributionRateDemand";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    GetContributionRateDemandResult getContributionRateDemand = wsOperation.GetContributionRateDemand(new GetContributionRateDemandInput
                    {
                        AuthenticationKey = cAuthenticatedString.Get,
                        PolicyNo = policyNo
                    });

                    if (getContributionRateDemand.ServiceResult.IsOk)
                    {
                        return getContributionRateDemand;
                    }
                    else
                    {
                        if (getContributionRateDemand.ServiceResult.ErrorList.Length > 0)
                        {
                            Logs.Exception(getContributionRateDemand.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return null;
        }
        #endregion

        #region GetFundDemandInfo
        public GetFundDemandInfoResult GetFundDemandInfo(long policyNo, string distributionType)
        {
            _strPage += "-GetFundDemandInfo";

            GetFundDemandInfoResult fundDemandInfo = null;
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    fundDemandInfo = wsOperation.GetFundDemandInfo(new GetFundDemandInfoInput
                    {
                        AuthenticationKey = cAuthenticatedString.Get,
                        SessionKey = Sessions.CurrentUser.strSessionKey,
                        Channel = "INTSUBE",
                        DistributionType = distributionType,
                        PolicyNo = policyNo
                    });

                    if (!fundDemandInfo.ServiceResult.IsOk)
                    {
                        if (fundDemandInfo.ServiceResult.ErrorList.Length > 0)
                        {
                            Logs.Exception(fundDemandInfo.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return fundDemandInfo;
        }
        #endregion

        #region GetRrpDefinition
        public GetRrpDefinitionResult GetRrpDefinition()
        {
            _strPage += "-GetRrpDefinition";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    GetRrpDefinitionResult getRrpDefinition = wsOperation.GetRrpDefinition(new GetRrpDefinitionInput
                    {
                        AuthenticationKey = cAuthenticatedString.Get
                    });

                    if (getRrpDefinition.ServiceResult.IsOk)
                    {
                        if (getRrpDefinition.RrpDefinitionList != null)
                        {
                            return getRrpDefinition;
                        }
                    }
                    else
                    {
                        if (getRrpDefinition.ServiceResult.ErrorList.Length > 0)
                        {
                            Logs.Exception(getRrpDefinition.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return null;
        }
        #endregion

        #region GetRrpPoint
        public GetRrpStrategyResult GetRrpPoint(int pointTotal, string PlanCode)
        {
            _strPage += "-GetRrpPoint";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    GetRrpStrategyResult rrpPoint = wsOperation.GetRrpPoint(new GetRrpStrategyInput
                    {
                        AuthenticationKey = cAuthenticatedString.Get,
                        Point = pointTotal,
                        PlanCode = PlanCode,
                        //PlanCode = "B03",
                        TenderDate = DateTime.Now.ToString("dd.MM.yyyy").Replace(".", "")
                    });

                    if (rrpPoint.ServiceResult.IsOk)
                    {
                        return rrpPoint;
                    }
                    else
                    {
                        if (rrpPoint.ServiceResult.ErrorList.Length > 0)
                        {
                            Logs.Exception(rrpPoint.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return null;
        }
        #endregion

        #region SaveFundDemand
        public bool SaveFundDemand(long policyNo, string demandType, string distributionType, string newDistribution, TAB_RGP_SONUC_DETAY rgpAnswer, TAB_RGP_SONUC_FON rgpFund, int rgpSelectType, ref long demandNo, ref string strMsg)
        {
            _strPage += "-SaveFundDemand";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var saveFundDemand = wsOperation.SaveFundDemand(cAuthenticatedString.Get, Sessions.CurrentUser.strSessionKey, "INT", policyNo, demandType, distributionType, newDistribution, rgpAnswer, rgpFund, rgpSelectType, ref demandNo);
                    if (saveFundDemand.IsOk)
                    {
                        return true;
                    }
                    else
                    {
                        if (saveFundDemand.ErrorList.Length > 0)
                        {
                            strMsg = saveFundDemand.ErrorList[0].Split('|')[1];

                            Logs.Exception(saveFundDemand.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region GetPremiumChangeInfo
        public bool GetPremiumChangeInfo(long policyNo, ref CurrentPremiumInfo currentPremiumInfo, ref KeyValue[] listOpenDues)
        {
            _strPage += "-GetPremiumChangeInfo";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var getPremiumChangeInfo = wsOperation.GetPremiumChangeInfo(cAuthenticatedString.Get, Sessions.CurrentUser.strSessionKey, "INTSUBE", policyNo, ref currentPremiumInfo, ref listOpenDues);
                    if (getPremiumChangeInfo.IsOk)
                    {
                        return true;
                    }
                    else
                    {
                        if (getPremiumChangeInfo.ErrorList.Length > 0)
                        {
                            Logs.Exception(getPremiumChangeInfo.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region SavePremiumDemand
        public bool SavePremiumDemand(long policyNo, string dueDate, float newAmount, ref long demandNo, ref string strMsg)
        {
            _strPage += "-SavePremiumDemand";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var savePremiumDemand = wsOperation.SavePremiumDemand(cAuthenticatedString.Get, Sessions.CurrentUser.strSessionKey, "INT", policyNo, dueDate, newAmount, ref demandNo);
                    if (savePremiumDemand.IsOk)
                    {
                        return true;
                    }
                    else
                    {
                        if (savePremiumDemand.ErrorList.Length > 0)
                        {
                            strMsg = savePremiumDemand.ErrorList[0].Split('|')[1];

                            Logs.Exception(savePremiumDemand.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region GetBankBranchList
        public GetBankBranchListResult GetBankBranchList(string BankCode)
        {
            _strPage += "-GetBankBranchList";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    GetBankBranchListResult getBankBranchList = wsOperation.GetBankBranchList(new GetBankBranchListInput
                    {
                        AuthenticationKey = cAuthenticatedString.Get,
                        BankCode = BankCode
                    });

                    if (getBankBranchList.ServiceResult.IsOk)
                    {
                        return getBankBranchList;
                    }
                    else
                    {
                        if (getBankBranchList.ServiceResult.ErrorList.Length > 0)
                        {
                            Logs.Exception(getBankBranchList.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return null;
        }
        #endregion

        #region SaveWithdrawalDemand
        public bool SaveWithdrawalDemand(long policyNo, string withdrawalReason, string withdrawalSubReason, string withdrawalType, ref string strMsg)
        {
            _strPage += "-SaveWithdrawalDemand";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    SaveWithdrawalDemandResult withdrawalDemand = wsOperation.SaveWithdrawalDemand(new SaveWithdrawalDemandResultInput()
                    {
                        PolicyNo = policyNo,
                        SessionKey = Sessions.CurrentUser.strSessionKey,
                        AuthenticationKey = cAuthenticatedString.Get,
                        Channel = "INTSUBE",
                        DemandChannel = "INT",
                        DemandDate = DateTime.Now,
                        DemandIncomingDate = DateTime.Now,
                        Description = "",
                        WithdrawalAccountInfo = new AcoountInfo { },
                        DocumentIncomingDate = DateTime.Now,
                        WithdrawalReason = withdrawalReason,// KENDİ İSTEĞİ İLE AYRILMA
                        WithdrawalSubReason = withdrawalSubReason, // CAY
                        WithdrawalType = withdrawalType // Ayrılma tablebi
                    });

                    if (withdrawalDemand.ServiceResult.IsOk)
                    {
                        return true;
                    }
                    else
                    {
                        if (withdrawalDemand.ServiceResult.ErrorList.Length > 0)
                        {
                            strMsg = withdrawalDemand.ServiceResult.ErrorList[0].Split('|')[1];

                            Logs.Exception(withdrawalDemand.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region GetPensionTotalAccumulation
        public GetPensionTotalAccumulationResult GetPensionTotalAccumulation(long customerNo)
        {
            _strPage += "-GetPensionTotalAccumulation";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    GetPensionTotalAccumulationResult pensionTotalAccumulation = wsOperation.GetPensionTotalAccumulation(new GetPensionTotalAccumulationInput
                    {
                        AuthenticationKey = cAuthenticatedString.Get,
                        Channel = "INTSUBE",
                        CustomerNo = customerNo,
                        SessionKey = Sessions.CurrentUser.strSessionKey
                    });

                    if (pensionTotalAccumulation.ServiceResult.IsOk)
                    {
                        if (pensionTotalAccumulation.TotalAccumulation != null)
                        {
                            return pensionTotalAccumulation;
                        }
                    }
                    else
                    {
                        if (pensionTotalAccumulation.ServiceResult.ErrorList.Length > 0)
                        {
                            Logs.Exception(pensionTotalAccumulation.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return null;
        }
        #endregion

        #region SaveContributionRateDemand
        public bool SaveContributionRateDemand(long policyNo, decimal Rate, ref string strMsg)
        {
            _strPage += "-SaveContributionRateDemand";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var saveContributionRateDemand = wsOperation.SaveContributionRateDemand(new SaveContributionRateDemandInput
                    {
                        AuthenticationKey = cAuthenticatedString.Get,
                        DemandChannel = "INT",
                        PolicyNo = policyNo,
                        Rate = Rate
                    });

                    if (saveContributionRateDemand.ServiceResult.IsOk)
                    {
                        return true;
                    }
                    else
                    {
                        if (saveContributionRateDemand.ServiceResult.ErrorList.Length > 0)
                        {
                            strMsg = saveContributionRateDemand.ServiceResult.ErrorList[0].Split('|')[1];

                            Logs.Exception(saveContributionRateDemand.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region GetFinancialRecords
        public GetFinancialRecordsResult GetFinancialRecords(long policyNo)
        {
            _strPage += "-GetFinancialRecords";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    GetFinancialRecordsResult getFinancialRecords = wsOperation.GetFinancialRecords(new GetFinancialRecordsInput
                    {
                        AuthKey = cAuthenticatedString.Get,
                        Policy = policyNo.ToString()
                    });

                    if (getFinancialRecords.ServiceResult.IsOk)
                    {
                        if (getFinancialRecords.FinancialInfo != null && getFinancialRecords.FinancialInfo.Length > 0)
                        {
                            return getFinancialRecords;
                        }
                    }
                    else
                    {
                        if (getFinancialRecords.ServiceResult.ErrorList.Length > 0)
                        {
                            Logs.Exception(getFinancialRecords.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return null;
        }
        #endregion

        #region SaveAdditionalContribution
        public bool SaveAdditionalContribution(long policyNo, decimal amount, DateTime? startDate, DateTime? enddate, long? mustMaliID, ref string strMsg)
        {
            _strPage += "-SaveAdditionalContribution";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var saveAdditionalContribution = wsOperation.SaveAdditionalContribution(new SaveAdditionalContributionInput
                    {
                        AuthKey = cAuthenticatedString.Get,
                        Amount = amount,
                        Demander = "S",
                        EndDate = enddate,
                        MustMaliID = mustMaliID,
                        PolicyNo = policyNo,
                        StartDate = startDate
                    });

                    if (saveAdditionalContribution.ServiceResult.IsOk)
                    {
                        return true;
                    }
                    else
                    {
                        if (saveAdditionalContribution.ServiceResult.ErrorList.Length > 0)
                        {
                            strMsg = saveAdditionalContribution.ServiceResult.ErrorList[0].Split('|')[1];

                            Logs.Exception(saveAdditionalContribution.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region GetLifeGeneralInfo
        public bool GetLifeGeneralInfo(long policyNo, ref LifeGeneralInfo lifeGeneralInfo, ref Beneficiary[] listBeneficiaryInfo, ref CoverageLastStatus[] listCoverageInfo, ref LifeCollectingInfo collectingInfo, ref MediatorInfo mediatorInfo)
        {
            _strPage += "-GetLifeGeneralInfo";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var getLifeGeneralInfo = wsOperation.GetLifeGeneralInfo(cAuthenticatedString.Get, Sessions.CurrentUser.strSessionKey, "INTSUBE", policyNo, ref lifeGeneralInfo, ref listBeneficiaryInfo, ref listCoverageInfo, ref collectingInfo, ref mediatorInfo);
                    if (getLifeGeneralInfo.IsOk)
                    {
                        return true;
                    }
                    else
                    {
                        if (getLifeGeneralInfo.ErrorList.Length > 0)
                        {
                            Logs.Exception(getLifeGeneralInfo.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region GetCustomerPersonalInfo
        public bool GetCustomerPersonalInfo(string sessionKey, long customerNo, ref CustomerInfo customerInfo, ref CustomerAddress customerAddress)
        {
            _strPage += "-GetCustomerPersonalInfo";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var getCustomerPersonalInfo = wsOperation.GetCustomerPersonalInfo(cAuthenticatedString.Get, sessionKey, "INTSUBE", customerNo, ref customerInfo, ref customerAddress);
                    if (getCustomerPersonalInfo.IsOk)
                    {
                        return true;
                    }
                    else
                    {
                        if (getCustomerPersonalInfo.ErrorList.Length > 0)
                        {
                            Logs.Exception(getCustomerPersonalInfo.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region GetReport
        public GetReportResult GetReport(KeyValuePair<string, string>[] parameters, ReportKey reportId, ref string strMsg)
        {
            _strPage += "-GetReport";
            try
            {
                using (ReportWSClient wsReport = new ReportWSClient())
                {
                    GetReportResult report = wsReport.GetReport(new GetReportInput
                    {
                        AuthKey = cAuthenticatedString.Get,
                        Parameters = parameters,
                        ReportId = reportId
                    });

                    if (report.Result.IsOk)
                    {
                        if (report != null && !string.IsNullOrEmpty(report.ReportPdf))
                        {
                            return report;
                        }
                    }
                    else if (report.Result.ErrorList.Length > 0)
                    {
                        Logs.Exception(report.Result.ErrorList[0], 0, _strPage);

                        if (report.Result.ErrorList.Length > 0)
                        {
                            strMsg = report.Result.ErrorList[0].Split('|')[1];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return null;
        }
        #endregion

        #region GetCollectingDetailInfo
        public bool GetCollectingDetailInfo(long policyNo, ref CollectingDetail[] collectingDetail)
        {
            _strPage += "-GetCollectingDetailInfo";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var collectingDetailInfo = wsOperation.GetCollectingDetailInfo(cAuthenticatedString.Get, Sessions.CurrentUser.strSessionKey, "INTSUBE", policyNo, "", "", ref collectingDetail);
                    if (collectingDetailInfo.IsOk)
                    {
                        if (collectingDetail != null && collectingDetail.Length > 0)
                        {
                            return true;
                        }
                        else
                        {
                            if (collectingDetailInfo.ErrorList.Length > 0)
                            {
                                Logs.Exception(collectingDetailInfo.ErrorList[0], 0, _strPage);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region GetHealthPolicyInfo
        public GetHealthPolicyInfoResult GetHealthPolicyInfo(long policyNo, string IdentificationNumber)
        {
            _strPage += "-GetHealthPolicyInfo";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    GetHealthPolicyInfoResult healthPolicyInfo = wsOperation.GetHealthPolicyInfo(new GetHealthPolicyInfoInput
                    {
                        AuthenticationKey = cAuthenticatedString.Get,
                        PolicyNo = (int)policyNo,
                        IdentificationNumber = IdentificationNumber
                    });

                    if (healthPolicyInfo.ServiceResult.IsOk)
                    {
                        if (healthPolicyInfo.HealthPolicyInfo != null && healthPolicyInfo.HealthPolicyInfo.Value != null && healthPolicyInfo.HealthPolicyInfo.Value.Length > 0)
                        {
                            return healthPolicyInfo;
                        }
                    }
                    else
                    {
                        if (healthPolicyInfo.ServiceResult.ErrorList.Length > 0)
                        {
                            Logs.Exception(healthPolicyInfo.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return null;
        }
        #endregion

        #region GetHealthPolicyInfoFromTPA
        public GetHealthPolicyInfoFromTPAOnlineOperationsResult GetHealthPolicyInfoFromTPA(long policyNo, string strCustomerNo)
        {
            _strPage += "-GetHealthPolicyInfoFromTPA";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    GetHealthPolicyInfoFromTPAOnlineOperationsResult healthPolicyInfoFromTPA = wsOperation.GetHealthPolicyInfoFromTPA(new GetHealthPolicyInfoFromTPAOnlineOperationsInput
                    {
                        AuthenticationKey = cAuthenticatedString.Get,
                        CustomerCode = strCustomerNo,
                        PolicyNo = (int)policyNo,
                        RenewalNo = "0",
                        TPACompanyCode = "8"
                    });

                    if (healthPolicyInfoFromTPA.ServiceResult.IsOk)
                    {
                        if (healthPolicyInfoFromTPA != null && healthPolicyInfoFromTPA.HealthPolicyInfo != null && healthPolicyInfoFromTPA.HealthPolicyInfo.Value != null && healthPolicyInfoFromTPA.HealthPolicyInfo.Value.Length > 0)
                        {
                            return healthPolicyInfoFromTPA;
                        }
                    }
                    else
                    {
                        if (healthPolicyInfoFromTPA.ServiceResult.ErrorList.Length > 0)
                        {
                            Logs.Exception(healthPolicyInfoFromTPA.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return null;
        }
        #endregion

        #region GetHealthFacilitiesInfo
        public OBJ_SAGLIK_KURUM_BILGI[] GetHealthFacilitiesInfo(string facilityName, string cityCode, string districtCode)
        {
            _strPage += "-GetHealthFacilitiesInfo";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    GetHealthFacilitiesInfoResult healthFacilitiesInfo = wsOperation.GetHealthFacilitiesInfo(new GetHealthFacilitiesInfoInput
                    {
                        AuthenticationKey = cAuthenticatedString.Get,
                        CityCode = cityCode,
                        FacilityName = facilityName,
                        ProvinceCode = districtCode
                    });

                    if (healthFacilitiesInfo.ServiceResult.IsOk)
                    {
                        if (healthFacilitiesInfo.HealthFacilitiesInfo != null && healthFacilitiesInfo.HealthFacilitiesInfo.Value != null && healthFacilitiesInfo.HealthFacilitiesInfo.Value.Length > 0)
                        {
                            return healthFacilitiesInfo.HealthFacilitiesInfo.Value;
                        }
                    }
                    else
                    {
                        if (healthFacilitiesInfo.ServiceResult.ErrorList.Length > 0)
                        {
                            Logs.Exception(healthFacilitiesInfo.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return null;
        }
        #endregion

        #region GetAdditonalContributionRecords
        public AdditionalContributionRecord[] GetAdditonalContributionRecords(string Policy)
        {
            _strPage += "-GetAdditonalContributionRecords";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    GetAdditonalContributionResult contributionRecords = wsOperation.GetAdditonalContributionRecords(new GetAdditonalContributionInput
                    {
                        AuthKey = cAuthenticatedString.Get,
                        Policy = Policy
                    });

                    if (contributionRecords.ServiceResult.IsOk)
                    {
                        if (contributionRecords.Records != null && contributionRecords.Records.Length > 0)
                        {
                            return contributionRecords.Records;
                        }
                    }
                    else
                    {
                        if (contributionRecords.ServiceResult.ErrorList.Length > 0)
                        {
                            Logs.Exception(contributionRecords.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return null;
        }
        #endregion

        #region GetKeyValuePair
        public bool GetKeyValuePair(WsOperations.TypeKey typeKey, string value, ref ListSource1[] listSource)
        {
            _strPage += "-GetKeyValuePair";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    var keyValuePair = wsOperation.GetKeyValuePair(cAuthenticatedString.Get, new GetKeyValuePairInput
                    {
                        TypeKey = typeKey,
                        FirstParam = value
                    }, ref listSource);

                    if (keyValuePair.IsOk)
                    {
                        if (listSource != null && listSource.Length > 0)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (keyValuePair.ErrorList.Length > 0)
                        {
                            Logs.Exception(keyValuePair.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return false;
        }
        #endregion

        #region GetIrrAndAccumulationInfo
        public OBJ_IRR_LIST[] GetIrrAndAccumulationInfo(long PolicyNo)
        {
            _strPage += "-GetIrrAndAccumulationInfo";
            try
            {
                using (wsOperation = new OnlineOperationsServiceClient())
                {
                    GetIrrAndAccumulationResult irrAndAccumulationResult = wsOperation.GetIrrAndAccumulationInfo(new GetIrrAndAccumulationInput
                    {
                        AuthenticationKey = cAuthenticatedString.Get,
                        PolicyNo = (int)PolicyNo
                    });

                    if (irrAndAccumulationResult.ServiceResult.IsOk)
                    {
                        if (irrAndAccumulationResult.IrrAndAccumulationList != null && irrAndAccumulationResult.IrrAndAccumulationList.Value != null && irrAndAccumulationResult.IrrAndAccumulationList.Value.Length > 0)
                        {
                            return irrAndAccumulationResult.IrrAndAccumulationList.Value;
                        }
                    }
                    else
                    {
                        if (irrAndAccumulationResult.ServiceResult.ErrorList.Length > 0)
                        {
                            Logs.Exception(irrAndAccumulationResult.ServiceResult.ErrorList[0], 0, _strPage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intLine = 0;
                try
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    intLine = frame.GetFileLineNumber();
                }
                catch { }

                Logs.Exception(ex.ToString(), intLine, _strPage + "-Ex");
            }

            return null;
        }
        #endregion
    }
}