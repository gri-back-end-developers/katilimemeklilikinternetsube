﻿var dont_confirm_leave = 0;
var validNavigation = false;
$(function () {
    wireUpEvents();
});

function CloseSession(e) {
    if (!validNavigation) {
        if (dont_confirm_leave !== 1) {
            var leave_message = 'Oturumunuz kapatıldı.';
            if (!e) e = window.event;
            e.cancelBubble = true;
            e.returnValue = leave_message;
            if (e.stopPropagation) {
                e.stopPropagation();
                e.preventDefault();
            }
            $.ajax({ type: "POST", url: "/user/brcntrl", cache: false, headers: { "cache-control": "no-cache" } });

            return leave_message;
        }
    }
} 
 
function wireUpEvents() {
    window.onbeforeunload = CloseSession;
    
    $(document).bind('keypress', function (e) {
        if (e.keyCode == 116) {
            validNavigation = true;
        }
    });
    $("a").bind("click", function () {
        validNavigation = true;
    });
    $("form").bind("submit", function () {
        validNavigation = true;
    });
    $("input[type=submit]").bind("click", function () {
        validNavigation = true;
    });
};

