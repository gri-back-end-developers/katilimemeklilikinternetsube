﻿function setCookie(e,t,o){var n=new Date;n.setTime(n.getTime()+24*o*60*60*1e3);var r="expires="+n.toUTCString();document.cookie=e+"="+t+";"+r+";path=/"}function getCookie(e){for(var t=e+"=",o=document.cookie.split(";"),n=0;n<o.length;n++){for(var r=o[n];" "==r.charAt(0);)r=r.substring(1);if(0==r.indexOf(t))return r.substring(t.length,r.length)}return""}function checkCookie(){var e=getCookie("username");""!=e?alert("Welcome again "+e):""!=(e=prompt("Please enter your name:",""))&&null!=e&&setCookie("username",e,365)}
  /*! (C) Andrea Giammarchi - Mit Style License */
  var URLSearchParams = URLSearchParams || function () { "use strict"; function URLSearchParams(query) { var index, key, value, pairs, i, length, dict = Object.create(null); this[secret] = dict; if (!query) return; if (typeof query === "string") { if (query.charAt(0) === "?") { query = query.slice(1) } for (pairs = query.split("&"), i = 0, length = pairs.length; i < length; i++) { value = pairs[i]; index = value.indexOf("="); if (-1 < index) { appendTo(dict, decode(value.slice(0, index)), decode(value.slice(index + 1))) } else if (value.length) { appendTo(dict, decode(value), "") } } } else { if (isArray(query)) { for (i = 0, length = query.length; i < length; i++) { value = query[i]; appendTo(dict, value[0], value[1]) } } else { for (key in query) { appendTo(dict, key, query[key]) } } } } var isArray = Array.isArray, URLSearchParamsProto = URLSearchParams.prototype, find = /[!'\(\)~]|%20|%00/g, plus = /\+/g, replace = { "!": "%21", "'": "%27", "(": "%28", ")": "%29", "~": "%7E", "%20": "+", "%00": "\0" }, replacer = function (match) { return replace[match] }, secret = "__URLSearchParams__:" + Math.random(); function appendTo(dict, name, value) { if (name in dict) { dict[name].push("" + value) } else { dict[name] = isArray(value) ? value : ["" + value] } } function decode(str) { return decodeURIComponent(str.replace(plus, " ")) } function encode(str) { return encodeURIComponent(str).replace(find, replacer) } URLSearchParamsProto.append = function append(name, value) { appendTo(this[secret], name, value) }; URLSearchParamsProto["delete"] = function del(name) { delete this[secret][name] }; URLSearchParamsProto.get = function get(name) { var dict = this[secret]; return name in dict ? dict[name][0] : null }; URLSearchParamsProto.getAll = function getAll(name) { var dict = this[secret]; return name in dict ? dict[name].slice(0) : [] }; URLSearchParamsProto.has = function has(name) { return name in this[secret] }; URLSearchParamsProto.set = function set(name, value) { this[secret][name] = ["" + value] }; URLSearchParamsProto.forEach = function forEach(callback, thisArg) { var dict = this[secret]; Object.getOwnPropertyNames(dict).forEach(function (name) { dict[name].forEach(function (value) { callback.call(thisArg, value, name, this) }, this) }, this) }; URLSearchParamsProto.toJSON = function toJSON() { return {} }; URLSearchParamsProto.toString = function toString() { var dict = this[secret], query = [], i, key, name, value; for (key in dict) { name = encode(key); for (i = 0, value = dict[key]; i < value.length; i++) { query.push(name + "=" + encode(value[i])) } } return query.join("&") }; var dP = Object.defineProperty, gOPD = Object.getOwnPropertyDescriptor, createSearchParamsPollute = function (search) { function append(name, value) { URLSearchParamsProto.append.call(this, name, value); name = this.toString(); search.set.call(this._usp, name ? "?" + name : "") } function del(name) { URLSearchParamsProto["delete"].call(this, name); name = this.toString(); search.set.call(this._usp, name ? "?" + name : "") } function set(name, value) { URLSearchParamsProto.set.call(this, name, value); name = this.toString(); search.set.call(this._usp, name ? "?" + name : "") } return function (sp, value) { sp.append = append; sp["delete"] = del; sp.set = set; return dP(sp, "_usp", { configurable: true, writable: true, value: value }) } }, createSearchParamsCreate = function (polluteSearchParams) { return function (obj, sp) { dP(obj, "_searchParams", { configurable: true, writable: true, value: polluteSearchParams(sp, obj) }); return sp } }, updateSearchParams = function (sp) { var append = sp.append; sp.append = URLSearchParamsProto.append; URLSearchParams.call(sp, sp._usp.search.slice(1)); sp.append = append }, verifySearchParams = function (obj, Class) { if (!(obj instanceof Class)) throw new TypeError("'searchParams' accessed on an object that " + "does not implement interface " + Class.name) }, upgradeClass = function (Class) { var ClassProto = Class.prototype, searchParams = gOPD(ClassProto, "searchParams"), href = gOPD(ClassProto, "href"), search = gOPD(ClassProto, "search"), createSearchParams; if (!searchParams && search && search.set) { createSearchParams = createSearchParamsCreate(createSearchParamsPollute(search)); Object.defineProperties(ClassProto, { href: { get: function () { return href.get.call(this) }, set: function (value) { var sp = this._searchParams; href.set.call(this, value); if (sp) updateSearchParams(sp) } }, search: { get: function () { return search.get.call(this) }, set: function (value) { var sp = this._searchParams; search.set.call(this, value); if (sp) updateSearchParams(sp) } }, searchParams: { get: function () { verifySearchParams(this, Class); return this._searchParams || createSearchParams(this, new URLSearchParams(this.search.slice(1))) }, set: function (sp) { verifySearchParams(this, Class); createSearchParams(this, sp) } } }) } }; upgradeClass(HTMLAnchorElement); if (/^function|object$/.test(typeof URL) && URL.prototype) upgradeClass(URL); return URLSearchParams }(); (function (URLSearchParamsProto) { var iterable = function () { try { return !!Symbol.iterator } catch (error) { return false } }(); if (!("forEach" in URLSearchParamsProto)) { URLSearchParamsProto.forEach = function forEach(callback, thisArg) { var names = Object.create(null); this.toString().replace(/=[\s\S]*?(?:&|$)/g, "=").split("=").forEach(function (name) { if (!name.length || name in names) return; (names[name] = this.getAll(name)).forEach(function (value) { callback.call(thisArg, value, name, this) }, this) }, this) } } if (!("keys" in URLSearchParamsProto)) { URLSearchParamsProto.keys = function keys() { var items = []; this.forEach(function (value, name) { items.push(name) }); var iterator = { next: function () { var value = items.shift(); return { done: value === undefined, value: value } } }; if (iterable) { iterator[Symbol.iterator] = function () { return iterator } } return iterator } } if (!("values" in URLSearchParamsProto)) { URLSearchParamsProto.values = function values() { var items = []; this.forEach(function (value) { items.push(value) }); var iterator = { next: function () { var value = items.shift(); return { done: value === undefined, value: value } } }; if (iterable) { iterator[Symbol.iterator] = function () { return iterator } } return iterator } } if (!("entries" in URLSearchParamsProto)) { URLSearchParamsProto.entries = function entries() { var items = []; this.forEach(function (value, name) { items.push([name, value]) }); var iterator = { next: function () { var value = items.shift(); return { done: value === undefined, value: value } } }; if (iterable) { iterator[Symbol.iterator] = function () { return iterator } } return iterator } } if (iterable && !(Symbol.iterator in URLSearchParamsProto)) { URLSearchParamsProto[Symbol.iterator] = URLSearchParamsProto.entries } if (!("sort" in URLSearchParamsProto)) { URLSearchParamsProto.sort = function sort() { var entries = this.entries(), entry = entries.next(), done = entry.done, keys = [], values = Object.create(null), i, key, value; while (!done) { value = entry.value; key = value[0]; keys.push(key); if (!(key in values)) { values[key] = [] } values[key].push(value[1]); entry = entries.next(); done = entry.done } keys.sort(); for (i = 0; i < keys.length; i++) { this["delete"](keys[i]) } for (i = 0; i < keys.length; i++) { key = keys[i]; this.append(key, values[key].shift()) } } } })(URLSearchParams.prototype);

  //Main Variables
  var userFakeEmail = "null@null.com"
  var waitingMessage = 20; // (param * 3sn)
  var chatAgentEndedText = "Katılım Emeklilik Müşteri Hizmetleri olarak iyi günler dileriz.";

  var startChatForm = document.getElementById('startChat');
  var startTalkForm = document.getElementById('startTalk');
  //var resultElement = document.getElementById('chatStatusCheck');
  var chatEnd = document.getElementById('canli__destek--end');
  var chatEndSecond = document.getElementById("chatAnketSbmtButton");
  var messageInput = document.getElementsByName("message")[0];
  var messageSendButton = document.getElementById("message-send");
  var messageContent = document.getElementById("canli__destek--conversations");


  var userTyping = false;

  var chatID, userId, secureKey, alias, nickname, userDataPhone, userDataEmail, userDataPhone, message, _agentName;
  var konusmaID, konusmaKisi, konusmaOperator, konusmaGecmisi, konusmaSuresi;
  var chatAktif, timer, messageTimer = 0, checkMessage = true;

     
  var nickname, userDataEmail, userDataPhone;

  $.ajax("/user/info").done(function (data) {
    nickname = data.namesurname;
    userDataEmail = data.email;
    userDataPhone = data.phone;

    $("input[name=nickname]").val(nickname);
    $("input[name=userDataEmail]").val(userDataEmail);
    $("input[name=userDataPhone]").val(userDataPhone);

    if (data.control == "0") {
      $("#canli__destek").remove();
    }

  });


  // Chat Baslama
  startChatForm.addEventListener('submit', function (e) {
    e.preventDefault();

    // Check Validation
    if ($(this).validationEngine('validate')) {
      //resultElement.innerHTML = '';
  
      var params = new URLSearchParams();
      params.append('nickname', nickname);
      params.append('userData[email]', userDataEmail);
      params.append('userData[phone]', userDataPhone);
      params.append('userData[chatType]', 'BireyselChat');

      axios.post('http://192.168.20.60:8080/genesys/2/chat/customer-support', params)
        .then(function (response) {
          //resultElement.innerHTML = generateSuccessHTMLOutput(response);
          window.ChatStartParams = response.data;
          chatID = response.data.chatId;
          userId = response.data.userId;
          secureKey = response.data.secureKey;
          alias = response.data.alias;
          chatType = "BireyselChat";
          document.getElementById('canli__destek--conversation--start').className += " hideThis";
          chatEnd.className += " active"
          // BROWSER CACHE OPTIONS COMES HERE
          
          setCookie('chatID', response.data.chatId);
          setCookie('userId', response.data.userId);
          setCookie('secureKey', response.data.secureKey);
          setCookie('alias', response.data.alias);
          
          // START CHAT
          baslat()
          // SAATİ BAŞLAT
          baslatTimer();
        
        })
        .catch(function (error) {
          //resultElement.innerHTML = generateErrorHTMLOutput(error);
        })
      e.preventDefault();


    }
    else {
      e.preventDefault();
      console.log('Validation Error')
    }
  });


  // Start Talk
  startTalkForm.addEventListener('submit', function (e) {
    //resultElement.innerHTML = '';
    message = document.getElementsByName("message")[0].value;

    // on change  or  Keyup will send button will active or deactive.
    // Servis Post URL /genesys/2 / chat / { serviceName } / { chatID } / send
    var paramsSend = new URLSearchParams();
    paramsSend.append('message', message);
    paramsSend.append('userId', userId);
    paramsSend.append('secureKey', secureKey);
    paramsSend.append('alias', alias);
    var postURL = 'http://192.168.20.60:8080/genesys/2/chat/customer-support/' + chatID + '/send';
    axios.post(postURL, paramsSend)
      .then(function (response) {
        //resultElement.innerHTML = generateSuccessHTMLOutput(response);
        //window.ChatSendMessageParams = response.data
        window.ChatYenile();
        // Use Browser Caches
        // Here come browser cache option 

      })
      .catch(function (error) {
        //resultElement.innerHTML = generateErrorHTMLOutput(error);
      })
    e.preventDefault();
    this.reset()
    userTyping = false;
  });


  // Start Typing
  messageInput.addEventListener('keyup', function (e) {
    if (messageInput.value.length > 1 && userTyping == false) {
      typingFunction(e);
      userTyping = true;
    }
    if (messageInput.value.length == 0) {
      stopTypeing(e);
      userTyping = false;
    }

  });

  // Typing Fucntion
  function typingFunction(e) {
    // Servis Post URL /genesys/2/chat/{serviceName}/{chatId}/startTyping 
    var paramsSend = new URLSearchParams();
    paramsSend.append('userId', userId);
    paramsSend.append('secureKey', secureKey);
    paramsSend.append('alias', alias);
    var postURL = 'http://192.168.20.60:8080/genesys/2/chat/customer-support/' + chatID + '/startTyping';
    axios.post(postURL, paramsSend)
      .then(function (response) {
        //resultElement.innerHTML = generateSuccessHTMLOutput(response);
        window.ChatSendMessageParams = response.data
        // Use Browser Caches
        // Here come browser cache option 

      })
      .catch(function (error) {
        //resultElement.innerHTML = generateErrorHTMLOutput(error);
      })

    e.preventDefault();
    userTyping = true;
  }

  //Stop Typing 
  function stopTypeing(e) {
    var paramsSend = new URLSearchParams();
    paramsSend.append('userId', userId);
    paramsSend.append('secureKey', secureKey);
    paramsSend.append('alias', alias);
    var postURL = 'http://192.168.20.60:8080/genesys/2/chat/customer-support/' + chatID + '/stopTyping';
    axios.post(postURL, paramsSend)
      .then(function (response) {
        //resultElement.innerHTML = generateSuccessHTMLOutput(response);
        window.ChatSendMessageParams = response.data
        // Use Browser Caches
        // Here come browser cache option 

      })
      .catch(function (error) {
        //resultElement.innerHTML = generateErrorHTMLOutput(error);
      })
    e.preventDefault();
  }


  // End Talk // Event  Name Click be carefull
  chatEnd.addEventListener("click", function (e) {
    chatEnd.className -= "active"

    e.stopPropagation();
    chatAnket(e);
  });

  // CHAT BAŞLADI 2.ADIM
  function baslat() {
    chatAktif = setInterval(window.ChatYenile, 3000);
  }
  function bitir() {
    clearInterval(chatAktif);
    setCookie('chatID', '');
    setCookie('userId', '');
    setCookie('secureKey', '');
    setCookie('alias', '');
  }
  function baslatTimer() {
    var d = new Date();
    timer = d.getMinutes();
  }

  //Refresh Chat
  window.ChatYenile = function refreshChat() {
    ////resultElement.innerHTML = '';
    var paramsSend = new URLSearchParams();
    paramsSend.append('userId', userId);
    paramsSend.append('secureKey', secureKey);
    paramsSend.append('alias', alias);
    //genesys/1/service/{id}/storage
    var postURL = 'http://192.168.20.60:8080/genesys/2/chat/customer-support/' + chatID + '/refresh';
    axios.post(postURL, paramsSend)
      .then(function (response) {
        // messageContent.innerHTML = '';
        //resultElement.innerHTML = generateSuccessHTMLOutput(response);
        window.ChatRefresh = response.data;


        if(typeof response.data.messages[1] != "undefined") {
          $('#startTalk label.input input[type="text"]').removeAttr("disabled");
          $('#startTalk button[type="submit"]').removeAttr("disabled");
        }

        
        // window.ChatRefresh.chatEnded = true Chat agent tarfından bitmiştir. 
        if (window.ChatRefresh.chatEnded == true){
          messageContent.innerHTML = '<div class="agentEndedArea">'+ chatAgentEndedText +'</div>';
        } else{
          // EGER AGENT MESSAGE LENGTH 0 DAN BÜYÜKSE yada AGENT YAZDIYSA
          for (var i = 0; i < response.data.messages.length; i++) {

            if (response.data.messages[i].from.type == "Agent") {
              
              // MESAJ YAZ FONKSİYONU GELECEK
              //linkConverter(response.data.messages)

              _agentName = response.data.messages[i].from.nickname;
              messageContent.innerHTML = writeMessage(response.data.messages, response.data.messages[i].from.nickname);

            } else {
              // KATILIM AGENT OTURUM BAŞLATMADIYSA
                if(checkMessage) {
                  messageContent.innerHTML = writeMessages(response.data.messages, "Katılım Emeklilik Müşteri Hizmetlerine Hoşgeldiniz.");
                }
                if(messageTimer < waitingMessage) {
                  //messageTimer++;
                } else {
                  messageTimer = 0;
                  messageContent.innerHTML += writeMessages(response.data.messages, "Müşteri Temsilcimiz en kısa sürede sizinle iletişime geçecektir. Lütfen bekleyiniz.");
                }
                checkMessage = false;
            }
          }
        }
        

        //Function Scroll 
        scrollTop();
      })
      .catch(function (error) {
        //resultElement.innerHTML = generateErrorHTMLOutput(error);
      })
  }




  // Write To Message Template
  function writeMessage(responseMessage, responseAgentName) {
    
        
    var agentNewName = responseAgentName.split(" ");
    // if (agentNewName.length>1){
    //   agentNewName.shift();
    // }
    var elemClass = "talk__conversations self";
    var newContent = '<div class="talk__conversations">' +
    '<div class="talk__conversations--icon" ></div>' +
    '<div class="talk__conversations-content">' +
    ' <div class="talk__conversations--info">' +
    '<div class="talk__conversations--info-person"> ' +
    agentNewName[1] +
    '</div >' +
    '<div class="talk__conversations--info-time">' +
    '</div>' +
    '</div>' +
    '<div class="talk__conversations-content-message">' +
    'Merhaba,ismim ' + agentNewName[1] +  ' size nasıl yardımcı olabilirim ?'+
    '</div>' +
    '</div>' +
    '</div>';
    
    
    for (var i = 0; i < responseMessage.length; i++) {
      var displayName = "Katılım Emeklilik";
      if (responseMessage[i].from.type == "Agent") {
        elemClass = "talk__conversations";        
        var checkdisplayName = responseMessage[i].from.nickname.split(" ");
        if (checkdisplayName.length > 1){
          //displayName = checkdisplayName.shift();
          displayName = agentNewName[1];
        }else{
          displayName = responseMessage[i].from.nickname;
        }
      }

      if (responseMessage[i].from.type == "Client") {
        displayName = responseMessage[i].from.nickname;
      }

      if (responseMessage[i].type == "Message") {
        
        var message = responseMessage[i].text;        
        
        if(responseMessage[i].from.type != "Client") {
          message = MessageLinkCheck(responseMessage[i].text)
        }

        newContent +=
          '<div class="' + elemClass + ' ' + responseMessage[i].from.type + '">' +
          '<div class="talk__conversations--icon" ></div>' +
          '<div class="talk__conversations-content">' +
          ' <div class="talk__conversations--info">' +
          '<div class="talk__conversations--info-person"> ' +
          displayName +
          '</div >' +
          '<div class="talk__conversations--info-time">' +
          convertToLocal(responseMessage[i].utcTime) +
          '</div>' +
          '</div>' +
          '<div class="talk__conversations-content-message">' +
          message +
          '</div>' +
          '</div>' +
          '</div>';
      }
    }

    
    return newContent;
  }

  function writeMessages(responseMessage, defineMessage) {    
    var elemClass = "talk__conversations self";
    var elemClass = "talk__conversations self";
    var newContent = '<div class="' + elemClass + '">' +
          '<div class="talk__conversations--icon" ></div>' +
          '<div class="talk__conversations-content">' +
          ' <div class="talk__conversations--info">' +
          '<div class="talk__conversations--info-person">KATILIM EMEKLİLİK</div >' +
          '<div class="talk__conversations--info-time"></div>' +
          '</div>' +
          '<div class="talk__conversations-content-message">' + defineMessage + '</div>' +
          '</div>' +
          '</div>';

    
    for (var i = 0; i < responseMessage.length; i++) {
      var nickname = responseMessage[i].from.nickname;
      
      if (responseMessage[i].from.type == "Agent") {
        elemClass = "talk__conversations";
        var newName = responseMessage[i].from.nickname.split(" ");
        nickname = newName[1];
      }

      if (responseMessage[i].type == "Message") {
        newContent +=
          '<div class="' + elemClass + ' ' + responseMessage[i].from.type + '">' +
          '<div class="talk__conversations--icon" ></div>' +
          '<div class="talk__conversations-content">' +
          ' <div class="talk__conversations--info">' +
          '<div class="talk__conversations--info-person"> ' +
          //responseMessage[i].from.nickname +
          nickname +
          '</div >' +
          '<div class="talk__conversations--info-time">' +
          convertToLocal(responseMessage[i].utcTime) +
          '</div>' +
          '</div>' +
          '<div class="talk__conversations-content-message">' +
          responseMessage[i].text +
          '</div>' +
          '</div>' +
          '</div>';
      }
    }
    
    return newContent;
  }



  // Local Time Converting
  function convertToLocal(utc) {
    return new Date(utc).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: false });
  }

  // Result Succes
  function generateSuccessHTMLOutput(response) {
    return '<h4>Result:</h4>' +
      '<h5>Status:</h5>' +
      '<pre>' + response.status + ' ' + response.statusText + '</pre>' +
      '<h5>Headers:</h5>' +
      '<pre>' + JSON.stringify(response.headers, null, '\t') + '</pre>' +
      '<h5>Data:</h5>' +
      '<pre>' + JSON.stringify(response.data, null, '\t') + '</pre>';
  }
  // Result Fail
  function generateErrorHTMLOutput(error) {
    return '<h4>Result:</h4>' +
      '<h5>Message:</h5>' +
      '<pre>' + error.message + '</pre>' +
      '<h5>Status:</h5>' +
      '<pre>' + error.response.status + ' ' + error.response.statusText + '</pre>' +
      '<h5>Headers:</h5>' +
      '<pre>' + JSON.stringify(error.response.headers, null, '\t') + '</pre>' +
      '<h5>Data:</h5>' +
      '<pre>' + JSON.stringify(error.response.data, null, '\t') + '</pre>';
  }

  // Animation Open
  var el = $("#canli__destek");

  el.find("#canli__destek--header a").on("click", function () {
    if (el.hasClass("canli__destek--active")) {
      el.removeClass("canli__destek--active")
    } else {
      el.addClass("canli__destek--active")
      $("#canli-destek-submit-btn").click();
    }
  })

  // Scroll Event
  //$("#canli__destek--conversations").niceScroll();

  // Perfect Form
  $("#perfect-form").validationEngine();

  //Mask 
  Inputmask({ "mask": "+\\90 (999) 999 99 99", showMaskOnHover: false, autoUnmask: true }).mask('.phone-mask');


  // Chat anket
  function chatAnket(e) {
    el.addClass("canli__destek--active")
    $("#canli__destek--conversation--talk").slideUp();

    // Chat Gerçek bitirme!
    var paramsSend = new URLSearchParams();
    paramsSend.append('userId', userId);
    paramsSend.append('secureKey', secureKey);
    paramsSend.append('alias', alias);
    // Post Url /genesys/2/chat/{serviceName}/{chatId}/disconnect

    var postURL = 'http://192.168.20.60:8080/genesys/2/chat/customer-support/' + chatID + '/disconnect';
    axios.post(postURL, paramsSend)
      .then(function (response) {
        //resultElement.innerHTML = generateSuccessHTMLOutput(response);
        window.ChatSendMessageParams = response.data
        // Use Browser Caches
        // Here come browser cache option 
      })
      .catch(function (error) {
        //resultElement.innerHTML = generateErrorHTMLOutput(error);
      })

    e.preventDefault();
    // e.stopPropagation();
    //  iptal  window.location.reload()
    bitir()
  }




var MessageLinkCheck = function(text) {     
    var regex = /<a[^>]*>([^<]+)<\/a>/g;    
    if(!regex.test(text)){
      var exp1 = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
      var result1 = text.replace(exp1, '<a target="_blank" href="$1">$1</a>');
      var exp2 =/(^|[^\/])(www\.[\S]+(\b|$))/gim;
      var result2 = result1.replace(exp2, '<a target="_blank" href="http://$2">$2</a>');  
      return result2;
    }else{
      return text;
    }
}


/*

  // Message All regexs
  function linkConverter(param){
    var regexURL = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/gi;
    var replaceItem = param.match(regexURL);
    if (replaceItem == null || replaceItem.lenght < 0) {
      return enterConverter(param);
    }
    var newTemplate =  '<a href="http://#" target="_blank">?</a>'
    var newTemplateExceptHTTP = '<a href="#" target="_blank">?</a>'
    for (var i = 0; i < replaceItem.length; i++) {
      var re = new RegExp("^(http|https)://", "i");
      var str = param;
      if( re.test(str) ){
        param = param.replace(replaceItem[i],newTemplateExceptHTTP.replace("#", replaceItem[i]).replace("?", replaceItem[i]))
      } else {
        param = param.replace(replaceItem[i],newTemplate.replace("#", replaceItem[i]).replace("?", replaceItem[i]))
      }
      
    }
    return enterConverter(param);
  }

  // Message All regexs
  function enterConverter(param){
  /*   var regexURL = RegExp("\\n/gi");
    var replaceItem = param.match(regexURL);
    if (replaceItem == null || replaceItem.lenght < 0) {
      return param;
    }
    var newTemplate =  '<br/>'
    for (var i = 0; i < replaceItem.length; i++) {
      param = param.replace(replaceItem[i],newTemplate.replace("#", replaceItem[i]).replace("?", replaceItem[i]))
    }
  */
    
    //return param.replace(/(?:\r\n|\r|\n)/g, '<br />');
  //}
  


  // Chat Anket
  document.getElementById("talkEnd").addEventListener("submit", function (e) {
    e.stopPropagation();
    e.preventDefault();
    el.addClass("canli__destek--active")
    var puan = document.querySelector('input[name="konusmaPuan"]:checked').value;
    if (puan == null || puan == undefined) {
      puan = "Puanlama Yapılmadı"
      console.log(puan)
    }
    console.log(puan)
    konusmaID = chatID;
    console.log(konusmaID);
    console.log(konusmaKisi);
    console.log(konusmaOperator);
    console.log(konusmaGecmisi);


    axios.post('/form', {
      puan: puan,
      chatID: chatID,
      konusmaOperator: _agentName || '',
    })
      .then(function (response) {
        el.addClass("canli__destek--active")
        $("#canli__destek--conversation--end").slideUp();
        setTimeout(chatThanks, 5000);
      })
      .catch(function (error) {
        el.addClass("canli__destek--active")
        $("#canli__destek--conversation--end").slideUp();
        setTimeout(chatThanks, 5000);
      });


  })

  function chatThanks() {
    el.removeClass("canli__destek--active")
    $("#canli__destek--conversation--talk").show();
    $("#canli__destek--conversations").html("");
    $("input[name='konusmaPuan']").prop('checked', false);
  }






  // Scroll Top 
  function scrollTop() {
    messageContent.scrollTop = messageContent.scrollHeight;
  };

  function checkPhone(field, rules, i, options) {
    var phone = field.val();
    var isPhone = (phone.length == 10) ? true : false;
    if (!isPhone) {
      return "* Geçersiz telefon numarası.";
    }
  }

  $(document).ready(function() {

    if (getCookie('chatID') != '') {

        chatID = getCookie('chatID');
        userId = getCookie('userId');
        secureKey = getCookie('secureKey');
        alias = getCookie('alias');
        document.getElementById('canli__destek--conversation--start').className += " hideThis";
        chatEnd.className += " active"
        baslat();
        $("#canli__destek").addClass('canli__destek--active');
    }
    
  })