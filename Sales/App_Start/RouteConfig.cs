﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Sales
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Sales", action = "Index", id = UrlParameter.Optional }
            );

            //  string strPrefix = ConfigurationManager.AppSettings["root"].ToString();
            //  if (!string.IsNullOrEmpty(strPrefix))
            //  {
            //      strPrefix = strPrefix + "/";
            //  }

            //  routes.MapRoute(
            //    name: "Default",
            //    url: strPrefix + "{controller}/{action}/{id}",
            //    defaults: new { controller = "Sales", action = "Index", id = UrlParameter.Optional }
            //);
        }
    }
}
