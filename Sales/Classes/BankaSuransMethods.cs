﻿using Sales.BankaSuransWs;
using System;

namespace Sales.Classes
{
    public class BankaSuransMethods
    {
        #region TeklifBasim
        public byte[] TeklifBasim(long teklifNo)
        {
            byte[] teklifPdf = null;

            try
            {
                BankaSuransWSClient service = new BankaSuransWSClient();
                
                Logs.FileLog("TeklifBasim", "[INPUT]");
                Logs.FileLog("", "teklifNo : " + teklifNo);
                
                ServiceResult serviceResult = service.TeklifBasim(AuthenticatedString.Get, teklifNo, ReportCopyEnum.Katilimci, ref teklifPdf);

                Logs.FileLog("", "[OUTPUT]");

                if (!serviceResult.IsOk)
                {
                    string strMsg = Utilities.MessageClear(serviceResult.ErrorList, "");

                    if (serviceResult.ErrorList[0] != null && serviceResult.ErrorList[0].Length > 0)
                    {
                        Logs.Exception(new Exception(serviceResult.ErrorList[0]), "TeklifBasim");
                    }
                    else
                    {
                        Logs.Exception(new Exception(strMsg), "TeklifBasim");
                    }
                }

                Logs.FileLog("", "ServiceResult");
                Logs.FileLog("", serviceResult != null ? Utilities.XmlSerialize<ServiceResult>(serviceResult) : "null");
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "TeklifBasim");
                Logs.FileLog("", ex.ToString());
            }

            return teklifPdf;
        }
        #endregion
    }
}