﻿using Sales.WsTenderService;
using System.Linq;
using System;

namespace Sales.Classes
{
    public class TenderServiceMethods
    {
        #region APSSorgu
        public string APSSorgu(string strIdentityNo, ref APSQueryInfo adressInfo)
        {
            string strMsg = "Adres bilgisi bulunamadı!";

            try
            {
                Logs.FileLog("APSSorgu", "[INPUT]");
                Logs.FileLog("", "identityNo : " + strIdentityNo);

                LifeInServiceClient serviceClient = new LifeInServiceClient();

                ServiceResult serviceResult = serviceClient.APSSorgu(AuthenticatedString.Get, strIdentityNo, ref adressInfo);

                Logs.FileLog("", "[OUTPUT]");

                if (!serviceResult.IsOk)
                {
                    strMsg = Utilities.MessageClear(serviceResult.ErrorList, strMsg);

                    if (serviceResult.ErrorList[0] != null && serviceResult.ErrorList[0].Length > 0)
                    {
                        Logs.Exception(new Exception(serviceResult.ErrorList[0]), "APSSorgu");
                    }
                    else
                    {
                        Logs.Exception(new Exception(strMsg), "APSSorgu");
                    }
                }
                else
                {
                    strMsg = "";

                    Logs.FileLog("", "adressInfo");
                    Logs.FileLog("", adressInfo != null ? Utilities.XmlSerialize<APSQueryInfo>(adressInfo) : "null");
                }

                Logs.FileLog("", "ServiceResult");
                Logs.FileLog("", serviceResult != null ? Utilities.XmlSerialize<ServiceResult>(serviceResult) : "null");
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "APSSorgu");
                Logs.FileLog("", ex.ToString());
            }

            return strMsg;
        }
        #endregion

        #region KPSBilesikKutukSorgu
        public string KPSBilesikKutukSorgu(long intIdentityNo, ref KPSIdentityInfo KPSKimlikBilgi)
        {
            string strMsg = "Müşteri bulunamadı!";

            try
            {
                Logs.FileLog("KPSBilesikKutukSorgu", "[INPUT]");
                Logs.FileLog("", "pKimlikNo : " + intIdentityNo);

                LifeInServiceClient serviceClient = new LifeInServiceClient();

                ServiceResult serviceResult = serviceClient.KPSBilesikKutukSorgu(AuthenticatedString.Get, intIdentityNo, ref KPSKimlikBilgi);

                Logs.FileLog("", "[OUTPUT]");

                if (!serviceResult.IsOk)
                {
                    strMsg = Utilities.MessageClear(serviceResult.ErrorList, strMsg);

                    if (serviceResult.ErrorList[0] != null && serviceResult.ErrorList[0].Length > 0)
                    {
                        Logs.Exception(new Exception(serviceResult.ErrorList[0]), "KPSBilesikKutukSorgu");
                    }
                    else
                    {
                        Logs.Exception(new Exception(strMsg), "KPSBilesikKutukSorgu");
                    }
                }
                else
                {
                    strMsg = "";

                    Logs.FileLog("", "KPSKimlikBilgi");
                    Logs.FileLog("", KPSKimlikBilgi != null ? Utilities.XmlSerialize<KPSIdentityInfo>(KPSKimlikBilgi) : "null");
                }

                Logs.FileLog("", "ServiceResult");
                Logs.FileLog("", serviceResult != null ? Utilities.XmlSerialize<ServiceResult>(serviceResult) : "null");
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "KPSBilesikKutukSorgu");
                Logs.FileLog("", ex.ToString());
            }

            return strMsg;
        }
        #endregion

        #region LfKaydetMusteriKisa
        public string LfKaydetMusteriKisa(KPSIdentityInfo KPSKimlikBilgi, string strPhone, string strCityCode, string strDistrictKod, string strAddress, string strSemt, string strJobCode, string strCardNo, string strCardCvc, DateTime? dtCardDate, WsTenderLife.OBJ_MUST_TANIM_KISA_WS curMusteri, ref long intMustKod)
        {
            string strMsg = "Müşteri eklenemedi!";
            try
            {
                LifeInServiceClient serviceClient = new LifeInServiceClient();

                OBJ_MUST_TANIM_KISA_WS musteriKisaInput = new OBJ_MUST_TANIM_KISA_WS
                {
                    ANNE_ADI = KPSKimlikBilgi.AnneAd,
                    BABA_ADI = KPSKimlikBilgi.BabaAd,
                    CINSIYET = KPSKimlikBilgi.Cinsiyet,
                    DOGUM_TARIH = KPSKimlikBilgi.DogumTarih,
                    DOGUM_YERI = KPSKimlikBilgi.DogumYerIlKod,
                    ISIM1 = KPSKimlikBilgi.Ad,
                    ISIM2 = KPSKimlikBilgi.Soyad,
                    KIMLIK_NO = KPSKimlikBilgi.KimlikNo,
                    KIMLIK_TIP = KPSKimlikBilgi.Tip,
                    MEDENI_DURUM = KPSKimlikBilgi.MedeniHal,
                    MUST_IRTIBAT = new TAB_MUST_IRTIBAT_WS { Value = new OBJ_MUST_IRTIBAT_WS[1] { new OBJ_MUST_IRTIBAT_WS { ILETISIM_KOD = "CEP", ONCELIK_NO = 1, NUMARA = strPhone } } },
                    UYRUK_KOD = "TR",
                    T_G = "G",
                    VERILDIGI_YER = KPSKimlikBilgi.DogumYerIlKod,
                    MESLEK_KOD = strJobCode
                };

                if (intMustKod == 0)
                {
                    musteriKisaInput.ActionType = 1;
                    musteriKisaInput.ADRES = new TAB_MUST_ADRES_KISA_WS { Value = new OBJ_MUST_ADRES_KISA_WS[1] { new OBJ_MUST_ADRES_KISA_WS { ActionType = 1, IL_KOD = strCityCode, ILCE_KOD = strDistrictKod, ADRES = strAddress, SEMT = strSemt, YAZISMA_ADRES = "E", APS_ADRES = 0, ULKE_KODU = "TR" } } };
                }
                else
                {
                    int SQ_MUST_IRTIBAT_ID = curMusteri.MUST_IRTIBAT != null && curMusteri.MUST_IRTIBAT.Value != null && curMusteri.MUST_IRTIBAT.Value.Length > 0 && curMusteri.MUST_IRTIBAT.Value[0].SQ_MUST_IRTIBAT_ID.HasValue ? curMusteri.MUST_IRTIBAT.Value[0].SQ_MUST_IRTIBAT_ID.Value : 0;
                    musteriKisaInput.MUST_IRTIBAT.Value[0].SQ_MUST_IRTIBAT_ID = SQ_MUST_IRTIBAT_ID;
                    musteriKisaInput.MUST_IRTIBAT.Value[0].MUST_KOD = (int)intMustKod;
                    musteriKisaInput.MUST_IRTIBAT.Value[0].SIRKET_KOD = 1;

                    if (curMusteri != null && curMusteri.ADRES != null && curMusteri.ADRES.Value != null && curMusteri.ADRES.Value.Length > 0)
                    {
                        var adres = curMusteri.ADRES.Value.Where(w => w.APS_ADRES == 1).FirstOrDefault();
                        if (adres != null)
                        {
                            musteriKisaInput.ADRES = new TAB_MUST_ADRES_KISA_WS { Value = new OBJ_MUST_ADRES_KISA_WS[1] { new OBJ_MUST_ADRES_KISA_WS { APS_ADRES = 1, ActionType = 2, SQ_MUST_ADRES_ID = adres.SQ_MUST_ADRES_ID, IL_KOD = adres.IL_KOD, ILCE_KOD = adres.ILCE_KOD, ADRES = adres.ADRES, SEMT = strSemt, YAZISMA_ADRES = adres.YAZISMA_ADRES, ULKE_KODU = adres.ULKE_KODU } } };
                        }
                    }

                    musteriKisaInput.MUST_KOD = intMustKod;
                    musteriKisaInput.MUST_MALI = new TAB_MUST_MALI_WS
                    {
                        Value = new OBJ_MUST_MALI_WS[1]
                        {
                            new OBJ_MUST_MALI_WS
                            {
                                HESAP_SAHIBI_ISIM = KPSKimlikBilgi.Ad + " " + KPSKimlikBilgi.Soyad,
                                CVC_KOD = strCardCvc,
                                SON_KULLANMA_TARIH = dtCardDate,
                                HESAP_NO = strCardNo,
                                HESAP_TIP = "KRD",
                                ActionType = 1
                            }
                        }
                    };
                }

                Logs.FileLog("LfKaydetMusteriKisa", "[INPUT]");
                Logs.FileLog("", "customerStats : null");
                Logs.FileLog("", "musteriKisaInput" + Environment.NewLine);
                Logs.FileLog("", Utilities.XmlSerialize<OBJ_MUST_TANIM_KISA_WS>(musteriKisaInput));

                ServiceResult serviceResult = serviceClient.LfKaydetMusteriKisa(AuthenticatedString.Get, ref musteriKisaInput, null);

                //string ssteklif = Utilities.XmlSerialize<OBJ_MUST_TANIM_KISA_WS>(musteriKisaInput);
                Logs.FileLog("", "[OUTPUT]");

                if (!serviceResult.IsOk)
                {
                    strMsg = Utilities.MessageClear(serviceResult.ErrorList, strMsg);

                    if (serviceResult.ErrorList[0] != null && serviceResult.ErrorList[0].Length > 0)
                    {
                        Logs.Exception(new Exception(serviceResult.ErrorList[0]), "LfKaydetMusteriKisa");
                    }
                    else
                    {
                        Logs.Exception(new Exception(strMsg), "LfKaydetMusteriKisa");
                    }
                }
                else if (musteriKisaInput.MUST_KOD.HasValue)
                {
                    strMsg = "";
                    intMustKod = musteriKisaInput.MUST_KOD.Value;

                    Logs.FileLog("", "musteriKisaInput");
                    Logs.FileLog("", musteriKisaInput != null ? Utilities.XmlSerialize<OBJ_MUST_TANIM_KISA_WS>(musteriKisaInput) : "null");
                }

                Logs.FileLog("", "ServiceResult");
                Logs.FileLog("", serviceResult != null ? Utilities.XmlSerialize<ServiceResult>(serviceResult) : "null");
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "LfKaydetMusteriKisa");
                Logs.FileLog("", ex.ToString());
            }

            return strMsg;
        }
        #endregion
    }
}