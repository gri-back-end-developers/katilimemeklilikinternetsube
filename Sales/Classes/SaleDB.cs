﻿using Sales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sales.Classes
{
    public class SaleDB
    {
        DbEntities db;

        #region SaleInsert
        public int SaleInsert(string strQuestionCode)
        {
            using (db = new DbEntities())
            {
                tblSales sale = new tblSales
                {
                    dtRegisterDate = DateTime.Now,
                    strQuestionCode = strQuestionCode
                };

                db.tblSales.Add(sale);
                db.SaveChanges();

                return sale.intSaleID;
            }
        }
        #endregion

        #region SaleUpdate
        public void SaleUpdate(Enums.SaleUpdateType updateType, int intSaleID, string strOfferJson = "", string strMessage = "", bool bolMesafeliSatis = false, bool bolBilgilendirme = false, bool bolIletisim = false, bool bolSaglikBeyani = false, long? intMustKod = null, byte? intInstallment = null)
        {
            try
            {
                using (db = new DbEntities())
                {
                    tblSales sale = db.tblSales.Where(w => w.intSaleID == intSaleID).FirstOrDefault();

                    if (sale != null)
                    {
                        switch (updateType)
                        {
                            case Enums.SaleUpdateType.offer:
                                sale.strOfferJson = strOfferJson; 
                                break;
                            case Enums.SaleUpdateType.finished:
                                sale.strOfferJson = strOfferJson;
                                sale.bolIsFinished = true;
                                break;
                            case Enums.SaleUpdateType.checbox:
                                sale.bolBilgilendirme = bolBilgilendirme;
                                sale.bolIletisim = bolIletisim;
                                sale.bolMesafeliSatis = bolMesafeliSatis;
                                sale.bolSaglikBeyani = bolSaglikBeyani;
                                break;
                            case Enums.SaleUpdateType.message:
                                sale.strMessage = strMessage;
                                break;
                            case Enums.SaleUpdateType.installment:
                                sale.intInstallment = intInstallment;
                                break;
                        }

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "SalesDB-SaleUpdate");
            }
        }
        #endregion

        #region SaleLogInsert
        public int SaleLogInsert(int intSaleID, string strIdentityNo, bool bolDifferent)
        {
            try
            {
                using (db = new DbEntities())
                {
                    tblSaleLogs saleLogs = db.tblSaleLogs.Where(w => w.intSaleID == intSaleID && w.strIdentityNo == strIdentityNo && w.bolDifferent == bolDifferent).FirstOrDefault();

                    if (saleLogs == null)
                    {
                        saleLogs = new tblSaleLogs
                        {
                            intSaleID = intSaleID,
                            bolDifferent = bolDifferent,
                            strIdentityNo = strIdentityNo,
                            dtRegisterDate = DateTime.Now
                        };

                        db.tblSaleLogs.Add(saleLogs);
                        db.SaveChanges();
                    }

                    return saleLogs.intSaleLogID;
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "SalesDB-SaleLogInsert");
            }

            return 0;
        }
        #endregion

        #region SaleLogUpdate
        public bool SaleLogUpdate(int intSaleID, int intSaleLogID, string strIdentityNo, string strName = "", string strPhone = "", string strSurname = "", DateTime? dtBirhDate = null, int? intSaleJobID = null, string strCityCode = "", string strDistrictCode = "", string strAddress = "")
        {
            try
            {
                using (db = new DbEntities())
                {
                    tblSaleLogs saleLogs = db.tblSaleLogs.Where(w => w.intSaleID == intSaleID && w.intSaleLogID == intSaleLogID).FirstOrDefault();
                    if (saleLogs != null)
                    {
                        if (!saleLogs.bolDifferent)
                        {
                            int intAge = DateTime.Now.Year - dtBirhDate.Value.Year;

                            tblSalePrices salePrices = db.tblSalePrices.Where(w => w.intStartAge <= intAge && w.intFinishAge >= intAge).FirstOrDefault();
                            if (salePrices != null)
                            {
                                saleLogs.tblSales.intSalePriceID = salePrices.intSalePriceID;
                            }
                        }

                        saleLogs.dtBirhDate = dtBirhDate;
                        saleLogs.intSaleID = intSaleID;
                        saleLogs.strIdentityNo = strIdentityNo;
                        saleLogs.intSaleJobID = intSaleJobID;
                        saleLogs.strName = strName;
                        saleLogs.strPhone = strPhone;
                        saleLogs.strSurname = strSurname;
                        saleLogs.strCityCode = strCityCode;
                        saleLogs.strDistrictCode = strDistrictCode;
                        saleLogs.strAddress = strAddress;
                        saleLogs.dtUpdateDate = DateTime.Now;

                        db.SaveChanges();

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "SalesDB-SaleLogUpdate");
            }

            return false;
        }
        #endregion

        #region SaleLogUpdateMusteriKod
        public void SaleLogUpdateMusteriKod(int intSaleID, bool bolDifferent, long intMustKod)
        {
            using (db = new DbEntities())
            {
                tblSales sale = db.tblSales.Where(w => w.intSaleID == intSaleID).FirstOrDefault();

                if (sale != null)
                {
                    tblSaleLogs saleLogs = sale.tblSaleLogs.Where(w => w.bolDifferent == bolDifferent).FirstOrDefault();
                    if (saleLogs != null)
                    {
                        saleLogs.intMustKod = intMustKod;

                        db.SaveChanges();
                    }
                }
            }
        }
        #endregion

        #region SaleLogUpdateKPS
        public void SaleLogUpdateKPS(int intSaleID, bool bolDifferent, string strFather, string strMother, string strGender, string strMaritalStatus, string strIdentityType)
        {
            using (db = new DbEntities())
            {
                tblSales sale = db.tblSales.Where(w => w.intSaleID == intSaleID).FirstOrDefault();

                if (sale != null)
                {
                    tblSaleLogs saleLogs = sale.tblSaleLogs.Where(w => w.bolDifferent == bolDifferent).FirstOrDefault();
                    if (saleLogs != null)
                    {
                        saleLogs.strFather = strFather;
                        saleLogs.strMother = strMother;
                        saleLogs.strGender = strGender;
                        saleLogs.strMaritalStatus = strMaritalStatus;
                        saleLogs.strIdentityType = strIdentityType; 

                        db.SaveChanges();
                    }
                }
            }
        }
        #endregion

        #region GetSaleLog
        public tblSaleLogs GetSaleLog(int intSaleID, bool bolDifferent)
        {
            try
            {
                db = new DbEntities();

                return db.tblSaleLogs.Where(w => w.intSaleID == intSaleID && w.bolDifferent == bolDifferent).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "SalesDB-GetSaleLog");
            }

            return null;
        }
        #endregion

        #region GetSale
        public tblSales GetSale(int intSaleID)
        {
            try
            {
                db = new DbEntities();
                return db.tblSales.Where(w => w.intSaleID == intSaleID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "SalesDB-GetSale");
            }

            return null;
        }
        #endregion

        #region GetSaleJobs
        public static List<tblSalesJob> GetSaleJobs()
        {
            try
            {
                using (DbEntities dbE = new DbEntities())
                {
                    return dbE.tblSalesJob.ToList();
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "SalesDB-GetSaleJobs");
            }

            return new List<tblSalesJob>();
        }

        public tblSalesJob GetSaleJobs(int intSaleJobID)
        {
            try
            {
                using (DbEntities dbE = new DbEntities())
                {
                    return dbE.tblSalesJob.Where(w => w.intSaleJobID == intSaleJobID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "SalesDB-GetSaleJobs");
            }

            return null;
        }
        #endregion
    }
}