﻿using Sales.WsTenderLife;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Sales.Classes
{
    public class TenderLifeMethods
    {
        #region SaglikBeyanSoruGetir
        public TenderHealthStatement[] SaglikBeyanSoruGetir()
        {
            TenderHealthStatement[] saglikSoruListe = null;
            try
            {
                TenderLifeServiceClient tenderLife = new TenderLifeServiceClient();

                long? teklifNo = null;
                string tarifeKod = ConfigurationManager.AppSettings["tarifeKod"].ToString();
                string sozlesmeNo = "";
                DateTime? sozBasTar = null;
                long mustKod = 0;
                DateTime policeBasTar = DateTime.Now;

                Logs.FileLog("SaglikBeyanSoruGetir", "[INPUT]");
                Logs.FileLog("", "teklifNo : " + teklifNo + Environment.NewLine + "tarifeKod : " + tarifeKod + Environment.NewLine + "sozlesmeNo : " + sozlesmeNo + Environment.NewLine + "sozBasTar : " + sozBasTar + Environment.NewLine + "mustKod : " + mustKod + Environment.NewLine + "policeBasTar : " + policeBasTar);

                ServiceResult serviceResult = tenderLife.SaglikBeyanSoruGetir(AuthenticatedString.Get, teklifNo, tarifeKod, sozlesmeNo, sozBasTar, mustKod, policeBasTar, ref saglikSoruListe);

                Logs.FileLog("", "[OUTPUT]");

                if (!serviceResult.IsOk)
                {
                    string strMsg = Utilities.MessageClear(serviceResult.ErrorList, "");

                    if (serviceResult.ErrorList[0] != null && serviceResult.ErrorList[0].Length > 0)
                    {
                        Logs.Exception(new Exception(serviceResult.ErrorList[0]), "SaglikBeyanSoruGetir");
                    }
                    else
                    {
                        Logs.Exception(new Exception(strMsg), "SaglikBeyanSoruGetir");
                    }
                }
                else
                {
                    Logs.FileLog("", "saglikSoruListe");
                    Logs.FileLog("", saglikSoruListe != null ? Utilities.XmlSerialize<TenderHealthStatement[]>(saglikSoruListe) : "null");
                }

                Logs.FileLog("", "ServiceResult");
                Logs.FileLog("", serviceResult != null ? Utilities.XmlSerialize<ServiceResult>(serviceResult) : "null");
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "SaglikBeyanSoruGetir");
                Logs.FileLog("", ex.ToString());
            }

            return saglikSoruListe;
        }
        #endregion

        #region LfGetMusteriKisa
        public string LfGetMusteriKisa(string strIdentityNo, ref OBJ_MUST_TANIM_KISA_WS curMusteri, ref long musteriKod)
        {
            string strMsg = "Müşteri bulunamadı!";
            try
            {
                TenderLifeServiceClient tenderLife = new TenderLifeServiceClient();

                TAB_MUST_IST customerStats = null;
                musteriKod = 0;

                Logs.FileLog("LfGetMusteriKisa", "[INPUT]");
                Logs.FileLog("", "pKaynakKod : 1" + Environment.NewLine + "pKimlikNo : " + strIdentityNo);

                ServiceResult serviceResult = tenderLife.LfGetMusteriKisa(AuthenticatedString.Get, 1, ref musteriKod, strIdentityNo, ref curMusteri, ref customerStats);

                Logs.FileLog("", "[OUTPUT]");

                if (!serviceResult.IsOk)
                {
                    strMsg = Utilities.MessageClear(serviceResult.ErrorList, strMsg);

                    if (serviceResult.ErrorList[0] != null && serviceResult.ErrorList[0].Length > 0)
                    {
                        Logs.Exception(new Exception(serviceResult.ErrorList[0]), "LfGetMusteriKisa");
                    }
                    else
                    {
                        Logs.Exception(new Exception(strMsg), "PrimHesapla");
                    }
                }
                else
                {
                    strMsg = "";

                    Logs.FileLog("", "curMusteri");
                    Logs.FileLog("", curMusteri != null ? Utilities.XmlSerialize<OBJ_MUST_TANIM_KISA_WS>(curMusteri) : "null");

                    Logs.FileLog("", "customerStats");
                    Logs.FileLog("", customerStats != null ? Utilities.XmlSerialize<TAB_MUST_IST>(customerStats) : "null");
                }

                Logs.FileLog("", "serviceResult");
                Logs.FileLog("", serviceResult != null ? Utilities.XmlSerialize<ServiceResult>(serviceResult) : "null");
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "LfGetMusteriKisa");
                Logs.FileLog("", ex.ToString());
            }

            return strMsg;
        }
        #endregion

        #region LfTeklifPck_SpTeklifIst
        public TenderStatistic[] LfTeklifPck_SpTeklifIst()
        {
            TenderStatistic[] pCurIst = null;

            try
            {
                Logs.FileLog("LfTeklifPck_SpTeklifIst", "[INPUT]");
                Logs.FileLog("", "tarifeKod : " + ConfigurationManager.AppSettings["tarifeKod"].ToString() + Environment.NewLine + "sigortaliNo : 0 " + Environment.NewLine + "sozlesmeNo : " + Environment.NewLine + "boy : 1" + Environment.NewLine + "kilo : 1" + Environment.NewLine + "policeBasTar : " + DateTime.Now.ToString() + Environment.NewLine + "sozBasTar : null" + Environment.NewLine + "teklifNo : null");

                TenderLifeServiceClient tenderLife = new TenderLifeServiceClient();

                ServiceResult serviceResult = tenderLife.LfTeklifPck_SpTeklifIst(AuthenticatedString.Get, ConfigurationManager.AppSettings["tarifeKod"].ToString(), 0, "", 1, 1, DateTime.Now, null, null, ref pCurIst);

                Logs.FileLog("", "[OUTPUT]");
                Logs.FileLog("", "customerStats");
                Logs.FileLog("", pCurIst != null ? Utilities.XmlSerialize<TenderStatistic[]>(pCurIst) : "null");
                Logs.FileLog("", "serviceResult");
                Logs.FileLog("", serviceResult != null ? Utilities.XmlSerialize<ServiceResult>(serviceResult) : "null");
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "LfTeklifPck_SpTeklifIst");
                Logs.FileLog("", ex.ToString());
            }

            return pCurIst;
        }
        #endregion

        #region PrimHesapla
        public string PrimHesapla(int intSaleID, decimal teminatBedel, string teminatKod, ref decimal netPrim)
        {
            string strMsg = "Prim hesaplanamadı!";
            try
            {
                SaleDB saleDB = new SaleDB();
                Models.tblSales sales = saleDB.GetSale(intSaleID);

                TenderLifeServiceClient tenderLife = new TenderLifeServiceClient();

                long? teklifNo = null;
                string tarifeKod = ConfigurationManager.AppSettings["tarifeKod"].ToString();
                string sozlesmeNo = "";
                DateTime? sozBasTar = null;
                long mustKod = sales.tblSaleLogs.First(f => !f.bolDifferent).intMustKod.Value;
                DateTime policeBasTar = DateTime.Now;
                int policeSure = Utilities.NullFixInt(ConfigurationManager.AppSettings["policeSure"].ToString());
                int primOdemeSuresi = Utilities.NullFixInt(ConfigurationManager.AppSettings["primOdemeSuresi"].ToString());
                int odemePeriyot = Utilities.NullFixInt(ConfigurationManager.AppSettings["odemePeriyot"].ToString());
                string policeTaksitTip = ConfigurationManager.AppSettings["policeTaksitTip"].ToString();
                int acenteKaynak = Utilities.NullFixInt(ConfigurationManager.AppSettings["acenteKaynak"].ToString());
                long acenteNo = Utilities.NullFixLong(ConfigurationManager.AppSettings["acenteNo"].ToString());

                List<BNKWS_TEKLIF_IST> istatistiklervalue = new List<BNKWS_TEKLIF_IST>();

                TenderStatistic[] tenderStatistic = LfTeklifPck_SpTeklifIst();
                foreach (var item in tenderStatistic)
                {
                    istatistiklervalue.Add(new BNKWS_TEKLIF_IST
                    {
                        DEGER_KOD = item.DegerKod,
                        IST_KOD = item.IstKod
                    });
                }

                BNKWS_TEKLIF_IST_TYP istatistikler = new BNKWS_TEKLIF_IST_TYP();
                istatistikler.Value = istatistiklervalue.ToArray();

                BNKWS_BEDEL_TABLO_TYP bedeller = new BNKWS_BEDEL_TABLO_TYP();

                BNKWS_BEDEL_TABLO[] bedelValue = new BNKWS_BEDEL_TABLO[2];
                bedelValue[0] = new BNKWS_BEDEL_TABLO()
                {
                    AY = null,
                    BEDEL = teminatBedel,
                    PRIM = null,
                    TEMINAT = teminatKod,
                    YIL = 1
                };

                bedeller.Value = bedelValue;

                BNKWS_PRIM_DETAY_TYP primler = null;
                netPrim = 0;

                Logs.FileLog("PrimHesapla", "[INPUT]");
                Logs.FileLog("", "teklifNo : " + teklifNo + Environment.NewLine + "tarifeKod : " + tarifeKod + Environment.NewLine + "sozlesmeNo : " + sozlesmeNo + Environment.NewLine + "sozBasTar : " + sozBasTar + Environment.NewLine + "mustKod : " + mustKod + Environment.NewLine + "policeBasTar : " + policeBasTar + "policeSure : " + policeSure + Environment.NewLine + "primOdemeSuresi : " + primOdemeSuresi + Environment.NewLine + "odemePeriyot : " + odemePeriyot + Environment.NewLine + "teminatBedel : " + teminatBedel + Environment.NewLine + "teminatKod : " + teminatKod + Environment.NewLine + "policeTaksitTip : " + policeTaksitTip + Environment.NewLine + "acenteKaynak : " + acenteKaynak + Environment.NewLine + "acenteNo : " + acenteNo);
                Logs.FileLog("", "istatistikler");
                Logs.FileLog("", Utilities.XmlSerialize<BNKWS_TEKLIF_IST_TYP>(istatistikler));
                Logs.FileLog("", "bedeller");
                Logs.FileLog("", Utilities.XmlSerialize<BNKWS_BEDEL_TABLO_TYP>(bedeller));

                ServiceResult serviceResult = tenderLife.PrimHesapla(AuthenticatedString.Get, teklifNo, tarifeKod, sozlesmeNo, sozBasTar, mustKod, policeBasTar, policeSure, primOdemeSuresi, odemePeriyot, teminatBedel, teminatKod, policeTaksitTip, acenteKaynak, acenteNo, istatistikler, bedeller, ref primler, ref netPrim);

                Logs.FileLog("", "[OUTPUT]");

                if (!serviceResult.IsOk)
                {
                    strMsg = Utilities.MessageClear(serviceResult.ErrorList, strMsg);

                    if (serviceResult.ErrorList[0] != null && serviceResult.ErrorList[0].Length > 0)
                    {
                        Logs.Exception(new Exception(serviceResult.ErrorList[0]), "PrimHesapla");
                    }
                    else
                    {
                        Logs.Exception(new Exception(strMsg), "PrimHesapla");
                    }
                }
                else
                {
                    strMsg = "";

                    Logs.FileLog("", "primler");
                    Logs.FileLog("", Utilities.XmlSerialize<BNKWS_PRIM_DETAY_TYP>(primler));
                    Logs.FileLog("", "netPrim : " + netPrim);
                }

                Logs.FileLog("", "serviceResult");
                Logs.FileLog("", Utilities.XmlSerialize<ServiceResult>(serviceResult));
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "PrimHesapla");
                Logs.FileLog("", ex.ToString());
            }

            return strMsg;
        }
        #endregion

        #region TeklifKaydet
        public string TeklifKaydet(int intSaleID, string strInstallment, string strCvv, string strCardNo, DateTime dtEndDate, ref BNKWS_POLICELESME_RES_TYP teklifCevap)
        {
            string strMsg = "Teklif oluştulamadı!";
            try
            {
                SaleDB saleDB = new SaleDB();
                Models.tblSales sales = saleDB.GetSale(intSaleID);

                var salesLog = sales.tblSaleLogs.Where(w => !w.bolDifferent).First();
                var salesDifferent = sales.tblSaleLogs.Where(w => w.bolDifferent).FirstOrDefault();

                TenderLifeServiceClient tenderLife = new TenderLifeServiceClient();

                string tarifeKod = ConfigurationManager.AppSettings["tarifeKod"].ToString();
                string sozlesmeNo = "";
                DateTime? sozBasTar = null;

                BNKWS_DAGITIM_KANAL_REQ_TYP dagitimKanali = new BNKWS_DAGITIM_KANAL_REQ_TYP
                {
                    Value = new BNKWS_DAGITIM_KANAL_REQ[1]
                    {
                        new BNKWS_DAGITIM_KANAL_REQ
                        {
                            ACENTEKAYNAK = Utilities.NullFixInt(ConfigurationManager.AppSettings["acenteKaynak"].ToString()),
                            ACENTENO = Utilities.NullFixLong(ConfigurationManager.AppSettings["acenteNo"].ToString()),
                            REFERANSKAYNAK = Utilities.NullFixDecimal(ConfigurationManager.AppSettings["REFERANSKAYNAK"].ToString()),
                        }
                    }
                };

                TAB_HYT_TEKLIF_MENFAATTAR menfaattar = new TAB_HYT_TEKLIF_MENFAATTAR
                {
                    Value = new OBJ_HYT_TEKLIF_MENFAATTAR[1]
                    {
                        new OBJ_HYT_TEKLIF_MENFAATTAR
                        {
                            MENFAATTARTIP = ConfigurationManager.AppSettings["MENFAATTARTIP"].ToString()
                        }
                    }
                };

                int odeyenKaynak = Utilities.NullFixInt(ConfigurationManager.AppSettings["odeyenKaynak"].ToString());
                long odeyenNo = salesDifferent != null ? salesDifferent.intMustKod.Value : salesLog.intMustKod.Value;
                TenderHealthStatement[] tenderHealth = SaglikBeyanSoruGetir();

                List<BNKWS_TEKLIF_IST> istatistiklervalue = new List<BNKWS_TEKLIF_IST>();

                TenderStatistic[] tenderStatistic = LfTeklifPck_SpTeklifIst();
                foreach (var item in tenderStatistic)
                {
                    istatistiklervalue.Add(new BNKWS_TEKLIF_IST
                    {
                        DEGER_KOD = item.DegerKod,
                        IST_KOD = item.IstKod
                    });
                }

                BNKWS_POLICELESME_REQ_TYP teklif = new BNKWS_POLICELESME_REQ_TYP
                {
                    Value = new BNKWS_POLICELESME_REQ[1]
                    {
                        new BNKWS_POLICELESME_REQ
                        {
                            ARACI = new BNKWS_ARACI_REQ_TYP
                            {
                                Value = new BNKWS_ARACI_REQ[1]
                                {
                                    new BNKWS_ARACI_REQ { }
                                }
                            },
                            ADRES = new BNKWS_OZLUK_ADRES_REQ_TYP
                            {
                                Value = new BNKWS_OZLUK_ADRES_REQ[]
                                {
                                    new BNKWS_OZLUK_ADRES_REQ
                                    {
                                        ADRES = salesLog.strAddress,
                                        ILCE_KOD = salesLog.strDistrictCode,
                                        IL_KOD = salesLog.strCityCode,
                                        ILETISIM_NO = salesLog.strPhone,
                                        ILETISIM_TIP = "CEP",
                                        ULKE_KOD = "TR"
                                    }
                                }
                            },
                            BEYAN = new BNKWS_SAGLIK_BEYAN_REQ_TYP
                            {
                                Value = new BNKWS_SAGLIK_BEYAN_REQ[1]
                                {
                                     new BNKWS_SAGLIK_BEYAN_REQ
                                     {
                                         SORU_ACIKLAMA = tenderHealth.First(f => f.BeyanKod == sales.strQuestionCode).Soru,
                                         CEVAP = sales.bolSaglikBeyani ? "E" : "H",
                                         SORU_KOD = sales.strQuestionCode
                                     }
                                }
                            },
                            ISTATISTIK = new BNKWS_TEKLIF_IST_TYP
                            {
                                Value = istatistiklervalue.ToArray()
                            },
                            KREDI = new BNKWS_KREDI_REQ_TYP
                            {
                                 Value = new BNKWS_KREDI_REQ[]
                                 {
                                     new BNKWS_KREDI_REQ
                                     {
                                         KREDI_DOVIZ_TIP = "TL",
                                         KREDI_SURE = 1,
                                         KREDI_TUTAR = Utilities.NullFixDecimal(ConfigurationManager.AppSettings["teminatBedel_1"].ToString()),
                                         POLICE_BAS_TAR = DateTime.Now,
                                         POLICE_TIP = "HYT",
                                         POLICE_UW = "H",
                                         YILLIK_KREDI_TAKSIT = new BNKWS_BEDEL_TABLO_TYP
                                         {
                                             Value = new BNKWS_BEDEL_TABLO[2]
                                             {
                                                 new BNKWS_BEDEL_TABLO
                                                 {
                                                     BEDEL = Utilities.NullFixDecimal(ConfigurationManager.AppSettings["teminatBedel_1"].ToString()),
                                                     TEMINAT = ConfigurationManager.AppSettings["teminatKod_1"].ToString(),
                                                     YIL = 1
                                                 },
                                                 new BNKWS_BEDEL_TABLO
                                                 {
                                                     BEDEL = Utilities.NullFixDecimal(ConfigurationManager.AppSettings["teminatBedel_2"].ToString()),
                                                     TEMINAT = ConfigurationManager.AppSettings["teminatKod_2"].ToString(),
                                                     YIL = 1
                                                 }
                                             }
                                         }
                                     }
                                 }
                            },
                            ODEME = new BNKWS_ODEME_REQ_TYP
                            {
                                Value = new BNKWS_ODEME_REQ[]
                                {
                                    new BNKWS_ODEME_REQ
                                    {
                                        CVC_KOD = strCvv,
                                        HESAP_NO = strCardNo,
                                        HESAP_TIP = "KRD",
                                        PRIM_ODEME_PERIYOD = Utilities.NullFixDecimal(ConfigurationManager.AppSettings["odemePeriyot"].ToString()),
                                        PRIM_ODEME_SURE = Utilities.NullFixDecimal(ConfigurationManager.AppSettings["primOdemeSuresi"].ToString()),
                                        SON_KULLANMA_TARIH = dtEndDate,
                                        TAHSILAT_KOD = ConfigurationManager.AppSettings["tahsilatkod_" + strInstallment].ToString()
                                    }
                                }
                            },
                            OZLUK = new BNKWS_OZLUK_REQ_TYP
                            {
                                Value = new BNKWS_OZLUK_REQ[1]
                                {
                                    new BNKWS_OZLUK_REQ
                                    {
                                        AD = salesLog.strName,
                                        BABA_AD = salesLog.strFather,
                                        BOY = 1,
                                        CINSIYET = salesLog.strGender,
                                        DOGUM_TARIH = salesLog.dtBirhDate,
                                        KILO = 1,
                                        KIMLIK_NO = salesLog.strIdentityNo,
                                        KIMLIK_TIP = salesLog.strIdentityType,
                                        MEDENI_DURUM = salesLog.strMaritalStatus,
                                        MESLEK_KOD = salesLog.tblSalesJob.strCode,
                                        MUSTERI_NO = salesLog.intMustKod.ToString(),
                                        SOYAD = salesLog.strSurname,
                                        UYRUK = "TR"
                                    }
                                }
                            }
                        }
                    }

                };

                BNKWS_SORU_REQ_TYP cevaplar = null;
                long referansNo = 0;

                //string ssteklif = Utilities.XmlSerialize<BNKWS_POLICELESME_REQ_TYP>(teklif);
                //string ssdag = Utilities.XmlSerialize<BNKWS_DAGITIM_KANAL_REQ_TYP>(dagitimKanali);
                //string ssmen = Utilities.XmlSerialize<TAB_HYT_TEKLIF_MENFAATTAR>(menfaattar);

                Logs.FileLog("TeklifKaydet", "[INPUT]");
                Logs.FileLog("", "tarifeKod : " + tarifeKod + Environment.NewLine + "sozlesmeNo : " + sozlesmeNo + Environment.NewLine + "sozBasTar : " + sozBasTar + Environment.NewLine + "odeyenKaynak : " + odeyenKaynak + Environment.NewLine + "odeyenNo : " + odeyenNo);
                Logs.FileLog("", "teklif");
                Logs.FileLog("", Utilities.XmlSerialize<BNKWS_POLICELESME_REQ_TYP>(teklif));
                Logs.FileLog("", "dagitimKanali");
                Logs.FileLog("", Utilities.XmlSerialize<BNKWS_DAGITIM_KANAL_REQ_TYP>(dagitimKanali));
                Logs.FileLog("", "menfaattar");
                Logs.FileLog("", Utilities.XmlSerialize<TAB_HYT_TEKLIF_MENFAATTAR>(menfaattar));

                ServiceResult serviceResult = tenderLife.TeklifKaydet(AuthenticatedString.Get, tarifeKod, sozlesmeNo, sozBasTar, dagitimKanali, menfaattar, odeyenKaynak, odeyenNo, teklif, ref teklifCevap, ref cevaplar, ref referansNo);

                Logs.FileLog("", "[OUTPUT]");

                if (!serviceResult.IsOk)
                {
                    strMsg = Utilities.MessageClear(serviceResult.ErrorList, strMsg);

                    if (serviceResult.ErrorList[0] != null && serviceResult.ErrorList[0].Length > 0)
                    {
                        Logs.Exception(new Exception(serviceResult.ErrorList[0]), "TeklifKaydet");
                    }
                    else
                    {
                        Logs.Exception(new Exception(strMsg), "PrimHesapla");
                    }
                }
                else
                {
                    strMsg = "";

                    Logs.FileLog("", "teklifCevap");
                    Logs.FileLog("", teklifCevap != null ? Utilities.XmlSerialize<BNKWS_POLICELESME_RES_TYP>(teklifCevap) : "null");
                    Logs.FileLog("", "cevaplar");
                    Logs.FileLog("", cevaplar != null ? Utilities.XmlSerialize<BNKWS_SORU_REQ_TYP>(cevaplar) : "null");
                    Logs.FileLog("", "referansNo : " + referansNo);
                }

                Logs.FileLog("", "ServiceResult");
                Logs.FileLog("", serviceResult != null ? Utilities.XmlSerialize<ServiceResult>(serviceResult) : "null");
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "LfKaydetMusteriKisa");
                Logs.Exception(ex, "TeklifKaydet");
            }

            return strMsg;
        }
        #endregion
    }
}