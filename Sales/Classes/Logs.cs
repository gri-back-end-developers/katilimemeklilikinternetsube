﻿using Sales.Models;
using System;
using System.IO;
using Newtonsoft.Json;
using System.Web;

namespace Sales.Classes
{
    public class Logs
    {
        static readonly object _FileLog = new object();

        #region Exception
        public static void Exception(Exception ex, string strPage)
        {
            try
            {
                using (DbEntities db = new DbEntities())
                {
                    db.tblSaleExceptions.Add(new tblSaleExceptions
                    {
                        dtRegisterDate = DateTime.Now,
                        strMessage = ex.ToString(),
                        strPage = strPage,
                        strIdentityNo = Sessions.IdentityNo
                    });
                    db.SaveChanges();
                }
            }
            catch { }
        }
        #endregion

        #region FileLog
        public static void FileLog(string strTitle, string oInput)
        {
            lock (_FileLog)
            {
                if (System.Configuration.ConfigurationManager.AppSettings["errortxt"].ToString() == "1")
                {
                    try
                    {
                        using (FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("~/Logs/Services.txt"), FileMode.Append))
                        {
                            using (StreamWriter w = new StreamWriter(fs, System.Text.Encoding.UTF8))
                            {
                                if (!string.IsNullOrEmpty(strTitle))
                                {
                                    w.WriteLine("---------------------------- " + strTitle + " ----------------------------");
                                    w.WriteLine(Environment.NewLine);
                                }

                                w.WriteLine(oInput);
                                w.WriteLine(Environment.NewLine);
                                w.Dispose();
                            }
                            fs.Dispose();
                        }
                    }
                    catch { }
                }
            }
        }
        #endregion

        #region FileLogClear
        public static void FileLogClear()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["errortxt"].ToString() == "1")
            {
                try
                {
                    using (FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("~/Logs/Services.txt"), FileMode.Create))
                    {
                        using (StreamWriter w = new StreamWriter(fs, System.Text.Encoding.UTF8))
                        {
                            w.Write("");
                        }
                    }
                }
                catch { }
            }
        }
        #endregion
    }
}