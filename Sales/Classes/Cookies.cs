﻿using System;
using System.Web;

namespace Sales.Classes
{
    public class Cookies
    {
        public static int SaleID
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["SaleID"].Value;
                    if (objValue == null)
                    {
                        return 0;
                    }
                    return Utilities.NullFixInt(objValue.ToString());
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("SaleID");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }

        public static void RemoveSaleID()
        {
            HttpContext.Current.Response.Cookies.Remove("SaleID");
            HttpContext.Current.Response.Cookies["SaleID"].Expires = DateTime.Now.AddDays(-1);
        }
    }
}