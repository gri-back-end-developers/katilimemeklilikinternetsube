﻿using System.Web;

namespace Sales.Classes
{
    public class Sessions
    {
        #region SaleID
        public static int SaleID
        {
            get
            {
                if (HttpContext.Current.Session["SaleID"] == null)
                {
                    int intSaleID = Cookies.SaleID;
                    if (intSaleID > 0)
                    {
                        HttpContext.Current.Session["SaleID"] = intSaleID;
                    }

                    return intSaleID;
                }
                else
                {
                    return Utilities.NullFixInt(HttpContext.Current.Session["SaleID"].ToString());
                }
            }
            set
            {
                Cookies.SaleID = value;

                HttpContext.Current.Session["SaleID"] = value;
            }
        }
        #endregion 

        #region IdentityNo
        public static string IdentityNo
        {
            get
            {
                if (HttpContext.Current.Session["IdentityNo"] == null)
                { 
                    return "";
                }
                else
                {
                    return HttpContext.Current.Session["IdentityNo"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["IdentityNo"] = value;
            }
        }
        #endregion 
    }
}