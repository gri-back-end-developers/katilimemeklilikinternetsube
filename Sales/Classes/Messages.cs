﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Sales.Classes
{
    public class Messages
    {
        public static string Error
        {
            get
            {
                return ConfigurationManager.AppSettings["errormsg"].ToString();
            }
        }

        public static string Kps
        {
            get
            {
                return ConfigurationManager.AppSettings["kpsmsg"].ToString();
            }
        }

        public static string KpsDifferent
        {
            get
            {
                return ConfigurationManager.AppSettings["kpsdifferentmsg"].ToString();
            }
        }

        public static string Gender
        {
            get
            {
                return ConfigurationManager.AppSettings["gendermsg"].ToString();
            }
        }
    }
}