﻿using Sales.WsSecurity;
using System.Configuration;

namespace Sales.Classes
{
    public class AuthenticatedString
    {
        public static string Get
        {
            get
            {
                string strAuthenticatedString = Caches.AuthenticatedString;
                if (string.IsNullOrEmpty(strAuthenticatedString)) 
                { 
                    using (SecurityServiceClient wsSecurity = new SecurityServiceClient())
                    {
                        var authenticationKey = wsSecurity.GetAuthenticationKey(ConfigurationManager.AppSettings["appSecurityKey"], ConfigurationManager.AppSettings["authUserName"], ConfigurationManager.AppSettings["authPassword"], ref strAuthenticatedString);

                        if (authenticationKey.IsOk && !string.IsNullOrEmpty(strAuthenticatedString))
                        {
                            Caches.AuthenticatedString = strAuthenticatedString;
                        }
                    }
                } 

                return strAuthenticatedString;
            }
        }
    }
}