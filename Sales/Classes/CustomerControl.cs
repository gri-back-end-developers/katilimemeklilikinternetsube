﻿using System;
using System.Linq;

namespace Sales.Classes
{
    public class CustomerControl
    {
        public bool Insert(long intMustKod, int intSaleID, bool bolDiffrent, string strIdentityNo, string strPhone, string strJobCode, string strCardNo, string strCardCvc, DateTime? dtCardDate, ref string strMessage, string strNameSurname, DateTime? dtBirthDate, ref string strGender, ref string strCityCode, ref string strDistrictCode, ref string strAddress)
        {
            bool bolNext = false;
            try
            {
                SaleDB saleDB = new SaleDB();

                TenderLifeMethods tenderLifeMethods = new TenderLifeMethods();
                TenderServiceMethods tenderServiceMethods = new TenderServiceMethods();

                WsTenderLife.OBJ_MUST_TANIM_KISA_WS curMusteri = null;
                WsTenderService.APSQueryInfo adressInfo = null;
                long musteriKod = 0;

                string getMusteriKisa = tenderLifeMethods.LfGetMusteriKisa(strIdentityNo, ref curMusteri, ref musteriKod);
                WsTenderService.KPSIdentityInfo KPSKimlikBilgi = null;

                string kpsBilesikKutukSorgu = tenderServiceMethods.KPSBilesikKutukSorgu(Utilities.NullFixLong(strIdentityNo), ref KPSKimlikBilgi);

                string strKpsNameSurname = "";
                DateTime? dtKpsBirthDate = null;
                string strKimlikTip = "";

                if (curMusteri != null)
                {
                    strKpsNameSurname = curMusteri.ISIM1.ToLower() + " " + curMusteri.ISIM2.ToLower();
                    dtKpsBirthDate = curMusteri.DOGUM_TARIH;
                    strGender = curMusteri.CINSIYET;
                    strKimlikTip = curMusteri.KIMLIK_TIP;
                }
                else if (KPSKimlikBilgi != null)
                {
                    strKpsNameSurname = KPSKimlikBilgi.Ad.ToLower() + " " + KPSKimlikBilgi.Soyad.ToLower();
                    dtKpsBirthDate = KPSKimlikBilgi.DogumTarih;
                    strGender = KPSKimlikBilgi.Cinsiyet;
                    strKimlikTip = curMusteri.KIMLIK_TIP;
                }

                if (strKimlikTip != "KNO")
                {
                    strMessage = Messages.Kps;

                    Logs.Exception(new Exception(strMessage), "pay-post");
                }
                else
                {
                    if (strNameSurname.ToLower().Trim() != strKpsNameSurname.Trim() || dtBirthDate != dtKpsBirthDate)
                    {
                        if (bolDiffrent)
                        {
                            strMessage = Messages.KpsDifferent;
                        }
                        else
                        {
                            strMessage = Messages.Kps;
                        }

                        Logs.Exception(new Exception(strMessage), "pay-post");
                    }
                    else
                    {
                        if (kpsBilesikKutukSorgu == "" && KPSKimlikBilgi != null)
                        {
                            saleDB.SaleLogUpdateKPS(intSaleID, bolDiffrent, KPSKimlikBilgi.BabaAd, KPSKimlikBilgi.AnneAd, KPSKimlikBilgi.Cinsiyet, KPSKimlikBilgi.MedeniHal, KPSKimlikBilgi.Tip);

                            if (bolDiffrent && getMusteriKisa == "" && curMusteri != null)
                            {
                                intMustKod = curMusteri.MUST_KOD.Value;
                            }

                            if (intMustKod > 0 || getMusteriKisa != "" || curMusteri == null)
                            {
                                strMessage = getMusteriKisa;

                                string apsSorgu = tenderServiceMethods.APSSorgu(strIdentityNo, ref adressInfo);
                                if (apsSorgu != "" || adressInfo == null)
                                {
                                    strMessage = apsSorgu;
                                }
                                else
                                {
                                    strCityCode = adressInfo.ILKODU;
                                    strDistrictCode = adressInfo.ILCEKODU;
                                    strAddress = adressInfo.ACIKADRES;

                                    bolNext = true;
                                    bool bolUpdate = true;
                                    if (intMustKod > 0)
                                    {
                                        if (curMusteri.MUST_MALI != null && curMusteri.MUST_MALI.Value != null && curMusteri.MUST_MALI.Value.Length > 0 && strCardNo.Length > 4)
                                        {
                                            if (curMusteri.MUST_MALI.Value.ToList().Count(c => !string.IsNullOrEmpty(c.HESAP_NO) && c.HESAP_NO.Length > 4 && c.HESAP_NO.Substring(0, 4) == strCardNo.Substring(0, 4) && c.HESAP_NO.Substring(c.HESAP_NO.Length - 4, 4) == strCardNo.Substring(strCardNo.Length - 4, 4)) > 0)
                                            {
                                                bolUpdate = false;
                                            }
                                        }
                                    }

                                    if (bolUpdate)
                                    {
                                        string kaydetMusteriKisa = tenderServiceMethods.LfKaydetMusteriKisa(KPSKimlikBilgi, strPhone, strCityCode, strDistrictCode, strAddress, adressInfo.MAHALLE, strJobCode, strCardNo, strCardCvc, dtCardDate, curMusteri, ref musteriKod);
                                        if (kaydetMusteriKisa != "")
                                        {
                                            strMessage = kaydetMusteriKisa;
                                            bolNext = false;
                                        }
                                    }

                                    if (bolNext)
                                    {
                                        bolNext = true;
                                        saleDB.SaleLogUpdateMusteriKod(intSaleID, bolDiffrent, musteriKod);
                                    }
                                }
                            }
                            else
                            {
                                saleDB.SaleLogUpdateMusteriKod(intSaleID, bolDiffrent, musteriKod);

                                string apsSorgu = tenderServiceMethods.APSSorgu(strIdentityNo, ref adressInfo);
                                if (apsSorgu != "" || adressInfo == null)
                                {
                                    strMessage = apsSorgu;
                                }
                                else
                                {
                                    bolNext = true;
                                    strCityCode = adressInfo.ILKODU;
                                    strDistrictCode = adressInfo.ILCEKODU;
                                    strAddress = adressInfo.ACIKADRES;
                                }
                            }
                        }
                        else
                        {
                            strMessage = kpsBilesikKutukSorgu;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "CustomerControl-Insert");

                strMessage = Messages.Error;
            }

            return bolNext;
        }
    }
}