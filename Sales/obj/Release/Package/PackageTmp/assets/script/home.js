"use strict";

// scroll


var scrollButtons = document.querySelectorAll("[scroll-button]");

for (var index = 0; index < scrollButtons.length; index++) {

	var element = scrollButtons[index];
	element.addEventListener("click", function () {
		window.scroll({
			top: document.querySelector("[data-scroll]").offsetTop,
			behavior: "smooth"
		});
	});
}

var tckn = function tckn() {
	var form = document.querySelector("#form1");
	form.reportValidity();
};

function openPopup(type, title, msg) {
	var popup = document.querySelector('[popup-modal]');
	var checkedInput = document.querySelector('[popup-success]');
	/*var popupTitle = document.querySelector('[popup-success-title]');*/
	var popupMsg = document.querySelector('[popup-success-msg]');

	popup.classList.remove('-error');
	popup.classList.remove('-success');
	popup.classList.add(type);

	checkedInput.checked = true;
	/*popupTitle.innerHTML = title;*/
	popupMsg.innerHTML = msg;
}
document.addEventListener("DOMContentLoaded", function () {
	VMasker(document.querySelector("[data-tckn]")).maskPattern('999999999999');
	VMasker(document.querySelector("[data-phone]")).maskPattern('9999999999');
	VMasker(document.querySelector("[data-date]")).maskPattern('99.99.9999');
});