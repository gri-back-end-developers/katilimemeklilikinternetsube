﻿using Newtonsoft.Json;
using Sales.Classes;
using System;
using System.Configuration;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Sales.Controllers
{
    public class SalesController : Controller
    {
        #region Index

        #region Get
        [HttpGet]
        public ActionResult Index()
        { 
            try
            {
                Cookies.RemoveSaleID();

                Session.Clear();
                Session.RemoveAll();
                Session.Abandon();

                Logs.FileLogClear();

                SessionIDManager manager = new SessionIDManager();
                string NewID = manager.CreateSessionID(this.HttpContext.ApplicationInstance.Context);
                bool redirected = false;
                bool IsAdded = false;
                manager.SaveSessionID(this.HttpContext.ApplicationInstance.Context, NewID, out redirected, out IsAdded);

                GetValues();
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "Index-Get");
            }
            return View();
        }
        #endregion

        #region GetValues
        private void GetValues()
        {
            ViewBag.SaleJobs = SaleDB.GetSaleJobs();

            TenderLifeMethods tenderLifeMethods = new TenderLifeMethods();

            WsTenderLife.TenderHealthStatement[] tenderHealth = tenderLifeMethods.SaglikBeyanSoruGetir();
            if (tenderHealth != null && tenderHealth.Length > 0)
            {
                ViewBag.QuestionCode = tenderHealth[0].BeyanKod;
                ViewBag.Question = tenderHealth[0].Soru;
            }
        }
        #endregion

        #region Post
        [HttpPost]
        public ActionResult Index(string strPost = "")
        {
            ViewBag.ad = Request.Form["ad"];
            ViewBag.soyad = Request.Form["soyad"];
            ViewBag.kimlik = Request.Form["kimlik"];
            ViewBag.telefon = Request.Form["telefon"];
            ViewBag.dogumtarihi = Request.Form["dogumtarihi"];
            ViewBag.meslek = Request.Form["meslek"];
            ViewBag.bilgilendirme = Request.Form["bilgilendirme"];
            ViewBag.mesafelisatis = Request.Form["mesafelisatis"];
            ViewBag.iletisim = Request.Form["iletisim"];
            ViewBag.saglikbeyani = Request.Form["saglikbeyani"];

            #region Validate
            if (string.IsNullOrEmpty(Request.Form["ad"]))
            {
                ViewBag.Message = "Ad giriniz!";
                return View();
            }
            else if (string.IsNullOrEmpty(Request.Form["soyad"]))
            {
                ViewBag.Message = "Soyad giriniz!";
                return View();
            }
            else if (string.IsNullOrEmpty(Request.Form["kimlik"]))
            {
                ViewBag.Message = "Kimlik numarasını giriniz!";
                return View();
            }
            else if (string.IsNullOrEmpty(Request.Form["telefon"]))
            {
                ViewBag.Message = "Telefon giriniz!";
                return View();
            }
            else if (string.IsNullOrEmpty(Request.Form["dogumtarihi"]))
            {
                ViewBag.Message = "Doğum tarihi giriniz!";
                return View();
            }
            else if (string.IsNullOrEmpty(Request.Form["meslek"]))
            {
                ViewBag.Message = "Meslek seçiniz!";
                return View();
            }
            #endregion

            string strMessage = "";

            Sessions.IdentityNo = Request.Form["kimlik"];

            SaleDB saleDB = new SaleDB();
            int intSaleID = saleDB.SaleInsert(Request.Form["questionCode"]);

            if (intSaleID == 0)
            {
                strMessage = Messages.Error;
            }
            else
            {
                try
                {
                    int intSaleLogID = saleDB.SaleLogInsert(intSaleID, Request.Form["kimlik"], false);
                    if (intSaleLogID > 0)
                    {
                        CustomerControl customerControl = new CustomerControl();

                        string strKpsGender = "";
                        string strCityCode = "";
                        string strDistrictCode = "";
                        string strAddress = "";
                        string strNameSurname = Request.Form["ad"].ToLower().Trim() + " " + Request.Form["soyad"].ToLower().Trim();
                        DateTime dtBirthDate = Utilities.NullFixDate(Request.Form["dogumtarihi"]);

                        bool bolNext = customerControl.Insert(0, intSaleID, false, Request.Form["kimlik"], Request.Form["telefon"], Request.Form["meslek"], "", "", null, ref strMessage, strNameSurname, dtBirthDate, ref strKpsGender, ref strCityCode, ref strDistrictCode, ref strAddress);
                        if (bolNext)
                        {
                            bool bolLogUpdate = saleDB.SaleLogUpdate(intSaleID, intSaleLogID, Request.Form["kimlik"], Request.Form["ad"], Request.Form["telefon"], Request.Form["soyad"], dtBirthDate, Utilities.NullFixInt(Request.Form["meslek"]), strCityCode, strDistrictCode, strAddress);
                            if (bolLogUpdate)
                            {
                                if (strKpsGender == "K")
                                {
                                    Models.tblSaleLogs saleLogs = saleDB.GetSaleLog(intSaleID, false);

                                    if (saleLogs.tblSales.intSalePriceID.HasValue)
                                    {
                                        bool bolMesafeliSatis = Request.Form["mesafelisatis"] == "on" || Request.Form["mesafelisatis"] == "checked" || Request.Form["mesafelisatis"] == "true";
                                        bool bolBilgilendirme = Request.Form["bilgilendirme"] == "on" || Request.Form["bilgilendirme"] == "checked" || Request.Form["bilgilendirme"] == "true";
                                        bool bolIletisim = Request.Form["iletisim"] == "on" || Request.Form["iletisim"] == "checked" || Request.Form["iletisim"] == "true";
                                        bool bolSaglikBeyani = Request.Form["saglikbeyani"] == "on" || Request.Form["saglikbeyani"] == "checked" || Request.Form["saglikbeyani"] == "true";

                                        saleDB.SaleUpdate(Enums.SaleUpdateType.checbox, intSaleID, "", "", bolMesafeliSatis, bolBilgilendirme, bolIletisim, bolSaglikBeyani);

                                        Sessions.SaleID = intSaleID;

                                        string strPrefix = ConfigurationManager.AppSettings["root"].ToString();
                                        if (!string.IsNullOrEmpty(strPrefix))
                                        {
                                            strPrefix = "/" + strPrefix;
                                        }

                                        return Redirect(strPrefix + "/sales/pay");
                                    }
                                    else
                                    {
                                        strMessage = Messages.Kps;
                                    }
                                }
                                else
                                {
                                    strMessage = Messages.Gender;
                                }
                            }
                            else
                            {
                                strMessage = Messages.Error;
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(strMessage))
                            {
                                strMessage = Messages.Kps;
                            }
                        }
                    }
                    else
                    {
                        strMessage = Messages.Error;
                    }
                }
                catch (Exception ex)
                {
                    Logs.Exception(ex, "Index-Post");

                    strMessage = Messages.Error;
                }

                if (!string.IsNullOrEmpty(strMessage))
                {
                    saleDB.SaleUpdate(Enums.SaleUpdateType.message, intSaleID, "", strMessage);
                }
            }

            ViewBag.Message = strMessage;

            GetValues();

            return View();
        }
        #endregion 

        #endregion

        #region Pay

        #region Get
        [HttpGet]
        public ActionResult Pay(string strMessage = "")
        {
            string strPrefix = ConfigurationManager.AppSettings["root"].ToString();
            if (!string.IsNullOrEmpty(strPrefix))
            {
                strPrefix = "/" + strPrefix;
            }

            if (Sessions.SaleID == 0)
            { 
                return Redirect(strPrefix + "/");
            }

            SaleDB saleDB = new SaleDB();
            Models.tblSaleLogs saleLogs = saleDB.GetSaleLog(Sessions.SaleID, false);
            if (saleLogs != null)
            {
                if (saleLogs.tblSales.bolIsFinished)
                {
                    return Redirect(strPrefix + "/");
                }
                else
                {
                    ViewBag.Message = strMessage;
                    ViewBag.SaleJobs = SaleDB.GetSaleJobs();

                    return View(saleLogs);
                }
            }
            else
            {
                return Redirect(strPrefix + "/");
            }
        }
        #endregion

        #region Post
        [HttpPost]
        public ActionResult Pay()
        {
            ViewBag.ad = Request.Form["ad"];
            ViewBag.soyad = Request.Form["soyad"];
            ViewBag.kimlik = Request.Form["kimlik"];
            ViewBag.telefon = Request.Form["telefon"];
            ViewBag.dogumtarihi = Request.Form["dogumtarihi"];
            ViewBag.meslek = Request.Form["meslek"];
            ViewBag.tabdata = Request.Form["tabdata"];
            ViewBag.taksit = Request.Form["taksit"];

            #region Validate
            if (!string.IsNullOrEmpty(Request.Form["tabdata"]))
            {
                if (string.IsNullOrEmpty(Request.Form["ad"]))
                {
                    return Pay("Ad giriniz!");
                }
                else if (string.IsNullOrEmpty(Request.Form["soyad"]))
                {
                    return Pay("Soyad giriniz!");
                }
                else if (string.IsNullOrEmpty(Request.Form["kimlik"]))
                {
                    return Pay("Kimlik numarasını giriniz!");
                }
                else if (string.IsNullOrEmpty(Request.Form["telefon"]))
                {
                    return Pay("Telefon giriniz!");
                }
                else if (string.IsNullOrEmpty(Request.Form["dogumtarihi"]))
                {
                    return Pay("Doğum tarihi giriniz!");
                }
                else if (string.IsNullOrEmpty(Request.Form["meslek"]))
                {
                    return Pay("Meslek seçiniz!");
                }
                else if (string.IsNullOrEmpty(Request.Form["kartno"]))
                {
                    return Pay("Kredi kartı numarası giriniz!");
                }
                else if (string.IsNullOrEmpty(Request.Form["cvv"]))
                {
                    return Pay("Kredi kartı güvenlik kodunu giriniz!");
                }
                else if (string.IsNullOrEmpty(Request.Form["year"]) || string.IsNullOrEmpty(Request.Form["month"]))
                {
                    return Pay("Kredi kartı son kullanma tarihini giriniz!");
                }
            }

            if (string.IsNullOrEmpty(Request.Form["taksit"]))
            {
                return Pay("Ödeme şeklini seçiniz!");
            }

            if (!Request.Url.ToString().Contains("localhost:"))
            {
                if (!Captcha)
                {
                    return Pay("Güvenlik kodunu hatalı girdiniz!");
                }
            }
            #endregion

            string strMessage = "";

            SaleDB saleDB = new SaleDB();
            int intSaleID = Sessions.SaleID;

            try
            {
                CustomerControl customerControl = new CustomerControl();
                string strKpsGender = "";
                string strCityCode = "";
                string strDistrictCode = "";
                string strAddress = "";

                string strNameSurname = Request.Form["ad"].ToLower().Trim() + " " + Request.Form["soyad"].ToLower().Trim();
                DateTime dtBirthDate = Utilities.NullFixDate(Request.Form["dogumtarihi"]);

                DateTime dtCardDate = new DateTime(int.Parse(Request.Form["year"].Length == 2 ? "20" + Request.Form["year"] : Request.Form["year"]), int.Parse(Request.Form["month"]), 1);

                if (!string.IsNullOrEmpty(Request.Form["tabdata"]))
                {
                    Sessions.IdentityNo = Request.Form["kimlik"];

                    int intSaleLogID = saleDB.SaleLogInsert(intSaleID, Request.Form["kimlik"], true);
                    if (intSaleLogID > 0)
                    {
                        bool bolNext = customerControl.Insert(0, intSaleID, true, Request.Form["kimlik"], Request.Form["telefon"], Request.Form["meslek"], Request.Form["kartno"], Request.Form["cvv"], dtCardDate, ref strMessage, strNameSurname, dtBirthDate, ref strKpsGender, ref strCityCode, ref strDistrictCode, ref strAddress);
                        if (bolNext)
                        {
                            bool bolLogUpdate = saleDB.SaleLogUpdate(intSaleID, intSaleLogID, Request.Form["kimlik"], Request.Form["ad"], Request.Form["telefon"], Request.Form["soyad"], dtBirthDate, Utilities.NullFixInt(Request.Form["meslek"]), strCityCode, strDistrictCode, strAddress);
                            if (!bolLogUpdate)
                            {
                                strMessage = Messages.Error;
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(strMessage))
                            {
                                strMessage = Messages.KpsDifferent;
                            }
                        }
                    }
                    else
                    {
                        strMessage = Messages.Error;
                    }

                    Sessions.IdentityNo = saleDB.GetSaleLog(intSaleID, false).strIdentityNo;
                }

                if (string.IsNullOrEmpty(strMessage))
                {
                    saleDB.SaleUpdate(Enums.SaleUpdateType.installment, intSaleID, null, null, false, false, false, false, null, Utilities.NullFixByte(Request.Form["taksit"]));

                    bool Update = true;
                    if (string.IsNullOrEmpty(Request.Form["tabdata"]))
                    {
                        var saleLog = saleDB.GetSaleLog(intSaleID, false);

                        Update = customerControl.Insert(saleLog.intMustKod.Value, intSaleID, false, saleLog.strIdentityNo, saleLog.strPhone, saleLog.tblSalesJob.strCode, Request.Form["kartno"], Request.Form["cvv"], dtCardDate, ref strMessage, saleLog.strName + " " + saleLog.strSurname, saleLog.dtBirhDate, ref strKpsGender, ref strCityCode, ref strDistrictCode, ref strAddress);
                    }

                    if (Update)
                    {
                        TenderLifeMethods tenderLifeMethods = new TenderLifeMethods();

                        decimal toplaNetPrim = 0;
                        decimal netPrim = 0;

                        string primHesapla = tenderLifeMethods.PrimHesapla(intSaleID, Utilities.NullFixDecimal(ConfigurationManager.AppSettings["teminatBedel_1"].ToString()), ConfigurationManager.AppSettings["teminatKod_1"].ToString(), ref netPrim);
                        if (primHesapla == "")
                        {
                            toplaNetPrim = netPrim;

                            primHesapla = tenderLifeMethods.PrimHesapla(intSaleID, Utilities.NullFixDecimal(ConfigurationManager.AppSettings["teminatBedel_2"].ToString()), ConfigurationManager.AppSettings["teminatKod_2"].ToString(), ref netPrim);

                            if (primHesapla == "")
                            {
                                toplaNetPrim += netPrim;
                            }
                        }

                        WsTenderLife.BNKWS_POLICELESME_RES_TYP teklifCevap = null;

                        string teklifKaydet = tenderLifeMethods.TeklifKaydet(intSaleID, Request.Form["taksit"], Request.Form["cvv"], Request.Form["kartno"], dtCardDate, ref teklifCevap);

                        if (string.IsNullOrEmpty(teklifKaydet))
                        {
                            saleDB.SaleUpdate(Enums.SaleUpdateType.finished, intSaleID, JsonConvert.SerializeObject(teklifCevap));

                            string strPrefix = ConfigurationManager.AppSettings["root"].ToString();
                            if (!string.IsNullOrEmpty(strPrefix))
                            {
                                strPrefix = "/" + strPrefix;
                            }

                            return Redirect(strPrefix + "/sales/finish");
                        }
                        else
                        {
                            saleDB.SaleUpdate(Enums.SaleUpdateType.offer, intSaleID, JsonConvert.SerializeObject(teklifCevap));

                            strMessage = "Teklif oluştulamadı!";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "Pay-Post");

                strMessage = Messages.Error;
            }

            if (!string.IsNullOrEmpty(strMessage))
            {
                saleDB.SaleUpdate(Enums.SaleUpdateType.message, intSaleID, "", strMessage);
            }

            if (!string.IsNullOrEmpty(strMessage) && strMessage.ToLower().Contains("girmiş olduğunuz hesap"))
            {
                strMessage = "Girdiğiniz kredi kartı bilgileri sistemimizde kayıtlı bir müşteriye ait olduğundan, farklı bir ödeme aracı girmeniz gerekmektedir.";
            }
            else if (!string.IsNullOrEmpty(strMessage) && strMessage.ToLower().Contains("prefix"))
            {
                strMessage = "Girdiğiniz kredi kartı bilgileri tanımlı değildir. info@katilimemeklilik.com.tr adresine iletişim bilgilerinizle bu durumu iletmenizi rica ederiz.";
            }

            return Pay(strMessage);
        }
        #endregion 

        #endregion

        #region Finish
        public ActionResult Finish()
        {
            SaleDB saleDB = new SaleDB();
            Models.tblSales sale = saleDB.GetSale(Sessions.SaleID);
            if (sale != null)
            {
                if (!string.IsNullOrEmpty(sale.strOfferJson))
                {
                    var offerData = JsonConvert.DeserializeObject<Sales.WsTenderLife.BNKWS_POLICELESME_RES_TYP>(sale.strOfferJson);
                    if (offerData != null && offerData.Value != null && offerData.Value.Length > 0)
                    {
                        ViewBag.OfferNo = offerData.Value[0].TEKLIF_NO.HasValue ? offerData.Value[0].TEKLIF_NO : 0;
                    }
                }
            }

            return View();
        }
        #endregion

        #region Pdf
        public EmptyResult Pdf()
        {
            SaleDB saleDB = new SaleDB();
            Models.tblSales sale = saleDB.GetSale(Sessions.SaleID);
            if (sale != null)
            {
                if (!string.IsNullOrEmpty(sale.strOfferJson))
                {
                    var offerData = JsonConvert.DeserializeObject<Sales.WsTenderLife.BNKWS_POLICELESME_RES_TYP>(sale.strOfferJson);
                    if (offerData != null && offerData.Value != null && offerData.Value.Length > 0)
                    {
                        if (offerData.Value[0].TEKLIF_NO.HasValue)
                        {
                            BankaSuransMethods bankaSuransMethods = new BankaSuransMethods();
                            byte[] teklifPdf = bankaSuransMethods.TeklifBasim(offerData.Value[0].TEKLIF_NO.Value);
                            if (teklifPdf != null)
                            {
                                Utilities.DownloadFile(teklifPdf, "Teklif_" + offerData.Value[0].TEKLIF_NO.Value.ToString() + ".pdf", "application/pdf");
                            }
                        }
                    }
                }
            }

            return new EmptyResult();
        }
        #endregion

        #region Captcha
        private bool Captcha
        {
            get
            {
                var response = Request["g-recaptcha-response"];
                var client = new System.Net.WebClient();
                var reply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", ConfigurationManager.AppSettings["CaptchaPrivateKey"], response));
                Newtonsoft.Json.Linq.JObject JsonText = Newtonsoft.Json.Linq.JObject.Parse(reply);
                dynamic captchaResponse = JsonText;
                if (captchaResponse.success == "false")
                    return false;
                else
                    return true;
            }
        }
        #endregion
    }
}