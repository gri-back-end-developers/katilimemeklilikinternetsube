"use strict";

if (document.getElementById('tabdata').value == '') {
    document.querySelector("[data-form-one]").classList.add("-tab-active");
    document.querySelector("[data-form-reset]").classList.remove("-tab-active");
    document.querySelector("[data-form-reset]").classList.add("-tab-passive");
    
    document.querySelector("[data-kvk]").style.display = "none";
    document.querySelector("[data-kvk] input").removeAttribute("required");
} else {
    document.querySelector("[data-form-reset]").classList.remove("-tab-passive");
    document.querySelector("[data-form-reset]").classList.add("-tab-active");
    document.querySelector("[data-form-one]").classList.remove("-tab-active");
    document.querySelector("[data-form-one]").classList.add("-tab-passive");

    document.querySelector("[data-kvk]").style.display = "flex";
    document.querySelector("[data-kvk] input").setAttribute("required", "");
}

var inputLength = document.querySelectorAll("#odeyenfarkli input").length;

document.querySelector("[data-form-reset]").addEventListener("click", function () {
    for (var i = 0; i < inputLength; i++) {
        document.querySelectorAll("#odeyenfarkli input")[i].value = "";
    }
    document.querySelector("select").selectedIndex = "0";
    document.querySelector("[data-form-reset]").classList.remove("-tab-passive");
    document.querySelector("[data-form-reset]").classList.add("-tab-active");
    document.querySelector("[data-form-one]").classList.remove("-tab-active");
    document.querySelector("[data-form-one]").classList.add("-tab-passive");

    document.getElementById('sigortali').style.display = 'none';
    document.getElementById('odeyenfarkli').style.display = 'block';
    document.getElementById('tabdata').value = 'odeyen';
    document.getElementById('ad').setAttribute('required', '');
    document.getElementById('soyad').setAttribute('required', '');
    document.getElementById('kimlik').setAttribute('required', '');
    document.getElementById('telefon').setAttribute('required', '');
    document.getElementById('dogumtarihi').setAttribute('required', '');
    document.getElementById('meslek').setAttribute('required', '');

    document.querySelector("[data-kvk]").style.display = "flex";
    document.querySelector("[data-kvk] input").setAttribute("required", "");
});

document.querySelector("[data-form-one]").addEventListener("click", function () {
    document.querySelector("[data-form-one]").classList.add("-tab-active");
    document.querySelector("[data-form-reset]").classList.remove("-tab-active");
    document.querySelector("[data-form-reset]").classList.add("-tab-passive");
    document.getElementById('sigortali').style.display = 'block';
    document.getElementById('odeyenfarkli').style.display = 'none';
    document.getElementById('tabdata').value = '';
    document.getElementById('ad').removeAttribute('required');
    document.getElementById('soyad').removeAttribute('required');
    document.getElementById('kimlik').removeAttribute('required');
    document.getElementById('telefon').removeAttribute('required');
    document.getElementById('dogumtarihi').removeAttribute('required');
    document.getElementById('meslek').removeAttribute('required');

    document.querySelector("[data-kvk]").style.display = "none";
    document.querySelector("[data-kvk] input").removeAttribute("required");
});

function openPopup(type, title, msg) {
    var popup = document.querySelector('[popup-modal]');
    var checkedInput = document.querySelector('[popup-success]');
    /*var popupTitle = document.querySelector('[popup-success-title]');*/
    var popupMsg = document.querySelector('[popup-success-msg]');

    popup.classList.remove('-error');
    popup.classList.remove('-success');
    popup.classList.add(type);

    checkedInput.checked = true;
    /*popupTitle.innerHTML = title;*/
    popupMsg.innerHTML = msg;
}

/*######################*/

var allowSubmit = false;

var capcha_filled = function capcha_filled() {
    allowSubmit = true;
};

var capcha_expired = function capcha_expired() {
    allowSubmit = false;
};

var onloadCallback = function onloadCallback() {
    grecaptcha.render('grecaptcha', {
        'sitekey': '6LfjgEYUAAAAAGniabbYaQr3G35fGuRC4cqAr5W5',
        'callback': capcha_filled,
        'expired-callback': capcha_expired
    });
};

document.getElementById("frm").onsubmit = function (e) {
    if (allowSubmit) {
        document.querySelector('.recaptcha-notification').classList.add("-hidden");
        return true;
    }
    e.preventDefault();
    document.querySelector('.recaptcha-notification').classList.remove("-hidden");
    grecaptcha.reset();
};

document.addEventListener("DOMContentLoaded", function () {
    VMasker(document.querySelector("[data-tckn]")).maskPattern('999999999999');
    VMasker(document.querySelector("[data-phone]")).maskPattern('9999999999');
    VMasker(document.querySelector("[data-card]")).maskPattern('9999999999999999');
    VMasker(document.querySelector("[data-cvv]")).maskPattern('999');
    VMasker(document.querySelector("[data-date='1']")).maskPattern('99.99.9999');
    VMasker(document.querySelector("[data-date='2']")).maskPattern('99.99.9999');
    VMasker(document.querySelector("[data-date='3']")).maskPattern('99');
    VMasker(document.querySelector("[data-date='4']")).maskPattern('99');
});