//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sales.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSalesJob
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblSalesJob()
        {
            this.tblSaleLogs = new HashSet<tblSaleLogs>();
        }
    
        public int intSaleJobID { get; set; }
        public string strCode { get; set; }
        public string strName { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblSaleLogs> tblSaleLogs { get; set; }
    }
}
