﻿using System.Web;

namespace AcenteLogin.Classes.Tools
{
    public class Sessions
    { 
        #region Message
        public static string Message
        {
            get
            {
                try
                {
                    object val = HttpContext.Current.Session["Message"];

                    return val == null ? "" : val.ToString();
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                HttpContext.Current.Session["Message"] = value;
            }
        }
        #endregion
    }
}