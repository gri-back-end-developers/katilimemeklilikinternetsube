﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace AcenteLogin.Classes.Tools
{
    /// <summary>
    /// Provides methods for encrypting and decrypting clear texts.
    /// </summary>
    public static class Encryptor
    {
        private const string strCaptchaPas = "brkn+d89brkn+d81";
        private const string strCaptchaSalt = "brkn+d83brkn+d82";
        
        /// <summary>
        /// Encrypts a clear text using specified password and salt.
        /// </summary>
        /// <param name="clearText">The text to encrypt.</param>
        /// <param name="password">The password to create key for.</param>
        /// <param name="salt">The salt to add to encrypted text to make it more secure.</param>
        public static string Encrypt(string clearText)
        {
            // Turn text to bytes
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            byte[] byteCaptchaSalt = Encoding.Unicode.GetBytes(strCaptchaSalt);
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(strCaptchaPas, byteCaptchaSalt);//(password, salt);

            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();

            alg.Key = pdb.GetBytes(32);
            alg.IV = pdb.GetBytes(16);

            CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);

            cs.Write(clearBytes, 0, clearBytes.Length);
            cs.Close();

            byte[] EncryptedData = ms.ToArray();

            return Convert.ToBase64String(EncryptedData);
        }
        /// <summary>
        /// Decrypts an encrypted text using specified password and salt.
        /// </summary>
        /// <param name="cipherText">The text to decrypt.</param>
        /// <param name="password">The password used to encrypt text.</param>
        /// <param name="salt">The salt added to encrypted text.</param>
        public static string Decrypt(string cipherText)
        {
            // Convert text to byte
            byte[] cipherBytes = Convert.FromBase64String(cipherText);

            byte[] byteCaptchaSalt = Encoding.Unicode.GetBytes(strCaptchaSalt);
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(strCaptchaPas, byteCaptchaSalt);

            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();

            alg.Key = pdb.GetBytes(32);
            alg.IV = pdb.GetBytes(16);

            CryptoStream cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write);

            cs.Write(cipherBytes, 0, cipherBytes.Length);
            cs.Close();

            byte[] DecryptedData = ms.ToArray();

            return Encoding.Unicode.GetString(DecryptedData);
        }
    }
}