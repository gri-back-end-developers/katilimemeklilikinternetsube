﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Script.Serialization;

namespace AcenteLogin.Classes.Tools
{
    public class Caches
    {
        #region AuthenticatedString
        public static string AuthenticatedString
        {
            get
            {
                if (HttpContext.Current.Cache["AuthenticatedString"] == null)
                {
                    return "";
                }
                else
                {
                    return HttpContext.Current.Cache["AuthenticatedString"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Cache.Add("AuthenticatedString", value, null, DateTime.Now.AddMinutes(100), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
            }
        }
        #endregion
    }
}