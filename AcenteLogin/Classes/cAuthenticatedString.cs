﻿using AcenteLogin.Classes.Tools;
using AcenteLogin.WsSecurity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AcenteLogin.Classes
{
    public class cAuthenticatedString
    {
        public static string Get
        {
            get
            {
                string strAuthenticatedString = Caches.AuthenticatedString;
                if (string.IsNullOrEmpty(strAuthenticatedString))
                { 
                    using (SecurityServiceClient wsSecurity = new SecurityServiceClient())
                    {
                        var authenticationKey = wsSecurity.GetAuthenticationKey(ConfigurationManager.AppSettings["appSecurityKey"], ConfigurationManager.AppSettings["authUserName"], ConfigurationManager.AppSettings["authPassword"], ref strAuthenticatedString);

                        if (authenticationKey.IsOk && !string.IsNullOrEmpty(strAuthenticatedString))
                        {
                            Caches.AuthenticatedString = strAuthenticatedString;
                        }
                    }
                } 

                return strAuthenticatedString;
            }
        }
    }
}