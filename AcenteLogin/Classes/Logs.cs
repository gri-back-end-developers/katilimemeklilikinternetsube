﻿using AcenteLogin.Classes.Tools;
using AcenteLogin.Models;
using System;

namespace AcenteLogin.Classes
{
    public class Logs
    {
        #region Exception
        public static void Exception(string strEx, string strPage)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    db.tblExceptions.Add(new tblExceptions
                    {
                        dtRegisterDate = DateTime.Now,
                        strException = strEx,
                        strPage = strPage,
                        intCustomerNo = 0,
                        intLine = 0
                    });
                    db.SaveChanges();
                }
            }
            catch { }
        } 
        #endregion

        #region LoginLog
        public static void LoginLog(string strUserName, string strALU)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    db.tblAcenteLoginLogs.Add(new tblAcenteLoginLogs
                    {
                        dtRegisterDate = DateTime.Now,
                        strALU = strALU,
                        strIP = Utilities.ClientIP,
                        strUserName = strUserName
                    });
                    db.SaveChanges();
                }
            }
            catch { }
        }
        #endregion
    }
}