﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using System.Configuration;
using System.Web.Security;
using AcenteLogin.Classes.Tools;
using AcenteLogin.WsAlu;
using AcenteLogin.Classes; 

namespace AcenteLogin.Controllers
{
    public class DefaultController : Controller
    {
        #region Index Get
        [HttpGet]
        [jCryptionHandler]
        public ActionResult Index(string strMessage = "")
        {
            if (Request.QueryString["getPublicKey"] == null)
            {
                Session.Clear();
                Session.RemoveAll();
                Session.Abandon();

                SessionIDManager manager = new SessionIDManager();
                string NewID = manager.CreateSessionID(this.HttpContext.ApplicationInstance.Context);
                bool redirected = false;
                bool IsAdded = false;
                manager.SaveSessionID(this.HttpContext.ApplicationInstance.Context, NewID, out redirected, out IsAdded);

                if (!string.IsNullOrEmpty(strMessage))
                {
                    ViewBag.strMessage = Utilities.Msg(Utilities.MsgType.warning, strMessage);
                }
            }

            return View();
        }
        #endregion

        #region Index Post
        [HttpPost]
        [jCryptionHandler]
        public ActionResult Index(string username, string userpss)
        {
            if (string.IsNullOrEmpty(username) && string.IsNullOrEmpty(userpss))
            {
                return View();
            }

            string strMessage = "";

            try
            {
                AutoLoginServiceClient aluWs = new AutoLoginServiceClient();

                string strKey = cAuthenticatedString.Get;
                string strAlu = "";

                var getAlu = aluWs.GetALU(strKey, ref strAlu);
                if (getAlu.IsOk)
                {
                    Logs.LoginLog(username, strAlu);

                    return Redirect(ConfigurationManager.AppSettings["LoginPath"] + "?ALU=" + strAlu);
                }



                //using (BireyselEntities db = new BireyselEntities())
                //{
                    //userpss = cCrypto.EncryptDES(userpss);
                    //var user = db.tblCmsUsers.Where(w => !w.bolIsDelete && w.strUserName == username && w.strPss == userpss && w.bolIsActive).FirstOrDefault();

                    //if (user != null)
                    //{
                    //    db.tblCmsUserLogs.Add(new tblCmsUserLogs
                    //    {
                    //        dtRegisterDate = DateTime.Now,
                    //        intCmsUserID = user.intCmsUserID,
                    //        strIp = Utilities.ClientIP
                    //    });
                    //    db.SaveChanges();

                    //    Sessions.CurrentUser = user;

                    //    FormsAuthentication.SetAuthCookie(username, false);

                    //    return Redirect(ConfigurationManager.AppSettings["DefaultPage"]);
                    //}
                    //else
                    //{
                    //    strMessage = "Kullanıcı bilgilerini hatalı girdiniz!";
                    //}
                //}
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "Acente Login");

                strMessage = "Bir hata oluştu! Tekrar deneyiniz.";
            }

            return Index(strMessage);
        }
        #endregion
    }
}
