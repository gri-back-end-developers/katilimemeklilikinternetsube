﻿using CMS.Models;
using System.Web;

namespace CMS.Classes.Tools
{
    public class Sessions
    {
        #region CurrentUser
        public static tblCmsUsers CurrentUser
        {
            get
            {
                try
                {
                    object val = HttpContext.Current.Session["CurrentUser"];

                    return val == null ? new tblCmsUsers() : (tblCmsUsers)val;
                }
                catch
                {
                    return new tblCmsUsers();
                }
            }
            set
            {
                HttpContext.Current.Session["CurrentUser"] = value;
            }
        }
        #endregion

        #region Message
        public static string Message
        {
            get
            {
                try
                {
                    object val = HttpContext.Current.Session["Message"];

                    return val == null ? "" : val.ToString();
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                HttpContext.Current.Session["Message"] = value;
            }
        }
        #endregion

        #region SIp
        public static string SIp
        {
            get
            {
                try
                {
                    object val = HttpContext.Current.Session["SIp"];

                    return val == null ? "" : val.ToString();
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                HttpContext.Current.Session["SIp"] = value;
            }
        }
        #endregion
    }
}