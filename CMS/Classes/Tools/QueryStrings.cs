#region Directives
using System;
using System.Collections.Generic;
using System.Web;
#endregion

namespace CMS.Classes.Tools
{
    public class QueryStrings
    {
        #region policy
        public static long policy
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["policy"];
                    if (value == null)
                    {
                        value = 0;
                    }
                    return Utilities.NullFixLong(cCrypto.DecryptDES(value.ToString()));
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion 

        #region page
        public static int page
        {
            get
            {
                try
                {
                    object value = HttpContext.Current.Request.QueryString["page"];
                    if (value == null)
                    {
                        value = 0;
                    }

                    return int.Parse(value.ToString());
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion
    }
}