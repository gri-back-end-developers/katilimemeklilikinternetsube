﻿namespace CMS.Classes
{
    public class Enums
    {
        public enum UserTypes
        {
            cms = 0,
            chat = 1,
            balancereturn = 2
        }

        public static string UserType(byte intUserType)
        {
            UserTypes userType = (UserTypes)intUserType;

            switch (userType)
            {
                case UserTypes.cms:
                    return "Cms";
                case UserTypes.chat:
                    return "Online Chat";
                case UserTypes.balancereturn:
                    return "Bakiye İade sorgulama";
                default:
                    return "";
            }     
        }

        public static string UserType(UserTypes userType)
        { 
            switch (userType)
            {
                case UserTypes.cms:
                    return "Cms";
                case UserTypes.chat:
                    return "Online Chat";
                case UserTypes.balancereturn:
                    return "Bakiye İade sorgulama";
                default:
                    return "";
            }
        }
    }
}