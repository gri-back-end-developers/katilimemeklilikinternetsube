﻿using CMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Classes.ClassData
{
    public class cDocumentAdd
    {
        public tblCmsDocuments document { get; set; }
        public List<tblCmsDocumentTypes> documentTypes { get; set; }

        public static List<tblCmsDocumentTypes> GetDocumentTypes()
        {
            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    return db.tblCmsDocumentTypes.Where(w => !w.bolIsDelete).OrderBy(o => o.strName).ToList();
                }
            }
            catch { }

            return null;
        }
    }
}