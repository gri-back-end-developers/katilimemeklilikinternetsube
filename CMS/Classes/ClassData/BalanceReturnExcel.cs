﻿using System;

namespace CMS.Classes.ClassData
{
    public class BalanceReturnExcel
    {
        public string tckn { get; set; }
        public string ad { get; set; }
        public string soyad { get; set; }
        public string turu { get; set; }
        public string police { get; set; }
        public string tutar { get; set; }
        public string telefon { get; set; }
        public string durumkod { get; set; }
    }
}