﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Classes.ClassData
{
    public class cUserLogs
    {
        public string strTitle { get; set; }
        public int intUserLoginCount { get; set; }
        public int intTotalLoginCount { get; set; }
    }

    public class cUserLogData
    {
        public DateTime dtTime { get; set; }
        public string strTCKN { get; set; } 
    }
}