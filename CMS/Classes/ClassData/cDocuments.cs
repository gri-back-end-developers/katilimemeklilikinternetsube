﻿using CMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Classes.ClassData
{
    public class cDocuments
    {
        public List<tblCmsDocuments> documents { get; set; }
        public string typeName { get; set; } 
    }
}