﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Classes.ClassData
{
    public class cSetting
    {
        public bool bolBireyselBakim { get; set; }
        public bool bolKurumsalBakim { get; set; }
        public bool bolBireyselChat { get; set; }
        public bool bolPortalChat { get; set; }
    }
}