﻿using CMS.Models;
using System; 

namespace CMS.Classes
{
    public class Logs
    {
        public static void Exception(string strEx, string strPage, int intCmsUserID = 0)
        {  
            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    db.tblCmsExceptions.Add(new tblCmsExceptions()
                    {
                        dtRegisterDate = DateTime.Now, 
                        strException = strEx,
                        strPage = strPage,
                        intCmsUserID = intCmsUserID
                    });
                    db.SaveChanges();
                }
            }
            catch { }
        }

        public static void Setting(int intSettingID, int intCmsUserID, string strMessage)
        {
            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    db.tblSettingLogs.Add(new tblSettingLogs()
                    {
                        dtRegisterDate = DateTime.Now,
                        strMessage = strMessage,
                        intCmsUserID = intCmsUserID,
                        intSettingID = intSettingID
                    });
                    db.SaveChanges();
                }
            }
            catch { }
        }
    }
}