﻿using CMS.Classes.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS.Classes
{
    public class UserFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                bool bolResponse = false; 

                if (HttpContext.Current.Session["CurrentUser"] != null)
                { 
                    if (Sessions.SIp != Utilities.ClientIP)
                    {
                        bolResponse = true;
                    }
                }
                else
                {
                    bolResponse = true;
                }

                if (bolResponse)
                {
                    Sessions.CurrentUser = null;
                    HttpContext.Current.Session.Clear();
                    HttpContext.Current.Session.RemoveAll();
                    HttpContext.Current.Session.Abandon();

                    HttpContext.Current.Response.Redirect("/");
                }
                else
                {
                    if ((Enums.UserTypes)Sessions.CurrentUser.intUserType == Enums.UserTypes.chat)
                    {
                        if (!HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("/settings/chat"))
                        {
                            HttpContext.Current.Response.Redirect("/Settings/Chat");
                        }
                    }
                    else if ((Enums.UserTypes)Sessions.CurrentUser.intUserType == Enums.UserTypes.balancereturn)
                    {
                        if (!HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("/balancereturn/query") && !HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("/balancereturn/queryinsupd") && !HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("/balancereturn/queryexcel") && !HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("/balancereturn/settings"))
                        {
                            HttpContext.Current.Response.Redirect("/balancereturn/query");
                        }
                    }
                }
            }
            catch { }
        }
    } 
}