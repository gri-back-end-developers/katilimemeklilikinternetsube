﻿using CMS.Classes;
using CMS.Classes.ClassData;
using CMS.Classes.Tools;
using CMS.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS.Controllers
{
    public class BalanceReturnController : Controller
    {
        #region Query

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult Query(string strMessage = "")
        {
            List<tblBalanceReturns> balanceReturns = null;

            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    if (Request.QueryString["i"] != null)
                    {
                        int intBalanceID = Utilities.NullFixInt(cCrypto.DecryptDES(Request.QueryString["i"]));

                        var del = db.tblBalanceReturns.Where(w => !w.bolIsDelete && w.intBalanceID == intBalanceID).FirstOrDefault();
                        if (del != null)
                        {
                            del.bolIsDelete = true;
                            db.SaveChanges();

                            ViewBag.Msg = Utilities.Msg(Utilities.MsgType.success, "Poliçe silindi.");
                        }
                    }

                    string strType = Request.QueryString["type"];
                    string strStatusCode = Request.QueryString["statuscode"];
                    string strPolicyNo = Request.QueryString["policyno"];

                    string strTCKN = Request.QueryString["tckn"];
                    if (!string.IsNullOrEmpty(strTCKN))
                    {
                        strTCKN = cCrypto.EncryptDES(strTCKN);
                    }

                    balanceReturns = db.tblBalanceReturns.Where(w => !w.bolIsDelete
                        && (!string.IsNullOrEmpty(strType) ? w.strType == strType : true)
                            && (!string.IsNullOrEmpty(strStatusCode) ? w.strStatusCode == strStatusCode : true)
                                && (!string.IsNullOrEmpty(strPolicyNo) ? w.strPolicyNo == strPolicyNo : true)
                                 && (!string.IsNullOrEmpty(strTCKN) ? w.strTCKN == strTCKN : true)).ToList();

                    if (Request.QueryString["statussave"] == "ok")
                    {
                        foreach (var item in balanceReturns)
                        {
                            item.strStatusCode = "ODN";
                            item.intUpdUserID = Sessions.CurrentUser.intCmsUserID;
                            item.dtUpdateDate = DateTime.Now;
                        }
                        db.SaveChanges();

                        strMessage = Utilities.Msg(Utilities.MsgType.success, "Poliçeler ödendi olarak işaretlendi.");
                    }

                    int intPageSize = 20;
                    ViewBag.intCount = (balanceReturns.Count / intPageSize) + (balanceReturns.Count % intPageSize > 0 ? 1 : 0);

                    int intPage = QueryStrings.page;
                    intPage = intPage > 0 ? intPage - 1 : intPage;

                    balanceReturns = balanceReturns.Skip(intPage * intPageSize).Take(intPageSize).ToList();
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/BalanceReturn/Query/Get", Sessions.CurrentUser.intCmsUserID);
            }

            if (!string.IsNullOrEmpty(strMessage))
            {
                ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
            }

            return View(balanceReturns);
        }
        #endregion

        #region Post
        [UserFilter]
        [HttpPost]
        public ActionResult Query()
        {
            string strMessage = "";

            if (string.IsNullOrEmpty(Request.Form["statusall"]))
            {
                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Poliçe seçiniz!");
            }
            else
            {
                try
                {
                    using (BireyselEntities db = new BireyselEntities())
                    {
                        List<int> banalceIDs = Request.Form["statusall"].Split(',').Select(s => int.Parse(s)).ToList();

                        foreach (var intBalanceID in banalceIDs)
                        {
                            var balanceReturn = db.tblBalanceReturns.Where(w => w.intBalanceID == intBalanceID).FirstOrDefault();
                            if (balanceReturn != null)
                            {
                                balanceReturn.strStatusCode = "ODN";
                                balanceReturn.dtUpdateDate = DateTime.Now;
                                balanceReturn.intUpdUserID = Sessions.CurrentUser.intCmsUserID;

                                db.SaveChanges();
                            }
                        }

                        strMessage = Utilities.Msg(Utilities.MsgType.success, "Poliçeler ödendi olarak işaretlendi.");
                    }
                }
                catch (Exception ex)
                {
                    Logs.Exception(ex.ToString(), "/BalanceReturn/Query/Post", Sessions.CurrentUser.intCmsUserID);
                }
            }

            return Query(strMessage);
        }
        #endregion

        #endregion

        #region QueryInsUpd

        #region Get
        [UserFilter]
        [HttpGet]
        [jCryptionHandler]
        public ActionResult QueryInsUpd(string strMessage = "")
        {
            tblBalanceReturns balanceReturn = null;

            if (Request.QueryString["getPublicKey"] == null)
            {
                try
                {
                    if (Request.QueryString["i"] != null)
                    {
                        int intBalanceID = Utilities.NullFixInt(cCrypto.DecryptDES(Request.QueryString["i"]));

                        using (BireyselEntities db = new BireyselEntities())
                        {
                            balanceReturn = db.tblBalanceReturns.FirstOrDefault(f => f.intBalanceID == intBalanceID);
                        }
                    }

                    if (!string.IsNullOrEmpty(strMessage))
                    {
                        ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
                    }
                }
                catch (Exception ex)
                {
                    Logs.Exception(ex.ToString(), "/BalanceReturn/QueryInsUpd/Get", Sessions.CurrentUser.intCmsUserID);
                }
            }

            return View(balanceReturn);
        }
        #endregion

        #region Post Form
        [UserFilter]
        [HttpPost]
        [jCryptionHandler]
        public ActionResult QueryInsUpd()
        {
            if (Request.Form["formvalue"] == null)
            {
                return View();
            }

            string strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");

            string strTCKN = Request.Form["strTCKN"].Trim();
            string strName = Request.Form["strName"].Trim();
            string strSurname = Request.Form["strSurname"].Trim();
            string strType = Request.Form["strType"].Trim();
            string strPolicyNo = Request.Form["strPolicyNo"].Trim();
            string flPrice = Request.Form["flPrice"].Trim();
            string strPhone = Request.Form["strPhone"].Trim();

            ViewBag.strTCKN = strTCKN;
            ViewBag.strName = strName;
            ViewBag.strSurname = strSurname;
            ViewBag.strType = strType;
            ViewBag.strPolicyNo = strPolicyNo;
            ViewBag.flPrice = flPrice;
            ViewBag.strPhone = strPhone;
            ViewBag.i = Request.Form["i"];

            if (string.IsNullOrEmpty(strTCKN) || string.IsNullOrEmpty(strPolicyNo) || string.IsNullOrEmpty(strPhone) || (string.IsNullOrEmpty(flPrice) || Utilities.NullFixDecimal(flPrice) == 0))
            {
                if (string.IsNullOrEmpty(strTCKN))
                {
                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Kimlik numarasını giriniz!");
                }
                else if (string.IsNullOrEmpty(strPolicyNo))
                {
                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Poliçe numarasını giriniz!");
                }
                else if (string.IsNullOrEmpty(strPhone))
                {
                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Telefon numarasını giriniz!");
                }
                else if (string.IsNullOrEmpty(flPrice) || Utilities.NullFixDecimal(flPrice) == 0)
                {
                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Tutarı giriniz!");
                }

                return QueryInsUpd(strMessage);
            }

            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    if (!string.IsNullOrEmpty(Request.Form["i"]))
                    {
                        int intBalanceID = Utilities.NullFixInt(cCrypto.DecryptDES(Request.Form["i"]));
                        if (intBalanceID > 0)
                        {
                            var balanceReturn = db.tblBalanceReturns.Where(w => w.intBalanceID == intBalanceID).FirstOrDefault();
                            if (balanceReturn != null)
                            {
                                balanceReturn.strTCKN = cCrypto.EncryptDES(strTCKN);
                                balanceReturn.strName = strName;
                                balanceReturn.strSurname = strSurname;
                                balanceReturn.strType = strType;
                                balanceReturn.strPolicyNo = strPolicyNo;
                                balanceReturn.flPrice = Utilities.NullFixDecimal(flPrice);
                                balanceReturn.strPhone = strPhone;
                                balanceReturn.dtUpdateDate = DateTime.Now;
                                balanceReturn.intUpdUserID = Sessions.CurrentUser.intCmsUserID;

                                db.SaveChanges();

                                Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Poliçe güncellendi.");

                                return Redirect("/balancereturn/query");
                            }
                        }
                    }
                    else
                    {
                        tblBalanceReturns balanceReturn = new tblBalanceReturns
                        {
                            strTCKN = cCrypto.EncryptDES(strTCKN),
                            strName = strName,
                            strSurname = strSurname,
                            strType = strType,
                            strPolicyNo = strPolicyNo,
                            flPrice = Utilities.NullFixDecimal(flPrice),
                            strPhone = strPhone,
                            strStatusCode = "GON",
                            bolIsDelete = false,
                            dtRegisterDate = DateTime.Now,
                            intInsUserID = Sessions.CurrentUser.intCmsUserID
                        };

                        db.tblBalanceReturns.Add(balanceReturn);
                        db.SaveChanges();

                        Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Poliçe eklendi.");

                        return Redirect("/balancereturn/query");
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/BalanceReturn/QueryInsUpd/Post", Sessions.CurrentUser.intCmsUserID);
            }

            return QueryInsUpd(strMessage);
        }
        #endregion

        #region Post Excel
        [UserFilter]
        [HttpPost]
        public EmptyResult QueryExcel(HttpPostedFileBase fileexcel)
        {
            string strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");
            bool bolSuccess = false;

            if (fileexcel == null || fileexcel.ContentLength == 0)
            {
                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Dosya seçiniz giriniz!");
            }
            else
            {
                try
                {
                    List<BalanceReturnExcel> balanceReturnExcels = new List<BalanceReturnExcel>();

                    using (ExcelPackage package = new ExcelPackage(fileexcel.InputStream))
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets[1];

                        for (int i = workSheet.Dimension.Start.Row + 1; i <= workSheet.Dimension.End.Row; i++)
                        {
                            BalanceReturnExcel balanceReturnExcel = new BalanceReturnExcel
                            {
                                tckn = workSheet.Cells[i, 1].Value.ToString(),
                                ad = workSheet.Cells[i, 2].Value.ToString(),
                                soyad = workSheet.Cells[i, 3].Value.ToString(),
                                turu = workSheet.Cells[i, 4].Value.ToString(),
                                police = workSheet.Cells[i, 5].Value.ToString(),
                                tutar = workSheet.Cells[i, 6].Value.ToString(),
                                telefon = workSheet.Cells[i, 7].Value.ToString(),
                                durumkod = workSheet.Cells[i, 8].Value.ToString()
                            };

                            if (string.IsNullOrEmpty(balanceReturnExcel.tckn)  || string.IsNullOrEmpty(balanceReturnExcel.police) || string.IsNullOrEmpty(balanceReturnExcel.telefon) || (string.IsNullOrEmpty(balanceReturnExcel.tutar) || Utilities.NullFixDecimal(balanceReturnExcel.tutar) == 0))
                            {
                                if (string.IsNullOrEmpty(balanceReturnExcel.tckn))
                                {
                                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Kimlik numarasını giriniz! Sıra [" + i + "]");
                                }
                                else if (string.IsNullOrEmpty(balanceReturnExcel.police))
                                {
                                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Poliçe numarasını giriniz! Sıra [" + i + "]");
                                }
                                else if (string.IsNullOrEmpty(balanceReturnExcel.telefon))
                                {
                                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Telefon numarasını giriniz! Sıra [" + i + "]");
                                }
                                else if (string.IsNullOrEmpty(balanceReturnExcel.tutar) || Utilities.NullFixDecimal(balanceReturnExcel.tutar) == 0)
                                {
                                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Tutarı giriniz! Sıra [" + i + "]");
                                }

                                balanceReturnExcels = null;

                                break;
                            }

                            balanceReturnExcels.Add(balanceReturnExcel);
                        }
                    }

                    if (balanceReturnExcels != null && balanceReturnExcels.Count > 0)
                    {
                        using (BireyselEntities db = new BireyselEntities())
                        {
                            foreach (var item in balanceReturnExcels)
                            {
                                tblBalanceReturns balanceReturn = new tblBalanceReturns
                                {
                                    strTCKN = cCrypto.EncryptDES(item.tckn),
                                    strName = item.ad,
                                    strSurname = item.soyad,
                                    strType = item.turu,
                                    strPolicyNo = item.police,
                                    flPrice = Utilities.NullFixDecimal(item.tutar),
                                    strPhone = item.telefon,
                                    strStatusCode = item.durumkod,
                                    bolIsDelete = false,
                                    dtRegisterDate = DateTime.Now,
                                    intInsUserID = Sessions.CurrentUser.intCmsUserID
                                };

                                db.tblBalanceReturns.Add(balanceReturn);
                            }

                            db.SaveChanges();

                            strMessage = Utilities.Msg(Utilities.MsgType.success, "Excel yüklendi.");
                            bolSuccess = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.Exception(ex.ToString(), "/BalanceReturn/QueryExcel/Post", Sessions.CurrentUser.intCmsUserID);
                }
            }

            Sessions.Message = strMessage;

            if (bolSuccess)
            {
                Response.Redirect("/balancereturn/query");
            }
            else
            {
                Response.Redirect("/balancereturn/queryinsupd?type=excel");
            }

            return new EmptyResult();
        }
        #endregion

        #endregion

        #region Settings

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult Settings(string strMessage = "")
        {
            tblBalanceReturnSettings balanceReturn = null;

            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    balanceReturn = db.tblBalanceReturnSettings.FirstOrDefault();
                }

                if (!string.IsNullOrEmpty(strMessage))
                {
                    ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/BalanceReturn/Settings/Get", Sessions.CurrentUser.intCmsUserID);
            }

            return View(balanceReturn);
        }
        #endregion

        #region Pos
        [UserFilter]
        [HttpPost]
        [jCryptionHandler]
        public ActionResult Settings()
        {
            string strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");

            string intYear = Request.Form["intYear"].Trim();
            string strReturnPrice1 = Request.Form["strReturnPrice1"].Trim();
            string strReturnPrice2 = Request.Form["strReturnPrice2"].Trim();

            ViewBag.intYear = intYear;
            ViewBag.strReturnPrice1 = strReturnPrice1;
            ViewBag.strReturnPrice2 = strReturnPrice2;

            if ((string.IsNullOrEmpty(intYear) || Utilities.NullFixInt(intYear) == 0) || string.IsNullOrEmpty(strReturnPrice1) || string.IsNullOrEmpty(strReturnPrice2))
            {
                if (string.IsNullOrEmpty(intYear) || Utilities.NullFixInt(intYear) == 0)
                {
                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "İade yılını giriniz!");
                }
                else if (string.IsNullOrEmpty(strReturnPrice1))
                {
                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "İade hakkı tutarını giriniz!");
                }
                else if (string.IsNullOrEmpty(strReturnPrice2))
                {
                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Bağış tutarı giriniz!");
                }

                return Settings(strMessage);
            }

            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    var balanceReturn = db.tblBalanceReturnSettings.FirstOrDefault();
                    if (balanceReturn != null)
                    {
                        balanceReturn.intYear = Utilities.NullFixInt(intYear);
                        balanceReturn.strReturnPrice1 = strReturnPrice1;
                        balanceReturn.strReturnPrice2 = strReturnPrice2;
                        balanceReturn.dtUpdateDate = DateTime.Now;
                        balanceReturn.intUpdUserID = Sessions.CurrentUser.intCmsUserID;

                        db.SaveChanges();
                    }
                    else
                    {
                        balanceReturn = new tblBalanceReturnSettings
                        {
                            intYear = Utilities.NullFixInt(intYear),
                            strReturnPrice1 = strReturnPrice1,
                            strReturnPrice2 = strReturnPrice2,
                            dtUpdateDate = DateTime.Now,
                            intUpdUserID = Sessions.CurrentUser.intCmsUserID
                        };

                        db.tblBalanceReturnSettings.Add(balanceReturn);
                        db.SaveChanges();
                    }

                    Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Ayarlar güncellendi.");

                    return Redirect("/balancereturn/settings");
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/BalanceReturn/Settings/Post", Sessions.CurrentUser.intCmsUserID);
            }

            return Settings(strMessage);
        }
        #endregion

        #endregion
    }
}
