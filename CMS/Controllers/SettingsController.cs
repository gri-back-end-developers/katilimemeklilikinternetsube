﻿using CMS.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Models;
using CMS.Classes.Tools;
using CMS.Classes.ClassData;
using System.Configuration;

namespace CMS.Controllers
{
    public class SettingsController : Controller
    {
        #region Maintenance

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult Maintenance(string strMessage = "")
        {
            cSetting setting = new cSetting();
            try
            {
                #region Bireysel
                using (BireyselEntities bireyselDB = new BireyselEntities())
                {
                    var bireyselBakim = bireyselDB.tblSettings.Where(w => w.strName == "bakim").FirstOrDefault();
                    if (bireyselBakim != null)
                    {
                        setting.bolBireyselBakim = bireyselBakim.strValue == "1";
                    }
                }
                #endregion

                #region Kurumsal
                using (KurumsalEntities kurumsalDB = new KurumsalEntities())
                {
                    var kurumsalBakim = kurumsalDB.tblSettings.Where(w => w.strName == "bakim").FirstOrDefault();
                    if (kurumsalBakim != null)
                    {
                        setting.bolKurumsalBakim = kurumsalBakim.strValue == "1";
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Settings/Maintenance/Get", Sessions.CurrentUser.intCmsUserID);
            }

            if (!string.IsNullOrEmpty(strMessage))
            {
                ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
            }

            return View(setting);
        }
        #endregion

        #region Post
        [UserFilter]
        [HttpPost]
        public ActionResult Maintenance()
        {
            string strMessage = "";
            try
            {
                bool bakimbireysel = Request.Form["bakimbireysel"] == "on" || Request.Form["bakimbireysel"] == "checked" || Request.Form["bakimbireysel"] == "true";
                bool bakimkurumsal = Request.Form["bakimkurumsal"] == "on" || Request.Form["bakimkurumsal"] == "checked" || Request.Form["bakimkurumsal"] == "true";

                #region Bireysel
                using (BireyselEntities bireyselDB = new BireyselEntities())
                {
                    var setting = bireyselDB.tblSettings.Where(w => w.strName == "bakim").FirstOrDefault();
                    if (setting == null)
                    {
                        bireyselDB.tblSettings.Add(new tblSettings
                        {
                            dtUpdateDate = DateTime.Now,
                            strName = "bakim",
                            strValue = bakimbireysel ? "1" : "0"
                        });
                    }
                    else
                    {
                        setting.strValue = bakimbireysel ? "1" : "0";
                        setting.dtUpdateDate = DateTime.Now;
                    }
                    bireyselDB.SaveChanges();
                }
                #endregion

                #region Kurumsal
                using (KurumsalEntities kurumsalDB = new KurumsalEntities())
                {
                    var setting = kurumsalDB.tblSettings.Where(w => w.strName == "bakim").FirstOrDefault();
                    if (setting == null)
                    {
                        kurumsalDB.tblSettings.Add(new tblSettings
                        {
                            dtUpdateDate = DateTime.Now,
                            strName = "bakim",
                            strValue = bakimkurumsal ? "1" : "0"
                        });
                    }
                    else
                    {
                        setting.strValue = bakimkurumsal ? "1" : "0";
                        setting.dtUpdateDate = DateTime.Now;
                    }
                    kurumsalDB.SaveChanges();
                }
                #endregion

                strMessage = Utilities.Msg(Utilities.MsgType.success, "Ayarlar kaydedildi.");

                Logs.Setting(1, Sessions.CurrentUser.intCmsUserID, "Bakım ayarları değiştirildi.");
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Settings/Maintenance/Post", Sessions.CurrentUser.intCmsUserID);

                strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return Maintenance(HttpUtility.HtmlEncode(strMessage));
        }
        #endregion

        #endregion

        #region Users
        [UserFilter]
        public ActionResult Users(string strMessage = "")
        {
            if (!CMS.Classes.Tools.Sessions.CurrentUser.bolIsAdmin)
            {
                return Redirect(ConfigurationManager.AppSettings["DefaultPage"]);
            }

            List<tblCmsUsers> users = null;

            if (!string.IsNullOrEmpty(strMessage))
            {
                ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
            }

            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    if (Request.QueryString["i"] != null)
                    {
                        int intCmsUserID = Utilities.NullFixInt(cCrypto.DecryptDES(Request.QueryString["i"]));

                        var delUser = db.tblCmsUsers.Where(w => !w.bolIsDelete && w.intCmsUserID == intCmsUserID).FirstOrDefault();
                        if (delUser != null)
                        {
                            delUser.bolIsDelete = true;
                            db.SaveChanges();

                            ViewBag.Msg = Utilities.Msg(Utilities.MsgType.success, "Kullanıcı silindi.");
                        }
                    }

                    users = db.tblCmsUsers.Where(w => !w.bolIsDelete).ToList();

                    int intPageSize = 10;
                    ViewBag.intCount = (users.Count / intPageSize) + (users.Count % intPageSize > 0 ? 1 : 0);

                    int intPage = QueryStrings.page;
                    intPage = intPage > 0 ? intPage - 1 : intPage;

                    users = users.Skip(intPage * intPageSize).Take(intPageSize).ToList();
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Settings/Users", Sessions.CurrentUser.intCmsUserID);
            }

            return View(users);
        }
        #endregion

        #region UserInsUpd

        #region Get
        [UserFilter]
        [HttpGet]
        [jCryptionHandler]
        public ActionResult UserInsUpd(string strMessage = "", string strGet = "")
        {
            tblCmsUsers user = null;

            if (Request.QueryString["getPublicKey"] == null)
            {
                if (!CMS.Classes.Tools.Sessions.CurrentUser.bolIsAdmin)
                {
                    return Redirect(ConfigurationManager.AppSettings["DefaultPage"]);
                }

                try
                {
                    if (Request.QueryString["i"] != null)
                    {
                        int intCmsUserID = Utilities.NullFixInt(cCrypto.DecryptDES(Request.QueryString["i"]));

                        using (BireyselEntities db = new BireyselEntities())
                        {
                            user = db.tblCmsUsers.FirstOrDefault(f => f.intCmsUserID == intCmsUserID);
                        }
                    }

                    if (!string.IsNullOrEmpty(strMessage))
                    {
                        ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
                    }
                }
                catch (Exception ex)
                {
                    Logs.Exception(ex.ToString(), "/Settings/UserInsUpd/Get", Sessions.CurrentUser.intCmsUserID);
                }
            }

            return View(user);
        }
        #endregion

        #region Post
        [UserFilter]
        [HttpPost]
        [jCryptionHandler]
        public ActionResult UserInsUpd()
        {
            if (Request.Form["formvalue"] == null)
            {
                return View();
            }

            string strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");

            if (Sessions.CurrentUser.bolIsAdmin)
            {
                byte intUserType = Utilities.NullFixByte(Request.Form["usertype"].Trim());
                string strUserName = Request.Form["username"].Trim();
                string strPss = Request.Form["pss"].Trim();
                bool bolIsActive = Request.Form["bolIsActive"] == "on" || Request.Form["bolIsActive"] == "checked" || Request.Form["bolIsActive"] == "true";
                bool bolIsAdmin = Request.Form["bolIsAdmin"] == "on" || Request.Form["bolIsAdmin"] == "checked" || Request.Form["bolIsAdmin"] == "true";

                ViewBag.username = strUserName;
                ViewBag.bolIsActive = bolIsActive;
                ViewBag.bolIsAdmin = bolIsAdmin;
                ViewBag.usertype = intUserType;
                ViewBag.pss = strPss;
                ViewBag.i = Request.Form["i"];

                if (string.IsNullOrEmpty(strUserName) || string.IsNullOrEmpty(strPss))
                {
                    if (string.IsNullOrEmpty(Request.Form["username"].Trim()))
                    {
                        strMessage = Utilities.Msg(Utilities.MsgType.warning, "Kullanıcı adını giriniz!");
                    }
                    else
                    {
                        strMessage = Utilities.Msg(Utilities.MsgType.warning, "Şifreyi giriniz!");
                    }

                    return UserInsUpd(strMessage, "");
                }

                try
                {
                    strPss = cCrypto.EncryptDES(strPss);

                    using (BireyselEntities db = new BireyselEntities())
                    {
                        if (!string.IsNullOrEmpty(Request.Form["i"]))
                        {
                            int intCmsUserID = Utilities.NullFixInt(cCrypto.DecryptDES(Request.Form["i"]));
                            if (intCmsUserID > 0)
                            {
                                if (db.tblCmsUsers.Count(c => !c.bolIsDelete && c.intCmsUserID != intCmsUserID && c.strUserName == strUserName) == 0)
                                {
                                    var user = db.tblCmsUsers.Where(w => w.intCmsUserID == intCmsUserID).FirstOrDefault();
                                    if (user != null)
                                    {
                                        user.intUserType = intUserType;
                                        user.strPss = strPss;
                                        user.strUserName = strUserName;
                                        user.bolIsActive = bolIsActive;
                                        user.bolIsAdmin = bolIsAdmin;
                                        db.SaveChanges();

                                        Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Kullanıcı güncellendi.");

                                        return Redirect("/settings/users");
                                    }
                                }
                                else
                                {
                                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Aynı isimde kullanıcı adı mevcut!");
                                }
                            }
                        }
                        else
                        {
                            if (db.tblCmsUsers.Count(c => !c.bolIsDelete && c.strUserName == strUserName) == 0)
                            {
                                tblCmsUsers user = new tblCmsUsers
                                {
                                    intUserType = intUserType,
                                    strPss = strPss,
                                    strUserName = strUserName,
                                    bolIsActive = bolIsActive,
                                    bolIsAdmin = bolIsAdmin,
                                    bolIsDelete = false,
                                    dtRegisterDate = DateTime.Now
                                };
                                db.tblCmsUsers.Add(user);
                                db.SaveChanges();

                                Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Kullanıcı eklendi.");

                                return Redirect("/settings/users");
                            }
                            else
                            {
                                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Aynı isimde kullanıcı adı mevcut!");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logs.Exception(ex.ToString(), "/Settings/UserInsUpd/Post", Sessions.CurrentUser.intCmsUserID);
                }

                return UserInsUpd(strMessage, "");
            }
            else
            {
                return Redirect(ConfigurationManager.AppSettings["DefaultPage"]);
            }
        }
        #endregion

        #endregion

        #region Chat

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult Chat(string strMessage = "")
        {
            cSetting setting = new cSetting();
            try
            {
                #region Bireysel
                using (BireyselEntities bireyselDB = new BireyselEntities())
                {
                    var bireyselChat = bireyselDB.tblSettings.Where(w => w.strName == "chat").FirstOrDefault();
                    if (bireyselChat != null)
                    {
                        setting.bolBireyselChat = bireyselChat.strValue == "1";
                    }
                } 
                #endregion

                #region KatilimEmeklilik
                using (KatilimEmeklilikEntities kurumsalDB = new KatilimEmeklilikEntities())
                {
                    var portalChat = kurumsalDB.tblChatSetting.FirstOrDefault();
                    if (portalChat != null)
                    {
                        setting.bolPortalChat = portalChat.bolChat;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Settings/Chat/Get", Sessions.CurrentUser.intCmsUserID);
            }

            if (!string.IsNullOrEmpty(strMessage))
            {
                ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
            }

            return View(setting);
        }
        #endregion

        #region Post
        [UserFilter]
        [HttpPost]
        public ActionResult Chat()
        {
            string strMessage = "";
            try
            {
                bool chatbireysel = Request.Form["chatbireysel"] == "on" || Request.Form["chatbireysel"] == "checked" || Request.Form["chatbireysel"] == "true";
                bool chatportal = Request.Form["chatportal"] == "on" || Request.Form["chatportal"] == "checked" || Request.Form["chatportal"] == "true";

                #region Bireysel
                using (BireyselEntities bireyselDB = new BireyselEntities())
                {
                    var setting = bireyselDB.tblSettings.Where(w => w.strName == "chat").FirstOrDefault();
                    if (setting == null)
                    {
                        bireyselDB.tblSettings.Add(new tblSettings
                        {
                            dtUpdateDate = DateTime.Now,
                            strName = "chat",
                            strValue = chatbireysel ? "1" : "0"
                        });
                    }
                    else
                    {
                        setting.strValue = chatbireysel ? "1" : "0";
                        setting.dtUpdateDate = DateTime.Now;
                    }
                    bireyselDB.SaveChanges();
                }
                #endregion

                #region KatilimEmeklilik
                using (KatilimEmeklilikEntities keDB = new KatilimEmeklilikEntities())
                {
                    var setting = keDB.tblChatSetting.FirstOrDefault();
                    if (setting == null)
                    {
                        keDB.tblChatSetting.Add(new tblChatSetting
                        {
                            dtUpdateDate = DateTime.Now,
                            bolChat = chatportal
                        });
                    }
                    else
                    {
                        setting.bolChat = chatportal;
                        setting.dtUpdateDate = DateTime.Now;
                    }
                    keDB.SaveChanges();
                }
                #endregion

                strMessage = Utilities.Msg(Utilities.MsgType.success, "Ayarlar kaydedildi.");

                Logs.Setting(2, Sessions.CurrentUser.intCmsUserID, "Online chat ayarları değiştirildi.");
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Settings/Chat/Post", Sessions.CurrentUser.intCmsUserID);

                strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return Chat(HttpUtility.HtmlEncode(strMessage));
        }
        #endregion

        #endregion
    }
}
