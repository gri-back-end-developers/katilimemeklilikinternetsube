﻿using CMS.Classes;
using CMS.Classes.ClassData;
using CMS.Classes.Tools;
using CMS.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS.Controllers
{
    public class RprtsController : Controller
    {
        #region CollectionFileLogs
        [UserFilter]
        public ActionResult CollectionFileLogs()
        {
            List<tblFileUploadLogs> fileUploadLogs = null;
            string strMessage = "";

            try
            {
                KurumsalEntities db = new KurumsalEntities();

                string strFileType = Request.QueryString["filetype"];
                string strCustomerCode = Request.QueryString["customercode"];
                string strRefNo = Request.QueryString["refno"];
                string strTCKN = Request.QueryString["tckn"];
                if (!string.IsNullOrEmpty(strTCKN))
                {
                    strTCKN = Encryptor.Encrypt(strTCKN);
                }

                fileUploadLogs = db.tblFileUploadLogs.Where(w =>
                    (string.IsNullOrEmpty(strFileType) ? true : w.strFileType == strFileType)
                    && (string.IsNullOrEmpty(strCustomerCode) ? true : w.strCustomerCode == strCustomerCode)
                    && (string.IsNullOrEmpty(strRefNo) ? true : w.strRefNo == strRefNo)
                    && (string.IsNullOrEmpty(strTCKN) ? true : w.tblUserLoginLogs.strTCKN == strTCKN)).OrderByDescending(o => o.dtRegisterDate).ToList();

                int intPageSize = 20;
                ViewBag.intCount = (fileUploadLogs.Count / intPageSize) + (fileUploadLogs.Count % intPageSize > 0 ? 1 : 0);

                int intPage = QueryStrings.page;
                intPage = intPage > 0 ? intPage - 1 : intPage;

                fileUploadLogs = fileUploadLogs.Skip(intPage * intPageSize).Take(intPageSize).ToList();

                if (fileUploadLogs.Count == 0)
                {
                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Kayıt bulunamadı!");
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Rprts/CollectionFileLogs", Sessions.CurrentUser.intCmsUserID);
                strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            if (!string.IsNullOrEmpty(strMessage))
            {
                ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
            }

            return View(fileUploadLogs);
        }
        #endregion

        #region WSLogs
        [UserFilter]
        public ActionResult WSLogs()
        {
            List<tblUserExceptionLogs> userExceptionLogs = null;
            string strMessage = "";

            try
            {
                KurumsalEntities db = new KurumsalEntities();

                string strPage = Request.QueryString["url"];
                string strMethodName = Request.QueryString["methodname"];
                string strJson = Request.QueryString["json"];
                string strTCKN = Request.QueryString["tckn"];
                if (!string.IsNullOrEmpty(strTCKN))
                {
                    strTCKN = Encryptor.Encrypt(strTCKN);

                    userExceptionLogs = db.tblUserExceptionLogs.Where(w =>
                        (string.IsNullOrEmpty(strPage) ? true : w.strPage == strPage)
                        && (string.IsNullOrEmpty(strMethodName) ? !string.IsNullOrEmpty(w.strMethodName) : w.strMethodName.Contains(strMethodName))
                        && (string.IsNullOrEmpty(strJson) ? true : w.strJson.Contains(strJson))
                        && (string.IsNullOrEmpty(strTCKN) ? true : w.tblUserLoginLogs.strTCKN == strTCKN)).OrderByDescending(o => o.dtRegisterDate).ToList();

                    int intPageSize = 20;
                    ViewBag.intCount = (userExceptionLogs.Count / intPageSize) + (userExceptionLogs.Count % intPageSize > 0 ? 1 : 0);

                    int intPage = QueryStrings.page;
                    intPage = intPage > 0 ? intPage - 1 : intPage;

                    userExceptionLogs = userExceptionLogs.Skip(intPage * intPageSize).Take(intPageSize).ToList();

                    if (userExceptionLogs.Count == 0)
                    {
                        strMessage = Utilities.Msg(Utilities.MsgType.warning, "Kayıt bulunamadı!");
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Rprts/WSLogs", Sessions.CurrentUser.intCmsUserID);
                strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            if (!string.IsNullOrEmpty(strMessage))
            {
                ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
            }

            return View(userExceptionLogs);
        }
        #endregion

        #region UserLogs
        [UserFilter]
        public ActionResult UserLogs()
        {
            List<cUserLogs> userLogs = new List<cUserLogs>();
            string strMessage = "";

            try
            {
                KurumsalEntities dbKurumsal = new KurumsalEntities();
                BireyselEntities dbBireysel = new BireyselEntities();

                string strType = Request.QueryString["type"];
                string strYear = Request.QueryString["year"];
                string strMonth = Request.QueryString["month"];

                List<cUserLogData> logs = new List<cUserLogData>();

                if (string.IsNullOrEmpty(strYear) && string.IsNullOrEmpty(strMonth))
                {
                    if (strType == "kurumsal")
                    {
                        logs = dbKurumsal.tblUserLoginLogs.Select(s => new cUserLogData { dtTime = s.dtRegisterDate, strTCKN = s.strTCKN }).ToList();
                    }
                    else if (strType == "bireysel")
                    {
                        logs = dbBireysel.tblUserLogs.Select(s => new cUserLogData { dtTime = s.dtRegisterDate, strTCKN = s.strTCKN }).ToList();
                    }

                    userLogs.Add(new cUserLogs
                    {
                        strTitle = "Tüm Yıllar",
                        intTotalLoginCount = logs.Count,
                        intUserLoginCount = logs.GroupBy(g => g.strTCKN).Count()
                    });
                }
                else if (!string.IsNullOrEmpty(strYear))
                { 
                    int intYear = Utilities.NullFixInt(strYear);

                    if (string.IsNullOrEmpty(strMonth))
                    {
                        if (strType == "kurumsal")
                        {
                            logs = dbKurumsal.tblUserLoginLogs.Where(w => w.dtRegisterDate.Year == intYear).Select(s => new cUserLogData { dtTime = s.dtRegisterDate, strTCKN = s.strTCKN }).ToList();
                        }
                        else if (strType == "bireysel")
                        {
                            logs = dbBireysel.tblUserLogs.Where(w => w.dtRegisterDate.Year == intYear).Select(s => new cUserLogData { dtTime = s.dtRegisterDate, strTCKN = s.strTCKN }).ToList();
                        }

                        for (int i = 1; i <= 12; i++)
                        {
                            var log = logs.Where(w => w.dtTime.Month == i).ToList();
                            userLogs.Add(new cUserLogs
                            {
                                strTitle = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i),
                                intTotalLoginCount = log.Count,
                                intUserLoginCount = log.GroupBy(g => g.strTCKN).Count()
                            });
                        }

                        userLogs.Add(new cUserLogs
                        {
                            strTitle = "<strong>Toplam</strong>",
                            intTotalLoginCount = logs.Count,
                            intUserLoginCount = logs.GroupBy(g => g.strTCKN).Count()
                        });
                    }
                    else
                    {
                        int intMonth = Utilities.NullFixInt(strMonth);

                        if (strType == "kurumsal")
                        {
                            logs = dbKurumsal.tblUserLoginLogs.Where(w => w.dtRegisterDate.Year == intYear && w.dtRegisterDate.Month == intMonth).Select(s => new cUserLogData { dtTime = s.dtRegisterDate, strTCKN = s.strTCKN }).ToList();
                        }
                        else if (strType == "bireysel")
                        {
                            logs = dbBireysel.tblUserLogs.Where(w => w.dtRegisterDate.Year == intYear && w.dtRegisterDate.Month == intMonth).Select(s => new cUserLogData { dtTime = s.dtRegisterDate, strTCKN = s.strTCKN }).ToList();
                        }

                        for (int i = 1; i <= 31; i++)
                        {
                            try
                            {
                                var log = logs.Where(w => w.dtTime.Day == i).ToList();
                                userLogs.Add(new cUserLogs
                                {
                                    strTitle = i.ToString() + " - " + new DateTime(intYear, intMonth, i).ToString("dddd"),
                                    intTotalLoginCount = log.Count,
                                    intUserLoginCount = log.GroupBy(g => g.strTCKN).Count()
                                });
                            }
                            catch { }
                        }

                        userLogs.Add(new cUserLogs
                        {
                            strTitle = "<strong>Toplam</strong>",
                            intTotalLoginCount = logs.Count,
                            intUserLoginCount = logs.GroupBy(g => g.strTCKN).Count()
                        });
                    }

                    if (userLogs.Count == 0)
                    {
                        userLogs = null;
                        strMessage = Utilities.Msg(Utilities.MsgType.warning, "Kayıt bulunamadı!");
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Rprts/UserLogs", Sessions.CurrentUser.intCmsUserID);
                strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            if (!string.IsNullOrEmpty(strMessage))
            {
                ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
            }

            return View(userLogs);
        }
        #endregion

        #region FormDataLogs
        [UserFilter]
        public ActionResult FormDataLogs()
        {
            List<tblFormDatas> formDatas = null;
            string strMessage = "";

            try
            {
                BireyselEntities db = new BireyselEntities();

                string strFormTypeName = Request.QueryString["formtypename"];
                if (!string.IsNullOrEmpty(strFormTypeName))
                {
                    if (strFormTypeName == "1")
                    {
                        strFormTypeName = "İletişim / Öneri Formları";
                    }
                    else if (strFormTypeName == "2")
                    {
                        strFormTypeName = "Bilgi Alma Formu";
                    }
                }

                int intCustomerNo = Request.QueryString["customerno"] != null ? Utilities.NullFixInt(Request.QueryString["customerno"]) : 0;
                string strPolicyNo = Request.QueryString["policyno"];
                string strEmail = Request.QueryString["email"];

                formDatas = db.tblFormDatas.Where(w =>
                    (string.IsNullOrEmpty(strFormTypeName) ? true : w.strFormTypeName == strFormTypeName)
                    && (intCustomerNo == 0 ? true : w.intCustomerNo == intCustomerNo)
                    && (string.IsNullOrEmpty(strPolicyNo) ? true : w.strPolicyNo == strPolicyNo)
                    && (string.IsNullOrEmpty(strEmail) ? true : w.strEmail == strEmail)).OrderByDescending(o => o.dtRegisterDate).ToList();

                int intPageSize = 20;
                ViewBag.intCount = (formDatas.Count / intPageSize) + (formDatas.Count % intPageSize > 0 ? 1 : 0);

                int intPage = QueryStrings.page;
                intPage = intPage > 0 ? intPage - 1 : intPage;

                formDatas = formDatas.Skip(intPage * intPageSize).Take(intPageSize).ToList();

                if (formDatas.Count == 0)
                {
                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Kayıt bulunamadı!");
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Rprts/FormDataLogs", Sessions.CurrentUser.intCmsUserID);
                strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            if (!string.IsNullOrEmpty(strMessage))
            {
                ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
            }

            return View(formDatas);
        }
        #endregion
    }
}
