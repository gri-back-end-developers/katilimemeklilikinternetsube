﻿using CMS.Classes;
using CMS.Classes.ClassData;
using CMS.Classes.Tools;
using CMS.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS.Controllers
{
    public class DocumentsController : Controller
    {
        #region DocumentTypes
        [UserFilter]
        public ActionResult DocumentTypes(string strMessage = "")
        {
            List<tblCmsDocumentTypes> documentTypes = null;

            if (!string.IsNullOrEmpty(strMessage))
            {
                ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
            }

            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    if (Request.QueryString["i"] != null)
                    {
                        int intDocumentTypeID = Utilities.NullFixInt(Request.QueryString["i"]);

                        var delDocumentTypes = db.tblCmsDocumentTypes.Where(w => !w.bolIsDelete && w.intDocumentTypeID == intDocumentTypeID).FirstOrDefault();
                        if (delDocumentTypes != null)
                        {
                            delDocumentTypes.bolIsDelete = true;
                            db.SaveChanges();

                            ViewBag.Msg = Utilities.Msg(Utilities.MsgType.success, "Kriter silindi.");
                        }
                    }

                    documentTypes = db.tblCmsDocumentTypes.Where(w => !w.bolIsDelete).ToList();

                    int intPageSize = 10;
                    ViewBag.intCount = (documentTypes.Count / intPageSize) + (documentTypes.Count % intPageSize > 0 ? 1 : 0);

                    int intPage = QueryStrings.page;
                    intPage = intPage > 0 ? intPage - 1 : intPage;

                    documentTypes = documentTypes.Skip(intPage * intPageSize).Take(intPageSize).ToList();
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Documents/DocumentTypes", Sessions.CurrentUser.intCmsUserID);
            }

            return View(documentTypes);
        }
        #endregion

        #region DocumentTypeAdd

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult DocumentTypeAdd(string strMessage = "")
        {
            tblCmsDocumentTypes documentType = null;

            try
            {
                if (Request.QueryString["i"] != null)
                {
                    int intDocumentTypeID = Utilities.NullFixInt(Request.QueryString["i"]);

                    using (BireyselEntities db = new BireyselEntities())
                    {
                        documentType = db.tblCmsDocumentTypes.FirstOrDefault(f => f.intDocumentTypeID == intDocumentTypeID);
                    }
                }

                if (!string.IsNullOrEmpty(strMessage))
                {
                    ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Documents/DocumentTypeAdd/Get", Sessions.CurrentUser.intCmsUserID);
            }

            return View(documentType);
        }
        #endregion

        #region Post
        [UserFilter]
        [HttpPost]
        public ActionResult DocumentTypeAdd()
        {
            string strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");
            string strName = Request.Form["name"].Trim();

            ViewBag.name = strName;

            if (string.IsNullOrEmpty(strName))
            {
                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Kriter adını giriniz!");

                return DocumentTypeAdd(strMessage);
            }

            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["i"]))
                    {
                        int intDocumentTypeID = Utilities.NullFixInt(Request.QueryString["i"]);
                        if (intDocumentTypeID > 0)
                        {
                            if (db.tblCmsDocumentTypes.Count(c => !c.bolIsDelete && c.intDocumentTypeID != intDocumentTypeID && c.strName == strName) == 0)
                            {
                                var documentType = db.tblCmsDocumentTypes.Where(w => w.intDocumentTypeID == intDocumentTypeID).FirstOrDefault();
                                if (documentType != null)
                                {
                                    documentType.strName = strName;
                                    documentType.dtRegisterDate = DateTime.Now;
                                    documentType.bolIsDelete = false;
                                    db.SaveChanges();

                                    Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Kriter güncellendi.");

                                    return Redirect("/documents/documenttypes");
                                }
                            }
                            else
                            {
                                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Aynı isimde kriter adı mevcut!");
                            }
                        }
                    }
                    else
                    {
                        if (db.tblCmsDocumentTypes.Count(c => !c.bolIsDelete && c.strName == strName) == 0)
                        {
                            tblCmsDocumentTypes documentType = new tblCmsDocumentTypes
                            {
                                strName = strName,
                                bolIsDelete = false,
                                dtRegisterDate = DateTime.Now
                            };
                            db.tblCmsDocumentTypes.Add(documentType);
                            db.SaveChanges();

                            Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Kriter eklendi.");

                            return Redirect("/documents/documenttypes");
                        }
                        else
                        {
                            strMessage = Utilities.Msg(Utilities.MsgType.warning, "Aynı isimde kriter adı mevcut!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Documents/DocumentTypeAdd/Post", Sessions.CurrentUser.intCmsUserID);
            }

            return DocumentTypeAdd(strMessage);
        }
        #endregion

        #endregion

        #region Documents
        [UserFilter]
        public ActionResult Documents()
        {
            cDocuments documents = new cDocuments();

            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    if (Request.QueryString["i"] != null)
                    {
                        int intDocumentID = Utilities.NullFixInt(Request.QueryString["i"]);

                        var delDocuments = db.tblCmsDocuments.Where(w => !w.bolIsDelete && w.intDocumentID == intDocumentID).FirstOrDefault();
                        if (delDocuments != null)
                        {
                            delDocuments.bolIsDelete = true;
                            db.SaveChanges();

                            try
                            {
                                System.IO.File.Delete(Server.MapPath(ConfigurationManager.AppSettings["DocumentSrc"] + delDocuments.strFile));
                            }
                            catch { }

                            ViewBag.Msg = Utilities.Msg(Utilities.MsgType.success, "Doküman silindi.");
                        }
                    }

                    int intDocumentTypeID = Utilities.NullFixInt(Request.QueryString["type"]);
                    var documentType = db.tblCmsDocumentTypes.Where(w => w.intDocumentTypeID == intDocumentTypeID).FirstOrDefault();
                    if (documentType != null)
                    {
                        documents.typeName = documentType.strName;
                    }

                    documents.documents = db.tblCmsDocuments.Where(w => !w.bolIsDelete && w.intDocumentTypeID == intDocumentTypeID).ToList();

                    int intPageSize = 10;
                    ViewBag.intCount = (documents.documents.Count / intPageSize) + (documents.documents.Count % intPageSize > 0 ? 1 : 0);

                    int intPage = QueryStrings.page;
                    intPage = intPage > 0 ? intPage - 1 : intPage;

                    documents.documents = documents.documents.Skip(intPage * intPageSize).Take(intPageSize).ToList();
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Documents/Documents", Sessions.CurrentUser.intCmsUserID);
            }

            return View(documents);
        }
        #endregion

        #region DocumentIns

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult DocumentIns(string strMessage = "")
        {
            cDocumentAdd documentAdd = new cDocumentAdd();

            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    if (Request.QueryString["i"] != null)
                    {
                        int intDocumentID = Utilities.NullFixInt(Request.QueryString["i"]);

                        documentAdd.document = db.tblCmsDocuments.FirstOrDefault(f => !f.bolIsDelete && f.intDocumentID == intDocumentID);
                    }

                    documentAdd.documentTypes = db.tblCmsDocumentTypes.Where(w => !w.bolIsDelete).OrderBy(o => o.strName).ToList();
                }

                if (!string.IsNullOrEmpty(strMessage))
                {
                    ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Documents/DocumentIns/Get", Sessions.CurrentUser.intCmsUserID);
            }

            return View(documentAdd);
        }
        #endregion

        #region Post
        [UserFilter]
        [HttpPost]
        public ActionResult DocumentIns(HttpPostedFileBase file)
        {
            string strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");
            string strName = Request.Form["name"].Trim();
            string strDocumentType = Request.Form["documenttype"];

            ViewBag.name = strName;
            ViewBag.documenttype = strDocumentType;

            if (string.IsNullOrEmpty(strName))
            {
                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Dosya adını giriniz!");

                return DocumentIns(strMessage);
            }

            var allowedExtensions = ConfigurationManager.AppSettings["DocumentExt"].Split(',').ToList();

            string strFileName = "";
            bool bolUpload = false;

            if (file != null)
            {
                if (file.ContentLength > 0)
                {
                    strFileName = Request.Form["name"] + Path.GetExtension(file.FileName);

                    var extension = Path.GetExtension(file.FileName);
                    if (allowedExtensions.Count(c => c == extension) == 0)
                    {
                        strMessage = Utilities.Msg(Utilities.MsgType.warning, "Geçersiz dosya. Sadece " + ConfigurationManager.AppSettings["DocumentExt"] + " uzantılı dosyaları yükleyebilirsiniz.");
                        return DocumentIns(strMessage);
                    }
                }

                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        file.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["DocumentSrc"] + strFileName));
                        bolUpload = true;
                    }
                }
            }
            else if (Request.QueryString["i"] == null)
            {
                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Dosya ekleyiniz!");
                return DocumentIns(strMessage);
            }

            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["i"]))
                    {
                        int intDocumentID = Utilities.NullFixInt(Request.QueryString["i"]);
                        if (intDocumentID > 0)
                        {
                            if (db.tblCmsDocuments.Count(c => !c.bolIsDelete && c.intDocumentID != intDocumentID && c.strName == strName) == 0)
                            {
                                var document = db.tblCmsDocuments.Where(w => w.intDocumentID == intDocumentID).FirstOrDefault();
                                if (document != null)
                                {
                                    document.strName = strName;
                                    document.dtUpdateDate = DateTime.Now; 
                                    if (bolUpload)
                                    {
                                        document.strFile = strFileName;
                                    }

                                    db.SaveChanges();

                                    Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Doküman güncellendi.");

                                    return Redirect("/documents/documents?type=" + document.intDocumentTypeID);
                                }
                            }
                            else
                            {
                                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Aynı isimde doküman adı mevcut!");
                            }
                        }
                    }
                    else
                    {
                        if (db.tblCmsDocuments.Count(c => !c.bolIsDelete && c.strName == strName) == 0)
                        {
                            int intDocumentTypeID = Utilities.NullFixInt(Request.Form["documenttype"]);

                            tblCmsDocuments document = new tblCmsDocuments
                            {
                                intDocumentTypeID = intDocumentTypeID,
                                strName = strName,
                                bolIsDelete = false,
                                strFile = strFileName,
                                dtRegisterDate = DateTime.Now
                            };
                            db.tblCmsDocuments.Add(document);
                            db.SaveChanges();

                            Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Doküman eklendi.");

                            return Redirect("/documents/documents?type=" + document.intDocumentTypeID);
                        }
                        else
                        {
                            strMessage = Utilities.Msg(Utilities.MsgType.warning, "Aynı isimde doküman adı mevcut!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Documents/DocumentIns/Post", Sessions.CurrentUser.intCmsUserID);
            }

            return DocumentIns(strMessage);
        }
        #endregion

        #endregion
    }
}
