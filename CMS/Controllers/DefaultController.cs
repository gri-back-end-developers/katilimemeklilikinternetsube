﻿using CMS.Classes.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using System.Configuration;
using System.Web.Security;
using CMS.Classes.ClassData;
using CMS.Models;

namespace CMS.Controllers
{
    public class DefaultController : Controller
    {
        #region Index Get
        [HttpGet]
        [jCryptionHandler]
        public ActionResult Index(string strMessage = "")
        {
            if (Request.QueryString["getPublicKey"] == null)
            {
                Session.Clear();
                Session.RemoveAll();
                Session.Abandon();

                SessionIDManager manager = new SessionIDManager();
                string NewID = manager.CreateSessionID(this.HttpContext.ApplicationInstance.Context);
                bool redirected = false;
                bool IsAdded = false;
                manager.SaveSessionID(this.HttpContext.ApplicationInstance.Context, NewID, out redirected, out IsAdded);

                if (!string.IsNullOrEmpty(strMessage))
                {
                    ViewBag.strMessage = Utilities.Msg(Utilities.MsgType.warning, strMessage);
                }
            }

            return View();
        }
        #endregion

        #region Index Post
        [HttpPost]
        [jCryptionHandler]
        public ActionResult Index(string username, string userpss)
        {
            if (string.IsNullOrEmpty(username) && string.IsNullOrEmpty(userpss))
            {
                return View();
            }

            string strMessage = "";

            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    userpss = cCrypto.EncryptDES(userpss);
                    var user = db.tblCmsUsers.Where(w => !w.bolIsDelete && w.strUserName == username && w.strPss == userpss && w.bolIsActive).FirstOrDefault();

                    if (user != null)
                    {
                        db.tblCmsUserLogs.Add(new tblCmsUserLogs
                        {
                            dtRegisterDate = DateTime.Now,
                            intCmsUserID = user.intCmsUserID,
                            strIp = Utilities.ClientIP
                        });
                        db.SaveChanges();

                        Sessions.CurrentUser = user;
                        Sessions.SIp = Utilities.ClientIP;

                        FormsAuthentication.SetAuthCookie(username, false);

                        if ((CMS.Classes.Enums.UserTypes)user.intUserType == Classes.Enums.UserTypes.cms)
                        {
                            return Redirect(ConfigurationManager.AppSettings["DefaultPage"]);
                        }
                        else if ((CMS.Classes.Enums.UserTypes)user.intUserType == Classes.Enums.UserTypes.chat)
                        {
                            return Redirect("/settings/chat");
                        }
                        else if ((CMS.Classes.Enums.UserTypes)user.intUserType == Classes.Enums.UserTypes.balancereturn)
                        {
                            return Redirect("/balancereturn/query");
                        }
                    }
                    else
                    {
                        strMessage = "Kullanıcı bilgilerini hatalı girdiniz!";
                    }
                }
            }
            catch
            {
                strMessage = "Bir hata oluştu! Tekrar deneyiniz.";
            }

            return Index(strMessage);
        }
        #endregion
    }
}
