﻿using CMS.Classes; 
using CMS.Classes.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CMS.Controllers
{
    public class ErrorController : Controller
    {
        [UserFilter]
        public ActionResult status404()
        {
            Response.StatusCode = 404;
            return View();
        }
         
        public ActionResult status500()
        {
            Response.StatusCode = 500;
            return View();
        }

        public ActionResult status505()
        {
            Response.StatusCode = 505;
            return View();
        }
    }
}
