﻿#region Directives
using CMS.Classes;
using CMS.Classes.Tools;
using CMS.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
#endregion

namespace CMS.Controllers
{
    public class UpdatesController : Controller
    {
        #region ProductionAndCollection

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult ProductionAndCollection(string strMessage = "")
        {
            List<tblFiles> files = null;
            try
            {
                using (KurumsalEntities db = new KurumsalEntities())
                {
                    files = db.tblFiles.ToList();
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Updates/ProductionAndCollection/Get", Sessions.CurrentUser.intCmsUserID);
            }

            if (!string.IsNullOrEmpty(strMessage))
            {
                ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
            }

            return View(files);
        }
        #endregion

        #region Post
        [UserFilter]
        [HttpPost]
        public ActionResult ProductionAndCollection(List<HttpPostedFileBase> files)
        {
            string strMessage = "";
            try
            {
                var allowedExtensions = new[] { ".xlsm" };

                string strUretimFileName = "";
                bool bolUretimUpload = false;
                string strTahsilatFileName = "";
                bool bolTahsilatUpload = false;

                if (files != null)
                {
                    if (files[0] != null)
                    {
                        if (files[0].ContentLength > 0)
                        {
                            strUretimFileName = Request.Form["txtUretim"] + Path.GetExtension(files[0].FileName);

                            var extension = Path.GetExtension(files[0].FileName);
                            if (!allowedExtensions.Contains(extension))
                            {
                                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Geçersiz dosya. Sadece .xlsm uzantılı dosya yükleyebilirsiniz.");
                                return ProductionAndCollection(HttpUtility.HtmlEncode(strMessage));
                            }
                        }
                    }

                    if (files[1] != null)
                    {
                        if (files[1].ContentLength > 0)
                        {
                            strTahsilatFileName = Request.Form["txtTahsilat"] + Path.GetExtension(files[1].FileName);

                            var extension = Path.GetExtension(files[1].FileName);
                            if (!allowedExtensions.Contains(extension))
                            {
                                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Geçersiz dosya. Sadece .xlsm uzantılı dosya yükleyebilirsiniz.");
                                return ProductionAndCollection(HttpUtility.HtmlEncode(strMessage));
                            }
                        }
                    }

                    string strSrc = ConfigurationManager.AppSettings["OksProductionAndCollection"];
                    if (files[0] != null)
                    {
                        if (files[0].ContentLength > 0)
                        {
                            files[0].SaveAs(strSrc + strUretimFileName);
                            bolUretimUpload = true;
                        }
                    }

                    if (files[1] != null)
                    {
                        if (files[1].ContentLength > 0)
                        {
                            files[1].SaveAs(strSrc + strTahsilatFileName);
                            bolTahsilatUpload = true;
                        }
                    }
                }

                using (KurumsalEntities db = new KurumsalEntities())
                {
                    var uretim = db.tblFiles.Where(w => w.strType == "uretim").FirstOrDefault();
                    if (uretim == null)
                    {
                        db.tblFiles.Add(new tblFiles()
                        {
                            dtUpdateDate = DateTime.Now,
                            strFile = strUretimFileName,
                            strFileName = Request.Form["txtUretim"],
                            strType = "uretim"
                        });
                    }
                    else
                    {
                        uretim.dtUpdateDate = DateTime.Now;
                        if (bolUretimUpload)
                        {
                            uretim.strFile = strUretimFileName;
                        }
                        uretim.strFileName = Request.Form["txtUretim"];
                    }
                    db.SaveChanges();

                    var tahsilat = db.tblFiles.Where(w => w.strType == "tahsilat").FirstOrDefault();
                    if (tahsilat == null)
                    {
                        db.tblFiles.Add(new tblFiles()
                        {
                            dtUpdateDate = DateTime.Now,
                            strFile = strTahsilatFileName,
                            strFileName = Request.Form["txtTahsilat"],
                            strType = "tahsilat"
                        });
                    }
                    else
                    {
                        tahsilat.dtUpdateDate = DateTime.Now;
                        if (bolTahsilatUpload)
                        {
                            tahsilat.strFile = strTahsilatFileName;
                        }
                        tahsilat.strFileName = Request.Form["txtTahsilat"];
                    }
                    db.SaveChanges();

                    strMessage = Utilities.Msg(Utilities.MsgType.success, "Dosyalar kaydedildi.");
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Updates/ProductionAndCollection/Post", Sessions.CurrentUser.intCmsUserID);

                strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");
            }

            return ProductionAndCollection(HttpUtility.HtmlEncode(strMessage));
        }
        #endregion

        #endregion

        #region Forms
        [UserFilter]
        [HttpGet]
        public ActionResult Forms()
        {
            List<tblCmsForms> forms = null;
            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    if (Request.QueryString["i"] != null)
                    {
                        int intFormID = Utilities.NullFixInt(Request.QueryString["i"]);

                        var delForm = db.tblCmsForms.Where(w => !w.bolIsDelete && w.intFormID == intFormID).FirstOrDefault();
                        if (delForm != null)
                        {
                            delForm.bolIsDelete = true;
                            db.SaveChanges();

                            ViewBag.Msg = Utilities.Msg(Utilities.MsgType.success, "Form silindi.");

                            try
                            {
                                System.IO.File.Delete(ConfigurationManager.AppSettings["FormSrc"] + delForm.strFile);
                            }
                            catch { }
                        }
                    }

                    forms = db.tblCmsForms.Where(w => !w.bolIsDelete).OrderBy(o => o.intOrder).ToList();

                    string strType = Request.QueryString["type"];
                    if (!string.IsNullOrEmpty(strType))
                    {
                        forms = forms.Where(w => w.strType == strType).ToList();
                    }
                     
                    int intPageSize = 10;
                    ViewBag.intCount = (forms.Count / intPageSize) + (forms.Count % intPageSize > 0 ? 1 : 0);

                    int intPage = QueryStrings.page;
                    intPage = intPage > 0 ? intPage - 1 : intPage;

                    forms = forms.Skip(intPage * intPageSize).Take(intPageSize).ToList();
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Updates/Forms", Sessions.CurrentUser.intCmsUserID);
            }

            return View(forms);
        }
        #endregion

        #region FormAdd

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult FormAdd(string strMessage = "")
        {
            tblCmsForms form = null;

            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    if (Request.QueryString["i"] != null)
                    {
                        int intFormID = Utilities.NullFixInt(Request.QueryString["i"]);

                        form = db.tblCmsForms.FirstOrDefault(f => !f.bolIsDelete && f.intFormID == intFormID);
                    }
                }

                if (!string.IsNullOrEmpty(strMessage))
                {
                    ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Updates/FormAdd/Get", Sessions.CurrentUser.intCmsUserID);
            }

            return View(form);
        }
        #endregion

        #region Post
        [UserFilter]
        [HttpPost]
        public ActionResult FormAdd(HttpPostedFileBase file)
        {
            string strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");
            string strName = Request.Form["name"].Trim();
            string strType = Request.Form["type"];
            string strOrder = Request.Form["order"];
            bool bolIsActive = Request.Form["bolIsActive"] == "on" || Request.Form["bolIsActive"] == "checked" || Request.Form["bolIsActive"] == "true";

            ViewBag.name = strName;
            ViewBag.type = strType;
            ViewBag.order = strOrder;
            ViewBag.bolIsActive = bolIsActive;

            if (string.IsNullOrEmpty(strName) || string.IsNullOrEmpty(strOrder))
            {
                if (string.IsNullOrEmpty(strName))
                {
                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Form adını giriniz!");
                }
                else if (string.IsNullOrEmpty(strOrder))
                {
                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Sıra giriniz!");
                }

                return FormAdd(strMessage);
            }

            var allowedExtensions = ConfigurationManager.AppSettings["FormExt"].Split(',').ToList();

            string strFileName = "";
            bool bolUpload = false;

            if (file != null)
            {
                if (file.ContentLength > 0)
                {
                    strFileName = Utilities.Friendly(Request.Form["name"]).Trim() + Path.GetExtension(file.FileName);

                    var extension = Path.GetExtension(file.FileName);
                    if (allowedExtensions.Count(c => c == extension) == 0)
                    {
                        strMessage = Utilities.Msg(Utilities.MsgType.warning, "Geçersiz dosya. Sadece " + ConfigurationManager.AppSettings["FormExt"] + " uzantılı dosyaları yükleyebilirsiniz.");
                        return FormAdd(strMessage);
                    }
                }

                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        file.SaveAs(ConfigurationManager.AppSettings["FormSrc"]  + strFileName);
                        bolUpload = true;
                    }
                }
            }
            else if (Request.QueryString["i"] == null)
            {
                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Dosya ekleyiniz!");
                return FormAdd(strMessage);
            }

            try
            {
                using (BireyselEntities db = new BireyselEntities())
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["i"]))
                    {
                        int intFormID = Utilities.NullFixInt(Request.QueryString["i"]);
                        if (intFormID > 0)
                        {
                            if (db.tblCmsForms.Count(c => !c.bolIsDelete && c.intFormID != intFormID && c.strName == strName) == 0)
                            {
                                var form = db.tblCmsForms.Where(w => w.intFormID == intFormID).FirstOrDefault();
                                if (form != null)
                                {
                                    form.strName = strName;
                                    form.intOrder = Utilities.NullFixInt(strOrder);
                                    form.strType = strType;
                                    form.bolIsActive = bolIsActive;
                                    form.dtUpdateDate = DateTime.Now; 
                                    if (bolUpload)
                                    {
                                        form.strFile = strFileName;
                                    }

                                    db.SaveChanges();

                                    Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Form güncellendi.");

                                    return Redirect("/updates/forms");
                                }
                            }
                            else
                            {
                                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Aynı isimde form adı mevcut!");
                            }
                        }
                    }
                    else
                    {
                        if (db.tblCmsForms.Count(c => !c.bolIsDelete && c.strName == strName) == 0)
                        {
                            tblCmsForms form = new tblCmsForms
                            {
                                bolIsActive = bolIsActive,
                                strName = strName,
                                strType = strType,
                                intOrder = Utilities.NullFixInt(strOrder),
                                bolIsDelete = false,
                                strFile = strFileName,
                                dtRegisterDate = DateTime.Now
                            };
                            db.tblCmsForms.Add(form);
                            db.SaveChanges();

                            Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Form eklendi.");

                            return Redirect("/updates/forms");
                        }
                        else
                        {
                            strMessage = Utilities.Msg(Utilities.MsgType.warning, "Aynı isimde form adı mevcut!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Updates/FormAdd/Post", Sessions.CurrentUser.intCmsUserID);
            }

            return FormAdd(strMessage);
        }
        #endregion

        #endregion

        #region Documents
        [UserFilter]
        [HttpGet]
        public ActionResult Documents()
        {
            List<tblCmsOksDocuments> oksDocuments = null;
            try
            {
                using (KurumsalEntities db = new KurumsalEntities())
                {
                    if (Request.QueryString["i"] != null)
                    {
                        int intOksDocumentID = Utilities.NullFixInt(Request.QueryString["i"]);

                        var delDocuments = db.tblCmsOksDocuments.Where(w => !w.bolIsDelete && w.intOksDocumentID == intOksDocumentID).FirstOrDefault();
                        if (delDocuments != null)
                        {
                            delDocuments.bolIsDelete = true;
                            db.SaveChanges();

                            ViewBag.Msg = Utilities.Msg(Utilities.MsgType.success, "Doküman silindi.");

                            try
                            {
                                System.IO.File.Delete(ConfigurationManager.AppSettings["OksDcumentSrc"] + delDocuments.strFile);
                            }
                            catch { }
                        }
                    }

                    oksDocuments = db.tblCmsOksDocuments.Where(w => !w.bolIsDelete).OrderBy(o => o.intOrder).ToList();
                     
                    int intPageSize = 10;
                    ViewBag.intCount = (oksDocuments.Count / intPageSize) + (oksDocuments.Count % intPageSize > 0 ? 1 : 0);

                    int intPage = QueryStrings.page;
                    intPage = intPage > 0 ? intPage - 1 : intPage;

                    oksDocuments = oksDocuments.Skip(intPage * intPageSize).Take(intPageSize).ToList();
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Updates/Documents", Sessions.CurrentUser.intCmsUserID);
            }

            return View(oksDocuments);
        }
        #endregion

        #region DocumentAdd

        #region Get
        [UserFilter]
        [HttpGet]
        public ActionResult DocumentAdd(string strMessage = "")
        {
            tblCmsOksDocuments oksDocument = null;

            try
            {
                using (KurumsalEntities db = new KurumsalEntities())
                {
                    if (Request.QueryString["i"] != null)
                    {
                        int intOksDocumentID = Utilities.NullFixInt(Request.QueryString["i"]);

                        oksDocument = db.tblCmsOksDocuments.FirstOrDefault(f => !f.bolIsDelete && f.intOksDocumentID == intOksDocumentID);
                    }
                }

                if (!string.IsNullOrEmpty(strMessage))
                {
                    ViewBag.Msg = HttpUtility.HtmlDecode(strMessage);
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Updates/DocumentAdd/Get", Sessions.CurrentUser.intCmsUserID);
            }

            return View(oksDocument);
        }
        #endregion

        #region Post
        [UserFilter]
        [HttpPost]
        public ActionResult DocumentAdd(HttpPostedFileBase file)
        {
            string strMessage = Utilities.Msg(Utilities.MsgType.error, "Bir hata oluştu! Tekrar deneyiniz.");
            string strName = Request.Form["name"].Trim(); 
            string strOrder = Request.Form["order"];
            bool bolIsActive = Request.Form["bolIsActive"] == "on" || Request.Form["bolIsActive"] == "checked" || Request.Form["bolIsActive"] == "true";

            ViewBag.name = strName; 
            ViewBag.order = strOrder;
            ViewBag.bolIsActive = bolIsActive;

            if (string.IsNullOrEmpty(strName) || string.IsNullOrEmpty(strOrder))
            {
                if (string.IsNullOrEmpty(strName))
                {
                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Doküman adını giriniz!");
                }
                else if (string.IsNullOrEmpty(strOrder))
                {
                    strMessage = Utilities.Msg(Utilities.MsgType.warning, "Sıra giriniz!");
                }

                return DocumentAdd(strMessage);
            }

            var allowedExtensions = ConfigurationManager.AppSettings["OksDcumentExt"].Split(',').ToList();

            string strFileName = "";
            bool bolUpload = false;

            if (file != null)
            {
                if (file.ContentLength > 0)
                {
                    strFileName = Utilities.Friendly(Request.Form["name"]).Trim() + Path.GetExtension(file.FileName);

                    var extension = Path.GetExtension(file.FileName);
                    if (allowedExtensions.Count(c => c == extension) == 0)
                    {
                        strMessage = Utilities.Msg(Utilities.MsgType.warning, "Geçersiz dosya. Sadece " + ConfigurationManager.AppSettings["OksDcumentExt"] + " uzantılı dosyaları yükleyebilirsiniz.");
                        return DocumentAdd(strMessage);
                    }
                }

                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        file.SaveAs(ConfigurationManager.AppSettings["OksDcumentSrc"] + strFileName);
                        bolUpload = true;
                    }
                }
            }
            else if (Request.QueryString["i"] == null)
            {
                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Dosya ekleyiniz!");
                return DocumentAdd(strMessage);
            }

            try
            {
                using (KurumsalEntities db = new KurumsalEntities())
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["i"]))
                    {
                        int intOksDocumentID = Utilities.NullFixInt(Request.QueryString["i"]);
                        if (intOksDocumentID > 0)
                        {
                            if (db.tblCmsOksDocuments.Count(c => !c.bolIsDelete && c.intOksDocumentID != intOksDocumentID && c.strName == strName) == 0)
                            {
                                var oksDocument = db.tblCmsOksDocuments.Where(w => w.intOksDocumentID == intOksDocumentID).FirstOrDefault();
                                if (oksDocument != null)
                                {
                                    oksDocument.strName = strName;
                                    oksDocument.intOrder = Utilities.NullFixInt(strOrder);
                                    oksDocument.bolIsActive = bolIsActive;
                                    oksDocument.dtUpdateDate = DateTime.Now;
                                    if (bolUpload)
                                    {
                                        oksDocument.strFile = strFileName;
                                    }

                                    db.SaveChanges();

                                    Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Doküman güncellendi.");

                                    return Redirect("/updates/documents");
                                }
                            }
                            else
                            {
                                strMessage = Utilities.Msg(Utilities.MsgType.warning, "Aynı isimde doküman adı mevcut!");
                            }
                        }
                    }
                    else
                    {
                        if (db.tblCmsOksDocuments.Count(c => !c.bolIsDelete && c.strName == strName) == 0)
                        {
                            tblCmsOksDocuments oksDocument = new tblCmsOksDocuments
                            {
                                bolIsActive = bolIsActive,
                                strName = strName, 
                                intOrder = Utilities.NullFixInt(strOrder),
                                bolIsDelete = false,
                                strFile = strFileName,
                                dtRegisterDate = DateTime.Now
                            };
                            db.tblCmsOksDocuments.Add(oksDocument);
                            db.SaveChanges();

                            Sessions.Message = Utilities.Msg(Utilities.MsgType.success, "Doküman eklendi.");

                            return Redirect("/updates/documents");
                        }
                        else
                        {
                            strMessage = Utilities.Msg(Utilities.MsgType.warning, "Aynı isimde doküman adı mevcut!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex.ToString(), "/Updates/DocumentAdd/Post", Sessions.CurrentUser.intCmsUserID);
            }

            return DocumentAdd(strMessage);
        }
        #endregion

        #endregion
    }
}
