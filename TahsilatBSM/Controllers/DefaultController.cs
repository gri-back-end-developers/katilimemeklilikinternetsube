﻿#region Directives
using System;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.SessionState;
using TahsilatBSM.Classes;
using TahsilatBSM.Classes.Tools;
using TahsilatBSM.Models;
using TahsilatBSM.WSReport;
#endregion

namespace TahsilatBSM.Controllers
{
    public class DefaultController : Controller
    {
        #region Index Get
        [HttpGet]
        [jCryptionHandler]
        public ActionResult Index(string strMessage = "")
        {
            if (Request.QueryString["getPublicKey"] == null)
            {
                try
                {
                    Session.Clear();
                    Session.RemoveAll();
                    Session.Abandon();

                    SessionIDManager manager = new SessionIDManager();
                    string NewID = manager.CreateSessionID(this.HttpContext.ApplicationInstance.Context);
                    bool redirected = false;
                    bool IsAdded = false;
                    manager.SaveSessionID(this.HttpContext.ApplicationInstance.Context, NewID, out redirected, out IsAdded);

                    if (!string.IsNullOrEmpty(strMessage))
                    {
                        ViewBag.strMessage = Utilities.Msg(Utilities.MsgType.warning, strMessage);
                    }

                    Logs.Log(false, true, "");
                }
                catch (Exception ex)
                {
                    Logs.Exception(ex, "Login Index Get");
                    strMessage = "Bir hata oluştu! Tekrar deneyiniz.";
                }
            }

            return View();
        }
        #endregion

        #region Index Post
        [HttpPost]
        [jCryptionHandler]
        public ActionResult Index(string hiddencontrol, string tckn = "", string bdate = "")
        {
            if (string.IsNullOrEmpty(hiddencontrol))
            {
                return View();
            }

            string strMessage = "";
            try
            {
                if (string.IsNullOrEmpty(tckn) && string.IsNullOrEmpty(bdate))
                {
                    return Index("Tckn veya doğum tarihi giriniz!");
                }

                string strCaptchaValue = Sessions.Captcha(Request.Form["captchaid"]);

                if (strCaptchaValue == Request.Form["captchakod"])
                {
                    ReportWSClient wsReport = new ReportWSClient();

                    GetReportFromURLInput getReportFromURLInput = new GetReportFromURLInput
                    {
                        AuthenticationKey = cAuthenticatedString.Get,
                        ControlType = !string.IsNullOrEmpty(tckn) ? ControlType.TCKN : ControlType.BirthDate,
                        ControlValue = !string.IsNullOrEmpty(tckn) ? tckn : bdate,
                        URL = Request.Url.AbsolutePath.Remove(0, 1)
                    };

                    string xmlI = Utilities.XmlSerialize<GetReportFromURLInput>(getReportFromURLInput);

                    GetReportFromURLResult reportFromURL = wsReport.GetReportFromURL(getReportFromURLInput);

                    string xmlO = Utilities.XmlSerialize<GetReportFromURLResult>(reportFromURL);

                    Logs.WsLog(new Models.tblTahsilatBSMWsLogs
                    {
                        strException = !reportFromURL.ServiceResult.IsOk ? (reportFromURL.ServiceResult.ErrorList.Length > 0 ? reportFromURL.ServiceResult.ErrorList[0] : "IsOk = false") : "",
                        strMethodName = "GetReportFromURL",
                        strPage = "Giriş",
                        strURL = Request.Url.AbsolutePath.Remove(0, 1)
                    }, reportFromURL);

                    if (reportFromURL.ServiceResult.IsOk)
                    {
                        var tahsilatBSMLog = Logs.Log(true, false, cCrypto.EncryptDES(tckn), reportFromURL.ReportCode);

                        Sessions.Log = tahsilatBSMLog;
                        Sessions.ReportFromURL = reportFromURL;
                        Cookies.ReportFromURL = Request.Url.AbsolutePath.Remove(0, 1);
                         
                        if (reportFromURL.ReportCode == "TAH")
                        {
                            return Redirect("/collection/list");
                        }
                        else if (string.IsNullOrEmpty(reportFromURL.ReportCode))
                        {
                            return Redirect("/publish/index");
                        }
                        else
                        {
                            strMessage = "Bilgileriniz hatalı!";
                        }
                    }
                    else
                    {
                        strMessage = "Bilgileriniz hatalı!";
                    }
                }
                else
                {
                    strMessage = "Güvenlik kodunu hatalı girdiniz!";
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "Login Index Post");
                strMessage = "Bir hata oluştu! Tekrar deneyiniz.";
            }

            return Index(strMessage);
        }
        #endregion 
    }
}
