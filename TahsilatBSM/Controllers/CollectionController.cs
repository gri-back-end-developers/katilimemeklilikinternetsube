﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TahsilatBSM.Classes;
using TahsilatBSM.Classes.ClassData;
using TahsilatBSM.Classes.Tools;
using TahsilatBSM.WSReport;

namespace TahsilatBSM.Controllers
{
    public class CollectionController : Controller
    {
        #region List Get
        [UserFilter]
        [HttpGet]
        public ActionResult List()
        {
            if (string.IsNullOrEmpty(Sessions.Log.strReportCode))
            {
                return Redirect("/publish/index");
            }
            else if (Sessions.Log.strReportCode != "TAH")
            {
               Session.Clear();
               Session.RemoveAll();
               Session.Abandon();

               return Redirect("/" + Cookies.ReportFromURL);
            }

            if (Request.QueryString["pdf"] != null)
            {
                try
                {
                    string[] CollectingReferenceNo = Request.QueryString["pdf"].Split(',').Select(s => cCrypto.DecryptDES(s)).ToArray();

                    ReportWSClient wsReport = new ReportWSClient();

                    var collectingReport = wsReport.GetCollectingReport(new GetCollectingReportInput
                    {
                        AuthenticationKey = cAuthenticatedString.Get,
                        CollectingReferenceNo = CollectingReferenceNo,
                        EmailAddress = ""
                    });

                    string strReportContent = collectingReport.ReportContent;
                    collectingReport.ReportContent = string.IsNullOrEmpty(strReportContent) ? "null" : "exist";

                    Logs.WsLog(new Models.tblTahsilatBSMWsLogs
                    {
                        strException = !collectingReport.ServiceResult.IsOk ? (collectingReport.ServiceResult.ErrorList.Length > 0 ? collectingReport.ServiceResult.ErrorList[0] : "IsOk = false") : "",
                        strMethodName = "GetCollectingReport",
                        strPage = "Tahsilat Makbuzu",
                        strURL = "/collection/list"
                    }, collectingReport);

                    string strFileName = "TahsilatBSMu";
                    foreach (var item in CollectingReferenceNo)
                    {
                        strFileName += "-" + item;
                    }

                    Response.Clear();

                    byte[] bytes = Convert.FromBase64String(strReportContent);

                    MemoryStream ms = new MemoryStream(bytes);
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment;filename=" + strFileName + ".pdf");
                    Response.Buffer = true;
                    ms.WriteTo(Response.OutputStream);
                    Response.End();
                }
                catch (Exception ex)
                {
                    Logs.Exception(ex, "collection/list pdf");

                    return Redirect("/collection/list");
                }
            }

            cTahsilatMakbuz tahsilatMakbuz = new cTahsilatMakbuz();

            try
            {
                ReportWSClient wsReport = new ReportWSClient();
                var collectingReportDetail = wsReport.GetCollectingReportDetail(new GetCollectingReportDetailInput
                 {
                     AuthenticationKey = cAuthenticatedString.Get,
                     URL = Sessions.Log.strURL,
                     Year = QueryStrings.year.ToString()
                 });

                Logs.WsLog(new Models.tblTahsilatBSMWsLogs
                {
                    strException = !collectingReportDetail.ServiceResult.IsOk ? (collectingReportDetail.ServiceResult.ErrorList.Length > 0 ? collectingReportDetail.ServiceResult.ErrorList[0] : "IsOk = false") : "",
                    strMethodName = "GetCollectingReportDetail",
                    strPage = "Tahsilat Makbuzu",
                    strURL = "/collection/list"
                }, collectingReportDetail);

                if (collectingReportDetail.ServiceResult.IsOk)
                {
                    if (collectingReportDetail.CollectingDetail != null)
                    {
                        tahsilatMakbuz.Data = new List<cTahsilatMakbuzData>();

                        foreach (var item in collectingReportDetail.CollectingDetail)
                        {
                            tahsilatMakbuz.Data.Add(new cTahsilatMakbuzData
                            {
                                KesintiTutari = Utilities.Price(item.MasrafTutar.HasValue ? item.MasrafTutar.Value : 0),
                                ReferansNo = item.ReferansNo.ToString(),
                                OdemeTarihi = item.TahsilTarih.HasValue ? item.TahsilTarih.Value.ToShortDateString() : "",
                                DtOdemeTarihi = item.TahsilTarih.HasValue ? item.TahsilTarih.Value : new DateTime(),
                                OdemeTutari = Utilities.Price(item.TutarToplam.HasValue ? item.TutarToplam.Value : 0),
                                VadeTarihi = item.Vade.HasValue ? item.Vade.Value.ToShortDateString() : "",
                                VadeTipi = item.TaksitTipAck
                            });
                        }

                        tahsilatMakbuz.Data = tahsilatMakbuz.Data.OrderByDescending(o => o.DtOdemeTarihi).ToList();
                    }

                    if (collectingReportDetail.Years != null)
                    {
                        tahsilatMakbuz.Years = new List<int>(Array.ConvertAll(collectingReportDetail.Years, Convert.ToInt32));
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "collection/list get");
            }

            return View(tahsilatMakbuz);
        }
        #endregion

        #region SendMail
        public string SendMail(string pdf, string mail)
        {
            try
            {
                string[] CollectingReferenceNo = pdf.Split(',').Select(s => cCrypto.DecryptDES(s)).ToArray();

                ReportWSClient wsReport = new ReportWSClient();

                var collectingReport = wsReport.GetCollectingReport(new GetCollectingReportInput
                {
                    AuthenticationKey = cAuthenticatedString.Get,
                    CollectingReferenceNo = CollectingReferenceNo,
                    EmailAddress = mail
                });

                Logs.WsLog(new Models.tblTahsilatBSMWsLogs
                {
                    strException = !collectingReport.ServiceResult.IsOk ? (collectingReport.ServiceResult.ErrorList.Length > 0 ? collectingReport.ServiceResult.ErrorList[0] : "IsOk = false") : "",
                    strMethodName = "GetCollectingReport",
                    strPage = "Tahsilat Makbuzu",
                    strURL = "/collection/list/sendmail"
                }, collectingReport);

                return collectingReport.ServiceResult.IsOk ? "1" : "0";
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "collection/list SendMail");
            }

            return "error";
        }
        #endregion
    }
}
