﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TahsilatBSM.Classes;
using TahsilatBSM.Classes.Tools;
using TahsilatBSM.WSReport;

namespace TahsilatBSM.Controllers
{
    public class PublishController : Controller
    {
        [UserFilter]
        [HttpGet]
        public ActionResult Index()
        {
            if (Sessions.Log.strReportCode == "TAH")
            {
                return Redirect("/collection/list");
            }
            else if (!string.IsNullOrEmpty(Sessions.Log.strReportCode))
            {
                Session.Clear();
                Session.RemoveAll();
                Session.Abandon();

                return Redirect("/" + Cookies.ReportFromURL);
            }

            return View();
        }

        [UserFilter]
        [HttpPost]
        public EmptyResult Download()
        {
            try
            {
                GetReportFromURLResult r = Sessions.ReportFromURL;

                Utilities.FileDownload("Basim", "." + r.ReportExtension, r.ReportContent);
            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "/publish/index");
            }

            return new EmptyResult();
        }
    }
}
