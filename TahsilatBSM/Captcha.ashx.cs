﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.SessionState;
using TahsilatBSM.Classes;
using TahsilatBSM.Classes.Tools;
using TahsilatBSM.Classes.Tools.Captcha;

namespace TahsilatBSM
{ 
    public class Captcha : IHttpHandler, IRequiresSessionState
    {
        private HttpContext _context;
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                _context = context;
                _context.Session["UserIP"] = Utilities.ClientIP;
                GetRequestParams();

                if (!string.IsNullOrEmpty(_context.Request.QueryString["op"]))
                {
                    Do();
                }

            }
            catch (Exception ex)
            {
                Logs.Exception(ex, "TahsilatBSM captcha"); 
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #region SetRequestParams
        string opStr;
        string code; 
        string requestID;
        string clientIP; 
        #endregion

        #region GetRequestParams
        private void GetRequestParams()
        {
            opStr = _context.Request.QueryString["op"];
            code = _context.Request.QueryString["code"];
            requestID = Utilities.NullFixInt(_context.Request.QueryString["requestID"]).ToString();
            clientIP = Utilities.ClientIP; 
        }
        #endregion

        #region DefineOps
        private void Do()
        {
            string r = "";
            switch (opStr)
            {
                case "getCaptchaResult": r = getCaptchaResult(requestID, clientIP, code);
                    break; 
                case "getCaptchaUrl": r = getCaptchaUrl(requestID, clientIP);
                    break; 
                case "getCaptchaImg": getCaptchaImage();
                    break; 
                default:
                    r = "[{\"message\":\"error\"}]";
                    break;
            }

            if (opStr != "getCaptchaImg") //return stream instead string
                _context.Response.Write(r);
        }
        #endregion 

        #region CAPTCHA
        /// <summary>
        /// CAPTCHA RESULT
        /// TRUE
        /// FALSE
        /// NEW CAPTCHAURL
        /// </summary>
        /// <param name="requestID"></param>
        /// <param name="ip"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private string getCaptchaResult(string requestID, string ip, string code)
        {
            cFormResult f = new cFormResult();
            f.strCaptchaID = requestID;
            f.strGuvenlikKodu = code;
            f.status = false;
            f.refresh = true;
            f.strCaptchaUrl = "";// getCaptchaUrl(requestID, ip);//new img url

            if (_context.Session["captcha_" + requestID + "_" + ip] != null && f.strGuvenlikKodu.ToLower() == Sessions.Captcha(requestID))
            {
                f.strCaptchaUrl = getCaptchaUrl(requestID, ip);
                if (f.strCaptchaUrl.Contains("Incorrect") && !f.strCaptchaUrl.Contains("ashx"))
                {
                    f.status = false;
                    f.result = Utilities.ToJson(f);
                    f.strCaptchaUrl = f.strCaptchaUrl;
                    return Utilities.ToJson(f);
                }
                f.status = true;
                return Utilities.ToJson(f);
            }

            f.status = false;
            f.result = "Incorrect parameters";
            f.strCaptchaUrl = "";

            return Utilities.ToJson(f);
        }

        /// <summary>
        /// CAPTCHAURL STRING
        /// </summary>
        /// <param name="requestID"></param>
        /// <param name="ip"></param>
        /// <returns></returns>
        private string getCaptchaUrl(string requestID, string ip)
        {
            int testint = 0;

            cFormResult f = new cFormResult();
            f.strCaptchaID = requestID;
            f.strGuvenlikKodu = code;
            f.status = false;
            f.refresh = true;
            f.strCaptchaUrl = "";// getCaptchaUrl(requestID, ip);//new img url

            if (Int32.TryParse(requestID.ToString(), out testint)) // && !string.IsNullOrEmpty(ip)
            {
                if (testint < 1 || testint > 9999)
                {
                    //f.result = "Incorrect Request";
                    //return Util.asJson(f);
                    return "Incorrect Request";
                }

                //if (!IPAddress.TryParse(ip, out testip))
                //{
                //    //f.result = "Incorrect IP";
                //    //return Util.asJson(f);
                //    return "Incorrect IP";
                //}

                cCaptchaImg ci = new cCaptchaImg();
                //f.strCaptchaUrl = ci.createCaptchaUrlStr(requestID, ip, _context);
                //f.status = true;
                //f.refresh = true;
                //return Util.asJson(f);
                return ci.createCaptchaUrlStr(requestID, ip, _context);
            }
            else
            {
                //f.result = "Incorrect IP";
                //return Util.asJson(f);
                return "Incorrect Request";
            }
        }

        /// <summary>
        /// CAPTCHAIMG STREAM IMAGE
        /// </summary>
        private void getCaptchaImage()
        {
            cCaptchaImg ci = new cCaptchaImg();
            ci.getCaptchaImg(_context);
        }
        #endregion
    }

    public class cFormResult
    {
        public string strCaptchaID { get; set; }
        public string strCaptchaUrl { get; set; } 
        public string strGuvenlikKodu { get; set; } 
        public string result { get; set; } 
        public bool status { get; set; }
        public bool refresh { get; set; }
    }
}