﻿using TahsilatBSM.Models;
using System;
using TahsilatBSM.Classes.Tools;
using System.Web;
using System.Web.Script.Serialization; 

namespace TahsilatBSM.Classes
{
    public class Logs
    {
        public static void Exception(Exception ex, string strPage)
        {  
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    db.tblTahsilatBSMExceptions.Add(new tblTahsilatBSMExceptions()
                    {
                        dtRegisterDate = DateTime.Now,
                        strException = ex.ToString(),
                        strPage = strPage,
                        intLogID = HttpContext.Current.Session["CurrentUser"] != null ? Sessions.Log.intLogID : 0
                    });
                    db.SaveChanges();
                }
            }
            catch { }
        }

        public static void WsLog(tblTahsilatBSMWsLogs wsLog, object json = null)
        {
            try
            {
                using (DBEntities db = new DBEntities())
                {
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    if (json != null)
                    {
                        wsLog.strJson = jss.Serialize(json);
                    }

                    wsLog.dtRegisterDate = DateTime.Now;
                    wsLog.intLogID = HttpContext.Current.Session["CurrentUser"] != null ? Sessions.Log.intLogID : 0;

                    db.tblTahsilatBSMWsLogs.Add(wsLog);
                    db.SaveChanges();
                }
            }
            catch { }
        }
         
        #region Log
        public static tblTahsilatBSMLogs Log(bool bolLogin, bool bolOpen, string strTCKN, string strReportCode = "")
        {
            using (DBEntities db = new DBEntities())
            {
                tblTahsilatBSMLogs log = new tblTahsilatBSMLogs
                {
                    dtRegisterDate = DateTime.Now,
                    strTCKN = strTCKN,
                    strURL = HttpContext.Current.Request.Url.AbsolutePath.Remove(0, 1),
                    strIp = Utilities.ClientIP,
                    bolLogin = true,
                    bolOpen = false,
                    strReportCode = strReportCode
                };

                db.tblTahsilatBSMLogs.Add(log);
                db.SaveChanges();

                return log;
            }
        }
        #endregion
    }
}