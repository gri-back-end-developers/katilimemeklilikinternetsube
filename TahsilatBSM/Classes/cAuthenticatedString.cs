﻿using System.Configuration;
using TahsilatBSM.Classes.Tools;
using TahsilatBSM.WsSecurity;

namespace TahsilatBSM.Classes
{
    public class cAuthenticatedString
    {
        public static string Get
        {
            get
            {
                string strAuthenticatedString = Caches.AuthenticatedString;
                if (string.IsNullOrEmpty(strAuthenticatedString))
                {
                    using (SecurityServiceClient wsSecurity = new SecurityServiceClient())
                    {
                        var authenticationKey = wsSecurity.GetAuthenticationKey(ConfigurationManager.AppSettings["appSecurityKey"], ConfigurationManager.AppSettings["authUserName"], ConfigurationManager.AppSettings["authPassword"], ref strAuthenticatedString);

                        if (authenticationKey.IsOk && !string.IsNullOrEmpty(strAuthenticatedString))
                        {
                            Caches.AuthenticatedString = strAuthenticatedString;
                        }
                        else
                        {
                            Logs.WsLog(new Models.tblTahsilatBSMWsLogs
                            {
                                strException = authenticationKey.ErrorList.Length > 0 ? authenticationKey.ErrorList[0] : "IsOk = false",
                                strMethodName = "GetAuthenticationKey"
                            }, authenticationKey);
                        }
                    }
                }

                return strAuthenticatedString;
            }
        }
    }
}