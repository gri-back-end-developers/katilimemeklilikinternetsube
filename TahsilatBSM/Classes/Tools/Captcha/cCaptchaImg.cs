﻿using System;
using System.Web;
using System.Drawing.Imaging;
using System.Drawing;

namespace TahsilatBSM.Classes.Tools.Captcha
{
    public class cCaptchaImg
    {
        public const string virtualDirStr = "";
        private const string strCaptchaBaseUrl = "/captcha.ashx?op=getCaptchaImg&w=305&h=92&c={0}&bc=#ffffff";
        public string createCaptchaUrlStr(string requestID, string ip, HttpContext _context)
        {
            // Set image
            string s = Utilities.RandomString(6).ToLower();

            // Encrypt
            string ens = cCrypto.EncryptDES(s);

            // Save to session
            _context.Session["captcha_" + requestID + "_" + ip] = s.ToLower();

            // Set url
            return virtualDirStr + String.Format(strCaptchaBaseUrl, ens);
        }

        public void getCaptchaImg(HttpContext _context)
        {
            // Get text
            string s = "No Text";
            if (_context.Request.QueryString["c"] != null &&
                _context.Request.QueryString["c"] != "")
            {
                string enc = _context.Request.QueryString["c"].ToString();

                // space was replaced with + to prevent error
                enc = enc.Replace(" ", "+");
                try
                {
                    s = cCrypto.DecryptDES(enc);
                }
                catch { }
            }
            // Get dimensions
            int w = 120;
            int h = 50;
            // Width
            if (_context.Request.QueryString["w"] != null &&
                _context.Request.QueryString["w"] != "")
            {
                try
                {
                    w = Utilities.NullFixInt(_context.Request.QueryString["w"]);
                }
                catch { }
            }
            // Height
            if (_context.Request.QueryString["h"] != null &&
                _context.Request.QueryString["h"] != "")
            {
                try
                {
                    h = Utilities.NullFixInt(_context.Request.QueryString["h"]);
                }
                catch { }
            }
            // Color
            Color Bc = Color.White;
            if (_context.Request.QueryString["bc"] != null &&
                _context.Request.QueryString["bc"] != "")
            {
                try
                {
                    string bc = _context.Request.QueryString["bc"].ToString().Insert(0, "#");
                    Bc = ColorTranslator.FromHtml(bc);
                }
                catch { }
            }
            // Generate image
            CaptchaImage ci = new CaptchaImage(s, Bc, w, h);

            // Return
            ci.Image.Save(_context.Response.OutputStream, ImageFormat.Jpeg);
            // Dispose
            ci.Dispose();
        }

    }
}