#region Directives 
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Script.Serialization;
#endregion

namespace TahsilatBSM.Classes.Tools
{
    public class Cookies
    {
        #region ReportFromURL
        public static string ReportFromURL
        {
            get
            {
                try
                {
                    object objValue = HttpContext.Current.Request.Cookies["RFURL"].Value;
                    if (objValue == null)
                    {
                        return "";
                    }
                    return objValue.ToString();
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("RFURL");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch { }
            }
        }
        #endregion
    }
}