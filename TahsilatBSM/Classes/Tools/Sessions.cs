﻿using TahsilatBSM.Models;
using System.Web;
using TahsilatBSM.WSReport;

namespace TahsilatBSM.Classes.Tools
{
    public class Sessions
    {
        #region Log
        public static tblTahsilatBSMLogs Log
        {
            get
            {
                try
                {
                    object val = HttpContext.Current.Session["TLog"];

                    return (tblTahsilatBSMLogs)val;
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["TLog"] = value;
            }
        }
        #endregion
         
        #region Captcha
        public static string Captcha(string captchaid)
        {
            try
            {
                object val = HttpContext.Current.Session["captcha_" + captchaid + "_" + Utilities.ClientIP];

                return val == null ? "" : val.ToString();
            }
            catch
            {
                return "";
            }
        }
        #endregion

        #region ReportFromURL
        public static GetReportFromURLResult ReportFromURL
        {
            get
            {
                try
                {
                    return (GetReportFromURLResult)HttpContext.Current.Session["ReportFromURL"]; 
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["ReportFromURL"] = value;
            }
        }
        #endregion
    }
}