﻿using TahsilatBSM.Classes.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TahsilatBSM.Classes
{
    public class UserFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                bool bolResponse = false;

                if (Sessions.Log != null)
                {
                    if (Sessions.Log.strIp != Utilities.ClientIP)
                    {
                        bolResponse = true;
                    }
                }
                else
                {
                    bolResponse = true;
                }

                if (bolResponse)
                {
                    Sessions.Log = null;
                    HttpContext.Current.Session.Clear();
                    HttpContext.Current.Session.RemoveAll();
                    HttpContext.Current.Session.Abandon();

                    HttpContext.Current.Response.Redirect("/" + Cookies.ReportFromURL);
                }
            }
            catch { }
        }
    }
}