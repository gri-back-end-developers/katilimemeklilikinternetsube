﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TahsilatBSM.Classes.ClassData
{
    public class CaptchaResponse
    {
        public bool Status { get; set; }
        public long cid { get; set; }
        public string b64img { get; set; }
    }
}