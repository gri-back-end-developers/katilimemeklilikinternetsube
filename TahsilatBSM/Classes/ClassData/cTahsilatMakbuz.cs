﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TahsilatBSM.Classes.ClassData
{
    public class cTahsilatMakbuz
    {
        public List<cTahsilatMakbuzData> Data { get; set; }
        public List<int> Years { get; set; } 
    }

    public class cTahsilatMakbuzData
    {
        public string ReferansNo { get; set; }
        public DateTime DtOdemeTarihi { get; set; }
        public string OdemeTarihi { get; set; }
        public string VadeTarihi { get; set; }
        public string VadeTipi { get; set; }
        public string OdemeTutari { get; set; }
        public string KesintiTutari { get; set; }
    }
}