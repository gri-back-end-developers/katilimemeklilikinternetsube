﻿using System.Collections.Generic;

namespace WebApi.Classes
{
    public class BalanceQuery
    {
        public BalanceQueryStatus status { get; set; }
        public List<BalanceQueryData> data { get; set; }

        public BalanceQuery()
        {
            data = new List<BalanceQueryData>();
            status = new BalanceQueryStatus();
        }
    }

    public class BalanceQueryData
    {
        public string policy { get; set; }
        public string price { get; set; }
    }

    public class BalanceQueryStatus
    {
        public int code { get; set; }
        public string message { get; set; }
    }
}