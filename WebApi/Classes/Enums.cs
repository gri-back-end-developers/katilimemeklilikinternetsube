﻿namespace WebApi.Classes
{
    public class Enums
    {
        public enum Types
        {
            GET,
            POST,
            INPUT,
            OUTPUT
        }

        public enum MediaType
        {
            text,
            json
        }

        public static string MediaTypeText(MediaType media)
        {
            switch (media)
            {
                case Enums.MediaType.text:
                    return "text/plain";
                case Enums.MediaType.json:
                    return "application/json";
            }

            return "";
        }
    }
}