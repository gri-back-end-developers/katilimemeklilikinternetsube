﻿namespace WebApi.Classes
{
    public class BalanceSettings
    {
        public string year { get; set; }
        public string price1 { get; set; }
        public string price2 { get; set; }
    }
}