﻿using System.Net;
using System.Net.Http;

namespace WebApi.Classes
{
    public class Messages
    {
        public static HttpResponseMessage ResponseMsg(HttpRequestMessage request, HttpStatusCode statusCode, string strContent, Enums.MediaType media)
        {
            HttpResponseMessage response = request.CreateResponse(statusCode);
            response.Content = new StringContent(strContent, System.Text.Encoding.UTF8, Enums.MediaTypeText(media));

            return response;
        }
    }
}