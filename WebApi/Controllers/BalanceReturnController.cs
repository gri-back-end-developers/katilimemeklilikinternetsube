﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.Classes;
using WebApi.Models;

namespace WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BalanceReturnController : ApiController
    {
        #region Settings
        [HttpGet]
        [Route("balancereturn/settings")]
        public HttpResponseMessage Settings()
        {
            BalanceSettings balanceSettings = new BalanceSettings();

            try
            {
                using (DbEntities db = new DbEntities())
                {
                    tblBalanceReturnSettings balanceReturnSettings = db.tblBalanceReturnSettings.FirstOrDefault();
                    if (balanceReturnSettings != null)
                    {
                        balanceSettings.year = balanceReturnSettings.intYear.ToString();
                        balanceSettings.price1 = balanceReturnSettings.strReturnPrice1;
                        balanceSettings.price2 = balanceReturnSettings.strReturnPrice2;
                    }
                }
            }
            catch { }

            string strData = JsonConvert.SerializeObject(balanceSettings);

            return Messages.ResponseMsg(this.Request, HttpStatusCode.OK, strData, Enums.MediaType.json);
        }
        #endregion

        #region Query
        [HttpGet]
        [Route("balancereturn/query")]
        public BalanceQuery Query([FromUri]string tckn, [FromUri]string phone, [FromUri]string type)
        {
            BalanceQuery balanceQuery = new BalanceQuery();

            try
            {
                using (DbEntities db = new DbEntities())
                {
                    type = type.ToUpper();
                    tckn = cCrypto.EncryptDES(tckn);

                    var balanceReturns = db.tblBalanceReturns.Where(w => !w.bolIsDelete && w.strType == type).ToList();

                    if (balanceReturns.Count > 0)
                    {
                        var balanceReturnFilter = balanceReturns.Where(w => w.strPhone == phone && w.strTCKN == tckn).Select(s => new
                        {
                            policy = s.strPolicyNo,
                            price = s.flPrice,
                            strStatusCode = s.strStatusCode
                        }).ToList();

                        if (balanceReturnFilter.Count > 0)
                        {
                            if (balanceReturnFilter.Count(c => c.strStatusCode == "GON") > 0)
                            {
                                balanceQuery.data = new List<BalanceQueryData>();

                                foreach (var item in balanceReturnFilter)
                                {
                                    balanceQuery.data.Add(new BalanceQueryData
                                    {
                                        policy = item.policy,
                                        price = Utilities.Price(item.price)
                                    });
                                }

                                balanceQuery.status.code = (int)HttpStatusCode.OK;
                                balanceQuery.status.message = "Başarılı.";
                            }
                            else
                            {
                                balanceQuery.status.code = (int)HttpStatusCode.NotFound;
                                balanceQuery.status.message = "Bakiye iadeniz yapılmış olup lütfen hesap bilgilerinizi kontrol ediniz.";
                            }
                        }
                        else
                        {
                            if (balanceReturns.Count(w => w.strTCKN == tckn) > 0)
                            {
                                balanceQuery.status.code = (int)HttpStatusCode.NotFound;
                                balanceQuery.status.message = "Lütfen telefon numaranızı kontrol ediniz.";
                            }
                            else if (balanceReturns.Count(w => w.strPhone == phone) > 0)
                            {
                                balanceQuery.status.code = (int)HttpStatusCode.NotFound;
                                balanceQuery.status.message = "Lütfen TCKN bilginizi kontrol ediniz.";
                            }
                            else
                            {
                                balanceQuery.status.code = (int)HttpStatusCode.NotFound;
                                balanceQuery.status.message = "Lütfen bilgilerinizi kontrol ediniz.";
                            }
                        }
                    }
                    else
                    {
                        balanceQuery.status.code = (int)HttpStatusCode.NotFound;
                        balanceQuery.status.message = "Lütfen bilgilerinizi kontrol ediniz.";
                    }
                }
            }
            catch
            {
                balanceQuery.status.code = (int)HttpStatusCode.BadRequest;
                balanceQuery.status.message = "Bir hata oluştu! Tekrar deneyiniz.";
            }

            return balanceQuery;
        }
        #endregion

        #region Forms
        [HttpGet]
        [Route("balancereturn/forms")]
        public HttpResponseMessage Forms([FromUri]string type, [FromUri]string name, [FromUri]string surname, [FromUri]string tckn, [FromUri]string iban)
        {
            BalanceForms balanceForms = new BalanceForms();

            string strMessage = "";
            string strStatus = "";

            try
            {
                using (DbEntities db = new DbEntities())
                {
                    if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(name) || string.IsNullOrEmpty(surname) || string.IsNullOrEmpty(tckn) || string.IsNullOrEmpty(iban))
                    {
                        strMessage = "Eksik veya hatalı giriş yaptınız!";
                        strStatus = "0";
                    }
                    else
                    {
                        string strTckn = cCrypto.EncryptDES(tckn.Trim());
                        string strIban = cCrypto.EncryptDES(iban.Trim());
                        name = name.Trim().ToLower();
                        surname = surname.Trim().ToLower();

                        bool bolNext = true;

                        var balanceReturn = db.tblBalanceReturns.Where(w => !w.bolIsDelete && w.strStatusCode == "GON" && w.strType == type).ToList();
                        if (balanceReturn != null)
                        {
                            if (balanceReturn.Count(c => c.strTCKN == strTckn) == 0)
                            {
                                if (balanceReturn.Count(c => c.strName.Trim().ToLower() == name && c.strSurname.Trim().ToLower() == surname) == 0)
                                {
                                    strMessage = "Lütfen bilgilerinizi kontrol ediniz!";
                                    strStatus = "0";
                                    bolNext = false;
                                }
                                else
                                {
                                    strMessage = "Girilen Tckn bilgisi poliçe üzerinde belirtilen sigortalı ad-soyad bilgisi ile uyuşmamaktadır. Lütfen kontrol ederek tekrar deneyiniz.";
                                    strStatus = "0";
                                    bolNext = false;
                                }
                            }
                            else if (balanceReturn.Count(c => c.strName.Trim().ToLower() == name && c.strSurname.Trim().ToLower() == surname) == 0)
                            {
                                strMessage = "Belirtilen ad-soyad bilgisi poliçe üzerinde belirtilen ödeyen ad-soyad bilgisi ile uyuşmamaktadır. Lütfen kontrol ederek tekrar deneyiniz.";
                                strStatus = "0";
                                bolNext = false;
                            }
                        }
                        else
                        {
                            strMessage = "Lütfen bilgilerinizi kontrol ediniz!";
                            strStatus = "0";
                            bolNext = false;
                        }

                        if (bolNext)
                        {
                            tblBalanceReturnForms balanceReturnForms = new tblBalanceReturnForms
                            {
                                dtRegisterDate = DateTime.Now,
                                bolMailSend = false,
                                strIBAN = strIban,
                                strName = name,
                                strSurname = surname,
                                strTCKN = tckn
                            };

                            db.tblBalanceReturnForms.Add(balanceReturnForms);
                            db.SaveChanges();

                            bool bolMailSend = Email.SendEmail("TCKN:" + tckn + "/AdSoyad:" + name + " " + surname + "/Iban:" + iban + "", tckn + " TCKN No’lu " + name + " " + surname + " isimli müşterinin ödeme bilgisinin " + iban + " no’lu İBAN’a ödenmesi için gerekli işlemin yapılması beklenmektedir.", ConfigurationManager.AppSettings["FormMail"].ToString());

                            balanceReturnForms.bolMailSend = bolMailSend;
                            db.SaveChanges();

                            if (bolMailSend)
                            {
                                strMessage = "Talebiniz alınmış olup iade ödemeniz ile ilgili bilgilendirme yapılacaktır.";
                                strStatus = "1";
                            }
                            else
                            {
                                strMessage = "E-posta gönderilemedi!";
                                strStatus = "0";
                            }
                        }
                    }
                }
            }
            catch
            {
                strMessage = "Bir hata oluştu! Tekrar deneyiniz.";
                strStatus = "0";
            }

            balanceForms.message = strMessage;
            balanceForms.status = strStatus;

            string strData = JsonConvert.SerializeObject(balanceForms);

            return Messages.ResponseMsg(this.Request, HttpStatusCode.OK, strData, Enums.MediaType.json);
        }
        #endregion
    }
}
