﻿
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApi.Providers
{
    public class OAuthProvider : OAuthAuthorizationServerProvider
    {
        public override async System.Threading.Tasks.Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var requestForm = await context.Request.ReadFormAsync();
            string strSecurityKey = requestForm["securitykey"]; 

            if (string.IsNullOrEmpty(strSecurityKey))
            {
                context.SetError("invalid_grant", "Güvenlik anahtarını giriniz!");
            }
            else if (string.IsNullOrEmpty(context.UserName) || string.IsNullOrEmpty(context.Password))
            {
                context.SetError("invalid_grant", "Eksik veya hatalı giriş!");
            } 
            else
            {
               // 
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                if (!property.Key.StartsWith("."))
                {
                    context.AdditionalResponseParameters.Add(property.Key, property.Value);
                }
            }

            return Task.FromResult<object>(null);
        }
    }
}